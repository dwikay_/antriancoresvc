application {
  config {
    databaseType sql
    devDatabaseType h2Disk
    enableHibernateCache true
    enableSwaggerCodegen false
    enableTranslation false
    jhiPrefix jhi
    languages [en]
    messageBroker false
    nativeLanguage en
    packageName com.antrian.inti.core
    packageFolder com/antrian/inti/core
    prodDatabaseType postgresql
    searchEngine false
    serviceDiscoveryType eureka
    skipClient true
    testFrameworks []
    websocket false
    applicationType microservice
    gitCompany 
    baseName antriancoresvc
    serverPort 6011
    authenticationType jwt
    uaaBaseName ../uaa
    cacheProvider hazelcast
    buildTool gradle
    clientPackageManager npm
    jhipsterVersion 6.8.0
    jwtSecretKey ZmVjMmVhYWVjMDhmNjUxNmQxNWUyZDk4MjEwYjIwN2ZmZmVkMjQ3YzkxMjJhYzFjYjhlMGY5NWZlZGEwYWI1Y2QzNTI4NTIwOTE1YjIzMTVmOWI2ODZkMTYxYTZhYTVhYTc4ZjQwZmQxZGJiNDVmNTc3N2FlZWY5MDQ5NDRhOGU=
    embeddableLaunchScript false
    creationTimestamp 1585137541930
    entitySuffix 
    dtoSuffix DTO
    otherModules []
    blueprints 
    skipUserManagement true
  }

  entities Banned, DataAntrian, HariLibur, Kiosk, Kloter, Kuota, Lantai, Layanan, Loket, PelayananLoket, Registrasi, RunningText, SubLayanan, TempPanggilan
}

entity Banned {
  nik String,
  email String,
  tanggal_banned LocalDate,
  finish_banned LocalDate,
  status_banned String maxlength(3),
  createSystemDate LocalDate,
  modificationSystemDate LocalDate,
  jumlah Integer
}
entity DataAntrian {
  tanggal LocalDate,
  noUrut Long,
  statusPanggilan Boolean,
  idSubLayanan Long,
  layanan String,
  loket Long,
  prefixSuara String,
  lantai Long,
  statusCurrent Boolean,
  kodeKonfirmasi String,
  imageWebcam ImageBlob,
  layananOnline Boolean,
  durasi String,
  sisaDurasi String,
  statusKonseling Boolean
}
entity HariLibur {
  tanggal LocalDate,
  keterangan String
}
entity Kiosk {
  lokasi String,
  username String,
  passwordKiosk String,
  deskripsi String,
  status String maxlength(3),
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationUserId Long,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime
}
entity Kloter {
  jamMulai String,
  jamAkhir String,
  quota String,
  namaKloter String,
  idSubLayanan Long,
  createdDate LocalDate,
  createDateTime ZonedDateTime,
  createUserId Long,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime,
  lastModificationUserId Long
}
entity Kuota {
  idSubLayanan Long,
  quota String,
  jenisLayanan String
}
entity Lantai {
  lantai Long,
  deskripsi String,
  status String maxlength(3),
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationUserId Long,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime
}
entity Layanan {
  kodeLayanan String,
  namaLayanan String,
  gambar ImageBlob,
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime,
  lastModificationUserId Long
}
entity Loket {
  username String,
  password String,
  status String maxlength(3),
  deskripsi String,
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationUserId Long,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime,
  idSubLayanan Long
}
entity PelayananLoket {
  idLayanan Long,
  idSubLayanan Long,
  idLoket Long,
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationUserId Long,
  lastModificationDateTime ZonedDateTime
}
entity Registrasi {
  idDaftar String,
  email String,
  hubunganPerusahaan String,
  namaPerusahaan String,
  tanggalPengambilan LocalDate,
  tanggalWaktuPengambilan String,
  pertanyaan TextBlob,
  telepon String,
  kodeVerifikasi TextBlob,
  namaLengkap String,
  verifikasiEmail Boolean,
  nik String maxlength(16),
  namaPerusahaanPendaftar String,
  kodeKonfirmasi String,
  idKloter Long,
  pengambilanStruk Boolean,
  jamKloter String,
  waktuJamKloter String,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  modificationDate LocalDate,
  idSubLayanan Long,
  ktp ImageBlob
}
entity RunningText {
  text TextBlob,
  status String maxlength(3),
  monitor String,
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationUserId Long,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime
}
entity SubLayanan {
  idLayanan Long,
  kodeLayanan String,
  namaLayanan String,
  idLantai Long,
  kuota Long,
  jamAwal String,
  jamAkhir String,
  durasi Long,
  status String maxlength(3),
  prefixSuara String maxlength(5),
  layananOnline Boolean,
  idKiosk Long,
  createUserId Long,
  createDate LocalDate,
  createDateTime ZonedDateTime,
  lastModificationUserId Long,
  lastModificationDate LocalDate,
  lastModificationDateTime ZonedDateTime
}
entity TempPanggilan {
  idTempPanggilan UUID,
  panggilan String,
  status Boolean,
  noUrut String,
  idSubLayanan Long
}
dto Banned, DataAntrian, HariLibur, Kiosk, Kloter, Kuota, Lantai, Layanan, Loket, PelayananLoket, Registrasi, RunningText, SubLayanan, TempPanggilan with mapstruct
paginate Banned, DataAntrian, HariLibur, Kiosk, Kloter, Kuota, Lantai, Layanan, Loket, PelayananLoket, Registrasi, RunningText, SubLayanan, TempPanggilan with pagination
service Banned, DataAntrian, HariLibur, Kiosk, Kloter, Kuota, Lantai, Layanan, Loket, PelayananLoket, Registrasi, RunningText, SubLayanan, TempPanggilan with serviceImpl
microservice Banned, DataAntrian, HariLibur, Kiosk, Kloter, Kuota, Lantai, Layanan, Loket, PelayananLoket, Registrasi, RunningText, SubLayanan, TempPanggilan with antriancoresvc
clientRootFolder Banned, DataAntrian, HariLibur, Kiosk, Kloter, Kuota, Lantai, Layanan, Loket, PelayananLoket, Registrasi, RunningText, SubLayanan, TempPanggilan with antriancoresvc
