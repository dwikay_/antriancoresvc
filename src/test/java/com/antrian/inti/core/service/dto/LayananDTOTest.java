package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class LayananDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LayananDTO.class);
        LayananDTO layananDTO1 = new LayananDTO();
        layananDTO1.setId(1L);
        LayananDTO layananDTO2 = new LayananDTO();
        assertThat(layananDTO1).isNotEqualTo(layananDTO2);
        layananDTO2.setId(layananDTO1.getId());
        assertThat(layananDTO1).isEqualTo(layananDTO2);
        layananDTO2.setId(2L);
        assertThat(layananDTO1).isNotEqualTo(layananDTO2);
        layananDTO1.setId(null);
        assertThat(layananDTO1).isNotEqualTo(layananDTO2);
    }
}
