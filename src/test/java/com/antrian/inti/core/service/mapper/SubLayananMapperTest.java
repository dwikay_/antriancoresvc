package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class SubLayananMapperTest {

    private SubLayananMapper subLayananMapper;

    @BeforeEach
    public void setUp() {
        subLayananMapper = new SubLayananMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(subLayananMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(subLayananMapper.fromId(null)).isNull();
    }
}
