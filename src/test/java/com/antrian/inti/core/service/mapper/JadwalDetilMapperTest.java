package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class JadwalDetilMapperTest {

    private JadwalDetilMapper jadwalDetilMapper;

    @BeforeEach
    public void setUp() {
        jadwalDetilMapper = new JadwalDetilMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(jadwalDetilMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(jadwalDetilMapper.fromId(null)).isNull();
    }
}
