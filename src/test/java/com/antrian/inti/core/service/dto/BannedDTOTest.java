package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class BannedDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BannedDTO.class);
        BannedDTO bannedDTO1 = new BannedDTO();
        bannedDTO1.setId(1L);
        BannedDTO bannedDTO2 = new BannedDTO();
        assertThat(bannedDTO1).isNotEqualTo(bannedDTO2);
        bannedDTO2.setId(bannedDTO1.getId());
        assertThat(bannedDTO1).isEqualTo(bannedDTO2);
        bannedDTO2.setId(2L);
        assertThat(bannedDTO1).isNotEqualTo(bannedDTO2);
        bannedDTO1.setId(null);
        assertThat(bannedDTO1).isNotEqualTo(bannedDTO2);
    }
}
