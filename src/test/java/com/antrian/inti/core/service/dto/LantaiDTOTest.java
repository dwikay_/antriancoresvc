package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class LantaiDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LantaiDTO.class);
        LantaiDTO lantaiDTO1 = new LantaiDTO();
        lantaiDTO1.setId(1L);
        LantaiDTO lantaiDTO2 = new LantaiDTO();
        assertThat(lantaiDTO1).isNotEqualTo(lantaiDTO2);
        lantaiDTO2.setId(lantaiDTO1.getId());
        assertThat(lantaiDTO1).isEqualTo(lantaiDTO2);
        lantaiDTO2.setId(2L);
        assertThat(lantaiDTO1).isNotEqualTo(lantaiDTO2);
        lantaiDTO1.setId(null);
        assertThat(lantaiDTO1).isNotEqualTo(lantaiDTO2);
    }
}
