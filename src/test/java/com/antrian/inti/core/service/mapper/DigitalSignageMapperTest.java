package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class DigitalSignageMapperTest {

    private DigitalSignageMapper digitalSignageMapper;

    @BeforeEach
    public void setUp() {
        digitalSignageMapper = new DigitalSignageMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(digitalSignageMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(digitalSignageMapper.fromId(null)).isNull();
    }
}
