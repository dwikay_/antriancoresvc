package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class AkumulasiKorespondenMapperTest {

    private AkumulasiKorespondenMapper akumulasiKorespondenMapper;

    @BeforeEach
    public void setUp() {
        akumulasiKorespondenMapper = new AkumulasiKorespondenMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(akumulasiKorespondenMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(akumulasiKorespondenMapper.fromId(null)).isNull();
    }
}
