package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class JadwalDetilDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JadwalDetilDTO.class);
        JadwalDetilDTO jadwalDetilDTO1 = new JadwalDetilDTO();
        jadwalDetilDTO1.setId(1L);
        JadwalDetilDTO jadwalDetilDTO2 = new JadwalDetilDTO();
        assertThat(jadwalDetilDTO1).isNotEqualTo(jadwalDetilDTO2);
        jadwalDetilDTO2.setId(jadwalDetilDTO1.getId());
        assertThat(jadwalDetilDTO1).isEqualTo(jadwalDetilDTO2);
        jadwalDetilDTO2.setId(2L);
        assertThat(jadwalDetilDTO1).isNotEqualTo(jadwalDetilDTO2);
        jadwalDetilDTO1.setId(null);
        assertThat(jadwalDetilDTO1).isNotEqualTo(jadwalDetilDTO2);
    }
}
