package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class PendaftaranMapperTest {

    private PendaftaranMapper pendaftaranMapper;

    @BeforeEach
    public void setUp() {
        pendaftaranMapper = new PendaftaranMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(pendaftaranMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(pendaftaranMapper.fromId(null)).isNull();
    }
}
