package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class KorespondenMapperTest {

    private KorespondenMapper korespondenMapper;

    @BeforeEach
    public void setUp() {
        korespondenMapper = new KorespondenMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(korespondenMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(korespondenMapper.fromId(null)).isNull();
    }
}
