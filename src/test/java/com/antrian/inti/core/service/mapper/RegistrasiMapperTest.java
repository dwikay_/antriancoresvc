package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class RegistrasiMapperTest {

    private RegistrasiMapper registrasiMapper;

    @BeforeEach
    public void setUp() {
        registrasiMapper = new RegistrasiMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(registrasiMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(registrasiMapper.fromId(null)).isNull();
    }
}
