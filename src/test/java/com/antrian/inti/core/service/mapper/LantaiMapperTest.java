package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class LantaiMapperTest {

    private LantaiMapper lantaiMapper;

    @BeforeEach
    public void setUp() {
        lantaiMapper = new LantaiMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(lantaiMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(lantaiMapper.fromId(null)).isNull();
    }
}
