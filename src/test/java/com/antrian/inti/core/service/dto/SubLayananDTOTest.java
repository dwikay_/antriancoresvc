package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class SubLayananDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubLayananDTO.class);
        SubLayananDTO subLayananDTO1 = new SubLayananDTO();
        subLayananDTO1.setId(1L);
        SubLayananDTO subLayananDTO2 = new SubLayananDTO();
        assertThat(subLayananDTO1).isNotEqualTo(subLayananDTO2);
        subLayananDTO2.setId(subLayananDTO1.getId());
        assertThat(subLayananDTO1).isEqualTo(subLayananDTO2);
        subLayananDTO2.setId(2L);
        assertThat(subLayananDTO1).isNotEqualTo(subLayananDTO2);
        subLayananDTO1.setId(null);
        assertThat(subLayananDTO1).isNotEqualTo(subLayananDTO2);
    }
}
