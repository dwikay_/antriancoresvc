package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class LoketMapperTest {

    private LoketMapper loketMapper;

    @BeforeEach
    public void setUp() {
        loketMapper = new LoketMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(loketMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(loketMapper.fromId(null)).isNull();
    }
}
