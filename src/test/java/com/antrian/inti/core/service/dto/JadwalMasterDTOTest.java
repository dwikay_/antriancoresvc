package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class JadwalMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JadwalMasterDTO.class);
        JadwalMasterDTO jadwalMasterDTO1 = new JadwalMasterDTO();
        jadwalMasterDTO1.setId(1L);
        JadwalMasterDTO jadwalMasterDTO2 = new JadwalMasterDTO();
        assertThat(jadwalMasterDTO1).isNotEqualTo(jadwalMasterDTO2);
        jadwalMasterDTO2.setId(jadwalMasterDTO1.getId());
        assertThat(jadwalMasterDTO1).isEqualTo(jadwalMasterDTO2);
        jadwalMasterDTO2.setId(2L);
        assertThat(jadwalMasterDTO1).isNotEqualTo(jadwalMasterDTO2);
        jadwalMasterDTO1.setId(null);
        assertThat(jadwalMasterDTO1).isNotEqualTo(jadwalMasterDTO2);
    }
}
