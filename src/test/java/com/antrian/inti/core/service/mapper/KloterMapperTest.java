package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class KloterMapperTest {

    private KloterMapper kloterMapper;

    @BeforeEach
    public void setUp() {
        kloterMapper = new KloterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(kloterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(kloterMapper.fromId(null)).isNull();
    }
}
