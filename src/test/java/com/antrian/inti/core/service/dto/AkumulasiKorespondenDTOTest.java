package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class AkumulasiKorespondenDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AkumulasiKorespondenDTO.class);
        AkumulasiKorespondenDTO akumulasiKorespondenDTO1 = new AkumulasiKorespondenDTO();
        akumulasiKorespondenDTO1.setId(1L);
        AkumulasiKorespondenDTO akumulasiKorespondenDTO2 = new AkumulasiKorespondenDTO();
        assertThat(akumulasiKorespondenDTO1).isNotEqualTo(akumulasiKorespondenDTO2);
        akumulasiKorespondenDTO2.setId(akumulasiKorespondenDTO1.getId());
        assertThat(akumulasiKorespondenDTO1).isEqualTo(akumulasiKorespondenDTO2);
        akumulasiKorespondenDTO2.setId(2L);
        assertThat(akumulasiKorespondenDTO1).isNotEqualTo(akumulasiKorespondenDTO2);
        akumulasiKorespondenDTO1.setId(null);
        assertThat(akumulasiKorespondenDTO1).isNotEqualTo(akumulasiKorespondenDTO2);
    }
}
