package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class KioskMapperTest {

    private KioskMapper kioskMapper;

    @BeforeEach
    public void setUp() {
        kioskMapper = new KioskMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(kioskMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(kioskMapper.fromId(null)).isNull();
    }
}
