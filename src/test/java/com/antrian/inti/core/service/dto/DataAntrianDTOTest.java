package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class DataAntrianDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataAntrianDTO.class);
        DataAntrianDTO dataAntrianDTO1 = new DataAntrianDTO();
        dataAntrianDTO1.setId(1L);
        DataAntrianDTO dataAntrianDTO2 = new DataAntrianDTO();
        assertThat(dataAntrianDTO1).isNotEqualTo(dataAntrianDTO2);
        dataAntrianDTO2.setId(dataAntrianDTO1.getId());
        assertThat(dataAntrianDTO1).isEqualTo(dataAntrianDTO2);
        dataAntrianDTO2.setId(2L);
        assertThat(dataAntrianDTO1).isNotEqualTo(dataAntrianDTO2);
        dataAntrianDTO1.setId(null);
        assertThat(dataAntrianDTO1).isNotEqualTo(dataAntrianDTO2);
    }
}
