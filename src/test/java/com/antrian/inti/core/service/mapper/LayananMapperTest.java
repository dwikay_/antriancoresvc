package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class LayananMapperTest {

    private LayananMapper layananMapper;

    @BeforeEach
    public void setUp() {
        layananMapper = new LayananMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(layananMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(layananMapper.fromId(null)).isNull();
    }
}
