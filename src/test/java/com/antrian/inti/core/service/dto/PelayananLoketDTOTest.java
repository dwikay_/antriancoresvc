package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PelayananLoketDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PelayananLoketDTO.class);
        PelayananLoketDTO pelayananLoketDTO1 = new PelayananLoketDTO();
        pelayananLoketDTO1.setId(1L);
        PelayananLoketDTO pelayananLoketDTO2 = new PelayananLoketDTO();
        assertThat(pelayananLoketDTO1).isNotEqualTo(pelayananLoketDTO2);
        pelayananLoketDTO2.setId(pelayananLoketDTO1.getId());
        assertThat(pelayananLoketDTO1).isEqualTo(pelayananLoketDTO2);
        pelayananLoketDTO2.setId(2L);
        assertThat(pelayananLoketDTO1).isNotEqualTo(pelayananLoketDTO2);
        pelayananLoketDTO1.setId(null);
        assertThat(pelayananLoketDTO1).isNotEqualTo(pelayananLoketDTO2);
    }
}
