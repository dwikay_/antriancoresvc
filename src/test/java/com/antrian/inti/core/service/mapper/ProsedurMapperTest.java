package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class ProsedurMapperTest {

    private ProsedurMapper prosedurMapper;

    @BeforeEach
    public void setUp() {
        prosedurMapper = new ProsedurMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(prosedurMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(prosedurMapper.fromId(null)).isNull();
    }
}
