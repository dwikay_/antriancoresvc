package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class DigitalSignageDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DigitalSignageDTO.class);
        DigitalSignageDTO digitalSignageDTO1 = new DigitalSignageDTO();
        digitalSignageDTO1.setId(1L);
        DigitalSignageDTO digitalSignageDTO2 = new DigitalSignageDTO();
        assertThat(digitalSignageDTO1).isNotEqualTo(digitalSignageDTO2);
        digitalSignageDTO2.setId(digitalSignageDTO1.getId());
        assertThat(digitalSignageDTO1).isEqualTo(digitalSignageDTO2);
        digitalSignageDTO2.setId(2L);
        assertThat(digitalSignageDTO1).isNotEqualTo(digitalSignageDTO2);
        digitalSignageDTO1.setId(null);
        assertThat(digitalSignageDTO1).isNotEqualTo(digitalSignageDTO2);
    }
}
