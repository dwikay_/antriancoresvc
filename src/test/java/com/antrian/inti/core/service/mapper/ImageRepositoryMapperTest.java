package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class ImageRepositoryMapperTest {

    private ImageRepositoryMapper imageRepositoryMapper;

    @BeforeEach
    public void setUp() {
        imageRepositoryMapper = new ImageRepositoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(imageRepositoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(imageRepositoryMapper.fromId(null)).isNull();
    }
}
