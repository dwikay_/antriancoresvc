package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class BannedMapperTest {

    private BannedMapper bannedMapper;

    @BeforeEach
    public void setUp() {
        bannedMapper = new BannedMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(bannedMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(bannedMapper.fromId(null)).isNull();
    }
}
