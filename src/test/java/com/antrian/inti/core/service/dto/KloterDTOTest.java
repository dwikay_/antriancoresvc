package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KloterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KloterDTO.class);
        KloterDTO kloterDTO1 = new KloterDTO();
        kloterDTO1.setId(1L);
        KloterDTO kloterDTO2 = new KloterDTO();
        assertThat(kloterDTO1).isNotEqualTo(kloterDTO2);
        kloterDTO2.setId(kloterDTO1.getId());
        assertThat(kloterDTO1).isEqualTo(kloterDTO2);
        kloterDTO2.setId(2L);
        assertThat(kloterDTO1).isNotEqualTo(kloterDTO2);
        kloterDTO1.setId(null);
        assertThat(kloterDTO1).isNotEqualTo(kloterDTO2);
    }
}
