package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class HariLiburMapperTest {

    private HariLiburMapper hariLiburMapper;

    @BeforeEach
    public void setUp() {
        hariLiburMapper = new HariLiburMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(hariLiburMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(hariLiburMapper.fromId(null)).isNull();
    }
}
