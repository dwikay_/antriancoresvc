package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class KuotaMapperTest {

    private KuotaMapper kuotaMapper;

    @BeforeEach
    public void setUp() {
        kuotaMapper = new KuotaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(kuotaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(kuotaMapper.fromId(null)).isNull();
    }
}
