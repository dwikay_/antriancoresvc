package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KioskDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KioskDTO.class);
        KioskDTO kioskDTO1 = new KioskDTO();
        kioskDTO1.setId(1L);
        KioskDTO kioskDTO2 = new KioskDTO();
        assertThat(kioskDTO1).isNotEqualTo(kioskDTO2);
        kioskDTO2.setId(kioskDTO1.getId());
        assertThat(kioskDTO1).isEqualTo(kioskDTO2);
        kioskDTO2.setId(2L);
        assertThat(kioskDTO1).isNotEqualTo(kioskDTO2);
        kioskDTO1.setId(null);
        assertThat(kioskDTO1).isNotEqualTo(kioskDTO2);
    }
}
