package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class PelayananLoketMapperTest {

    private PelayananLoketMapper pelayananLoketMapper;

    @BeforeEach
    public void setUp() {
        pelayananLoketMapper = new PelayananLoketMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(pelayananLoketMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(pelayananLoketMapper.fromId(null)).isNull();
    }
}
