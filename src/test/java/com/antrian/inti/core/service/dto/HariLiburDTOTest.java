package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class HariLiburDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HariLiburDTO.class);
        HariLiburDTO hariLiburDTO1 = new HariLiburDTO();
        hariLiburDTO1.setId(1L);
        HariLiburDTO hariLiburDTO2 = new HariLiburDTO();
        assertThat(hariLiburDTO1).isNotEqualTo(hariLiburDTO2);
        hariLiburDTO2.setId(hariLiburDTO1.getId());
        assertThat(hariLiburDTO1).isEqualTo(hariLiburDTO2);
        hariLiburDTO2.setId(2L);
        assertThat(hariLiburDTO1).isNotEqualTo(hariLiburDTO2);
        hariLiburDTO1.setId(null);
        assertThat(hariLiburDTO1).isNotEqualTo(hariLiburDTO2);
    }
}
