package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PendaftaranDumyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PendaftaranDumyDTO.class);
        PendaftaranDumyDTO pendaftaranDumyDTO1 = new PendaftaranDumyDTO();
        pendaftaranDumyDTO1.setId(1L);
        PendaftaranDumyDTO pendaftaranDumyDTO2 = new PendaftaranDumyDTO();
        assertThat(pendaftaranDumyDTO1).isNotEqualTo(pendaftaranDumyDTO2);
        pendaftaranDumyDTO2.setId(pendaftaranDumyDTO1.getId());
        assertThat(pendaftaranDumyDTO1).isEqualTo(pendaftaranDumyDTO2);
        pendaftaranDumyDTO2.setId(2L);
        assertThat(pendaftaranDumyDTO1).isNotEqualTo(pendaftaranDumyDTO2);
        pendaftaranDumyDTO1.setId(null);
        assertThat(pendaftaranDumyDTO1).isNotEqualTo(pendaftaranDumyDTO2);
    }
}
