package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class JadwalMasterMapperTest {

    private JadwalMasterMapper jadwalMasterMapper;

    @BeforeEach
    public void setUp() {
        jadwalMasterMapper = new JadwalMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(jadwalMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(jadwalMasterMapper.fromId(null)).isNull();
    }
}
