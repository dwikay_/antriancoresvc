package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class PendaftaranOfflineMapperTest {

    private PendaftaranOfflineMapper pendaftaranOfflineMapper;

    @BeforeEach
    public void setUp() {
        pendaftaranOfflineMapper = new PendaftaranOfflineMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(pendaftaranOfflineMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(pendaftaranOfflineMapper.fromId(null)).isNull();
    }
}
