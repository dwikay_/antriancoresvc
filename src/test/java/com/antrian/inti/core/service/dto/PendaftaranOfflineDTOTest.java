package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PendaftaranOfflineDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PendaftaranOfflineDTO.class);
        PendaftaranOfflineDTO pendaftaranOfflineDTO1 = new PendaftaranOfflineDTO();
        pendaftaranOfflineDTO1.setId(1L);
        PendaftaranOfflineDTO pendaftaranOfflineDTO2 = new PendaftaranOfflineDTO();
        assertThat(pendaftaranOfflineDTO1).isNotEqualTo(pendaftaranOfflineDTO2);
        pendaftaranOfflineDTO2.setId(pendaftaranOfflineDTO1.getId());
        assertThat(pendaftaranOfflineDTO1).isEqualTo(pendaftaranOfflineDTO2);
        pendaftaranOfflineDTO2.setId(2L);
        assertThat(pendaftaranOfflineDTO1).isNotEqualTo(pendaftaranOfflineDTO2);
        pendaftaranOfflineDTO1.setId(null);
        assertThat(pendaftaranOfflineDTO1).isNotEqualTo(pendaftaranOfflineDTO2);
    }
}
