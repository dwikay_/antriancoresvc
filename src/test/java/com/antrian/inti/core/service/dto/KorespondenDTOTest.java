package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KorespondenDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KorespondenDTO.class);
        KorespondenDTO korespondenDTO1 = new KorespondenDTO();
        korespondenDTO1.setId(1L);
        KorespondenDTO korespondenDTO2 = new KorespondenDTO();
        assertThat(korespondenDTO1).isNotEqualTo(korespondenDTO2);
        korespondenDTO2.setId(korespondenDTO1.getId());
        assertThat(korespondenDTO1).isEqualTo(korespondenDTO2);
        korespondenDTO2.setId(2L);
        assertThat(korespondenDTO1).isNotEqualTo(korespondenDTO2);
        korespondenDTO1.setId(null);
        assertThat(korespondenDTO1).isNotEqualTo(korespondenDTO2);
    }
}
