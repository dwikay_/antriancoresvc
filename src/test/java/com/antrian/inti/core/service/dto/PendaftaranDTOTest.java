package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PendaftaranDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PendaftaranDTO.class);
        PendaftaranDTO pendaftaranDTO1 = new PendaftaranDTO();
        pendaftaranDTO1.setId(1L);
        PendaftaranDTO pendaftaranDTO2 = new PendaftaranDTO();
        assertThat(pendaftaranDTO1).isNotEqualTo(pendaftaranDTO2);
        pendaftaranDTO2.setId(pendaftaranDTO1.getId());
        assertThat(pendaftaranDTO1).isEqualTo(pendaftaranDTO2);
        pendaftaranDTO2.setId(2L);
        assertThat(pendaftaranDTO1).isNotEqualTo(pendaftaranDTO2);
        pendaftaranDTO1.setId(null);
        assertThat(pendaftaranDTO1).isNotEqualTo(pendaftaranDTO2);
    }
}
