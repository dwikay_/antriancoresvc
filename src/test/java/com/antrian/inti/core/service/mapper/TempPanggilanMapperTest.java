package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class TempPanggilanMapperTest {

    private TempPanggilanMapper tempPanggilanMapper;

    @BeforeEach
    public void setUp() {
        tempPanggilanMapper = new TempPanggilanMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(tempPanggilanMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(tempPanggilanMapper.fromId(null)).isNull();
    }
}
