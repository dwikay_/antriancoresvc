package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class LoketDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoketDTO.class);
        LoketDTO loketDTO1 = new LoketDTO();
        loketDTO1.setId(1L);
        LoketDTO loketDTO2 = new LoketDTO();
        assertThat(loketDTO1).isNotEqualTo(loketDTO2);
        loketDTO2.setId(loketDTO1.getId());
        assertThat(loketDTO1).isEqualTo(loketDTO2);
        loketDTO2.setId(2L);
        assertThat(loketDTO1).isNotEqualTo(loketDTO2);
        loketDTO1.setId(null);
        assertThat(loketDTO1).isNotEqualTo(loketDTO2);
    }
}
