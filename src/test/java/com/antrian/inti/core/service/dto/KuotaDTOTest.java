package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KuotaDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KuotaDTO.class);
        KuotaDTO kuotaDTO1 = new KuotaDTO();
        kuotaDTO1.setId(1L);
        KuotaDTO kuotaDTO2 = new KuotaDTO();
        assertThat(kuotaDTO1).isNotEqualTo(kuotaDTO2);
        kuotaDTO2.setId(kuotaDTO1.getId());
        assertThat(kuotaDTO1).isEqualTo(kuotaDTO2);
        kuotaDTO2.setId(2L);
        assertThat(kuotaDTO1).isNotEqualTo(kuotaDTO2);
        kuotaDTO1.setId(null);
        assertThat(kuotaDTO1).isNotEqualTo(kuotaDTO2);
    }
}
