package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class DataAntrianMapperTest {

    private DataAntrianMapper dataAntrianMapper;

    @BeforeEach
    public void setUp() {
        dataAntrianMapper = new DataAntrianMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(dataAntrianMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(dataAntrianMapper.fromId(null)).isNull();
    }
}
