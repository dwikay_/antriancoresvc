package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class ImageRepositoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageRepositoryDTO.class);
        ImageRepositoryDTO imageRepositoryDTO1 = new ImageRepositoryDTO();
        imageRepositoryDTO1.setId(1L);
        ImageRepositoryDTO imageRepositoryDTO2 = new ImageRepositoryDTO();
        assertThat(imageRepositoryDTO1).isNotEqualTo(imageRepositoryDTO2);
        imageRepositoryDTO2.setId(imageRepositoryDTO1.getId());
        assertThat(imageRepositoryDTO1).isEqualTo(imageRepositoryDTO2);
        imageRepositoryDTO2.setId(2L);
        assertThat(imageRepositoryDTO1).isNotEqualTo(imageRepositoryDTO2);
        imageRepositoryDTO1.setId(null);
        assertThat(imageRepositoryDTO1).isNotEqualTo(imageRepositoryDTO2);
    }
}
