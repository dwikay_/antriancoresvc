package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class PendaftaranDumyMapperTest {

    private PendaftaranDumyMapper pendaftaranDumyMapper;

    @BeforeEach
    public void setUp() {
        pendaftaranDumyMapper = new PendaftaranDumyMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(pendaftaranDumyMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(pendaftaranDumyMapper.fromId(null)).isNull();
    }
}
