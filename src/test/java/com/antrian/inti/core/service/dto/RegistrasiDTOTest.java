package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class RegistrasiDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistrasiDTO.class);
        RegistrasiDTO registrasiDTO1 = new RegistrasiDTO();
        registrasiDTO1.setId(1L);
        RegistrasiDTO registrasiDTO2 = new RegistrasiDTO();
        assertThat(registrasiDTO1).isNotEqualTo(registrasiDTO2);
        registrasiDTO2.setId(registrasiDTO1.getId());
        assertThat(registrasiDTO1).isEqualTo(registrasiDTO2);
        registrasiDTO2.setId(2L);
        assertThat(registrasiDTO1).isNotEqualTo(registrasiDTO2);
        registrasiDTO1.setId(null);
        assertThat(registrasiDTO1).isNotEqualTo(registrasiDTO2);
    }
}
