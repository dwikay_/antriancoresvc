package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class TempPanggilanDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TempPanggilanDTO.class);
        TempPanggilanDTO tempPanggilanDTO1 = new TempPanggilanDTO();
        tempPanggilanDTO1.setId(1L);
        TempPanggilanDTO tempPanggilanDTO2 = new TempPanggilanDTO();
        assertThat(tempPanggilanDTO1).isNotEqualTo(tempPanggilanDTO2);
        tempPanggilanDTO2.setId(tempPanggilanDTO1.getId());
        assertThat(tempPanggilanDTO1).isEqualTo(tempPanggilanDTO2);
        tempPanggilanDTO2.setId(2L);
        assertThat(tempPanggilanDTO1).isNotEqualTo(tempPanggilanDTO2);
        tempPanggilanDTO1.setId(null);
        assertThat(tempPanggilanDTO1).isNotEqualTo(tempPanggilanDTO2);
    }
}
