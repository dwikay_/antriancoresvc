package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class RunningTextDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RunningTextDTO.class);
        RunningTextDTO runningTextDTO1 = new RunningTextDTO();
        runningTextDTO1.setId(1L);
        RunningTextDTO runningTextDTO2 = new RunningTextDTO();
        assertThat(runningTextDTO1).isNotEqualTo(runningTextDTO2);
        runningTextDTO2.setId(runningTextDTO1.getId());
        assertThat(runningTextDTO1).isEqualTo(runningTextDTO2);
        runningTextDTO2.setId(2L);
        assertThat(runningTextDTO1).isNotEqualTo(runningTextDTO2);
        runningTextDTO1.setId(null);
        assertThat(runningTextDTO1).isNotEqualTo(runningTextDTO2);
    }
}
