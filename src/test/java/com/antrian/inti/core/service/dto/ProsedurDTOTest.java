package com.antrian.inti.core.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class ProsedurDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProsedurDTO.class);
        ProsedurDTO prosedurDTO1 = new ProsedurDTO();
        prosedurDTO1.setId(1L);
        ProsedurDTO prosedurDTO2 = new ProsedurDTO();
        assertThat(prosedurDTO1).isNotEqualTo(prosedurDTO2);
        prosedurDTO2.setId(prosedurDTO1.getId());
        assertThat(prosedurDTO1).isEqualTo(prosedurDTO2);
        prosedurDTO2.setId(2L);
        assertThat(prosedurDTO1).isNotEqualTo(prosedurDTO2);
        prosedurDTO1.setId(null);
        assertThat(prosedurDTO1).isNotEqualTo(prosedurDTO2);
    }
}
