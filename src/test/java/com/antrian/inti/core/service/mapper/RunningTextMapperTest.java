package com.antrian.inti.core.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class RunningTextMapperTest {

    private RunningTextMapper runningTextMapper;

    @BeforeEach
    public void setUp() {
        runningTextMapper = new RunningTextMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(runningTextMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(runningTextMapper.fromId(null)).isNull();
    }
}
