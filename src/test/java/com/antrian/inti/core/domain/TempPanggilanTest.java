package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class TempPanggilanTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TempPanggilan.class);
        TempPanggilan tempPanggilan1 = new TempPanggilan();
        tempPanggilan1.setId(1L);
        TempPanggilan tempPanggilan2 = new TempPanggilan();
        tempPanggilan2.setId(tempPanggilan1.getId());
        assertThat(tempPanggilan1).isEqualTo(tempPanggilan2);
        tempPanggilan2.setId(2L);
        assertThat(tempPanggilan1).isNotEqualTo(tempPanggilan2);
        tempPanggilan1.setId(null);
        assertThat(tempPanggilan1).isNotEqualTo(tempPanggilan2);
    }
}
