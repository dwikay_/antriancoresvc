package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KloterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kloter.class);
        Kloter kloter1 = new Kloter();
        kloter1.setId(1L);
        Kloter kloter2 = new Kloter();
        kloter2.setId(kloter1.getId());
        assertThat(kloter1).isEqualTo(kloter2);
        kloter2.setId(2L);
        assertThat(kloter1).isNotEqualTo(kloter2);
        kloter1.setId(null);
        assertThat(kloter1).isNotEqualTo(kloter2);
    }
}
