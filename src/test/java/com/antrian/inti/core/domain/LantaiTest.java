package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class LantaiTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Lantai.class);
        Lantai lantai1 = new Lantai();
        lantai1.setId(1L);
        Lantai lantai2 = new Lantai();
        lantai2.setId(lantai1.getId());
        assertThat(lantai1).isEqualTo(lantai2);
        lantai2.setId(2L);
        assertThat(lantai1).isNotEqualTo(lantai2);
        lantai1.setId(null);
        assertThat(lantai1).isNotEqualTo(lantai2);
    }
}
