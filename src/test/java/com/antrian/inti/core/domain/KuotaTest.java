package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KuotaTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kuota.class);
        Kuota kuota1 = new Kuota();
        kuota1.setId(1L);
        Kuota kuota2 = new Kuota();
        kuota2.setId(kuota1.getId());
        assertThat(kuota1).isEqualTo(kuota2);
        kuota2.setId(2L);
        assertThat(kuota1).isNotEqualTo(kuota2);
        kuota1.setId(null);
        assertThat(kuota1).isNotEqualTo(kuota2);
    }
}
