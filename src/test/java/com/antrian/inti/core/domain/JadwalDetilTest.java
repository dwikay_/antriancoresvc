package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class JadwalDetilTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JadwalDetil.class);
        JadwalDetil jadwalDetil1 = new JadwalDetil();
        jadwalDetil1.setId(1L);
        JadwalDetil jadwalDetil2 = new JadwalDetil();
        jadwalDetil2.setId(jadwalDetil1.getId());
        assertThat(jadwalDetil1).isEqualTo(jadwalDetil2);
        jadwalDetil2.setId(2L);
        assertThat(jadwalDetil1).isNotEqualTo(jadwalDetil2);
        jadwalDetil1.setId(null);
        assertThat(jadwalDetil1).isNotEqualTo(jadwalDetil2);
    }
}
