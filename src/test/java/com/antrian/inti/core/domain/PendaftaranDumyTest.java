package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PendaftaranDumyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PendaftaranDumy.class);
        PendaftaranDumy pendaftaranDumy1 = new PendaftaranDumy();
        pendaftaranDumy1.setId(1L);
        PendaftaranDumy pendaftaranDumy2 = new PendaftaranDumy();
        pendaftaranDumy2.setId(pendaftaranDumy1.getId());
        assertThat(pendaftaranDumy1).isEqualTo(pendaftaranDumy2);
        pendaftaranDumy2.setId(2L);
        assertThat(pendaftaranDumy1).isNotEqualTo(pendaftaranDumy2);
        pendaftaranDumy1.setId(null);
        assertThat(pendaftaranDumy1).isNotEqualTo(pendaftaranDumy2);
    }
}
