package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class LayananTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Layanan.class);
        Layanan layanan1 = new Layanan();
        layanan1.setId(1L);
        Layanan layanan2 = new Layanan();
        layanan2.setId(layanan1.getId());
        assertThat(layanan1).isEqualTo(layanan2);
        layanan2.setId(2L);
        assertThat(layanan1).isNotEqualTo(layanan2);
        layanan1.setId(null);
        assertThat(layanan1).isNotEqualTo(layanan2);
    }
}
