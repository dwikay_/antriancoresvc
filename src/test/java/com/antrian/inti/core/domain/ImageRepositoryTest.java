package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class ImageRepositoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageRepository.class);
        ImageRepository imageRepository1 = new ImageRepository();
        imageRepository1.setId(1L);
        ImageRepository imageRepository2 = new ImageRepository();
        imageRepository2.setId(imageRepository1.getId());
        assertThat(imageRepository1).isEqualTo(imageRepository2);
        imageRepository2.setId(2L);
        assertThat(imageRepository1).isNotEqualTo(imageRepository2);
        imageRepository1.setId(null);
        assertThat(imageRepository1).isNotEqualTo(imageRepository2);
    }
}
