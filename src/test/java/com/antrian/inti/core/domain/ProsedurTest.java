package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class ProsedurTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Prosedur.class);
        Prosedur prosedur1 = new Prosedur();
        prosedur1.setId(1L);
        Prosedur prosedur2 = new Prosedur();
        prosedur2.setId(prosedur1.getId());
        assertThat(prosedur1).isEqualTo(prosedur2);
        prosedur2.setId(2L);
        assertThat(prosedur1).isNotEqualTo(prosedur2);
        prosedur1.setId(null);
        assertThat(prosedur1).isNotEqualTo(prosedur2);
    }
}
