package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KioskTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kiosk.class);
        Kiosk kiosk1 = new Kiosk();
        kiosk1.setId(1L);
        Kiosk kiosk2 = new Kiosk();
        kiosk2.setId(kiosk1.getId());
        assertThat(kiosk1).isEqualTo(kiosk2);
        kiosk2.setId(2L);
        assertThat(kiosk1).isNotEqualTo(kiosk2);
        kiosk1.setId(null);
        assertThat(kiosk1).isNotEqualTo(kiosk2);
    }
}
