package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class RegistrasiTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Registrasi.class);
        Registrasi registrasi1 = new Registrasi();
        registrasi1.setId(1L);
        Registrasi registrasi2 = new Registrasi();
        registrasi2.setId(registrasi1.getId());
        assertThat(registrasi1).isEqualTo(registrasi2);
        registrasi2.setId(2L);
        assertThat(registrasi1).isNotEqualTo(registrasi2);
        registrasi1.setId(null);
        assertThat(registrasi1).isNotEqualTo(registrasi2);
    }
}
