package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PendaftaranTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pendaftaran.class);
        Pendaftaran pendaftaran1 = new Pendaftaran();
        pendaftaran1.setId(1L);
        Pendaftaran pendaftaran2 = new Pendaftaran();
        pendaftaran2.setId(pendaftaran1.getId());
        assertThat(pendaftaran1).isEqualTo(pendaftaran2);
        pendaftaran2.setId(2L);
        assertThat(pendaftaran1).isNotEqualTo(pendaftaran2);
        pendaftaran1.setId(null);
        assertThat(pendaftaran1).isNotEqualTo(pendaftaran2);
    }
}
