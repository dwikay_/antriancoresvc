package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class HariLiburTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HariLibur.class);
        HariLibur hariLibur1 = new HariLibur();
        hariLibur1.setId(1L);
        HariLibur hariLibur2 = new HariLibur();
        hariLibur2.setId(hariLibur1.getId());
        assertThat(hariLibur1).isEqualTo(hariLibur2);
        hariLibur2.setId(2L);
        assertThat(hariLibur1).isNotEqualTo(hariLibur2);
        hariLibur1.setId(null);
        assertThat(hariLibur1).isNotEqualTo(hariLibur2);
    }
}
