package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class JadwalMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JadwalMaster.class);
        JadwalMaster jadwalMaster1 = new JadwalMaster();
        jadwalMaster1.setId(1L);
        JadwalMaster jadwalMaster2 = new JadwalMaster();
        jadwalMaster2.setId(jadwalMaster1.getId());
        assertThat(jadwalMaster1).isEqualTo(jadwalMaster2);
        jadwalMaster2.setId(2L);
        assertThat(jadwalMaster1).isNotEqualTo(jadwalMaster2);
        jadwalMaster1.setId(null);
        assertThat(jadwalMaster1).isNotEqualTo(jadwalMaster2);
    }
}
