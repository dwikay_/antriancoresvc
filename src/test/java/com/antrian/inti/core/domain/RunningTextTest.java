package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class RunningTextTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RunningText.class);
        RunningText runningText1 = new RunningText();
        runningText1.setId(1L);
        RunningText runningText2 = new RunningText();
        runningText2.setId(runningText1.getId());
        assertThat(runningText1).isEqualTo(runningText2);
        runningText2.setId(2L);
        assertThat(runningText1).isNotEqualTo(runningText2);
        runningText1.setId(null);
        assertThat(runningText1).isNotEqualTo(runningText2);
    }
}
