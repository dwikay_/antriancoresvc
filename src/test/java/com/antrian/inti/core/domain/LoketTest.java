package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class LoketTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Loket.class);
        Loket loket1 = new Loket();
        loket1.setId(1L);
        Loket loket2 = new Loket();
        loket2.setId(loket1.getId());
        assertThat(loket1).isEqualTo(loket2);
        loket2.setId(2L);
        assertThat(loket1).isNotEqualTo(loket2);
        loket1.setId(null);
        assertThat(loket1).isNotEqualTo(loket2);
    }
}
