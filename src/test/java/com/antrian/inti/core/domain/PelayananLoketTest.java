package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PelayananLoketTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PelayananLoket.class);
        PelayananLoket pelayananLoket1 = new PelayananLoket();
        pelayananLoket1.setId(1L);
        PelayananLoket pelayananLoket2 = new PelayananLoket();
        pelayananLoket2.setId(pelayananLoket1.getId());
        assertThat(pelayananLoket1).isEqualTo(pelayananLoket2);
        pelayananLoket2.setId(2L);
        assertThat(pelayananLoket1).isNotEqualTo(pelayananLoket2);
        pelayananLoket1.setId(null);
        assertThat(pelayananLoket1).isNotEqualTo(pelayananLoket2);
    }
}
