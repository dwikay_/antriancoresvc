package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class DataAntrianTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataAntrian.class);
        DataAntrian dataAntrian1 = new DataAntrian();
        dataAntrian1.setId(1L);
        DataAntrian dataAntrian2 = new DataAntrian();
        dataAntrian2.setId(dataAntrian1.getId());
        assertThat(dataAntrian1).isEqualTo(dataAntrian2);
        dataAntrian2.setId(2L);
        assertThat(dataAntrian1).isNotEqualTo(dataAntrian2);
        dataAntrian1.setId(null);
        assertThat(dataAntrian1).isNotEqualTo(dataAntrian2);
    }
}
