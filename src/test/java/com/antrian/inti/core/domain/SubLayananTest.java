package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class SubLayananTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubLayanan.class);
        SubLayanan subLayanan1 = new SubLayanan();
        subLayanan1.setId(1L);
        SubLayanan subLayanan2 = new SubLayanan();
        subLayanan2.setId(subLayanan1.getId());
        assertThat(subLayanan1).isEqualTo(subLayanan2);
        subLayanan2.setId(2L);
        assertThat(subLayanan1).isNotEqualTo(subLayanan2);
        subLayanan1.setId(null);
        assertThat(subLayanan1).isNotEqualTo(subLayanan2);
    }
}
