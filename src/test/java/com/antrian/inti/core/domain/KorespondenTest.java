package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class KorespondenTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Koresponden.class);
        Koresponden koresponden1 = new Koresponden();
        koresponden1.setId(1L);
        Koresponden koresponden2 = new Koresponden();
        koresponden2.setId(koresponden1.getId());
        assertThat(koresponden1).isEqualTo(koresponden2);
        koresponden2.setId(2L);
        assertThat(koresponden1).isNotEqualTo(koresponden2);
        koresponden1.setId(null);
        assertThat(koresponden1).isNotEqualTo(koresponden2);
    }
}
