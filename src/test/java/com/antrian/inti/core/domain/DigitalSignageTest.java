package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class DigitalSignageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DigitalSignage.class);
        DigitalSignage digitalSignage1 = new DigitalSignage();
        digitalSignage1.setId(1L);
        DigitalSignage digitalSignage2 = new DigitalSignage();
        digitalSignage2.setId(digitalSignage1.getId());
        assertThat(digitalSignage1).isEqualTo(digitalSignage2);
        digitalSignage2.setId(2L);
        assertThat(digitalSignage1).isNotEqualTo(digitalSignage2);
        digitalSignage1.setId(null);
        assertThat(digitalSignage1).isNotEqualTo(digitalSignage2);
    }
}
