package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class PendaftaranOfflineTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PendaftaranOffline.class);
        PendaftaranOffline pendaftaranOffline1 = new PendaftaranOffline();
        pendaftaranOffline1.setId(1L);
        PendaftaranOffline pendaftaranOffline2 = new PendaftaranOffline();
        pendaftaranOffline2.setId(pendaftaranOffline1.getId());
        assertThat(pendaftaranOffline1).isEqualTo(pendaftaranOffline2);
        pendaftaranOffline2.setId(2L);
        assertThat(pendaftaranOffline1).isNotEqualTo(pendaftaranOffline2);
        pendaftaranOffline1.setId(null);
        assertThat(pendaftaranOffline1).isNotEqualTo(pendaftaranOffline2);
    }
}
