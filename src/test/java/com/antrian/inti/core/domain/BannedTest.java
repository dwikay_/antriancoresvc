package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class BannedTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Banned.class);
        Banned banned1 = new Banned();
        banned1.setId(1L);
        Banned banned2 = new Banned();
        banned2.setId(banned1.getId());
        assertThat(banned1).isEqualTo(banned2);
        banned2.setId(2L);
        assertThat(banned1).isNotEqualTo(banned2);
        banned1.setId(null);
        assertThat(banned1).isNotEqualTo(banned2);
    }
}
