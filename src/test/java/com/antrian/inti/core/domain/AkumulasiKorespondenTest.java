package com.antrian.inti.core.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.antrian.inti.core.web.rest.TestUtil;

public class AkumulasiKorespondenTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AkumulasiKoresponden.class);
        AkumulasiKoresponden akumulasiKoresponden1 = new AkumulasiKoresponden();
        akumulasiKoresponden1.setId(1L);
        AkumulasiKoresponden akumulasiKoresponden2 = new AkumulasiKoresponden();
        akumulasiKoresponden2.setId(akumulasiKoresponden1.getId());
        assertThat(akumulasiKoresponden1).isEqualTo(akumulasiKoresponden2);
        akumulasiKoresponden2.setId(2L);
        assertThat(akumulasiKoresponden1).isNotEqualTo(akumulasiKoresponden2);
        akumulasiKoresponden1.setId(null);
        assertThat(akumulasiKoresponden1).isNotEqualTo(akumulasiKoresponden2);
    }
}
