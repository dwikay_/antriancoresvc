package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.PelayananLoket;
import com.antrian.inti.core.repository.PelayananLoketRepository;
import com.antrian.inti.core.service.PelayananLoketService;
import com.antrian.inti.core.service.dto.PelayananLoketDTO;
import com.antrian.inti.core.service.mapper.PelayananLoketMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PelayananLoketResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class PelayananLoketResourceIT {

    private static final Long DEFAULT_ID_LAYANAN = 1L;
    private static final Long UPDATED_ID_LAYANAN = 2L;

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final Long DEFAULT_ID_LOKET = 1L;
    private static final Long UPDATED_ID_LOKET = 2L;

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private PelayananLoketRepository pelayananLoketRepository;

    @Autowired
    private PelayananLoketMapper pelayananLoketMapper;

    @Autowired
    private PelayananLoketService pelayananLoketService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPelayananLoketMockMvc;

    private PelayananLoket pelayananLoket;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PelayananLoketResource pelayananLoketResource = new PelayananLoketResource(pelayananLoketService);
        this.restPelayananLoketMockMvc = MockMvcBuilders.standaloneSetup(pelayananLoketResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PelayananLoket createEntity(EntityManager em) {
        PelayananLoket pelayananLoket = new PelayananLoket()
            .idLayanan(DEFAULT_ID_LAYANAN)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .idLoket(DEFAULT_ID_LOKET)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        return pelayananLoket;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PelayananLoket createUpdatedEntity(EntityManager em) {
        PelayananLoket pelayananLoket = new PelayananLoket()
            .idLayanan(UPDATED_ID_LAYANAN)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .idLoket(UPDATED_ID_LOKET)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        return pelayananLoket;
    }

    @BeforeEach
    public void initTest() {
        pelayananLoket = createEntity(em);
    }

    @Test
    @Transactional
    public void createPelayananLoket() throws Exception {
        int databaseSizeBeforeCreate = pelayananLoketRepository.findAll().size();

        // Create the PelayananLoket
        PelayananLoketDTO pelayananLoketDTO = pelayananLoketMapper.toDto(pelayananLoket);
        restPelayananLoketMockMvc.perform(post("/api/pelayanan-lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pelayananLoketDTO)))
            .andExpect(status().isCreated());

        // Validate the PelayananLoket in the database
        List<PelayananLoket> pelayananLoketList = pelayananLoketRepository.findAll();
        assertThat(pelayananLoketList).hasSize(databaseSizeBeforeCreate + 1);
        PelayananLoket testPelayananLoket = pelayananLoketList.get(pelayananLoketList.size() - 1);
        assertThat(testPelayananLoket.getIdLayanan()).isEqualTo(DEFAULT_ID_LAYANAN);
        assertThat(testPelayananLoket.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testPelayananLoket.getIdLoket()).isEqualTo(DEFAULT_ID_LOKET);
        assertThat(testPelayananLoket.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testPelayananLoket.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testPelayananLoket.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testPelayananLoket.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testPelayananLoket.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void createPelayananLoketWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pelayananLoketRepository.findAll().size();

        // Create the PelayananLoket with an existing ID
        pelayananLoket.setId(1L);
        PelayananLoketDTO pelayananLoketDTO = pelayananLoketMapper.toDto(pelayananLoket);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPelayananLoketMockMvc.perform(post("/api/pelayanan-lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pelayananLoketDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PelayananLoket in the database
        List<PelayananLoket> pelayananLoketList = pelayananLoketRepository.findAll();
        assertThat(pelayananLoketList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPelayananLokets() throws Exception {
        // Initialize the database
        pelayananLoketRepository.saveAndFlush(pelayananLoket);

        // Get all the pelayananLoketList
        restPelayananLoketMockMvc.perform(get("/api/pelayanan-lokets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pelayananLoket.getId().intValue())))
            .andExpect(jsonPath("$.[*].idLayanan").value(hasItem(DEFAULT_ID_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].idLoket").value(hasItem(DEFAULT_ID_LOKET.intValue())))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))));
    }

    @Test
    @Transactional
    public void getPelayananLoket() throws Exception {
        // Initialize the database
        pelayananLoketRepository.saveAndFlush(pelayananLoket);

        // Get the pelayananLoket
        restPelayananLoketMockMvc.perform(get("/api/pelayanan-lokets/{id}", pelayananLoket.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pelayananLoket.getId().intValue()))
            .andExpect(jsonPath("$.idLayanan").value(DEFAULT_ID_LAYANAN.intValue()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.idLoket").value(DEFAULT_ID_LOKET.intValue()))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)));
    }

    @Test
    @Transactional
    public void getNonExistingPelayananLoket() throws Exception {
        // Get the pelayananLoket
        restPelayananLoketMockMvc.perform(get("/api/pelayanan-lokets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePelayananLoket() throws Exception {
        // Initialize the database
        pelayananLoketRepository.saveAndFlush(pelayananLoket);

        int databaseSizeBeforeUpdate = pelayananLoketRepository.findAll().size();

        // Update the pelayananLoket
        PelayananLoket updatedPelayananLoket = pelayananLoketRepository.findById(pelayananLoket.getId()).get();
        // Disconnect from session so that the updates on updatedPelayananLoket are not directly saved in db
        em.detach(updatedPelayananLoket);
        updatedPelayananLoket
            .idLayanan(UPDATED_ID_LAYANAN)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .idLoket(UPDATED_ID_LOKET)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        PelayananLoketDTO pelayananLoketDTO = pelayananLoketMapper.toDto(updatedPelayananLoket);

        restPelayananLoketMockMvc.perform(put("/api/pelayanan-lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pelayananLoketDTO)))
            .andExpect(status().isOk());

        // Validate the PelayananLoket in the database
        List<PelayananLoket> pelayananLoketList = pelayananLoketRepository.findAll();
        assertThat(pelayananLoketList).hasSize(databaseSizeBeforeUpdate);
        PelayananLoket testPelayananLoket = pelayananLoketList.get(pelayananLoketList.size() - 1);
        assertThat(testPelayananLoket.getIdLayanan()).isEqualTo(UPDATED_ID_LAYANAN);
        assertThat(testPelayananLoket.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testPelayananLoket.getIdLoket()).isEqualTo(UPDATED_ID_LOKET);
        assertThat(testPelayananLoket.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testPelayananLoket.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testPelayananLoket.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testPelayananLoket.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testPelayananLoket.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void updateNonExistingPelayananLoket() throws Exception {
        int databaseSizeBeforeUpdate = pelayananLoketRepository.findAll().size();

        // Create the PelayananLoket
        PelayananLoketDTO pelayananLoketDTO = pelayananLoketMapper.toDto(pelayananLoket);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPelayananLoketMockMvc.perform(put("/api/pelayanan-lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pelayananLoketDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PelayananLoket in the database
        List<PelayananLoket> pelayananLoketList = pelayananLoketRepository.findAll();
        assertThat(pelayananLoketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePelayananLoket() throws Exception {
        // Initialize the database
        pelayananLoketRepository.saveAndFlush(pelayananLoket);

        int databaseSizeBeforeDelete = pelayananLoketRepository.findAll().size();

        // Delete the pelayananLoket
        restPelayananLoketMockMvc.perform(delete("/api/pelayanan-lokets/{id}", pelayananLoket.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PelayananLoket> pelayananLoketList = pelayananLoketRepository.findAll();
        assertThat(pelayananLoketList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
