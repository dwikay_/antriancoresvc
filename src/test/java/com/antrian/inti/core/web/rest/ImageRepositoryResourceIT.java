package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.ImageRepository;
import com.antrian.inti.core.repository.ImageRepositoryRepository;
import com.antrian.inti.core.service.ImageRepositoryService;
import com.antrian.inti.core.service.dto.ImageRepositoryDTO;
import com.antrian.inti.core.service.mapper.ImageRepositoryMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImageRepositoryResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class ImageRepositoryResourceIT {

    private static final String DEFAULT_NAMA_FILE = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_FILE = "BBBBBBBBBB";

    private static final Long DEFAULT_RELASI_ID = 1L;
    private static final Long UPDATED_RELASI_ID = 2L;

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final String DEFAULT_DOCUMENT = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT = "BBBBBBBBBB";

    @Autowired
    private ImageRepositoryRepository imageRepositoryRepository;

    @Autowired
    private ImageRepositoryMapper imageRepositoryMapper;

    @Autowired
    private ImageRepositoryService imageRepositoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restImageRepositoryMockMvc;

    private ImageRepository imageRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ImageRepositoryResource imageRepositoryResource = new ImageRepositoryResource(imageRepositoryService);
        this.restImageRepositoryMockMvc = MockMvcBuilders.standaloneSetup(imageRepositoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageRepository createEntity(EntityManager em) {
        ImageRepository imageRepository = new ImageRepository()
            .namaFile(DEFAULT_NAMA_FILE)
            .relasiId(DEFAULT_RELASI_ID)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .document(DEFAULT_DOCUMENT);
        return imageRepository;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageRepository createUpdatedEntity(EntityManager em) {
        ImageRepository imageRepository = new ImageRepository()
            .namaFile(UPDATED_NAMA_FILE)
            .relasiId(UPDATED_RELASI_ID)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .document(UPDATED_DOCUMENT);
        return imageRepository;
    }

    @BeforeEach
    public void initTest() {
        imageRepository = createEntity(em);
    }

    @Test
    @Transactional
    public void createImageRepository() throws Exception {
        int databaseSizeBeforeCreate = imageRepositoryRepository.findAll().size();

        // Create the ImageRepository
        ImageRepositoryDTO imageRepositoryDTO = imageRepositoryMapper.toDto(imageRepository);
        restImageRepositoryMockMvc.perform(post("/api/image-repositories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageRepositoryDTO)))
            .andExpect(status().isCreated());

        // Validate the ImageRepository in the database
        List<ImageRepository> imageRepositoryList = imageRepositoryRepository.findAll();
        assertThat(imageRepositoryList).hasSize(databaseSizeBeforeCreate + 1);
        ImageRepository testImageRepository = imageRepositoryList.get(imageRepositoryList.size() - 1);
        assertThat(testImageRepository.getNamaFile()).isEqualTo(DEFAULT_NAMA_FILE);
        assertThat(testImageRepository.getRelasiId()).isEqualTo(DEFAULT_RELASI_ID);
        assertThat(testImageRepository.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testImageRepository.getDocument()).isEqualTo(DEFAULT_DOCUMENT);
    }

    @Test
    @Transactional
    public void createImageRepositoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageRepositoryRepository.findAll().size();

        // Create the ImageRepository with an existing ID
        imageRepository.setId(1L);
        ImageRepositoryDTO imageRepositoryDTO = imageRepositoryMapper.toDto(imageRepository);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageRepositoryMockMvc.perform(post("/api/image-repositories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageRepositoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImageRepository in the database
        List<ImageRepository> imageRepositoryList = imageRepositoryRepository.findAll();
        assertThat(imageRepositoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllImageRepositories() throws Exception {
        // Initialize the database
        imageRepositoryRepository.saveAndFlush(imageRepository);

        // Get all the imageRepositoryList
        restImageRepositoryMockMvc.perform(get("/api/image-repositories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageRepository.getId().intValue())))
            .andExpect(jsonPath("$.[*].namaFile").value(hasItem(DEFAULT_NAMA_FILE)))
            .andExpect(jsonPath("$.[*].relasiId").value(hasItem(DEFAULT_RELASI_ID.intValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].document").value(hasItem(DEFAULT_DOCUMENT.toString())));
    }
    
    @Test
    @Transactional
    public void getImageRepository() throws Exception {
        // Initialize the database
        imageRepositoryRepository.saveAndFlush(imageRepository);

        // Get the imageRepository
        restImageRepositoryMockMvc.perform(get("/api/image-repositories/{id}", imageRepository.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(imageRepository.getId().intValue()))
            .andExpect(jsonPath("$.namaFile").value(DEFAULT_NAMA_FILE))
            .andExpect(jsonPath("$.relasiId").value(DEFAULT_RELASI_ID.intValue()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.document").value(DEFAULT_DOCUMENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingImageRepository() throws Exception {
        // Get the imageRepository
        restImageRepositoryMockMvc.perform(get("/api/image-repositories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImageRepository() throws Exception {
        // Initialize the database
        imageRepositoryRepository.saveAndFlush(imageRepository);

        int databaseSizeBeforeUpdate = imageRepositoryRepository.findAll().size();

        // Update the imageRepository
        ImageRepository updatedImageRepository = imageRepositoryRepository.findById(imageRepository.getId()).get();
        // Disconnect from session so that the updates on updatedImageRepository are not directly saved in db
        em.detach(updatedImageRepository);
        updatedImageRepository
            .namaFile(UPDATED_NAMA_FILE)
            .relasiId(UPDATED_RELASI_ID)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .document(UPDATED_DOCUMENT);
        ImageRepositoryDTO imageRepositoryDTO = imageRepositoryMapper.toDto(updatedImageRepository);

        restImageRepositoryMockMvc.perform(put("/api/image-repositories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageRepositoryDTO)))
            .andExpect(status().isOk());

        // Validate the ImageRepository in the database
        List<ImageRepository> imageRepositoryList = imageRepositoryRepository.findAll();
        assertThat(imageRepositoryList).hasSize(databaseSizeBeforeUpdate);
        ImageRepository testImageRepository = imageRepositoryList.get(imageRepositoryList.size() - 1);
        assertThat(testImageRepository.getNamaFile()).isEqualTo(UPDATED_NAMA_FILE);
        assertThat(testImageRepository.getRelasiId()).isEqualTo(UPDATED_RELASI_ID);
        assertThat(testImageRepository.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testImageRepository.getDocument()).isEqualTo(UPDATED_DOCUMENT);
    }

    @Test
    @Transactional
    public void updateNonExistingImageRepository() throws Exception {
        int databaseSizeBeforeUpdate = imageRepositoryRepository.findAll().size();

        // Create the ImageRepository
        ImageRepositoryDTO imageRepositoryDTO = imageRepositoryMapper.toDto(imageRepository);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageRepositoryMockMvc.perform(put("/api/image-repositories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageRepositoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImageRepository in the database
        List<ImageRepository> imageRepositoryList = imageRepositoryRepository.findAll();
        assertThat(imageRepositoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteImageRepository() throws Exception {
        // Initialize the database
        imageRepositoryRepository.saveAndFlush(imageRepository);

        int databaseSizeBeforeDelete = imageRepositoryRepository.findAll().size();

        // Delete the imageRepository
        restImageRepositoryMockMvc.perform(delete("/api/image-repositories/{id}", imageRepository.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ImageRepository> imageRepositoryList = imageRepositoryRepository.findAll();
        assertThat(imageRepositoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
