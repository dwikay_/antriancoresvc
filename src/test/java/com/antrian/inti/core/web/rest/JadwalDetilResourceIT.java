package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.JadwalDetil;
import com.antrian.inti.core.repository.JadwalDetilRepository;
import com.antrian.inti.core.service.JadwalDetilService;
import com.antrian.inti.core.service.dto.JadwalDetilDTO;
import com.antrian.inti.core.service.mapper.JadwalDetilMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JadwalDetilResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class JadwalDetilResourceIT {

    private static final Long DEFAULT_ID_MASTER = 1L;
    private static final Long UPDATED_ID_MASTER = 2L;

    private static final String DEFAULT_JAM_AWAL = "AAAAAAAAAA";
    private static final String UPDATED_JAM_AWAL = "BBBBBBBBBB";

    private static final String DEFAULT_JAM_AKHIR = "AAAAAAAAAA";
    private static final String UPDATED_JAM_AKHIR = "BBBBBBBBBB";

    private static final String DEFAULT_KLOTER = "AAAAAAAAAA";
    private static final String UPDATED_KLOTER = "BBBBBBBBBB";

    private static final String DEFAULT_JUMLAH_KUOTA = "AAAAAAAAAA";
    private static final String UPDATED_JUMLAH_KUOTA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_VIRTUAL = false;
    private static final Boolean UPDATED_VIRTUAL = true;

    @Autowired
    private JadwalDetilRepository jadwalDetilRepository;

    @Autowired
    private JadwalDetilMapper jadwalDetilMapper;

    @Autowired
    private JadwalDetilService jadwalDetilService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restJadwalDetilMockMvc;

    private JadwalDetil jadwalDetil;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JadwalDetilResource jadwalDetilResource = new JadwalDetilResource(jadwalDetilService);
        this.restJadwalDetilMockMvc = MockMvcBuilders.standaloneSetup(jadwalDetilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JadwalDetil createEntity(EntityManager em) {
        JadwalDetil jadwalDetil = new JadwalDetil()
            .idMaster(DEFAULT_ID_MASTER)
            .jamAwal(DEFAULT_JAM_AWAL)
            .jamAkhir(DEFAULT_JAM_AKHIR)
            .kloter(DEFAULT_KLOTER)
            .jumlahKuota(DEFAULT_JUMLAH_KUOTA)
            .virtual(DEFAULT_VIRTUAL);
        return jadwalDetil;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JadwalDetil createUpdatedEntity(EntityManager em) {
        JadwalDetil jadwalDetil = new JadwalDetil()
            .idMaster(UPDATED_ID_MASTER)
            .jamAwal(UPDATED_JAM_AWAL)
            .jamAkhir(UPDATED_JAM_AKHIR)
            .kloter(UPDATED_KLOTER)
            .jumlahKuota(UPDATED_JUMLAH_KUOTA)
            .virtual(UPDATED_VIRTUAL);
        return jadwalDetil;
    }

    @BeforeEach
    public void initTest() {
        jadwalDetil = createEntity(em);
    }

    @Test
    @Transactional
    public void createJadwalDetil() throws Exception {
        int databaseSizeBeforeCreate = jadwalDetilRepository.findAll().size();

        // Create the JadwalDetil
        JadwalDetilDTO jadwalDetilDTO = jadwalDetilMapper.toDto(jadwalDetil);
        restJadwalDetilMockMvc.perform(post("/api/jadwal-detils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalDetilDTO)))
            .andExpect(status().isCreated());

        // Validate the JadwalDetil in the database
        List<JadwalDetil> jadwalDetilList = jadwalDetilRepository.findAll();
        assertThat(jadwalDetilList).hasSize(databaseSizeBeforeCreate + 1);
        JadwalDetil testJadwalDetil = jadwalDetilList.get(jadwalDetilList.size() - 1);
        assertThat(testJadwalDetil.getIdMaster()).isEqualTo(DEFAULT_ID_MASTER);
        assertThat(testJadwalDetil.getJamAwal()).isEqualTo(DEFAULT_JAM_AWAL);
        assertThat(testJadwalDetil.getJamAkhir()).isEqualTo(DEFAULT_JAM_AKHIR);
        assertThat(testJadwalDetil.getKloter()).isEqualTo(DEFAULT_KLOTER);
        assertThat(testJadwalDetil.getJumlahKuota()).isEqualTo(DEFAULT_JUMLAH_KUOTA);
        assertThat(testJadwalDetil.isVirtual()).isEqualTo(DEFAULT_VIRTUAL);
    }

    @Test
    @Transactional
    public void createJadwalDetilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jadwalDetilRepository.findAll().size();

        // Create the JadwalDetil with an existing ID
        jadwalDetil.setId(1L);
        JadwalDetilDTO jadwalDetilDTO = jadwalDetilMapper.toDto(jadwalDetil);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJadwalDetilMockMvc.perform(post("/api/jadwal-detils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalDetilDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JadwalDetil in the database
        List<JadwalDetil> jadwalDetilList = jadwalDetilRepository.findAll();
        assertThat(jadwalDetilList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllJadwalDetils() throws Exception {
        // Initialize the database
        jadwalDetilRepository.saveAndFlush(jadwalDetil);

        // Get all the jadwalDetilList
        restJadwalDetilMockMvc.perform(get("/api/jadwal-detils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jadwalDetil.getId().intValue())))
            .andExpect(jsonPath("$.[*].idMaster").value(hasItem(DEFAULT_ID_MASTER.intValue())))
            .andExpect(jsonPath("$.[*].jamAwal").value(hasItem(DEFAULT_JAM_AWAL)))
            .andExpect(jsonPath("$.[*].jamAkhir").value(hasItem(DEFAULT_JAM_AKHIR)))
            .andExpect(jsonPath("$.[*].kloter").value(hasItem(DEFAULT_KLOTER)))
            .andExpect(jsonPath("$.[*].jumlahKuota").value(hasItem(DEFAULT_JUMLAH_KUOTA)))
            .andExpect(jsonPath("$.[*].virtual").value(hasItem(DEFAULT_VIRTUAL.booleanValue())));
    }

    @Test
    @Transactional
    public void getJadwalDetil() throws Exception {
        // Initialize the database
        jadwalDetilRepository.saveAndFlush(jadwalDetil);

        // Get the jadwalDetil
        restJadwalDetilMockMvc.perform(get("/api/jadwal-detils/{id}", jadwalDetil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(jadwalDetil.getId().intValue()))
            .andExpect(jsonPath("$.idMaster").value(DEFAULT_ID_MASTER.intValue()))
            .andExpect(jsonPath("$.jamAwal").value(DEFAULT_JAM_AWAL))
            .andExpect(jsonPath("$.jamAkhir").value(DEFAULT_JAM_AKHIR))
            .andExpect(jsonPath("$.kloter").value(DEFAULT_KLOTER))
            .andExpect(jsonPath("$.jumlahKuota").value(DEFAULT_JUMLAH_KUOTA))
            .andExpect(jsonPath("$.virtual").value(DEFAULT_VIRTUAL.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingJadwalDetil() throws Exception {
        // Get the jadwalDetil
        restJadwalDetilMockMvc.perform(get("/api/jadwal-detils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJadwalDetil() throws Exception {
        // Initialize the database
        jadwalDetilRepository.saveAndFlush(jadwalDetil);

        int databaseSizeBeforeUpdate = jadwalDetilRepository.findAll().size();

        // Update the jadwalDetil
        JadwalDetil updatedJadwalDetil = jadwalDetilRepository.findById(jadwalDetil.getId()).get();
        // Disconnect from session so that the updates on updatedJadwalDetil are not directly saved in db
        em.detach(updatedJadwalDetil);
        updatedJadwalDetil
            .idMaster(UPDATED_ID_MASTER)
            .jamAwal(UPDATED_JAM_AWAL)
            .jamAkhir(UPDATED_JAM_AKHIR)
            .kloter(UPDATED_KLOTER)
            .jumlahKuota(UPDATED_JUMLAH_KUOTA)
            .virtual(UPDATED_VIRTUAL);
        JadwalDetilDTO jadwalDetilDTO = jadwalDetilMapper.toDto(updatedJadwalDetil);

        restJadwalDetilMockMvc.perform(put("/api/jadwal-detils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalDetilDTO)))
            .andExpect(status().isOk());

        // Validate the JadwalDetil in the database
        List<JadwalDetil> jadwalDetilList = jadwalDetilRepository.findAll();
        assertThat(jadwalDetilList).hasSize(databaseSizeBeforeUpdate);
        JadwalDetil testJadwalDetil = jadwalDetilList.get(jadwalDetilList.size() - 1);
        assertThat(testJadwalDetil.getIdMaster()).isEqualTo(UPDATED_ID_MASTER);
        assertThat(testJadwalDetil.getJamAwal()).isEqualTo(UPDATED_JAM_AWAL);
        assertThat(testJadwalDetil.getJamAkhir()).isEqualTo(UPDATED_JAM_AKHIR);
        assertThat(testJadwalDetil.getKloter()).isEqualTo(UPDATED_KLOTER);
        assertThat(testJadwalDetil.getJumlahKuota()).isEqualTo(UPDATED_JUMLAH_KUOTA);
        assertThat(testJadwalDetil.isVirtual()).isEqualTo(UPDATED_VIRTUAL);
    }

    @Test
    @Transactional
    public void updateNonExistingJadwalDetil() throws Exception {
        int databaseSizeBeforeUpdate = jadwalDetilRepository.findAll().size();

        // Create the JadwalDetil
        JadwalDetilDTO jadwalDetilDTO = jadwalDetilMapper.toDto(jadwalDetil);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJadwalDetilMockMvc.perform(put("/api/jadwal-detils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalDetilDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JadwalDetil in the database
        List<JadwalDetil> jadwalDetilList = jadwalDetilRepository.findAll();
        assertThat(jadwalDetilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJadwalDetil() throws Exception {
        // Initialize the database
        jadwalDetilRepository.saveAndFlush(jadwalDetil);

        int databaseSizeBeforeDelete = jadwalDetilRepository.findAll().size();

        // Delete the jadwalDetil
        restJadwalDetilMockMvc.perform(delete("/api/jadwal-detils/{id}", jadwalDetil.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<JadwalDetil> jadwalDetilList = jadwalDetilRepository.findAll();
        assertThat(jadwalDetilList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
