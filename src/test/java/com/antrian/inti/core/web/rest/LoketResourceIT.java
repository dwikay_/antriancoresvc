package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Loket;
import com.antrian.inti.core.repository.LoketRepository;
import com.antrian.inti.core.service.LoketService;
import com.antrian.inti.core.service.dto.LoketDTO;
import com.antrian.inti.core.service.mapper.LoketMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LoketResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class LoketResourceIT {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAA";
    private static final String UPDATED_STATUS = "BBB";

    private static final String DEFAULT_DESKRIPSI = "AAAAAAAAAA";
    private static final String UPDATED_DESKRIPSI = "BBBBBBBBBB";

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final String DEFAULT_IP_CONTROLLER = "AAAAAAAAAA";
    private static final String UPDATED_IP_CONTROLLER = "BBBBBBBBBB";

    @Autowired
    private LoketRepository loketRepository;

    @Autowired
    private LoketMapper loketMapper;

    @Autowired
    private LoketService loketService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLoketMockMvc;

    private Loket loket;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LoketResource loketResource = new LoketResource(loketService);
        this.restLoketMockMvc = MockMvcBuilders.standaloneSetup(loketResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Loket createEntity(EntityManager em) {
        Loket loket = new Loket()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .status(DEFAULT_STATUS)
            .deskripsi(DEFAULT_DESKRIPSI)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .ipController(DEFAULT_IP_CONTROLLER);
        return loket;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Loket createUpdatedEntity(EntityManager em) {
        Loket loket = new Loket()
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .status(UPDATED_STATUS)
            .deskripsi(UPDATED_DESKRIPSI)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .ipController(UPDATED_IP_CONTROLLER);
        return loket;
    }

    @BeforeEach
    public void initTest() {
        loket = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoket() throws Exception {
        int databaseSizeBeforeCreate = loketRepository.findAll().size();

        // Create the Loket
        LoketDTO loketDTO = loketMapper.toDto(loket);
        restLoketMockMvc.perform(post("/api/lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loketDTO)))
            .andExpect(status().isCreated());

        // Validate the Loket in the database
        List<Loket> loketList = loketRepository.findAll();
        assertThat(loketList).hasSize(databaseSizeBeforeCreate + 1);
        Loket testLoket = loketList.get(loketList.size() - 1);
        assertThat(testLoket.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testLoket.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testLoket.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testLoket.getDeskripsi()).isEqualTo(DEFAULT_DESKRIPSI);
        assertThat(testLoket.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testLoket.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testLoket.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testLoket.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testLoket.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testLoket.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        assertThat(testLoket.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testLoket.getIpController()).isEqualTo(DEFAULT_IP_CONTROLLER);
    }

    @Test
    @Transactional
    public void createLoketWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loketRepository.findAll().size();

        // Create the Loket with an existing ID
        loket.setId(1L);
        LoketDTO loketDTO = loketMapper.toDto(loket);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoketMockMvc.perform(post("/api/lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loketDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Loket in the database
        List<Loket> loketList = loketRepository.findAll();
        assertThat(loketList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLokets() throws Exception {
        // Initialize the database
        loketRepository.saveAndFlush(loket);

        // Get all the loketList
        restLoketMockMvc.perform(get("/api/lokets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loket.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].deskripsi").value(hasItem(DEFAULT_DESKRIPSI)))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].ipController").value(hasItem(DEFAULT_IP_CONTROLLER)));
    }

    @Test
    @Transactional
    public void getLoket() throws Exception {
        // Initialize the database
        loketRepository.saveAndFlush(loket);

        // Get the loket
        restLoketMockMvc.perform(get("/api/lokets/{id}", loket.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(loket.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.deskripsi").value(DEFAULT_DESKRIPSI))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.ipController").value(DEFAULT_IP_CONTROLLER));
    }

    @Test
    @Transactional
    public void getNonExistingLoket() throws Exception {
        // Get the loket
        restLoketMockMvc.perform(get("/api/lokets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoket() throws Exception {
        // Initialize the database
        loketRepository.saveAndFlush(loket);

        int databaseSizeBeforeUpdate = loketRepository.findAll().size();

        // Update the loket
        Loket updatedLoket = loketRepository.findById(loket.getId()).get();
        // Disconnect from session so that the updates on updatedLoket are not directly saved in db
        em.detach(updatedLoket);
        updatedLoket
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .status(UPDATED_STATUS)
            .deskripsi(UPDATED_DESKRIPSI)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .ipController(UPDATED_IP_CONTROLLER);
        LoketDTO loketDTO = loketMapper.toDto(updatedLoket);

        restLoketMockMvc.perform(put("/api/lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loketDTO)))
            .andExpect(status().isOk());

        // Validate the Loket in the database
        List<Loket> loketList = loketRepository.findAll();
        assertThat(loketList).hasSize(databaseSizeBeforeUpdate);
        Loket testLoket = loketList.get(loketList.size() - 1);
        assertThat(testLoket.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testLoket.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testLoket.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testLoket.getDeskripsi()).isEqualTo(UPDATED_DESKRIPSI);
        assertThat(testLoket.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testLoket.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testLoket.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testLoket.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testLoket.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testLoket.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
        assertThat(testLoket.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testLoket.getIpController()).isEqualTo(UPDATED_IP_CONTROLLER);
    }

    @Test
    @Transactional
    public void updateNonExistingLoket() throws Exception {
        int databaseSizeBeforeUpdate = loketRepository.findAll().size();

        // Create the Loket
        LoketDTO loketDTO = loketMapper.toDto(loket);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLoketMockMvc.perform(put("/api/lokets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(loketDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Loket in the database
        List<Loket> loketList = loketRepository.findAll();
        assertThat(loketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLoket() throws Exception {
        // Initialize the database
        loketRepository.saveAndFlush(loket);

        int databaseSizeBeforeDelete = loketRepository.findAll().size();

        // Delete the loket
        restLoketMockMvc.perform(delete("/api/lokets/{id}", loket.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Loket> loketList = loketRepository.findAll();
        assertThat(loketList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
