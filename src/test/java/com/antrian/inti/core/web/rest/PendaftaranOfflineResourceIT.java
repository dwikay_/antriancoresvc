package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.PendaftaranOffline;
import com.antrian.inti.core.repository.PendaftaranOfflineRepository;
import com.antrian.inti.core.service.PendaftaranOfflineService;
import com.antrian.inti.core.service.dto.PendaftaranOfflineDTO;
import com.antrian.inti.core.service.mapper.PendaftaranOfflineMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PendaftaranOfflineResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class PendaftaranOfflineResourceIT {

    private static final String DEFAULT_NAMA_PELAKU = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PELAKU = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_NIK = "AAAAAAAAAA";
    private static final String UPDATED_NIK = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PERUSAHAAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PERUSAHAAN = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPON = "AAAAAAAAAA";
    private static final String UPDATED_TELEPON = "BBBBBBBBBB";

    private static final String DEFAULT_PERTANYAAN = "AAAAAAAAAA";
    private static final String UPDATED_PERTANYAAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_URUT = "AAAAAAAAAA";
    private static final String UPDATED_NO_URUT = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final String DEFAULT_TOKEN_VALIDASI = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN_VALIDASI = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PendaftaranOfflineRepository pendaftaranOfflineRepository;

    @Autowired
    private PendaftaranOfflineMapper pendaftaranOfflineMapper;

    @Autowired
    private PendaftaranOfflineService pendaftaranOfflineService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPendaftaranOfflineMockMvc;

    private PendaftaranOffline pendaftaranOffline;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PendaftaranOfflineResource pendaftaranOfflineResource = new PendaftaranOfflineResource(pendaftaranOfflineService);
        this.restPendaftaranOfflineMockMvc = MockMvcBuilders.standaloneSetup(pendaftaranOfflineResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PendaftaranOffline createEntity(EntityManager em) {
        PendaftaranOffline pendaftaranOffline = new PendaftaranOffline()
            .namaPelaku(DEFAULT_NAMA_PELAKU)
            .email(DEFAULT_EMAIL)
            .nik(DEFAULT_NIK)
            .namaPerusahaan(DEFAULT_NAMA_PERUSAHAAN)
            .telepon(DEFAULT_TELEPON)
            .pertanyaan(DEFAULT_PERTANYAAN)
            .noUrut(DEFAULT_NO_URUT)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .tokenValidasi(DEFAULT_TOKEN_VALIDASI)
            .tanggal(DEFAULT_TANGGAL);
        return pendaftaranOffline;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PendaftaranOffline createUpdatedEntity(EntityManager em) {
        PendaftaranOffline pendaftaranOffline = new PendaftaranOffline()
            .namaPelaku(UPDATED_NAMA_PELAKU)
            .email(UPDATED_EMAIL)
            .nik(UPDATED_NIK)
            .namaPerusahaan(UPDATED_NAMA_PERUSAHAAN)
            .telepon(UPDATED_TELEPON)
            .pertanyaan(UPDATED_PERTANYAAN)
            .noUrut(UPDATED_NO_URUT)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .tokenValidasi(UPDATED_TOKEN_VALIDASI)
            .tanggal(UPDATED_TANGGAL);
        return pendaftaranOffline;
    }

    @BeforeEach
    public void initTest() {
        pendaftaranOffline = createEntity(em);
    }

    @Test
    @Transactional
    public void createPendaftaranOffline() throws Exception {
        int databaseSizeBeforeCreate = pendaftaranOfflineRepository.findAll().size();

        // Create the PendaftaranOffline
        PendaftaranOfflineDTO pendaftaranOfflineDTO = pendaftaranOfflineMapper.toDto(pendaftaranOffline);
        restPendaftaranOfflineMockMvc.perform(post("/api/pendaftaran-offlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranOfflineDTO)))
            .andExpect(status().isCreated());

        // Validate the PendaftaranOffline in the database
        List<PendaftaranOffline> pendaftaranOfflineList = pendaftaranOfflineRepository.findAll();
        assertThat(pendaftaranOfflineList).hasSize(databaseSizeBeforeCreate + 1);
        PendaftaranOffline testPendaftaranOffline = pendaftaranOfflineList.get(pendaftaranOfflineList.size() - 1);
        assertThat(testPendaftaranOffline.getNamaPelaku()).isEqualTo(DEFAULT_NAMA_PELAKU);
        assertThat(testPendaftaranOffline.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPendaftaranOffline.getNik()).isEqualTo(DEFAULT_NIK);
        assertThat(testPendaftaranOffline.getNamaPerusahaan()).isEqualTo(DEFAULT_NAMA_PERUSAHAAN);
        assertThat(testPendaftaranOffline.getTelepon()).isEqualTo(DEFAULT_TELEPON);
        assertThat(testPendaftaranOffline.getPertanyaan()).isEqualTo(DEFAULT_PERTANYAAN);
        assertThat(testPendaftaranOffline.getNoUrut()).isEqualTo(DEFAULT_NO_URUT);
        assertThat(testPendaftaranOffline.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testPendaftaranOffline.getTokenValidasi()).isEqualTo(DEFAULT_TOKEN_VALIDASI);
        assertThat(testPendaftaranOffline.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
    }

    @Test
    @Transactional
    public void createPendaftaranOfflineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pendaftaranOfflineRepository.findAll().size();

        // Create the PendaftaranOffline with an existing ID
        pendaftaranOffline.setId(1L);
        PendaftaranOfflineDTO pendaftaranOfflineDTO = pendaftaranOfflineMapper.toDto(pendaftaranOffline);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPendaftaranOfflineMockMvc.perform(post("/api/pendaftaran-offlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranOfflineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PendaftaranOffline in the database
        List<PendaftaranOffline> pendaftaranOfflineList = pendaftaranOfflineRepository.findAll();
        assertThat(pendaftaranOfflineList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPendaftaranOfflines() throws Exception {
        // Initialize the database
        pendaftaranOfflineRepository.saveAndFlush(pendaftaranOffline);

        // Get all the pendaftaranOfflineList
        restPendaftaranOfflineMockMvc.perform(get("/api/pendaftaran-offlines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pendaftaranOffline.getId().intValue())))
            .andExpect(jsonPath("$.[*].namaPelaku").value(hasItem(DEFAULT_NAMA_PELAKU)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].nik").value(hasItem(DEFAULT_NIK)))
            .andExpect(jsonPath("$.[*].namaPerusahaan").value(hasItem(DEFAULT_NAMA_PERUSAHAAN)))
            .andExpect(jsonPath("$.[*].telepon").value(hasItem(DEFAULT_TELEPON)))
            .andExpect(jsonPath("$.[*].pertanyaan").value(hasItem(DEFAULT_PERTANYAAN.toString())))
            .andExpect(jsonPath("$.[*].noUrut").value(hasItem(DEFAULT_NO_URUT)))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].tokenValidasi").value(hasItem(DEFAULT_TOKEN_VALIDASI)))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())));
    }

    @Test
    @Transactional
    public void getPendaftaranOffline() throws Exception {
        // Initialize the database
        pendaftaranOfflineRepository.saveAndFlush(pendaftaranOffline);

        // Get the pendaftaranOffline
        restPendaftaranOfflineMockMvc.perform(get("/api/pendaftaran-offlines/{id}", pendaftaranOffline.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pendaftaranOffline.getId().intValue()))
            .andExpect(jsonPath("$.namaPelaku").value(DEFAULT_NAMA_PELAKU))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.nik").value(DEFAULT_NIK))
            .andExpect(jsonPath("$.namaPerusahaan").value(DEFAULT_NAMA_PERUSAHAAN))
            .andExpect(jsonPath("$.telepon").value(DEFAULT_TELEPON))
            .andExpect(jsonPath("$.pertanyaan").value(DEFAULT_PERTANYAAN.toString()))
            .andExpect(jsonPath("$.noUrut").value(DEFAULT_NO_URUT))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.tokenValidasi").value(DEFAULT_TOKEN_VALIDASI))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPendaftaranOffline() throws Exception {
        // Get the pendaftaranOffline
        restPendaftaranOfflineMockMvc.perform(get("/api/pendaftaran-offlines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePendaftaranOffline() throws Exception {
        // Initialize the database
        pendaftaranOfflineRepository.saveAndFlush(pendaftaranOffline);

        int databaseSizeBeforeUpdate = pendaftaranOfflineRepository.findAll().size();

        // Update the pendaftaranOffline
        PendaftaranOffline updatedPendaftaranOffline = pendaftaranOfflineRepository.findById(pendaftaranOffline.getId()).get();
        // Disconnect from session so that the updates on updatedPendaftaranOffline are not directly saved in db
        em.detach(updatedPendaftaranOffline);
        updatedPendaftaranOffline
            .namaPelaku(UPDATED_NAMA_PELAKU)
            .email(UPDATED_EMAIL)
            .nik(UPDATED_NIK)
            .namaPerusahaan(UPDATED_NAMA_PERUSAHAAN)
            .telepon(UPDATED_TELEPON)
            .pertanyaan(UPDATED_PERTANYAAN)
            .noUrut(UPDATED_NO_URUT)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .tokenValidasi(UPDATED_TOKEN_VALIDASI)
            .tanggal(UPDATED_TANGGAL);
        PendaftaranOfflineDTO pendaftaranOfflineDTO = pendaftaranOfflineMapper.toDto(updatedPendaftaranOffline);

        restPendaftaranOfflineMockMvc.perform(put("/api/pendaftaran-offlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranOfflineDTO)))
            .andExpect(status().isOk());

        // Validate the PendaftaranOffline in the database
        List<PendaftaranOffline> pendaftaranOfflineList = pendaftaranOfflineRepository.findAll();
        assertThat(pendaftaranOfflineList).hasSize(databaseSizeBeforeUpdate);
        PendaftaranOffline testPendaftaranOffline = pendaftaranOfflineList.get(pendaftaranOfflineList.size() - 1);
        assertThat(testPendaftaranOffline.getNamaPelaku()).isEqualTo(UPDATED_NAMA_PELAKU);
        assertThat(testPendaftaranOffline.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPendaftaranOffline.getNik()).isEqualTo(UPDATED_NIK);
        assertThat(testPendaftaranOffline.getNamaPerusahaan()).isEqualTo(UPDATED_NAMA_PERUSAHAAN);
        assertThat(testPendaftaranOffline.getTelepon()).isEqualTo(UPDATED_TELEPON);
        assertThat(testPendaftaranOffline.getPertanyaan()).isEqualTo(UPDATED_PERTANYAAN);
        assertThat(testPendaftaranOffline.getNoUrut()).isEqualTo(UPDATED_NO_URUT);
        assertThat(testPendaftaranOffline.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testPendaftaranOffline.getTokenValidasi()).isEqualTo(UPDATED_TOKEN_VALIDASI);
        assertThat(testPendaftaranOffline.getTanggal()).isEqualTo(UPDATED_TANGGAL);
    }

    @Test
    @Transactional
    public void updateNonExistingPendaftaranOffline() throws Exception {
        int databaseSizeBeforeUpdate = pendaftaranOfflineRepository.findAll().size();

        // Create the PendaftaranOffline
        PendaftaranOfflineDTO pendaftaranOfflineDTO = pendaftaranOfflineMapper.toDto(pendaftaranOffline);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPendaftaranOfflineMockMvc.perform(put("/api/pendaftaran-offlines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranOfflineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PendaftaranOffline in the database
        List<PendaftaranOffline> pendaftaranOfflineList = pendaftaranOfflineRepository.findAll();
        assertThat(pendaftaranOfflineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePendaftaranOffline() throws Exception {
        // Initialize the database
        pendaftaranOfflineRepository.saveAndFlush(pendaftaranOffline);

        int databaseSizeBeforeDelete = pendaftaranOfflineRepository.findAll().size();

        // Delete the pendaftaranOffline
        restPendaftaranOfflineMockMvc.perform(delete("/api/pendaftaran-offlines/{id}", pendaftaranOffline.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PendaftaranOffline> pendaftaranOfflineList = pendaftaranOfflineRepository.findAll();
        assertThat(pendaftaranOfflineList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
