package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Layanan;
import com.antrian.inti.core.repository.LayananRepository;
import com.antrian.inti.core.service.LayananService;
import com.antrian.inti.core.service.dto.LayananDTO;
import com.antrian.inti.core.service.mapper.LayananMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LayananResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class LayananResourceIT {

    private static final String DEFAULT_KODE_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_KODE_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_LAYANAN = "BBBBBBBBBB";

    private static final byte[] DEFAULT_GAMBAR = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_GAMBAR = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_GAMBAR_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_GAMBAR_CONTENT_TYPE = "image/png";

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final Long DEFAULT_ID_LANTAI = 1L;
    private static final Long UPDATED_ID_LANTAI = 2L;

    @Autowired
    private LayananRepository layananRepository;

    @Autowired
    private LayananMapper layananMapper;

    @Autowired
    private LayananService layananService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLayananMockMvc;

    private Layanan layanan;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LayananResource layananResource = new LayananResource(layananService);
        this.restLayananMockMvc = MockMvcBuilders.standaloneSetup(layananResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Layanan createEntity(EntityManager em) {
        Layanan layanan = new Layanan()
            .kodeLayanan(DEFAULT_KODE_LAYANAN)
            .namaLayanan(DEFAULT_NAMA_LAYANAN)
            .gambar(DEFAULT_GAMBAR)
            .gambarContentType(DEFAULT_GAMBAR_CONTENT_TYPE)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .idLantai(DEFAULT_ID_LANTAI);
        return layanan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Layanan createUpdatedEntity(EntityManager em) {
        Layanan layanan = new Layanan()
            .kodeLayanan(UPDATED_KODE_LAYANAN)
            .namaLayanan(UPDATED_NAMA_LAYANAN)
            .gambar(UPDATED_GAMBAR)
            .gambarContentType(UPDATED_GAMBAR_CONTENT_TYPE)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .idLantai(UPDATED_ID_LANTAI);
        return layanan;
    }

    @BeforeEach
    public void initTest() {
        layanan = createEntity(em);
    }

    @Test
    @Transactional
    public void createLayanan() throws Exception {
        int databaseSizeBeforeCreate = layananRepository.findAll().size();

        // Create the Layanan
        LayananDTO layananDTO = layananMapper.toDto(layanan);
        restLayananMockMvc.perform(post("/api/layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(layananDTO)))
            .andExpect(status().isCreated());

        // Validate the Layanan in the database
        List<Layanan> layananList = layananRepository.findAll();
        assertThat(layananList).hasSize(databaseSizeBeforeCreate + 1);
        Layanan testLayanan = layananList.get(layananList.size() - 1);
        assertThat(testLayanan.getKodeLayanan()).isEqualTo(DEFAULT_KODE_LAYANAN);
        assertThat(testLayanan.getNamaLayanan()).isEqualTo(DEFAULT_NAMA_LAYANAN);
        assertThat(testLayanan.getGambar()).isEqualTo(DEFAULT_GAMBAR);
        assertThat(testLayanan.getGambarContentType()).isEqualTo(DEFAULT_GAMBAR_CONTENT_TYPE);
        assertThat(testLayanan.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testLayanan.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testLayanan.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testLayanan.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testLayanan.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        assertThat(testLayanan.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testLayanan.getIdLantai()).isEqualTo(DEFAULT_ID_LANTAI);
    }

    @Test
    @Transactional
    public void createLayananWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = layananRepository.findAll().size();

        // Create the Layanan with an existing ID
        layanan.setId(1L);
        LayananDTO layananDTO = layananMapper.toDto(layanan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLayananMockMvc.perform(post("/api/layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(layananDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Layanan in the database
        List<Layanan> layananList = layananRepository.findAll();
        assertThat(layananList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLayanans() throws Exception {
        // Initialize the database
        layananRepository.saveAndFlush(layanan);

        // Get all the layananList
        restLayananMockMvc.perform(get("/api/layanans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(layanan.getId().intValue())))
            .andExpect(jsonPath("$.[*].kodeLayanan").value(hasItem(DEFAULT_KODE_LAYANAN)))
            .andExpect(jsonPath("$.[*].namaLayanan").value(hasItem(DEFAULT_NAMA_LAYANAN)))
            .andExpect(jsonPath("$.[*].gambarContentType").value(hasItem(DEFAULT_GAMBAR_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].gambar").value(hasItem(Base64Utils.encodeToString(DEFAULT_GAMBAR))))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].idLantai").value(hasItem(DEFAULT_ID_LANTAI.intValue())));
    }

    @Test
    @Transactional
    public void getLayanan() throws Exception {
        // Initialize the database
        layananRepository.saveAndFlush(layanan);

        // Get the layanan
        restLayananMockMvc.perform(get("/api/layanans/{id}", layanan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(layanan.getId().intValue()))
            .andExpect(jsonPath("$.kodeLayanan").value(DEFAULT_KODE_LAYANAN))
            .andExpect(jsonPath("$.namaLayanan").value(DEFAULT_NAMA_LAYANAN))
            .andExpect(jsonPath("$.gambarContentType").value(DEFAULT_GAMBAR_CONTENT_TYPE))
            .andExpect(jsonPath("$.gambar").value(Base64Utils.encodeToString(DEFAULT_GAMBAR)))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.idLantai").value(DEFAULT_ID_LANTAI.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLayanan() throws Exception {
        // Get the layanan
        restLayananMockMvc.perform(get("/api/layanans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLayanan() throws Exception {
        // Initialize the database
        layananRepository.saveAndFlush(layanan);

        int databaseSizeBeforeUpdate = layananRepository.findAll().size();

        // Update the layanan
        Layanan updatedLayanan = layananRepository.findById(layanan.getId()).get();
        // Disconnect from session so that the updates on updatedLayanan are not directly saved in db
        em.detach(updatedLayanan);
        updatedLayanan
            .kodeLayanan(UPDATED_KODE_LAYANAN)
            .namaLayanan(UPDATED_NAMA_LAYANAN)
            .gambar(UPDATED_GAMBAR)
            .gambarContentType(UPDATED_GAMBAR_CONTENT_TYPE)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .idLantai(UPDATED_ID_LANTAI);
        LayananDTO layananDTO = layananMapper.toDto(updatedLayanan);

        restLayananMockMvc.perform(put("/api/layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(layananDTO)))
            .andExpect(status().isOk());

        // Validate the Layanan in the database
        List<Layanan> layananList = layananRepository.findAll();
        assertThat(layananList).hasSize(databaseSizeBeforeUpdate);
        Layanan testLayanan = layananList.get(layananList.size() - 1);
        assertThat(testLayanan.getKodeLayanan()).isEqualTo(UPDATED_KODE_LAYANAN);
        assertThat(testLayanan.getNamaLayanan()).isEqualTo(UPDATED_NAMA_LAYANAN);
        assertThat(testLayanan.getGambar()).isEqualTo(UPDATED_GAMBAR);
        assertThat(testLayanan.getGambarContentType()).isEqualTo(UPDATED_GAMBAR_CONTENT_TYPE);
        assertThat(testLayanan.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testLayanan.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testLayanan.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testLayanan.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testLayanan.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
        assertThat(testLayanan.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testLayanan.getIdLantai()).isEqualTo(UPDATED_ID_LANTAI);
    }

    @Test
    @Transactional
    public void updateNonExistingLayanan() throws Exception {
        int databaseSizeBeforeUpdate = layananRepository.findAll().size();

        // Create the Layanan
        LayananDTO layananDTO = layananMapper.toDto(layanan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLayananMockMvc.perform(put("/api/layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(layananDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Layanan in the database
        List<Layanan> layananList = layananRepository.findAll();
        assertThat(layananList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLayanan() throws Exception {
        // Initialize the database
        layananRepository.saveAndFlush(layanan);

        int databaseSizeBeforeDelete = layananRepository.findAll().size();

        // Delete the layanan
        restLayananMockMvc.perform(delete("/api/layanans/{id}", layanan.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Layanan> layananList = layananRepository.findAll();
        assertThat(layananList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
