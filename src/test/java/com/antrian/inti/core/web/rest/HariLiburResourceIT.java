package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.HariLibur;
import com.antrian.inti.core.repository.HariLiburRepository;
import com.antrian.inti.core.service.HariLiburService;
import com.antrian.inti.core.service.dto.HariLiburDTO;
import com.antrian.inti.core.service.mapper.HariLiburMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link HariLiburResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class HariLiburResourceIT {

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_KETERANGAN = "AAAAAAAAAA";
    private static final String UPDATED_KETERANGAN = "BBBBBBBBBB";

    @Autowired
    private HariLiburRepository hariLiburRepository;

    @Autowired
    private HariLiburMapper hariLiburMapper;

    @Autowired
    private HariLiburService hariLiburService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHariLiburMockMvc;

    private HariLibur hariLibur;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HariLiburResource hariLiburResource = new HariLiburResource(hariLiburService);
        this.restHariLiburMockMvc = MockMvcBuilders.standaloneSetup(hariLiburResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HariLibur createEntity(EntityManager em) {
        HariLibur hariLibur = new HariLibur()
            .tanggal(DEFAULT_TANGGAL)
            .keterangan(DEFAULT_KETERANGAN);
        return hariLibur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HariLibur createUpdatedEntity(EntityManager em) {
        HariLibur hariLibur = new HariLibur()
            .tanggal(UPDATED_TANGGAL)
            .keterangan(UPDATED_KETERANGAN);
        return hariLibur;
    }

    @BeforeEach
    public void initTest() {
        hariLibur = createEntity(em);
    }

    @Test
    @Transactional
    public void createHariLibur() throws Exception {
        int databaseSizeBeforeCreate = hariLiburRepository.findAll().size();

        // Create the HariLibur
        HariLiburDTO hariLiburDTO = hariLiburMapper.toDto(hariLibur);
        restHariLiburMockMvc.perform(post("/api/hari-liburs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hariLiburDTO)))
            .andExpect(status().isCreated());

        // Validate the HariLibur in the database
        List<HariLibur> hariLiburList = hariLiburRepository.findAll();
        assertThat(hariLiburList).hasSize(databaseSizeBeforeCreate + 1);
        HariLibur testHariLibur = hariLiburList.get(hariLiburList.size() - 1);
        assertThat(testHariLibur.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testHariLibur.getKeterangan()).isEqualTo(DEFAULT_KETERANGAN);
    }

    @Test
    @Transactional
    public void createHariLiburWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = hariLiburRepository.findAll().size();

        // Create the HariLibur with an existing ID
        hariLibur.setId(1L);
        HariLiburDTO hariLiburDTO = hariLiburMapper.toDto(hariLibur);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHariLiburMockMvc.perform(post("/api/hari-liburs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hariLiburDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HariLibur in the database
        List<HariLibur> hariLiburList = hariLiburRepository.findAll();
        assertThat(hariLiburList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllHariLiburs() throws Exception {
        // Initialize the database
        hariLiburRepository.saveAndFlush(hariLibur);

        // Get all the hariLiburList
        restHariLiburMockMvc.perform(get("/api/hari-liburs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hariLibur.getId().intValue())))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].keterangan").value(hasItem(DEFAULT_KETERANGAN)));
    }

    @Test
    @Transactional
    public void getHariLibur() throws Exception {
        // Initialize the database
        hariLiburRepository.saveAndFlush(hariLibur);

        // Get the hariLibur
        restHariLiburMockMvc.perform(get("/api/hari-liburs/{id}", hariLibur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hariLibur.getId().intValue()))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.keterangan").value(DEFAULT_KETERANGAN));
    }

    @Test
    @Transactional
    public void getNonExistingHariLibur() throws Exception {
        // Get the hariLibur
        restHariLiburMockMvc.perform(get("/api/hari-liburs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHariLibur() throws Exception {
        // Initialize the database
        hariLiburRepository.saveAndFlush(hariLibur);

        int databaseSizeBeforeUpdate = hariLiburRepository.findAll().size();

        // Update the hariLibur
        HariLibur updatedHariLibur = hariLiburRepository.findById(hariLibur.getId()).get();
        // Disconnect from session so that the updates on updatedHariLibur are not directly saved in db
        em.detach(updatedHariLibur);
        updatedHariLibur
            .tanggal(UPDATED_TANGGAL)
            .keterangan(UPDATED_KETERANGAN);
        HariLiburDTO hariLiburDTO = hariLiburMapper.toDto(updatedHariLibur);

        restHariLiburMockMvc.perform(put("/api/hari-liburs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hariLiburDTO)))
            .andExpect(status().isOk());

        // Validate the HariLibur in the database
        List<HariLibur> hariLiburList = hariLiburRepository.findAll();
        assertThat(hariLiburList).hasSize(databaseSizeBeforeUpdate);
        HariLibur testHariLibur = hariLiburList.get(hariLiburList.size() - 1);
        assertThat(testHariLibur.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testHariLibur.getKeterangan()).isEqualTo(UPDATED_KETERANGAN);
    }

    @Test
    @Transactional
    public void updateNonExistingHariLibur() throws Exception {
        int databaseSizeBeforeUpdate = hariLiburRepository.findAll().size();

        // Create the HariLibur
        HariLiburDTO hariLiburDTO = hariLiburMapper.toDto(hariLibur);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHariLiburMockMvc.perform(put("/api/hari-liburs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hariLiburDTO)))
            .andExpect(status().isBadRequest());

        // Validate the HariLibur in the database
        List<HariLibur> hariLiburList = hariLiburRepository.findAll();
        assertThat(hariLiburList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHariLibur() throws Exception {
        // Initialize the database
        hariLiburRepository.saveAndFlush(hariLibur);

        int databaseSizeBeforeDelete = hariLiburRepository.findAll().size();

        // Delete the hariLibur
        restHariLiburMockMvc.perform(delete("/api/hari-liburs/{id}", hariLibur.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HariLibur> hariLiburList = hariLiburRepository.findAll();
        assertThat(hariLiburList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
