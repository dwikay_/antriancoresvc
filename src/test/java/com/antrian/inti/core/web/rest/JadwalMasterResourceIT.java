package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.JadwalMaster;
import com.antrian.inti.core.repository.JadwalMasterRepository;
import com.antrian.inti.core.service.JadwalMasterService;
import com.antrian.inti.core.service.dto.JadwalMasterDTO;
import com.antrian.inti.core.service.mapper.JadwalMasterMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JadwalMasterResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class JadwalMasterResourceIT {

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    @Autowired
    private JadwalMasterRepository jadwalMasterRepository;

    @Autowired
    private JadwalMasterMapper jadwalMasterMapper;

    @Autowired
    private JadwalMasterService jadwalMasterService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restJadwalMasterMockMvc;

    private JadwalMaster jadwalMaster;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JadwalMasterResource jadwalMasterResource = new JadwalMasterResource(jadwalMasterService);
        this.restJadwalMasterMockMvc = MockMvcBuilders.standaloneSetup(jadwalMasterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JadwalMaster createEntity(EntityManager em) {
        JadwalMaster jadwalMaster = new JadwalMaster()
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN);
        return jadwalMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JadwalMaster createUpdatedEntity(EntityManager em) {
        JadwalMaster jadwalMaster = new JadwalMaster()
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN);
        return jadwalMaster;
    }

    @BeforeEach
    public void initTest() {
        jadwalMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createJadwalMaster() throws Exception {
        int databaseSizeBeforeCreate = jadwalMasterRepository.findAll().size();

        // Create the JadwalMaster
        JadwalMasterDTO jadwalMasterDTO = jadwalMasterMapper.toDto(jadwalMaster);
        restJadwalMasterMockMvc.perform(post("/api/jadwal-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the JadwalMaster in the database
        List<JadwalMaster> jadwalMasterList = jadwalMasterRepository.findAll();
        assertThat(jadwalMasterList).hasSize(databaseSizeBeforeCreate + 1);
        JadwalMaster testJadwalMaster = jadwalMasterList.get(jadwalMasterList.size() - 1);
        assertThat(testJadwalMaster.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void createJadwalMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jadwalMasterRepository.findAll().size();

        // Create the JadwalMaster with an existing ID
        jadwalMaster.setId(1L);
        JadwalMasterDTO jadwalMasterDTO = jadwalMasterMapper.toDto(jadwalMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJadwalMasterMockMvc.perform(post("/api/jadwal-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JadwalMaster in the database
        List<JadwalMaster> jadwalMasterList = jadwalMasterRepository.findAll();
        assertThat(jadwalMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllJadwalMasters() throws Exception {
        // Initialize the database
        jadwalMasterRepository.saveAndFlush(jadwalMaster);

        // Get all the jadwalMasterList
        restJadwalMasterMockMvc.perform(get("/api/jadwal-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jadwalMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())));
    }

    @Test
    @Transactional
    public void getJadwalMaster() throws Exception {
        // Initialize the database
        jadwalMasterRepository.saveAndFlush(jadwalMaster);

        // Get the jadwalMaster
        restJadwalMasterMockMvc.perform(get("/api/jadwal-masters/{id}", jadwalMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(jadwalMaster.getId().intValue()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingJadwalMaster() throws Exception {
        // Get the jadwalMaster
        restJadwalMasterMockMvc.perform(get("/api/jadwal-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJadwalMaster() throws Exception {
        // Initialize the database
        jadwalMasterRepository.saveAndFlush(jadwalMaster);

        int databaseSizeBeforeUpdate = jadwalMasterRepository.findAll().size();

        // Update the jadwalMaster
        JadwalMaster updatedJadwalMaster = jadwalMasterRepository.findById(jadwalMaster.getId()).get();
        // Disconnect from session so that the updates on updatedJadwalMaster are not directly saved in db
        em.detach(updatedJadwalMaster);
        updatedJadwalMaster
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN);
        JadwalMasterDTO jadwalMasterDTO = jadwalMasterMapper.toDto(updatedJadwalMaster);

        restJadwalMasterMockMvc.perform(put("/api/jadwal-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalMasterDTO)))
            .andExpect(status().isOk());

        // Validate the JadwalMaster in the database
        List<JadwalMaster> jadwalMasterList = jadwalMasterRepository.findAll();
        assertThat(jadwalMasterList).hasSize(databaseSizeBeforeUpdate);
        JadwalMaster testJadwalMaster = jadwalMasterList.get(jadwalMasterList.size() - 1);
        assertThat(testJadwalMaster.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void updateNonExistingJadwalMaster() throws Exception {
        int databaseSizeBeforeUpdate = jadwalMasterRepository.findAll().size();

        // Create the JadwalMaster
        JadwalMasterDTO jadwalMasterDTO = jadwalMasterMapper.toDto(jadwalMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJadwalMasterMockMvc.perform(put("/api/jadwal-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jadwalMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JadwalMaster in the database
        List<JadwalMaster> jadwalMasterList = jadwalMasterRepository.findAll();
        assertThat(jadwalMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJadwalMaster() throws Exception {
        // Initialize the database
        jadwalMasterRepository.saveAndFlush(jadwalMaster);

        int databaseSizeBeforeDelete = jadwalMasterRepository.findAll().size();

        // Delete the jadwalMaster
        restJadwalMasterMockMvc.perform(delete("/api/jadwal-masters/{id}", jadwalMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<JadwalMaster> jadwalMasterList = jadwalMasterRepository.findAll();
        assertThat(jadwalMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
