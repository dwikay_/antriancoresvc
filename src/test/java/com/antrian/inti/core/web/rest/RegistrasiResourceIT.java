package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Registrasi;
import com.antrian.inti.core.repository.RegistrasiRepository;
import com.antrian.inti.core.service.RegistrasiService;
import com.antrian.inti.core.service.dto.RegistrasiDTO;
import com.antrian.inti.core.service.mapper.RegistrasiMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RegistrasiResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class RegistrasiResourceIT {

    private static final String DEFAULT_ID_DAFTAR = "AAAAAAAAAA";
    private static final String UPDATED_ID_DAFTAR = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_HUBUNGAN_PERUSAHAAN = "AAAAAAAAAA";
    private static final String UPDATED_HUBUNGAN_PERUSAHAAN = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PERUSAHAAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PERUSAHAAN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_PENGAMBILAN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_PENGAMBILAN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TANGGAL_WAKTU_PENGAMBILAN = "AAAAAAAAAA";
    private static final String UPDATED_TANGGAL_WAKTU_PENGAMBILAN = "BBBBBBBBBB";

    private static final String DEFAULT_PERTANYAAN = "AAAAAAAAAA";
    private static final String UPDATED_PERTANYAAN = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPON = "AAAAAAAAAA";
    private static final String UPDATED_TELEPON = "BBBBBBBBBB";

    private static final String DEFAULT_KODE_VERIFIKASI = "AAAAAAAAAA";
    private static final String UPDATED_KODE_VERIFIKASI = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_LENGKAP = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_LENGKAP = "BBBBBBBBBB";

    private static final Boolean DEFAULT_VERIFIKASI_EMAIL = false;
    private static final Boolean UPDATED_VERIFIKASI_EMAIL = true;

    private static final String DEFAULT_NIK = "AAAAAAAAAA";
    private static final String UPDATED_NIK = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PERUSAHAAN_PENDAFTAR = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PERUSAHAAN_PENDAFTAR = "BBBBBBBBBB";

    private static final String DEFAULT_KODE_KONFIRMASI = "AAAAAAAAAA";
    private static final String UPDATED_KODE_KONFIRMASI = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_KLOTER = 1L;
    private static final Long UPDATED_ID_KLOTER = 2L;

    private static final Boolean DEFAULT_PENGAMBILAN_STRUK = false;
    private static final Boolean UPDATED_PENGAMBILAN_STRUK = true;

    private static final String DEFAULT_JAM_KLOTER = "AAAAAAAAAA";
    private static final String UPDATED_JAM_KLOTER = "BBBBBBBBBB";

    private static final String DEFAULT_WAKTU_JAM_KLOTER = "AAAAAAAAAA";
    private static final String UPDATED_WAKTU_JAM_KLOTER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final LocalDate DEFAULT_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final byte[] DEFAULT_KTP = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_KTP = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_KTP_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_KTP_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_JAWABAN = "AAAAAAAAAA";
    private static final String UPDATED_JAWABAN = "BBBBBBBBBB";

    private static final String DEFAULT_TIPE_KTP = "AAAAAAAAAA";
    private static final String UPDATED_TIPE_KTP = "BBBBBBBBBB";

    private static final String DEFAULT_PATH_FILES = "AAAAAAAAAA";
    private static final String UPDATED_PATH_FILES = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS_TIKET = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_TIKET = "BBBBBBBBBB";

    private static final String DEFAULT_JAM_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_JAM_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_TIKET = "AAAAAAAAAA";
    private static final String UPDATED_NO_TIKET = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TYPE_VIRTUAL = false;
    private static final Boolean UPDATED_TYPE_VIRTUAL = true;

    private static final Boolean DEFAULT_AGENT = false;
    private static final Boolean UPDATED_AGENT = true;

    @Autowired
    private RegistrasiRepository registrasiRepository;

    @Autowired
    private RegistrasiMapper registrasiMapper;

    @Autowired
    private RegistrasiService registrasiService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegistrasiMockMvc;

    private Registrasi registrasi;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegistrasiResource registrasiResource = new RegistrasiResource(registrasiService);
        this.restRegistrasiMockMvc = MockMvcBuilders.standaloneSetup(registrasiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Registrasi createEntity(EntityManager em) {
        Registrasi registrasi = new Registrasi()
            .idDaftar(DEFAULT_ID_DAFTAR)
            .email(DEFAULT_EMAIL)
            .hubunganPerusahaan(DEFAULT_HUBUNGAN_PERUSAHAAN)
            .namaPerusahaan(DEFAULT_NAMA_PERUSAHAAN)
            .tanggalPengambilan(DEFAULT_TANGGAL_PENGAMBILAN)
            .tanggalWaktuPengambilan(DEFAULT_TANGGAL_WAKTU_PENGAMBILAN)
            .pertanyaan(DEFAULT_PERTANYAAN)
            .telepon(DEFAULT_TELEPON)
            .kodeVerifikasi(DEFAULT_KODE_VERIFIKASI)
            .namaLengkap(DEFAULT_NAMA_LENGKAP)
            .verifikasiEmail(DEFAULT_VERIFIKASI_EMAIL)
            .nik(DEFAULT_NIK)
            .namaPerusahaanPendaftar(DEFAULT_NAMA_PERUSAHAAN_PENDAFTAR)
            .kodeKonfirmasi(DEFAULT_KODE_KONFIRMASI)
            .idKloter(DEFAULT_ID_KLOTER)
            .pengambilanStruk(DEFAULT_PENGAMBILAN_STRUK)
            .jamKloter(DEFAULT_JAM_KLOTER)
            .waktuJamKloter(DEFAULT_WAKTU_JAM_KLOTER)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .modificationDate(DEFAULT_MODIFICATION_DATE)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .ktp(DEFAULT_KTP)
            .ktpContentType(DEFAULT_KTP_CONTENT_TYPE)
            .jawaban(DEFAULT_JAWABAN)
            .tipe_ktp(DEFAULT_TIPE_KTP)
            .path_files(DEFAULT_PATH_FILES)
            .statusTiket(DEFAULT_STATUS_TIKET)
            .jamLayanan(DEFAULT_JAM_LAYANAN)
            .noTiket(DEFAULT_NO_TIKET)
            .typeVirtual(DEFAULT_TYPE_VIRTUAL)
            .agent(DEFAULT_AGENT);
        return registrasi;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Registrasi createUpdatedEntity(EntityManager em) {
        Registrasi registrasi = new Registrasi()
            .idDaftar(UPDATED_ID_DAFTAR)
            .email(UPDATED_EMAIL)
            .hubunganPerusahaan(UPDATED_HUBUNGAN_PERUSAHAAN)
            .namaPerusahaan(UPDATED_NAMA_PERUSAHAAN)
            .tanggalPengambilan(UPDATED_TANGGAL_PENGAMBILAN)
            .tanggalWaktuPengambilan(UPDATED_TANGGAL_WAKTU_PENGAMBILAN)
            .pertanyaan(UPDATED_PERTANYAAN)
            .telepon(UPDATED_TELEPON)
            .kodeVerifikasi(UPDATED_KODE_VERIFIKASI)
            .namaLengkap(UPDATED_NAMA_LENGKAP)
            .verifikasiEmail(UPDATED_VERIFIKASI_EMAIL)
            .nik(UPDATED_NIK)
            .namaPerusahaanPendaftar(UPDATED_NAMA_PERUSAHAAN_PENDAFTAR)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .idKloter(UPDATED_ID_KLOTER)
            .pengambilanStruk(UPDATED_PENGAMBILAN_STRUK)
            .jamKloter(UPDATED_JAM_KLOTER)
            .waktuJamKloter(UPDATED_WAKTU_JAM_KLOTER)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .modificationDate(UPDATED_MODIFICATION_DATE)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .ktp(UPDATED_KTP)
            .ktpContentType(UPDATED_KTP_CONTENT_TYPE)
            .jawaban(UPDATED_JAWABAN)
            .tipe_ktp(UPDATED_TIPE_KTP)
            .path_files(UPDATED_PATH_FILES)
            .statusTiket(UPDATED_STATUS_TIKET)
            .jamLayanan(UPDATED_JAM_LAYANAN)
            .noTiket(UPDATED_NO_TIKET)
            .typeVirtual(UPDATED_TYPE_VIRTUAL)
            .agent(UPDATED_AGENT);
        return registrasi;
    }

    @BeforeEach
    public void initTest() {
        registrasi = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegistrasi() throws Exception {
        int databaseSizeBeforeCreate = registrasiRepository.findAll().size();

        // Create the Registrasi
        RegistrasiDTO registrasiDTO = registrasiMapper.toDto(registrasi);
        restRegistrasiMockMvc.perform(post("/api/registrasis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(registrasiDTO)))
            .andExpect(status().isCreated());

        // Validate the Registrasi in the database
        List<Registrasi> registrasiList = registrasiRepository.findAll();
        assertThat(registrasiList).hasSize(databaseSizeBeforeCreate + 1);
        Registrasi testRegistrasi = registrasiList.get(registrasiList.size() - 1);
        assertThat(testRegistrasi.getIdDaftar()).isEqualTo(DEFAULT_ID_DAFTAR);
        assertThat(testRegistrasi.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testRegistrasi.getHubunganPerusahaan()).isEqualTo(DEFAULT_HUBUNGAN_PERUSAHAAN);
        assertThat(testRegistrasi.getNamaPerusahaan()).isEqualTo(DEFAULT_NAMA_PERUSAHAAN);
        assertThat(testRegistrasi.getTanggalPengambilan()).isEqualTo(DEFAULT_TANGGAL_PENGAMBILAN);
        assertThat(testRegistrasi.getTanggalWaktuPengambilan()).isEqualTo(DEFAULT_TANGGAL_WAKTU_PENGAMBILAN);
        assertThat(testRegistrasi.getPertanyaan()).isEqualTo(DEFAULT_PERTANYAAN);
        assertThat(testRegistrasi.getTelepon()).isEqualTo(DEFAULT_TELEPON);
        assertThat(testRegistrasi.getKodeVerifikasi()).isEqualTo(DEFAULT_KODE_VERIFIKASI);
        assertThat(testRegistrasi.getNamaLengkap()).isEqualTo(DEFAULT_NAMA_LENGKAP);
        assertThat(testRegistrasi.isVerifikasiEmail()).isEqualTo(DEFAULT_VERIFIKASI_EMAIL);
        assertThat(testRegistrasi.getNik()).isEqualTo(DEFAULT_NIK);
        assertThat(testRegistrasi.getNamaPerusahaanPendaftar()).isEqualTo(DEFAULT_NAMA_PERUSAHAAN_PENDAFTAR);
        assertThat(testRegistrasi.getKodeKonfirmasi()).isEqualTo(DEFAULT_KODE_KONFIRMASI);
        assertThat(testRegistrasi.getIdKloter()).isEqualTo(DEFAULT_ID_KLOTER);
        assertThat(testRegistrasi.isPengambilanStruk()).isEqualTo(DEFAULT_PENGAMBILAN_STRUK);
        assertThat(testRegistrasi.getJamKloter()).isEqualTo(DEFAULT_JAM_KLOTER);
        assertThat(testRegistrasi.getWaktuJamKloter()).isEqualTo(DEFAULT_WAKTU_JAM_KLOTER);
        assertThat(testRegistrasi.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testRegistrasi.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testRegistrasi.getModificationDate()).isEqualTo(DEFAULT_MODIFICATION_DATE);
        assertThat(testRegistrasi.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testRegistrasi.getKtp()).isEqualTo(DEFAULT_KTP);
        assertThat(testRegistrasi.getKtpContentType()).isEqualTo(DEFAULT_KTP_CONTENT_TYPE);
        assertThat(testRegistrasi.getJawaban()).isEqualTo(DEFAULT_JAWABAN);
        assertThat(testRegistrasi.getTipe_ktp()).isEqualTo(DEFAULT_TIPE_KTP);
        assertThat(testRegistrasi.getPath_files()).isEqualTo(DEFAULT_PATH_FILES);
        assertThat(testRegistrasi.getStatusTiket()).isEqualTo(DEFAULT_STATUS_TIKET);
        assertThat(testRegistrasi.getJamLayanan()).isEqualTo(DEFAULT_JAM_LAYANAN);
        assertThat(testRegistrasi.getNoTiket()).isEqualTo(DEFAULT_NO_TIKET);
        assertThat(testRegistrasi.isTypeVirtual()).isEqualTo(DEFAULT_TYPE_VIRTUAL);
        assertThat(testRegistrasi.isAgent()).isEqualTo(DEFAULT_AGENT);
    }

    @Test
    @Transactional
    public void createRegistrasiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = registrasiRepository.findAll().size();

        // Create the Registrasi with an existing ID
        registrasi.setId(1L);
        RegistrasiDTO registrasiDTO = registrasiMapper.toDto(registrasi);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegistrasiMockMvc.perform(post("/api/registrasis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(registrasiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Registrasi in the database
        List<Registrasi> registrasiList = registrasiRepository.findAll();
        assertThat(registrasiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRegistrasis() throws Exception {
        // Initialize the database
        registrasiRepository.saveAndFlush(registrasi);

        // Get all the registrasiList
        restRegistrasiMockMvc.perform(get("/api/registrasis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(registrasi.getId().intValue())))
            .andExpect(jsonPath("$.[*].idDaftar").value(hasItem(DEFAULT_ID_DAFTAR)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].hubunganPerusahaan").value(hasItem(DEFAULT_HUBUNGAN_PERUSAHAAN)))
            .andExpect(jsonPath("$.[*].namaPerusahaan").value(hasItem(DEFAULT_NAMA_PERUSAHAAN)))
            .andExpect(jsonPath("$.[*].tanggalPengambilan").value(hasItem(DEFAULT_TANGGAL_PENGAMBILAN.toString())))
            .andExpect(jsonPath("$.[*].tanggalWaktuPengambilan").value(hasItem(DEFAULT_TANGGAL_WAKTU_PENGAMBILAN)))
            .andExpect(jsonPath("$.[*].pertanyaan").value(hasItem(DEFAULT_PERTANYAAN.toString())))
            .andExpect(jsonPath("$.[*].telepon").value(hasItem(DEFAULT_TELEPON)))
            .andExpect(jsonPath("$.[*].kodeVerifikasi").value(hasItem(DEFAULT_KODE_VERIFIKASI.toString())))
            .andExpect(jsonPath("$.[*].namaLengkap").value(hasItem(DEFAULT_NAMA_LENGKAP)))
            .andExpect(jsonPath("$.[*].verifikasiEmail").value(hasItem(DEFAULT_VERIFIKASI_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].nik").value(hasItem(DEFAULT_NIK)))
            .andExpect(jsonPath("$.[*].namaPerusahaanPendaftar").value(hasItem(DEFAULT_NAMA_PERUSAHAAN_PENDAFTAR)))
            .andExpect(jsonPath("$.[*].kodeKonfirmasi").value(hasItem(DEFAULT_KODE_KONFIRMASI)))
            .andExpect(jsonPath("$.[*].idKloter").value(hasItem(DEFAULT_ID_KLOTER.intValue())))
            .andExpect(jsonPath("$.[*].pengambilanStruk").value(hasItem(DEFAULT_PENGAMBILAN_STRUK.booleanValue())))
            .andExpect(jsonPath("$.[*].jamKloter").value(hasItem(DEFAULT_JAM_KLOTER)))
            .andExpect(jsonPath("$.[*].waktuJamKloter").value(hasItem(DEFAULT_WAKTU_JAM_KLOTER)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].modificationDate").value(hasItem(DEFAULT_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].ktpContentType").value(hasItem(DEFAULT_KTP_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].ktp").value(hasItem(Base64Utils.encodeToString(DEFAULT_KTP))))
            .andExpect(jsonPath("$.[*].jawaban").value(hasItem(DEFAULT_JAWABAN.toString())))
            .andExpect(jsonPath("$.[*].tipe_ktp").value(hasItem(DEFAULT_TIPE_KTP)))
            .andExpect(jsonPath("$.[*].path_files").value(hasItem(DEFAULT_PATH_FILES)))
            .andExpect(jsonPath("$.[*].statusTiket").value(hasItem(DEFAULT_STATUS_TIKET)))
            .andExpect(jsonPath("$.[*].jamLayanan").value(hasItem(DEFAULT_JAM_LAYANAN)))
            .andExpect(jsonPath("$.[*].noTiket").value(hasItem(DEFAULT_NO_TIKET)))
            .andExpect(jsonPath("$.[*].typeVirtual").value(hasItem(DEFAULT_TYPE_VIRTUAL.booleanValue())))
            .andExpect(jsonPath("$.[*].agent").value(hasItem(DEFAULT_AGENT.booleanValue())));
    }

    @Test
    @Transactional
    public void getRegistrasi() throws Exception {
        // Initialize the database
        registrasiRepository.saveAndFlush(registrasi);

        // Get the registrasi
        restRegistrasiMockMvc.perform(get("/api/registrasis/{id}", registrasi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(registrasi.getId().intValue()))
            .andExpect(jsonPath("$.idDaftar").value(DEFAULT_ID_DAFTAR))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.hubunganPerusahaan").value(DEFAULT_HUBUNGAN_PERUSAHAAN))
            .andExpect(jsonPath("$.namaPerusahaan").value(DEFAULT_NAMA_PERUSAHAAN))
            .andExpect(jsonPath("$.tanggalPengambilan").value(DEFAULT_TANGGAL_PENGAMBILAN.toString()))
            .andExpect(jsonPath("$.tanggalWaktuPengambilan").value(DEFAULT_TANGGAL_WAKTU_PENGAMBILAN))
            .andExpect(jsonPath("$.pertanyaan").value(DEFAULT_PERTANYAAN.toString()))
            .andExpect(jsonPath("$.telepon").value(DEFAULT_TELEPON))
            .andExpect(jsonPath("$.kodeVerifikasi").value(DEFAULT_KODE_VERIFIKASI.toString()))
            .andExpect(jsonPath("$.namaLengkap").value(DEFAULT_NAMA_LENGKAP))
            .andExpect(jsonPath("$.verifikasiEmail").value(DEFAULT_VERIFIKASI_EMAIL.booleanValue()))
            .andExpect(jsonPath("$.nik").value(DEFAULT_NIK))
            .andExpect(jsonPath("$.namaPerusahaanPendaftar").value(DEFAULT_NAMA_PERUSAHAAN_PENDAFTAR))
            .andExpect(jsonPath("$.kodeKonfirmasi").value(DEFAULT_KODE_KONFIRMASI))
            .andExpect(jsonPath("$.idKloter").value(DEFAULT_ID_KLOTER.intValue()))
            .andExpect(jsonPath("$.pengambilanStruk").value(DEFAULT_PENGAMBILAN_STRUK.booleanValue()))
            .andExpect(jsonPath("$.jamKloter").value(DEFAULT_JAM_KLOTER))
            .andExpect(jsonPath("$.waktuJamKloter").value(DEFAULT_WAKTU_JAM_KLOTER))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.modificationDate").value(DEFAULT_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.ktpContentType").value(DEFAULT_KTP_CONTENT_TYPE))
            .andExpect(jsonPath("$.ktp").value(Base64Utils.encodeToString(DEFAULT_KTP)))
            .andExpect(jsonPath("$.jawaban").value(DEFAULT_JAWABAN.toString()))
            .andExpect(jsonPath("$.tipe_ktp").value(DEFAULT_TIPE_KTP))
            .andExpect(jsonPath("$.path_files").value(DEFAULT_PATH_FILES))
            .andExpect(jsonPath("$.statusTiket").value(DEFAULT_STATUS_TIKET))
            .andExpect(jsonPath("$.jamLayanan").value(DEFAULT_JAM_LAYANAN))
            .andExpect(jsonPath("$.noTiket").value(DEFAULT_NO_TIKET))
            .andExpect(jsonPath("$.typeVirtual").value(DEFAULT_TYPE_VIRTUAL.booleanValue()))
            .andExpect(jsonPath("$.agent").value(DEFAULT_AGENT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRegistrasi() throws Exception {
        // Get the registrasi
        restRegistrasiMockMvc.perform(get("/api/registrasis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegistrasi() throws Exception {
        // Initialize the database
        registrasiRepository.saveAndFlush(registrasi);

        int databaseSizeBeforeUpdate = registrasiRepository.findAll().size();

        // Update the registrasi
        Registrasi updatedRegistrasi = registrasiRepository.findById(registrasi.getId()).get();
        // Disconnect from session so that the updates on updatedRegistrasi are not directly saved in db
        em.detach(updatedRegistrasi);
        updatedRegistrasi
            .idDaftar(UPDATED_ID_DAFTAR)
            .email(UPDATED_EMAIL)
            .hubunganPerusahaan(UPDATED_HUBUNGAN_PERUSAHAAN)
            .namaPerusahaan(UPDATED_NAMA_PERUSAHAAN)
            .tanggalPengambilan(UPDATED_TANGGAL_PENGAMBILAN)
            .tanggalWaktuPengambilan(UPDATED_TANGGAL_WAKTU_PENGAMBILAN)
            .pertanyaan(UPDATED_PERTANYAAN)
            .telepon(UPDATED_TELEPON)
            .kodeVerifikasi(UPDATED_KODE_VERIFIKASI)
            .namaLengkap(UPDATED_NAMA_LENGKAP)
            .verifikasiEmail(UPDATED_VERIFIKASI_EMAIL)
            .nik(UPDATED_NIK)
            .namaPerusahaanPendaftar(UPDATED_NAMA_PERUSAHAAN_PENDAFTAR)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .idKloter(UPDATED_ID_KLOTER)
            .pengambilanStruk(UPDATED_PENGAMBILAN_STRUK)
            .jamKloter(UPDATED_JAM_KLOTER)
            .waktuJamKloter(UPDATED_WAKTU_JAM_KLOTER)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .modificationDate(UPDATED_MODIFICATION_DATE)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .ktp(UPDATED_KTP)
            .ktpContentType(UPDATED_KTP_CONTENT_TYPE)
            .jawaban(UPDATED_JAWABAN)
            .tipe_ktp(UPDATED_TIPE_KTP)
            .path_files(UPDATED_PATH_FILES)
            .statusTiket(UPDATED_STATUS_TIKET)
            .jamLayanan(UPDATED_JAM_LAYANAN)
            .noTiket(UPDATED_NO_TIKET)
            .typeVirtual(UPDATED_TYPE_VIRTUAL)
            .agent(UPDATED_AGENT);
        RegistrasiDTO registrasiDTO = registrasiMapper.toDto(updatedRegistrasi);

        restRegistrasiMockMvc.perform(put("/api/registrasis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(registrasiDTO)))
            .andExpect(status().isOk());

        // Validate the Registrasi in the database
        List<Registrasi> registrasiList = registrasiRepository.findAll();
        assertThat(registrasiList).hasSize(databaseSizeBeforeUpdate);
        Registrasi testRegistrasi = registrasiList.get(registrasiList.size() - 1);
        assertThat(testRegistrasi.getIdDaftar()).isEqualTo(UPDATED_ID_DAFTAR);
        assertThat(testRegistrasi.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testRegistrasi.getHubunganPerusahaan()).isEqualTo(UPDATED_HUBUNGAN_PERUSAHAAN);
        assertThat(testRegistrasi.getNamaPerusahaan()).isEqualTo(UPDATED_NAMA_PERUSAHAAN);
        assertThat(testRegistrasi.getTanggalPengambilan()).isEqualTo(UPDATED_TANGGAL_PENGAMBILAN);
        assertThat(testRegistrasi.getTanggalWaktuPengambilan()).isEqualTo(UPDATED_TANGGAL_WAKTU_PENGAMBILAN);
        assertThat(testRegistrasi.getPertanyaan()).isEqualTo(UPDATED_PERTANYAAN);
        assertThat(testRegistrasi.getTelepon()).isEqualTo(UPDATED_TELEPON);
        assertThat(testRegistrasi.getKodeVerifikasi()).isEqualTo(UPDATED_KODE_VERIFIKASI);
        assertThat(testRegistrasi.getNamaLengkap()).isEqualTo(UPDATED_NAMA_LENGKAP);
        assertThat(testRegistrasi.isVerifikasiEmail()).isEqualTo(UPDATED_VERIFIKASI_EMAIL);
        assertThat(testRegistrasi.getNik()).isEqualTo(UPDATED_NIK);
        assertThat(testRegistrasi.getNamaPerusahaanPendaftar()).isEqualTo(UPDATED_NAMA_PERUSAHAAN_PENDAFTAR);
        assertThat(testRegistrasi.getKodeKonfirmasi()).isEqualTo(UPDATED_KODE_KONFIRMASI);
        assertThat(testRegistrasi.getIdKloter()).isEqualTo(UPDATED_ID_KLOTER);
        assertThat(testRegistrasi.isPengambilanStruk()).isEqualTo(UPDATED_PENGAMBILAN_STRUK);
        assertThat(testRegistrasi.getJamKloter()).isEqualTo(UPDATED_JAM_KLOTER);
        assertThat(testRegistrasi.getWaktuJamKloter()).isEqualTo(UPDATED_WAKTU_JAM_KLOTER);
        assertThat(testRegistrasi.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testRegistrasi.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testRegistrasi.getModificationDate()).isEqualTo(UPDATED_MODIFICATION_DATE);
        assertThat(testRegistrasi.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testRegistrasi.getKtp()).isEqualTo(UPDATED_KTP);
        assertThat(testRegistrasi.getKtpContentType()).isEqualTo(UPDATED_KTP_CONTENT_TYPE);
        assertThat(testRegistrasi.getJawaban()).isEqualTo(UPDATED_JAWABAN);
        assertThat(testRegistrasi.getTipe_ktp()).isEqualTo(UPDATED_TIPE_KTP);
        assertThat(testRegistrasi.getPath_files()).isEqualTo(UPDATED_PATH_FILES);
        assertThat(testRegistrasi.getStatusTiket()).isEqualTo(UPDATED_STATUS_TIKET);
        assertThat(testRegistrasi.getJamLayanan()).isEqualTo(UPDATED_JAM_LAYANAN);
        assertThat(testRegistrasi.getNoTiket()).isEqualTo(UPDATED_NO_TIKET);
        assertThat(testRegistrasi.isTypeVirtual()).isEqualTo(UPDATED_TYPE_VIRTUAL);
        assertThat(testRegistrasi.isAgent()).isEqualTo(UPDATED_AGENT);
    }

    @Test
    @Transactional
    public void updateNonExistingRegistrasi() throws Exception {
        int databaseSizeBeforeUpdate = registrasiRepository.findAll().size();

        // Create the Registrasi
        RegistrasiDTO registrasiDTO = registrasiMapper.toDto(registrasi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegistrasiMockMvc.perform(put("/api/registrasis")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(registrasiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Registrasi in the database
        List<Registrasi> registrasiList = registrasiRepository.findAll();
        assertThat(registrasiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegistrasi() throws Exception {
        // Initialize the database
        registrasiRepository.saveAndFlush(registrasi);

        int databaseSizeBeforeDelete = registrasiRepository.findAll().size();

        // Delete the registrasi
        restRegistrasiMockMvc.perform(delete("/api/registrasis/{id}", registrasi.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Registrasi> registrasiList = registrasiRepository.findAll();
        assertThat(registrasiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
