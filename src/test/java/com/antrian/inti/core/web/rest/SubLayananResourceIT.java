package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.SubLayanan;
import com.antrian.inti.core.repository.SubLayananRepository;
import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.SubLayananDTO;
import com.antrian.inti.core.service.mapper.SubLayananMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SubLayananResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class SubLayananResourceIT {

    private static final Long DEFAULT_ID_LAYANAN = 1L;
    private static final Long UPDATED_ID_LAYANAN = 2L;

    private static final String DEFAULT_KODE_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_KODE_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_LAYANAN = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_LANTAI = 1L;
    private static final Long UPDATED_ID_LANTAI = 2L;

    private static final Long DEFAULT_KUOTA = 1L;
    private static final Long UPDATED_KUOTA = 2L;

    private static final String DEFAULT_JAM_AWAL = "AAAAAAAAAA";
    private static final String UPDATED_JAM_AWAL = "BBBBBBBBBB";

    private static final String DEFAULT_JAM_AKHIR = "AAAAAAAAAA";
    private static final String UPDATED_JAM_AKHIR = "BBBBBBBBBB";

    private static final Long DEFAULT_DURASI = 1L;
    private static final Long UPDATED_DURASI = 2L;

    private static final String DEFAULT_STATUS = "AAA";
    private static final String UPDATED_STATUS = "BBB";

    private static final String DEFAULT_PREFIX_SUARA = "AAAAA";
    private static final String UPDATED_PREFIX_SUARA = "BBBBB";

    private static final Boolean DEFAULT_LAYANAN_ONLINE = false;
    private static final Boolean UPDATED_LAYANAN_ONLINE = true;

    private static final Long DEFAULT_ID_KIOSK = 1L;
    private static final Long UPDATED_ID_KIOSK = 2L;

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_SK = "AAAAAAAAAA";
    private static final String UPDATED_SK = "BBBBBBBBBB";

    @Autowired
    private SubLayananRepository subLayananRepository;

    @Autowired
    private SubLayananMapper subLayananMapper;

    @Autowired
    private SubLayananService subLayananService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSubLayananMockMvc;

    private SubLayanan subLayanan;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubLayananResource subLayananResource = new SubLayananResource(subLayananService);
        this.restSubLayananMockMvc = MockMvcBuilders.standaloneSetup(subLayananResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubLayanan createEntity(EntityManager em) {
        SubLayanan subLayanan = new SubLayanan()
            .idLayanan(DEFAULT_ID_LAYANAN)
            .kodeLayanan(DEFAULT_KODE_LAYANAN)
            .namaLayanan(DEFAULT_NAMA_LAYANAN)
            .idLantai(DEFAULT_ID_LANTAI)
            .kuota(DEFAULT_KUOTA)
            .jamAwal(DEFAULT_JAM_AWAL)
            .jamAkhir(DEFAULT_JAM_AKHIR)
            .durasi(DEFAULT_DURASI)
            .status(DEFAULT_STATUS)
            .prefixSuara(DEFAULT_PREFIX_SUARA)
            .layananOnline(DEFAULT_LAYANAN_ONLINE)
            .idKiosk(DEFAULT_ID_KIOSK)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME)
            .sk(DEFAULT_SK);
        return subLayanan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubLayanan createUpdatedEntity(EntityManager em) {
        SubLayanan subLayanan = new SubLayanan()
            .idLayanan(UPDATED_ID_LAYANAN)
            .kodeLayanan(UPDATED_KODE_LAYANAN)
            .namaLayanan(UPDATED_NAMA_LAYANAN)
            .idLantai(UPDATED_ID_LANTAI)
            .kuota(UPDATED_KUOTA)
            .jamAwal(UPDATED_JAM_AWAL)
            .jamAkhir(UPDATED_JAM_AKHIR)
            .durasi(UPDATED_DURASI)
            .status(UPDATED_STATUS)
            .prefixSuara(UPDATED_PREFIX_SUARA)
            .layananOnline(UPDATED_LAYANAN_ONLINE)
            .idKiosk(UPDATED_ID_KIOSK)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .sk(UPDATED_SK);
        return subLayanan;
    }

    @BeforeEach
    public void initTest() {
        subLayanan = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubLayanan() throws Exception {
        int databaseSizeBeforeCreate = subLayananRepository.findAll().size();

        // Create the SubLayanan
        SubLayananDTO subLayananDTO = subLayananMapper.toDto(subLayanan);
        restSubLayananMockMvc.perform(post("/api/sub-layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subLayananDTO)))
            .andExpect(status().isCreated());

        // Validate the SubLayanan in the database
        List<SubLayanan> subLayananList = subLayananRepository.findAll();
        assertThat(subLayananList).hasSize(databaseSizeBeforeCreate + 1);
        SubLayanan testSubLayanan = subLayananList.get(subLayananList.size() - 1);
        assertThat(testSubLayanan.getIdLayanan()).isEqualTo(DEFAULT_ID_LAYANAN);
        assertThat(testSubLayanan.getKodeLayanan()).isEqualTo(DEFAULT_KODE_LAYANAN);
        assertThat(testSubLayanan.getNamaLayanan()).isEqualTo(DEFAULT_NAMA_LAYANAN);
        assertThat(testSubLayanan.getIdLantai()).isEqualTo(DEFAULT_ID_LANTAI);
        assertThat(testSubLayanan.getKuota()).isEqualTo(DEFAULT_KUOTA);
        assertThat(testSubLayanan.getJamAwal()).isEqualTo(DEFAULT_JAM_AWAL);
        assertThat(testSubLayanan.getJamAkhir()).isEqualTo(DEFAULT_JAM_AKHIR);
        assertThat(testSubLayanan.getDurasi()).isEqualTo(DEFAULT_DURASI);
        assertThat(testSubLayanan.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSubLayanan.getPrefixSuara()).isEqualTo(DEFAULT_PREFIX_SUARA);
        assertThat(testSubLayanan.isLayananOnline()).isEqualTo(DEFAULT_LAYANAN_ONLINE);
        assertThat(testSubLayanan.getIdKiosk()).isEqualTo(DEFAULT_ID_KIOSK);
        assertThat(testSubLayanan.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testSubLayanan.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testSubLayanan.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testSubLayanan.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testSubLayanan.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testSubLayanan.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        assertThat(testSubLayanan.getSk()).isEqualTo(DEFAULT_SK);
    }

    @Test
    @Transactional
    public void createSubLayananWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subLayananRepository.findAll().size();

        // Create the SubLayanan with an existing ID
        subLayanan.setId(1L);
        SubLayananDTO subLayananDTO = subLayananMapper.toDto(subLayanan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubLayananMockMvc.perform(post("/api/sub-layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subLayananDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubLayanan in the database
        List<SubLayanan> subLayananList = subLayananRepository.findAll();
        assertThat(subLayananList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSubLayanans() throws Exception {
        // Initialize the database
        subLayananRepository.saveAndFlush(subLayanan);

        // Get all the subLayananList
        restSubLayananMockMvc.perform(get("/api/sub-layanans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subLayanan.getId().intValue())))
            .andExpect(jsonPath("$.[*].idLayanan").value(hasItem(DEFAULT_ID_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].kodeLayanan").value(hasItem(DEFAULT_KODE_LAYANAN)))
            .andExpect(jsonPath("$.[*].namaLayanan").value(hasItem(DEFAULT_NAMA_LAYANAN)))
            .andExpect(jsonPath("$.[*].idLantai").value(hasItem(DEFAULT_ID_LANTAI.intValue())))
            .andExpect(jsonPath("$.[*].kuota").value(hasItem(DEFAULT_KUOTA.intValue())))
            .andExpect(jsonPath("$.[*].jamAwal").value(hasItem(DEFAULT_JAM_AWAL)))
            .andExpect(jsonPath("$.[*].jamAkhir").value(hasItem(DEFAULT_JAM_AKHIR)))
            .andExpect(jsonPath("$.[*].durasi").value(hasItem(DEFAULT_DURASI.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].prefixSuara").value(hasItem(DEFAULT_PREFIX_SUARA)))
            .andExpect(jsonPath("$.[*].layananOnline").value(hasItem(DEFAULT_LAYANAN_ONLINE.booleanValue())))
            .andExpect(jsonPath("$.[*].idKiosk").value(hasItem(DEFAULT_ID_KIOSK.intValue())))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))))
            .andExpect(jsonPath("$.[*].sk").value(hasItem(DEFAULT_SK.toString())));
    }

    @Test
    @Transactional
    public void getSubLayanan() throws Exception {
        // Initialize the database
        subLayananRepository.saveAndFlush(subLayanan);

        // Get the subLayanan
        restSubLayananMockMvc.perform(get("/api/sub-layanans/{id}", subLayanan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subLayanan.getId().intValue()))
            .andExpect(jsonPath("$.idLayanan").value(DEFAULT_ID_LAYANAN.intValue()))
            .andExpect(jsonPath("$.kodeLayanan").value(DEFAULT_KODE_LAYANAN))
            .andExpect(jsonPath("$.namaLayanan").value(DEFAULT_NAMA_LAYANAN))
            .andExpect(jsonPath("$.idLantai").value(DEFAULT_ID_LANTAI.intValue()))
            .andExpect(jsonPath("$.kuota").value(DEFAULT_KUOTA.intValue()))
            .andExpect(jsonPath("$.jamAwal").value(DEFAULT_JAM_AWAL))
            .andExpect(jsonPath("$.jamAkhir").value(DEFAULT_JAM_AKHIR))
            .andExpect(jsonPath("$.durasi").value(DEFAULT_DURASI.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.prefixSuara").value(DEFAULT_PREFIX_SUARA))
            .andExpect(jsonPath("$.layananOnline").value(DEFAULT_LAYANAN_ONLINE.booleanValue()))
            .andExpect(jsonPath("$.idKiosk").value(DEFAULT_ID_KIOSK.intValue()))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)))
            .andExpect(jsonPath("$.sk").value(DEFAULT_SK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubLayanan() throws Exception {
        // Get the subLayanan
        restSubLayananMockMvc.perform(get("/api/sub-layanans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubLayanan() throws Exception {
        // Initialize the database
        subLayananRepository.saveAndFlush(subLayanan);

        int databaseSizeBeforeUpdate = subLayananRepository.findAll().size();

        // Update the subLayanan
        SubLayanan updatedSubLayanan = subLayananRepository.findById(subLayanan.getId()).get();
        // Disconnect from session so that the updates on updatedSubLayanan are not directly saved in db
        em.detach(updatedSubLayanan);
        updatedSubLayanan
            .idLayanan(UPDATED_ID_LAYANAN)
            .kodeLayanan(UPDATED_KODE_LAYANAN)
            .namaLayanan(UPDATED_NAMA_LAYANAN)
            .idLantai(UPDATED_ID_LANTAI)
            .kuota(UPDATED_KUOTA)
            .jamAwal(UPDATED_JAM_AWAL)
            .jamAkhir(UPDATED_JAM_AKHIR)
            .durasi(UPDATED_DURASI)
            .status(UPDATED_STATUS)
            .prefixSuara(UPDATED_PREFIX_SUARA)
            .layananOnline(UPDATED_LAYANAN_ONLINE)
            .idKiosk(UPDATED_ID_KIOSK)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .sk(UPDATED_SK);
        SubLayananDTO subLayananDTO = subLayananMapper.toDto(updatedSubLayanan);

        restSubLayananMockMvc.perform(put("/api/sub-layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subLayananDTO)))
            .andExpect(status().isOk());

        // Validate the SubLayanan in the database
        List<SubLayanan> subLayananList = subLayananRepository.findAll();
        assertThat(subLayananList).hasSize(databaseSizeBeforeUpdate);
        SubLayanan testSubLayanan = subLayananList.get(subLayananList.size() - 1);
        assertThat(testSubLayanan.getIdLayanan()).isEqualTo(UPDATED_ID_LAYANAN);
        assertThat(testSubLayanan.getKodeLayanan()).isEqualTo(UPDATED_KODE_LAYANAN);
        assertThat(testSubLayanan.getNamaLayanan()).isEqualTo(UPDATED_NAMA_LAYANAN);
        assertThat(testSubLayanan.getIdLantai()).isEqualTo(UPDATED_ID_LANTAI);
        assertThat(testSubLayanan.getKuota()).isEqualTo(UPDATED_KUOTA);
        assertThat(testSubLayanan.getJamAwal()).isEqualTo(UPDATED_JAM_AWAL);
        assertThat(testSubLayanan.getJamAkhir()).isEqualTo(UPDATED_JAM_AKHIR);
        assertThat(testSubLayanan.getDurasi()).isEqualTo(UPDATED_DURASI);
        assertThat(testSubLayanan.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSubLayanan.getPrefixSuara()).isEqualTo(UPDATED_PREFIX_SUARA);
        assertThat(testSubLayanan.isLayananOnline()).isEqualTo(UPDATED_LAYANAN_ONLINE);
        assertThat(testSubLayanan.getIdKiosk()).isEqualTo(UPDATED_ID_KIOSK);
        assertThat(testSubLayanan.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testSubLayanan.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testSubLayanan.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testSubLayanan.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testSubLayanan.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testSubLayanan.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
        assertThat(testSubLayanan.getSk()).isEqualTo(UPDATED_SK);
    }

    @Test
    @Transactional
    public void updateNonExistingSubLayanan() throws Exception {
        int databaseSizeBeforeUpdate = subLayananRepository.findAll().size();

        // Create the SubLayanan
        SubLayananDTO subLayananDTO = subLayananMapper.toDto(subLayanan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubLayananMockMvc.perform(put("/api/sub-layanans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subLayananDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubLayanan in the database
        List<SubLayanan> subLayananList = subLayananRepository.findAll();
        assertThat(subLayananList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSubLayanan() throws Exception {
        // Initialize the database
        subLayananRepository.saveAndFlush(subLayanan);

        int databaseSizeBeforeDelete = subLayananRepository.findAll().size();

        // Delete the subLayanan
        restSubLayananMockMvc.perform(delete("/api/sub-layanans/{id}", subLayanan.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubLayanan> subLayananList = subLayananRepository.findAll();
        assertThat(subLayananList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
