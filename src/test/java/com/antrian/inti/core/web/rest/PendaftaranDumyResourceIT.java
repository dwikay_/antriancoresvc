package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.PendaftaranDumy;
import com.antrian.inti.core.repository.PendaftaranDumyRepository;
import com.antrian.inti.core.service.PendaftaranDumyService;
import com.antrian.inti.core.service.dto.PendaftaranDumyDTO;
import com.antrian.inti.core.service.mapper.PendaftaranDumyMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;
import com.antrian.inti.core.service.dto.PendaftaranDumyCriteria;
import com.antrian.inti.core.service.PendaftaranDumyQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PendaftaranDumyResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class PendaftaranDumyResourceIT {

    private static final String DEFAULT_NAMA_BADAN_USAHA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_BADAN_USAHA = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_DIREKSI = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_DIREKSI = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_PENGAMBILAN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_PENGAMBILAN = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_TANGGAL_PENGAMBILAN = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_KODE_KONFIRMASI = "AAAAAAAAAA";
    private static final String UPDATED_KODE_KONFIRMASI = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_KLOTER = 1L;
    private static final Long UPDATED_ID_KLOTER = 2L;
    private static final Long SMALLER_ID_KLOTER = 1L - 1L;

    private static final Boolean DEFAULT_PENGAMBILAN_STRUK = false;
    private static final Boolean UPDATED_PENGAMBILAN_STRUK = true;

    private static final String DEFAULT_JAM_KLOTER = "AAAAAAAAAA";
    private static final String UPDATED_JAM_KLOTER = "BBBBBBBBBB";

    private static final String DEFAULT_PERTANYAAN = "AAAAAAAAAA";
    private static final String UPDATED_PERTANYAAN = "BBBBBBBBBB";

    private static final String DEFAULT_JAWABAN = "AAAAAAAAAA";
    private static final String UPDATED_JAWABAN = "BBBBBBBBBB";

    private static final String DEFAULT_TIPE_KTP = "AAAAAAAAAA";
    private static final String UPDATED_TIPE_KTP = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS_TIKET = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_TIKET = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TYPE_VIRTUAL = false;
    private static final Boolean UPDATED_TYPE_VIRTUAL = true;

    private static final String DEFAULT_JAM_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_JAM_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_TIKET = "AAAAAAAAAA";
    private static final String UPDATED_NO_TIKET = "BBBBBBBBBB";

    private static final Boolean DEFAULT_AGENT = false;
    private static final Boolean UPDATED_AGENT = true;

    private static final UUID DEFAULT_ID_DAFTAR = UUID.randomUUID();
    private static final UUID UPDATED_ID_DAFTAR = UUID.randomUUID();

    private static final String DEFAULT_NAMA_PERSEORANGAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PERSEORANGAN = "BBBBBBBBBB";

    private static final String DEFAULT_INSTANSI = "AAAAAAAAAA";
    private static final String UPDATED_INSTANSI = "BBBBBBBBBB";

    private static final String DEFAULT_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_UNIT = "BBBBBBBBBB";

    private static final String DEFAULT_JABATAN = "AAAAAAAAAA";
    private static final String UPDATED_JABATAN = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PEJABAT = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PEJABAT = "BBBBBBBBBB";

    private static final String DEFAULT_UNTUK_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_UNTUK_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_JWT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_JWT_TOKEN = "BBBBBBBBBB";

    private static final String DEFAULT_NIK = "AAAAAAAAAA";
    private static final String UPDATED_NIK = "BBBBBBBBBB";

    private static final Long DEFAULT_FLAG_BANNED = 1L;
    private static final Long UPDATED_FLAG_BANNED = 2L;
    private static final Long SMALLER_FLAG_BANNED = 1L - 1L;

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;
    private static final Long SMALLER_ID_SUB_LAYANAN = 1L - 1L;

    private static final String DEFAULT_JENIS_KENDALA = "AAAAAAAAAA";
    private static final String UPDATED_JENIS_KENDALA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATE_DATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private PendaftaranDumyRepository pendaftaranDumyRepository;

    @Autowired
    private PendaftaranDumyMapper pendaftaranDumyMapper;

    @Autowired
    private PendaftaranDumyService pendaftaranDumyService;

    @Autowired
    private PendaftaranDumyQueryService pendaftaranDumyQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPendaftaranDumyMockMvc;

    private PendaftaranDumy pendaftaranDumy;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PendaftaranDumyResource pendaftaranDumyResource = new PendaftaranDumyResource(pendaftaranDumyService, pendaftaranDumyQueryService);
        this.restPendaftaranDumyMockMvc = MockMvcBuilders.standaloneSetup(pendaftaranDumyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PendaftaranDumy createEntity(EntityManager em) {
        PendaftaranDumy pendaftaranDumy = new PendaftaranDumy()
            .namaBadanUsaha(DEFAULT_NAMA_BADAN_USAHA)
            .namaDireksi(DEFAULT_NAMA_DIREKSI)
            .email(DEFAULT_EMAIL)
            .tanggalPengambilan(DEFAULT_TANGGAL_PENGAMBILAN)
            .kodeKonfirmasi(DEFAULT_KODE_KONFIRMASI)
            .idKloter(DEFAULT_ID_KLOTER)
            .pengambilanStruk(DEFAULT_PENGAMBILAN_STRUK)
            .jamKloter(DEFAULT_JAM_KLOTER)
            .pertanyaan(DEFAULT_PERTANYAAN)
            .jawaban(DEFAULT_JAWABAN)
            .tipeKtp(DEFAULT_TIPE_KTP)
            .statusTiket(DEFAULT_STATUS_TIKET)
            .typeVirtual(DEFAULT_TYPE_VIRTUAL)
            .jamLayanan(DEFAULT_JAM_LAYANAN)
            .noTiket(DEFAULT_NO_TIKET)
            .agent(DEFAULT_AGENT)
            .idDaftar(DEFAULT_ID_DAFTAR)
            .namaPerseorangan(DEFAULT_NAMA_PERSEORANGAN)
            .instansi(DEFAULT_INSTANSI)
            .unit(DEFAULT_UNIT)
            .jabatan(DEFAULT_JABATAN)
            .namaPejabat(DEFAULT_NAMA_PEJABAT)
            .untukLayanan(DEFAULT_UNTUK_LAYANAN)
            .jwtToken(DEFAULT_JWT_TOKEN)
            .nik(DEFAULT_NIK)
            .flagBanned(DEFAULT_FLAG_BANNED)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .jenisKendala(DEFAULT_JENIS_KENDALA)
            .createDate(DEFAULT_CREATE_DATE);
        return pendaftaranDumy;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PendaftaranDumy createUpdatedEntity(EntityManager em) {
        PendaftaranDumy pendaftaranDumy = new PendaftaranDumy()
            .namaBadanUsaha(UPDATED_NAMA_BADAN_USAHA)
            .namaDireksi(UPDATED_NAMA_DIREKSI)
            .email(UPDATED_EMAIL)
            .tanggalPengambilan(UPDATED_TANGGAL_PENGAMBILAN)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .idKloter(UPDATED_ID_KLOTER)
            .pengambilanStruk(UPDATED_PENGAMBILAN_STRUK)
            .jamKloter(UPDATED_JAM_KLOTER)
            .pertanyaan(UPDATED_PERTANYAAN)
            .jawaban(UPDATED_JAWABAN)
            .tipeKtp(UPDATED_TIPE_KTP)
            .statusTiket(UPDATED_STATUS_TIKET)
            .typeVirtual(UPDATED_TYPE_VIRTUAL)
            .jamLayanan(UPDATED_JAM_LAYANAN)
            .noTiket(UPDATED_NO_TIKET)
            .agent(UPDATED_AGENT)
            .idDaftar(UPDATED_ID_DAFTAR)
            .namaPerseorangan(UPDATED_NAMA_PERSEORANGAN)
            .instansi(UPDATED_INSTANSI)
            .unit(UPDATED_UNIT)
            .jabatan(UPDATED_JABATAN)
            .namaPejabat(UPDATED_NAMA_PEJABAT)
            .untukLayanan(UPDATED_UNTUK_LAYANAN)
            .jwtToken(UPDATED_JWT_TOKEN)
            .nik(UPDATED_NIK)
            .flagBanned(UPDATED_FLAG_BANNED)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .jenisKendala(UPDATED_JENIS_KENDALA)
            .createDate(UPDATED_CREATE_DATE);
        return pendaftaranDumy;
    }

    @BeforeEach
    public void initTest() {
        pendaftaranDumy = createEntity(em);
    }

    @Test
    @Transactional
    public void createPendaftaranDumy() throws Exception {
        int databaseSizeBeforeCreate = pendaftaranDumyRepository.findAll().size();

        // Create the PendaftaranDumy
        PendaftaranDumyDTO pendaftaranDumyDTO = pendaftaranDumyMapper.toDto(pendaftaranDumy);
        restPendaftaranDumyMockMvc.perform(post("/api/pendaftaran-dumies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDumyDTO)))
            .andExpect(status().isCreated());

        // Validate the PendaftaranDumy in the database
        List<PendaftaranDumy> pendaftaranDumyList = pendaftaranDumyRepository.findAll();
        assertThat(pendaftaranDumyList).hasSize(databaseSizeBeforeCreate + 1);
        PendaftaranDumy testPendaftaranDumy = pendaftaranDumyList.get(pendaftaranDumyList.size() - 1);
        assertThat(testPendaftaranDumy.getNamaBadanUsaha()).isEqualTo(DEFAULT_NAMA_BADAN_USAHA);
        assertThat(testPendaftaranDumy.getNamaDireksi()).isEqualTo(DEFAULT_NAMA_DIREKSI);
        assertThat(testPendaftaranDumy.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPendaftaranDumy.getTanggalPengambilan()).isEqualTo(DEFAULT_TANGGAL_PENGAMBILAN);
        assertThat(testPendaftaranDumy.getKodeKonfirmasi()).isEqualTo(DEFAULT_KODE_KONFIRMASI);
        assertThat(testPendaftaranDumy.getIdKloter()).isEqualTo(DEFAULT_ID_KLOTER);
        assertThat(testPendaftaranDumy.isPengambilanStruk()).isEqualTo(DEFAULT_PENGAMBILAN_STRUK);
        assertThat(testPendaftaranDumy.getJamKloter()).isEqualTo(DEFAULT_JAM_KLOTER);
        assertThat(testPendaftaranDumy.getPertanyaan()).isEqualTo(DEFAULT_PERTANYAAN);
        assertThat(testPendaftaranDumy.getJawaban()).isEqualTo(DEFAULT_JAWABAN);
        assertThat(testPendaftaranDumy.getTipeKtp()).isEqualTo(DEFAULT_TIPE_KTP);
        assertThat(testPendaftaranDumy.getStatusTiket()).isEqualTo(DEFAULT_STATUS_TIKET);
        assertThat(testPendaftaranDumy.isTypeVirtual()).isEqualTo(DEFAULT_TYPE_VIRTUAL);
        assertThat(testPendaftaranDumy.getJamLayanan()).isEqualTo(DEFAULT_JAM_LAYANAN);
        assertThat(testPendaftaranDumy.getNoTiket()).isEqualTo(DEFAULT_NO_TIKET);
        assertThat(testPendaftaranDumy.isAgent()).isEqualTo(DEFAULT_AGENT);
        assertThat(testPendaftaranDumy.getIdDaftar()).isEqualTo(DEFAULT_ID_DAFTAR);
        assertThat(testPendaftaranDumy.getNamaPerseorangan()).isEqualTo(DEFAULT_NAMA_PERSEORANGAN);
        assertThat(testPendaftaranDumy.getInstansi()).isEqualTo(DEFAULT_INSTANSI);
        assertThat(testPendaftaranDumy.getUnit()).isEqualTo(DEFAULT_UNIT);
        assertThat(testPendaftaranDumy.getJabatan()).isEqualTo(DEFAULT_JABATAN);
        assertThat(testPendaftaranDumy.getNamaPejabat()).isEqualTo(DEFAULT_NAMA_PEJABAT);
        assertThat(testPendaftaranDumy.getUntukLayanan()).isEqualTo(DEFAULT_UNTUK_LAYANAN);
        assertThat(testPendaftaranDumy.getJwtToken()).isEqualTo(DEFAULT_JWT_TOKEN);
        assertThat(testPendaftaranDumy.getNik()).isEqualTo(DEFAULT_NIK);
        assertThat(testPendaftaranDumy.getFlagBanned()).isEqualTo(DEFAULT_FLAG_BANNED);
        assertThat(testPendaftaranDumy.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testPendaftaranDumy.getJenisKendala()).isEqualTo(DEFAULT_JENIS_KENDALA);
        assertThat(testPendaftaranDumy.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createPendaftaranDumyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pendaftaranDumyRepository.findAll().size();

        // Create the PendaftaranDumy with an existing ID
        pendaftaranDumy.setId(1L);
        PendaftaranDumyDTO pendaftaranDumyDTO = pendaftaranDumyMapper.toDto(pendaftaranDumy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPendaftaranDumyMockMvc.perform(post("/api/pendaftaran-dumies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDumyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PendaftaranDumy in the database
        List<PendaftaranDumy> pendaftaranDumyList = pendaftaranDumyRepository.findAll();
        assertThat(pendaftaranDumyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumies() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pendaftaranDumy.getId().intValue())))
            .andExpect(jsonPath("$.[*].namaBadanUsaha").value(hasItem(DEFAULT_NAMA_BADAN_USAHA)))
            .andExpect(jsonPath("$.[*].namaDireksi").value(hasItem(DEFAULT_NAMA_DIREKSI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].tanggalPengambilan").value(hasItem(DEFAULT_TANGGAL_PENGAMBILAN.toString())))
            .andExpect(jsonPath("$.[*].kodeKonfirmasi").value(hasItem(DEFAULT_KODE_KONFIRMASI)))
            .andExpect(jsonPath("$.[*].idKloter").value(hasItem(DEFAULT_ID_KLOTER.intValue())))
            .andExpect(jsonPath("$.[*].pengambilanStruk").value(hasItem(DEFAULT_PENGAMBILAN_STRUK.booleanValue())))
            .andExpect(jsonPath("$.[*].jamKloter").value(hasItem(DEFAULT_JAM_KLOTER)))
            .andExpect(jsonPath("$.[*].pertanyaan").value(hasItem(DEFAULT_PERTANYAAN.toString())))
            .andExpect(jsonPath("$.[*].jawaban").value(hasItem(DEFAULT_JAWABAN.toString())))
            .andExpect(jsonPath("$.[*].tipeKtp").value(hasItem(DEFAULT_TIPE_KTP)))
            .andExpect(jsonPath("$.[*].statusTiket").value(hasItem(DEFAULT_STATUS_TIKET)))
            .andExpect(jsonPath("$.[*].typeVirtual").value(hasItem(DEFAULT_TYPE_VIRTUAL.booleanValue())))
            .andExpect(jsonPath("$.[*].jamLayanan").value(hasItem(DEFAULT_JAM_LAYANAN)))
            .andExpect(jsonPath("$.[*].noTiket").value(hasItem(DEFAULT_NO_TIKET)))
            .andExpect(jsonPath("$.[*].agent").value(hasItem(DEFAULT_AGENT.booleanValue())))
            .andExpect(jsonPath("$.[*].idDaftar").value(hasItem(DEFAULT_ID_DAFTAR.toString())))
            .andExpect(jsonPath("$.[*].namaPerseorangan").value(hasItem(DEFAULT_NAMA_PERSEORANGAN)))
            .andExpect(jsonPath("$.[*].instansi").value(hasItem(DEFAULT_INSTANSI)))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].jabatan").value(hasItem(DEFAULT_JABATAN)))
            .andExpect(jsonPath("$.[*].namaPejabat").value(hasItem(DEFAULT_NAMA_PEJABAT)))
            .andExpect(jsonPath("$.[*].untukLayanan").value(hasItem(DEFAULT_UNTUK_LAYANAN)))
            .andExpect(jsonPath("$.[*].jwtToken").value(hasItem(DEFAULT_JWT_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].nik").value(hasItem(DEFAULT_NIK)))
            .andExpect(jsonPath("$.[*].flagBanned").value(hasItem(DEFAULT_FLAG_BANNED.intValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].jenisKendala").value(hasItem(DEFAULT_JENIS_KENDALA)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getPendaftaranDumy() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get the pendaftaranDumy
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies/{id}", pendaftaranDumy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pendaftaranDumy.getId().intValue()))
            .andExpect(jsonPath("$.namaBadanUsaha").value(DEFAULT_NAMA_BADAN_USAHA))
            .andExpect(jsonPath("$.namaDireksi").value(DEFAULT_NAMA_DIREKSI))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.tanggalPengambilan").value(DEFAULT_TANGGAL_PENGAMBILAN.toString()))
            .andExpect(jsonPath("$.kodeKonfirmasi").value(DEFAULT_KODE_KONFIRMASI))
            .andExpect(jsonPath("$.idKloter").value(DEFAULT_ID_KLOTER.intValue()))
            .andExpect(jsonPath("$.pengambilanStruk").value(DEFAULT_PENGAMBILAN_STRUK.booleanValue()))
            .andExpect(jsonPath("$.jamKloter").value(DEFAULT_JAM_KLOTER))
            .andExpect(jsonPath("$.pertanyaan").value(DEFAULT_PERTANYAAN.toString()))
            .andExpect(jsonPath("$.jawaban").value(DEFAULT_JAWABAN.toString()))
            .andExpect(jsonPath("$.tipeKtp").value(DEFAULT_TIPE_KTP))
            .andExpect(jsonPath("$.statusTiket").value(DEFAULT_STATUS_TIKET))
            .andExpect(jsonPath("$.typeVirtual").value(DEFAULT_TYPE_VIRTUAL.booleanValue()))
            .andExpect(jsonPath("$.jamLayanan").value(DEFAULT_JAM_LAYANAN))
            .andExpect(jsonPath("$.noTiket").value(DEFAULT_NO_TIKET))
            .andExpect(jsonPath("$.agent").value(DEFAULT_AGENT.booleanValue()))
            .andExpect(jsonPath("$.idDaftar").value(DEFAULT_ID_DAFTAR.toString()))
            .andExpect(jsonPath("$.namaPerseorangan").value(DEFAULT_NAMA_PERSEORANGAN))
            .andExpect(jsonPath("$.instansi").value(DEFAULT_INSTANSI))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT))
            .andExpect(jsonPath("$.jabatan").value(DEFAULT_JABATAN))
            .andExpect(jsonPath("$.namaPejabat").value(DEFAULT_NAMA_PEJABAT))
            .andExpect(jsonPath("$.untukLayanan").value(DEFAULT_UNTUK_LAYANAN))
            .andExpect(jsonPath("$.jwtToken").value(DEFAULT_JWT_TOKEN.toString()))
            .andExpect(jsonPath("$.nik").value(DEFAULT_NIK))
            .andExpect(jsonPath("$.flagBanned").value(DEFAULT_FLAG_BANNED.intValue()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.jenisKendala").value(DEFAULT_JENIS_KENDALA))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()));
    }


    @Test
    @Transactional
    public void getPendaftaranDumiesByIdFiltering() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        Long id = pendaftaranDumy.getId();

        defaultPendaftaranDumyShouldBeFound("id.equals=" + id);
        defaultPendaftaranDumyShouldNotBeFound("id.notEquals=" + id);

        defaultPendaftaranDumyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPendaftaranDumyShouldNotBeFound("id.greaterThan=" + id);

        defaultPendaftaranDumyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPendaftaranDumyShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaBadanUsahaIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaBadanUsaha equals to DEFAULT_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldBeFound("namaBadanUsaha.equals=" + DEFAULT_NAMA_BADAN_USAHA);

        // Get all the pendaftaranDumyList where namaBadanUsaha equals to UPDATED_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldNotBeFound("namaBadanUsaha.equals=" + UPDATED_NAMA_BADAN_USAHA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaBadanUsahaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaBadanUsaha not equals to DEFAULT_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldNotBeFound("namaBadanUsaha.notEquals=" + DEFAULT_NAMA_BADAN_USAHA);

        // Get all the pendaftaranDumyList where namaBadanUsaha not equals to UPDATED_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldBeFound("namaBadanUsaha.notEquals=" + UPDATED_NAMA_BADAN_USAHA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaBadanUsahaIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaBadanUsaha in DEFAULT_NAMA_BADAN_USAHA or UPDATED_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldBeFound("namaBadanUsaha.in=" + DEFAULT_NAMA_BADAN_USAHA + "," + UPDATED_NAMA_BADAN_USAHA);

        // Get all the pendaftaranDumyList where namaBadanUsaha equals to UPDATED_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldNotBeFound("namaBadanUsaha.in=" + UPDATED_NAMA_BADAN_USAHA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaBadanUsahaIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaBadanUsaha is not null
        defaultPendaftaranDumyShouldBeFound("namaBadanUsaha.specified=true");

        // Get all the pendaftaranDumyList where namaBadanUsaha is null
        defaultPendaftaranDumyShouldNotBeFound("namaBadanUsaha.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaBadanUsahaContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaBadanUsaha contains DEFAULT_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldBeFound("namaBadanUsaha.contains=" + DEFAULT_NAMA_BADAN_USAHA);

        // Get all the pendaftaranDumyList where namaBadanUsaha contains UPDATED_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldNotBeFound("namaBadanUsaha.contains=" + UPDATED_NAMA_BADAN_USAHA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaBadanUsahaNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaBadanUsaha does not contain DEFAULT_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldNotBeFound("namaBadanUsaha.doesNotContain=" + DEFAULT_NAMA_BADAN_USAHA);

        // Get all the pendaftaranDumyList where namaBadanUsaha does not contain UPDATED_NAMA_BADAN_USAHA
        defaultPendaftaranDumyShouldBeFound("namaBadanUsaha.doesNotContain=" + UPDATED_NAMA_BADAN_USAHA);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaDireksiIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaDireksi equals to DEFAULT_NAMA_DIREKSI
        defaultPendaftaranDumyShouldBeFound("namaDireksi.equals=" + DEFAULT_NAMA_DIREKSI);

        // Get all the pendaftaranDumyList where namaDireksi equals to UPDATED_NAMA_DIREKSI
        defaultPendaftaranDumyShouldNotBeFound("namaDireksi.equals=" + UPDATED_NAMA_DIREKSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaDireksiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaDireksi not equals to DEFAULT_NAMA_DIREKSI
        defaultPendaftaranDumyShouldNotBeFound("namaDireksi.notEquals=" + DEFAULT_NAMA_DIREKSI);

        // Get all the pendaftaranDumyList where namaDireksi not equals to UPDATED_NAMA_DIREKSI
        defaultPendaftaranDumyShouldBeFound("namaDireksi.notEquals=" + UPDATED_NAMA_DIREKSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaDireksiIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaDireksi in DEFAULT_NAMA_DIREKSI or UPDATED_NAMA_DIREKSI
        defaultPendaftaranDumyShouldBeFound("namaDireksi.in=" + DEFAULT_NAMA_DIREKSI + "," + UPDATED_NAMA_DIREKSI);

        // Get all the pendaftaranDumyList where namaDireksi equals to UPDATED_NAMA_DIREKSI
        defaultPendaftaranDumyShouldNotBeFound("namaDireksi.in=" + UPDATED_NAMA_DIREKSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaDireksiIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaDireksi is not null
        defaultPendaftaranDumyShouldBeFound("namaDireksi.specified=true");

        // Get all the pendaftaranDumyList where namaDireksi is null
        defaultPendaftaranDumyShouldNotBeFound("namaDireksi.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaDireksiContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaDireksi contains DEFAULT_NAMA_DIREKSI
        defaultPendaftaranDumyShouldBeFound("namaDireksi.contains=" + DEFAULT_NAMA_DIREKSI);

        // Get all the pendaftaranDumyList where namaDireksi contains UPDATED_NAMA_DIREKSI
        defaultPendaftaranDumyShouldNotBeFound("namaDireksi.contains=" + UPDATED_NAMA_DIREKSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaDireksiNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaDireksi does not contain DEFAULT_NAMA_DIREKSI
        defaultPendaftaranDumyShouldNotBeFound("namaDireksi.doesNotContain=" + DEFAULT_NAMA_DIREKSI);

        // Get all the pendaftaranDumyList where namaDireksi does not contain UPDATED_NAMA_DIREKSI
        defaultPendaftaranDumyShouldBeFound("namaDireksi.doesNotContain=" + UPDATED_NAMA_DIREKSI);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where email equals to DEFAULT_EMAIL
        defaultPendaftaranDumyShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the pendaftaranDumyList where email equals to UPDATED_EMAIL
        defaultPendaftaranDumyShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where email not equals to DEFAULT_EMAIL
        defaultPendaftaranDumyShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the pendaftaranDumyList where email not equals to UPDATED_EMAIL
        defaultPendaftaranDumyShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultPendaftaranDumyShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the pendaftaranDumyList where email equals to UPDATED_EMAIL
        defaultPendaftaranDumyShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where email is not null
        defaultPendaftaranDumyShouldBeFound("email.specified=true");

        // Get all the pendaftaranDumyList where email is null
        defaultPendaftaranDumyShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByEmailContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where email contains DEFAULT_EMAIL
        defaultPendaftaranDumyShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the pendaftaranDumyList where email contains UPDATED_EMAIL
        defaultPendaftaranDumyShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where email does not contain DEFAULT_EMAIL
        defaultPendaftaranDumyShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the pendaftaranDumyList where email does not contain UPDATED_EMAIL
        defaultPendaftaranDumyShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan equals to DEFAULT_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.equals=" + DEFAULT_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan equals to UPDATED_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.equals=" + UPDATED_TANGGAL_PENGAMBILAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan not equals to DEFAULT_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.notEquals=" + DEFAULT_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan not equals to UPDATED_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.notEquals=" + UPDATED_TANGGAL_PENGAMBILAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan in DEFAULT_TANGGAL_PENGAMBILAN or UPDATED_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.in=" + DEFAULT_TANGGAL_PENGAMBILAN + "," + UPDATED_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan equals to UPDATED_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.in=" + UPDATED_TANGGAL_PENGAMBILAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan is not null
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.specified=true");

        // Get all the pendaftaranDumyList where tanggalPengambilan is null
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan is greater than or equal to DEFAULT_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.greaterThanOrEqual=" + DEFAULT_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan is greater than or equal to UPDATED_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.greaterThanOrEqual=" + UPDATED_TANGGAL_PENGAMBILAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan is less than or equal to DEFAULT_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.lessThanOrEqual=" + DEFAULT_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan is less than or equal to SMALLER_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.lessThanOrEqual=" + SMALLER_TANGGAL_PENGAMBILAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsLessThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan is less than DEFAULT_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.lessThan=" + DEFAULT_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan is less than UPDATED_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.lessThan=" + UPDATED_TANGGAL_PENGAMBILAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTanggalPengambilanIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tanggalPengambilan is greater than DEFAULT_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldNotBeFound("tanggalPengambilan.greaterThan=" + DEFAULT_TANGGAL_PENGAMBILAN);

        // Get all the pendaftaranDumyList where tanggalPengambilan is greater than SMALLER_TANGGAL_PENGAMBILAN
        defaultPendaftaranDumyShouldBeFound("tanggalPengambilan.greaterThan=" + SMALLER_TANGGAL_PENGAMBILAN);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByKodeKonfirmasiIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where kodeKonfirmasi equals to DEFAULT_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldBeFound("kodeKonfirmasi.equals=" + DEFAULT_KODE_KONFIRMASI);

        // Get all the pendaftaranDumyList where kodeKonfirmasi equals to UPDATED_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldNotBeFound("kodeKonfirmasi.equals=" + UPDATED_KODE_KONFIRMASI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByKodeKonfirmasiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where kodeKonfirmasi not equals to DEFAULT_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldNotBeFound("kodeKonfirmasi.notEquals=" + DEFAULT_KODE_KONFIRMASI);

        // Get all the pendaftaranDumyList where kodeKonfirmasi not equals to UPDATED_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldBeFound("kodeKonfirmasi.notEquals=" + UPDATED_KODE_KONFIRMASI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByKodeKonfirmasiIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where kodeKonfirmasi in DEFAULT_KODE_KONFIRMASI or UPDATED_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldBeFound("kodeKonfirmasi.in=" + DEFAULT_KODE_KONFIRMASI + "," + UPDATED_KODE_KONFIRMASI);

        // Get all the pendaftaranDumyList where kodeKonfirmasi equals to UPDATED_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldNotBeFound("kodeKonfirmasi.in=" + UPDATED_KODE_KONFIRMASI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByKodeKonfirmasiIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where kodeKonfirmasi is not null
        defaultPendaftaranDumyShouldBeFound("kodeKonfirmasi.specified=true");

        // Get all the pendaftaranDumyList where kodeKonfirmasi is null
        defaultPendaftaranDumyShouldNotBeFound("kodeKonfirmasi.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByKodeKonfirmasiContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where kodeKonfirmasi contains DEFAULT_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldBeFound("kodeKonfirmasi.contains=" + DEFAULT_KODE_KONFIRMASI);

        // Get all the pendaftaranDumyList where kodeKonfirmasi contains UPDATED_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldNotBeFound("kodeKonfirmasi.contains=" + UPDATED_KODE_KONFIRMASI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByKodeKonfirmasiNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where kodeKonfirmasi does not contain DEFAULT_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldNotBeFound("kodeKonfirmasi.doesNotContain=" + DEFAULT_KODE_KONFIRMASI);

        // Get all the pendaftaranDumyList where kodeKonfirmasi does not contain UPDATED_KODE_KONFIRMASI
        defaultPendaftaranDumyShouldBeFound("kodeKonfirmasi.doesNotContain=" + UPDATED_KODE_KONFIRMASI);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter equals to DEFAULT_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.equals=" + DEFAULT_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter equals to UPDATED_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.equals=" + UPDATED_ID_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter not equals to DEFAULT_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.notEquals=" + DEFAULT_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter not equals to UPDATED_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.notEquals=" + UPDATED_ID_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter in DEFAULT_ID_KLOTER or UPDATED_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.in=" + DEFAULT_ID_KLOTER + "," + UPDATED_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter equals to UPDATED_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.in=" + UPDATED_ID_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter is not null
        defaultPendaftaranDumyShouldBeFound("idKloter.specified=true");

        // Get all the pendaftaranDumyList where idKloter is null
        defaultPendaftaranDumyShouldNotBeFound("idKloter.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter is greater than or equal to DEFAULT_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.greaterThanOrEqual=" + DEFAULT_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter is greater than or equal to UPDATED_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.greaterThanOrEqual=" + UPDATED_ID_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter is less than or equal to DEFAULT_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.lessThanOrEqual=" + DEFAULT_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter is less than or equal to SMALLER_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.lessThanOrEqual=" + SMALLER_ID_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsLessThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter is less than DEFAULT_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.lessThan=" + DEFAULT_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter is less than UPDATED_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.lessThan=" + UPDATED_ID_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdKloterIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idKloter is greater than DEFAULT_ID_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("idKloter.greaterThan=" + DEFAULT_ID_KLOTER);

        // Get all the pendaftaranDumyList where idKloter is greater than SMALLER_ID_KLOTER
        defaultPendaftaranDumyShouldBeFound("idKloter.greaterThan=" + SMALLER_ID_KLOTER);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByPengambilanStrukIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where pengambilanStruk equals to DEFAULT_PENGAMBILAN_STRUK
        defaultPendaftaranDumyShouldBeFound("pengambilanStruk.equals=" + DEFAULT_PENGAMBILAN_STRUK);

        // Get all the pendaftaranDumyList where pengambilanStruk equals to UPDATED_PENGAMBILAN_STRUK
        defaultPendaftaranDumyShouldNotBeFound("pengambilanStruk.equals=" + UPDATED_PENGAMBILAN_STRUK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByPengambilanStrukIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where pengambilanStruk not equals to DEFAULT_PENGAMBILAN_STRUK
        defaultPendaftaranDumyShouldNotBeFound("pengambilanStruk.notEquals=" + DEFAULT_PENGAMBILAN_STRUK);

        // Get all the pendaftaranDumyList where pengambilanStruk not equals to UPDATED_PENGAMBILAN_STRUK
        defaultPendaftaranDumyShouldBeFound("pengambilanStruk.notEquals=" + UPDATED_PENGAMBILAN_STRUK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByPengambilanStrukIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where pengambilanStruk in DEFAULT_PENGAMBILAN_STRUK or UPDATED_PENGAMBILAN_STRUK
        defaultPendaftaranDumyShouldBeFound("pengambilanStruk.in=" + DEFAULT_PENGAMBILAN_STRUK + "," + UPDATED_PENGAMBILAN_STRUK);

        // Get all the pendaftaranDumyList where pengambilanStruk equals to UPDATED_PENGAMBILAN_STRUK
        defaultPendaftaranDumyShouldNotBeFound("pengambilanStruk.in=" + UPDATED_PENGAMBILAN_STRUK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByPengambilanStrukIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where pengambilanStruk is not null
        defaultPendaftaranDumyShouldBeFound("pengambilanStruk.specified=true");

        // Get all the pendaftaranDumyList where pengambilanStruk is null
        defaultPendaftaranDumyShouldNotBeFound("pengambilanStruk.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamKloterIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamKloter equals to DEFAULT_JAM_KLOTER
        defaultPendaftaranDumyShouldBeFound("jamKloter.equals=" + DEFAULT_JAM_KLOTER);

        // Get all the pendaftaranDumyList where jamKloter equals to UPDATED_JAM_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("jamKloter.equals=" + UPDATED_JAM_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamKloterIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamKloter not equals to DEFAULT_JAM_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("jamKloter.notEquals=" + DEFAULT_JAM_KLOTER);

        // Get all the pendaftaranDumyList where jamKloter not equals to UPDATED_JAM_KLOTER
        defaultPendaftaranDumyShouldBeFound("jamKloter.notEquals=" + UPDATED_JAM_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamKloterIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamKloter in DEFAULT_JAM_KLOTER or UPDATED_JAM_KLOTER
        defaultPendaftaranDumyShouldBeFound("jamKloter.in=" + DEFAULT_JAM_KLOTER + "," + UPDATED_JAM_KLOTER);

        // Get all the pendaftaranDumyList where jamKloter equals to UPDATED_JAM_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("jamKloter.in=" + UPDATED_JAM_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamKloterIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamKloter is not null
        defaultPendaftaranDumyShouldBeFound("jamKloter.specified=true");

        // Get all the pendaftaranDumyList where jamKloter is null
        defaultPendaftaranDumyShouldNotBeFound("jamKloter.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamKloterContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamKloter contains DEFAULT_JAM_KLOTER
        defaultPendaftaranDumyShouldBeFound("jamKloter.contains=" + DEFAULT_JAM_KLOTER);

        // Get all the pendaftaranDumyList where jamKloter contains UPDATED_JAM_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("jamKloter.contains=" + UPDATED_JAM_KLOTER);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamKloterNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamKloter does not contain DEFAULT_JAM_KLOTER
        defaultPendaftaranDumyShouldNotBeFound("jamKloter.doesNotContain=" + DEFAULT_JAM_KLOTER);

        // Get all the pendaftaranDumyList where jamKloter does not contain UPDATED_JAM_KLOTER
        defaultPendaftaranDumyShouldBeFound("jamKloter.doesNotContain=" + UPDATED_JAM_KLOTER);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTipeKtpIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tipeKtp equals to DEFAULT_TIPE_KTP
        defaultPendaftaranDumyShouldBeFound("tipeKtp.equals=" + DEFAULT_TIPE_KTP);

        // Get all the pendaftaranDumyList where tipeKtp equals to UPDATED_TIPE_KTP
        defaultPendaftaranDumyShouldNotBeFound("tipeKtp.equals=" + UPDATED_TIPE_KTP);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTipeKtpIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tipeKtp not equals to DEFAULT_TIPE_KTP
        defaultPendaftaranDumyShouldNotBeFound("tipeKtp.notEquals=" + DEFAULT_TIPE_KTP);

        // Get all the pendaftaranDumyList where tipeKtp not equals to UPDATED_TIPE_KTP
        defaultPendaftaranDumyShouldBeFound("tipeKtp.notEquals=" + UPDATED_TIPE_KTP);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTipeKtpIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tipeKtp in DEFAULT_TIPE_KTP or UPDATED_TIPE_KTP
        defaultPendaftaranDumyShouldBeFound("tipeKtp.in=" + DEFAULT_TIPE_KTP + "," + UPDATED_TIPE_KTP);

        // Get all the pendaftaranDumyList where tipeKtp equals to UPDATED_TIPE_KTP
        defaultPendaftaranDumyShouldNotBeFound("tipeKtp.in=" + UPDATED_TIPE_KTP);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTipeKtpIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tipeKtp is not null
        defaultPendaftaranDumyShouldBeFound("tipeKtp.specified=true");

        // Get all the pendaftaranDumyList where tipeKtp is null
        defaultPendaftaranDumyShouldNotBeFound("tipeKtp.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByTipeKtpContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tipeKtp contains DEFAULT_TIPE_KTP
        defaultPendaftaranDumyShouldBeFound("tipeKtp.contains=" + DEFAULT_TIPE_KTP);

        // Get all the pendaftaranDumyList where tipeKtp contains UPDATED_TIPE_KTP
        defaultPendaftaranDumyShouldNotBeFound("tipeKtp.contains=" + UPDATED_TIPE_KTP);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTipeKtpNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where tipeKtp does not contain DEFAULT_TIPE_KTP
        defaultPendaftaranDumyShouldNotBeFound("tipeKtp.doesNotContain=" + DEFAULT_TIPE_KTP);

        // Get all the pendaftaranDumyList where tipeKtp does not contain UPDATED_TIPE_KTP
        defaultPendaftaranDumyShouldBeFound("tipeKtp.doesNotContain=" + UPDATED_TIPE_KTP);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByStatusTiketIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where statusTiket equals to DEFAULT_STATUS_TIKET
        defaultPendaftaranDumyShouldBeFound("statusTiket.equals=" + DEFAULT_STATUS_TIKET);

        // Get all the pendaftaranDumyList where statusTiket equals to UPDATED_STATUS_TIKET
        defaultPendaftaranDumyShouldNotBeFound("statusTiket.equals=" + UPDATED_STATUS_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByStatusTiketIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where statusTiket not equals to DEFAULT_STATUS_TIKET
        defaultPendaftaranDumyShouldNotBeFound("statusTiket.notEquals=" + DEFAULT_STATUS_TIKET);

        // Get all the pendaftaranDumyList where statusTiket not equals to UPDATED_STATUS_TIKET
        defaultPendaftaranDumyShouldBeFound("statusTiket.notEquals=" + UPDATED_STATUS_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByStatusTiketIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where statusTiket in DEFAULT_STATUS_TIKET or UPDATED_STATUS_TIKET
        defaultPendaftaranDumyShouldBeFound("statusTiket.in=" + DEFAULT_STATUS_TIKET + "," + UPDATED_STATUS_TIKET);

        // Get all the pendaftaranDumyList where statusTiket equals to UPDATED_STATUS_TIKET
        defaultPendaftaranDumyShouldNotBeFound("statusTiket.in=" + UPDATED_STATUS_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByStatusTiketIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where statusTiket is not null
        defaultPendaftaranDumyShouldBeFound("statusTiket.specified=true");

        // Get all the pendaftaranDumyList where statusTiket is null
        defaultPendaftaranDumyShouldNotBeFound("statusTiket.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByStatusTiketContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where statusTiket contains DEFAULT_STATUS_TIKET
        defaultPendaftaranDumyShouldBeFound("statusTiket.contains=" + DEFAULT_STATUS_TIKET);

        // Get all the pendaftaranDumyList where statusTiket contains UPDATED_STATUS_TIKET
        defaultPendaftaranDumyShouldNotBeFound("statusTiket.contains=" + UPDATED_STATUS_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByStatusTiketNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where statusTiket does not contain DEFAULT_STATUS_TIKET
        defaultPendaftaranDumyShouldNotBeFound("statusTiket.doesNotContain=" + DEFAULT_STATUS_TIKET);

        // Get all the pendaftaranDumyList where statusTiket does not contain UPDATED_STATUS_TIKET
        defaultPendaftaranDumyShouldBeFound("statusTiket.doesNotContain=" + UPDATED_STATUS_TIKET);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTypeVirtualIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where typeVirtual equals to DEFAULT_TYPE_VIRTUAL
        defaultPendaftaranDumyShouldBeFound("typeVirtual.equals=" + DEFAULT_TYPE_VIRTUAL);

        // Get all the pendaftaranDumyList where typeVirtual equals to UPDATED_TYPE_VIRTUAL
        defaultPendaftaranDumyShouldNotBeFound("typeVirtual.equals=" + UPDATED_TYPE_VIRTUAL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTypeVirtualIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where typeVirtual not equals to DEFAULT_TYPE_VIRTUAL
        defaultPendaftaranDumyShouldNotBeFound("typeVirtual.notEquals=" + DEFAULT_TYPE_VIRTUAL);

        // Get all the pendaftaranDumyList where typeVirtual not equals to UPDATED_TYPE_VIRTUAL
        defaultPendaftaranDumyShouldBeFound("typeVirtual.notEquals=" + UPDATED_TYPE_VIRTUAL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTypeVirtualIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where typeVirtual in DEFAULT_TYPE_VIRTUAL or UPDATED_TYPE_VIRTUAL
        defaultPendaftaranDumyShouldBeFound("typeVirtual.in=" + DEFAULT_TYPE_VIRTUAL + "," + UPDATED_TYPE_VIRTUAL);

        // Get all the pendaftaranDumyList where typeVirtual equals to UPDATED_TYPE_VIRTUAL
        defaultPendaftaranDumyShouldNotBeFound("typeVirtual.in=" + UPDATED_TYPE_VIRTUAL);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByTypeVirtualIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where typeVirtual is not null
        defaultPendaftaranDumyShouldBeFound("typeVirtual.specified=true");

        // Get all the pendaftaranDumyList where typeVirtual is null
        defaultPendaftaranDumyShouldNotBeFound("typeVirtual.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamLayananIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamLayanan equals to DEFAULT_JAM_LAYANAN
        defaultPendaftaranDumyShouldBeFound("jamLayanan.equals=" + DEFAULT_JAM_LAYANAN);

        // Get all the pendaftaranDumyList where jamLayanan equals to UPDATED_JAM_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("jamLayanan.equals=" + UPDATED_JAM_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamLayananIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamLayanan not equals to DEFAULT_JAM_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("jamLayanan.notEquals=" + DEFAULT_JAM_LAYANAN);

        // Get all the pendaftaranDumyList where jamLayanan not equals to UPDATED_JAM_LAYANAN
        defaultPendaftaranDumyShouldBeFound("jamLayanan.notEquals=" + UPDATED_JAM_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamLayananIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamLayanan in DEFAULT_JAM_LAYANAN or UPDATED_JAM_LAYANAN
        defaultPendaftaranDumyShouldBeFound("jamLayanan.in=" + DEFAULT_JAM_LAYANAN + "," + UPDATED_JAM_LAYANAN);

        // Get all the pendaftaranDumyList where jamLayanan equals to UPDATED_JAM_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("jamLayanan.in=" + UPDATED_JAM_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamLayananIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamLayanan is not null
        defaultPendaftaranDumyShouldBeFound("jamLayanan.specified=true");

        // Get all the pendaftaranDumyList where jamLayanan is null
        defaultPendaftaranDumyShouldNotBeFound("jamLayanan.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamLayananContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamLayanan contains DEFAULT_JAM_LAYANAN
        defaultPendaftaranDumyShouldBeFound("jamLayanan.contains=" + DEFAULT_JAM_LAYANAN);

        // Get all the pendaftaranDumyList where jamLayanan contains UPDATED_JAM_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("jamLayanan.contains=" + UPDATED_JAM_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJamLayananNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jamLayanan does not contain DEFAULT_JAM_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("jamLayanan.doesNotContain=" + DEFAULT_JAM_LAYANAN);

        // Get all the pendaftaranDumyList where jamLayanan does not contain UPDATED_JAM_LAYANAN
        defaultPendaftaranDumyShouldBeFound("jamLayanan.doesNotContain=" + UPDATED_JAM_LAYANAN);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNoTiketIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where noTiket equals to DEFAULT_NO_TIKET
        defaultPendaftaranDumyShouldBeFound("noTiket.equals=" + DEFAULT_NO_TIKET);

        // Get all the pendaftaranDumyList where noTiket equals to UPDATED_NO_TIKET
        defaultPendaftaranDumyShouldNotBeFound("noTiket.equals=" + UPDATED_NO_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNoTiketIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where noTiket not equals to DEFAULT_NO_TIKET
        defaultPendaftaranDumyShouldNotBeFound("noTiket.notEquals=" + DEFAULT_NO_TIKET);

        // Get all the pendaftaranDumyList where noTiket not equals to UPDATED_NO_TIKET
        defaultPendaftaranDumyShouldBeFound("noTiket.notEquals=" + UPDATED_NO_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNoTiketIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where noTiket in DEFAULT_NO_TIKET or UPDATED_NO_TIKET
        defaultPendaftaranDumyShouldBeFound("noTiket.in=" + DEFAULT_NO_TIKET + "," + UPDATED_NO_TIKET);

        // Get all the pendaftaranDumyList where noTiket equals to UPDATED_NO_TIKET
        defaultPendaftaranDumyShouldNotBeFound("noTiket.in=" + UPDATED_NO_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNoTiketIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where noTiket is not null
        defaultPendaftaranDumyShouldBeFound("noTiket.specified=true");

        // Get all the pendaftaranDumyList where noTiket is null
        defaultPendaftaranDumyShouldNotBeFound("noTiket.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByNoTiketContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where noTiket contains DEFAULT_NO_TIKET
        defaultPendaftaranDumyShouldBeFound("noTiket.contains=" + DEFAULT_NO_TIKET);

        // Get all the pendaftaranDumyList where noTiket contains UPDATED_NO_TIKET
        defaultPendaftaranDumyShouldNotBeFound("noTiket.contains=" + UPDATED_NO_TIKET);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNoTiketNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where noTiket does not contain DEFAULT_NO_TIKET
        defaultPendaftaranDumyShouldNotBeFound("noTiket.doesNotContain=" + DEFAULT_NO_TIKET);

        // Get all the pendaftaranDumyList where noTiket does not contain UPDATED_NO_TIKET
        defaultPendaftaranDumyShouldBeFound("noTiket.doesNotContain=" + UPDATED_NO_TIKET);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByAgentIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where agent equals to DEFAULT_AGENT
        defaultPendaftaranDumyShouldBeFound("agent.equals=" + DEFAULT_AGENT);

        // Get all the pendaftaranDumyList where agent equals to UPDATED_AGENT
        defaultPendaftaranDumyShouldNotBeFound("agent.equals=" + UPDATED_AGENT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByAgentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where agent not equals to DEFAULT_AGENT
        defaultPendaftaranDumyShouldNotBeFound("agent.notEquals=" + DEFAULT_AGENT);

        // Get all the pendaftaranDumyList where agent not equals to UPDATED_AGENT
        defaultPendaftaranDumyShouldBeFound("agent.notEquals=" + UPDATED_AGENT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByAgentIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where agent in DEFAULT_AGENT or UPDATED_AGENT
        defaultPendaftaranDumyShouldBeFound("agent.in=" + DEFAULT_AGENT + "," + UPDATED_AGENT);

        // Get all the pendaftaranDumyList where agent equals to UPDATED_AGENT
        defaultPendaftaranDumyShouldNotBeFound("agent.in=" + UPDATED_AGENT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByAgentIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where agent is not null
        defaultPendaftaranDumyShouldBeFound("agent.specified=true");

        // Get all the pendaftaranDumyList where agent is null
        defaultPendaftaranDumyShouldNotBeFound("agent.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdDaftarIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idDaftar equals to DEFAULT_ID_DAFTAR
        defaultPendaftaranDumyShouldBeFound("idDaftar.equals=" + DEFAULT_ID_DAFTAR);

        // Get all the pendaftaranDumyList where idDaftar equals to UPDATED_ID_DAFTAR
        defaultPendaftaranDumyShouldNotBeFound("idDaftar.equals=" + UPDATED_ID_DAFTAR);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdDaftarIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idDaftar not equals to DEFAULT_ID_DAFTAR
        defaultPendaftaranDumyShouldNotBeFound("idDaftar.notEquals=" + DEFAULT_ID_DAFTAR);

        // Get all the pendaftaranDumyList where idDaftar not equals to UPDATED_ID_DAFTAR
        defaultPendaftaranDumyShouldBeFound("idDaftar.notEquals=" + UPDATED_ID_DAFTAR);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdDaftarIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idDaftar in DEFAULT_ID_DAFTAR or UPDATED_ID_DAFTAR
        defaultPendaftaranDumyShouldBeFound("idDaftar.in=" + DEFAULT_ID_DAFTAR + "," + UPDATED_ID_DAFTAR);

        // Get all the pendaftaranDumyList where idDaftar equals to UPDATED_ID_DAFTAR
        defaultPendaftaranDumyShouldNotBeFound("idDaftar.in=" + UPDATED_ID_DAFTAR);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdDaftarIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idDaftar is not null
        defaultPendaftaranDumyShouldBeFound("idDaftar.specified=true");

        // Get all the pendaftaranDumyList where idDaftar is null
        defaultPendaftaranDumyShouldNotBeFound("idDaftar.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPerseoranganIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPerseorangan equals to DEFAULT_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldBeFound("namaPerseorangan.equals=" + DEFAULT_NAMA_PERSEORANGAN);

        // Get all the pendaftaranDumyList where namaPerseorangan equals to UPDATED_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldNotBeFound("namaPerseorangan.equals=" + UPDATED_NAMA_PERSEORANGAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPerseoranganIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPerseorangan not equals to DEFAULT_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldNotBeFound("namaPerseorangan.notEquals=" + DEFAULT_NAMA_PERSEORANGAN);

        // Get all the pendaftaranDumyList where namaPerseorangan not equals to UPDATED_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldBeFound("namaPerseorangan.notEquals=" + UPDATED_NAMA_PERSEORANGAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPerseoranganIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPerseorangan in DEFAULT_NAMA_PERSEORANGAN or UPDATED_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldBeFound("namaPerseorangan.in=" + DEFAULT_NAMA_PERSEORANGAN + "," + UPDATED_NAMA_PERSEORANGAN);

        // Get all the pendaftaranDumyList where namaPerseorangan equals to UPDATED_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldNotBeFound("namaPerseorangan.in=" + UPDATED_NAMA_PERSEORANGAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPerseoranganIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPerseorangan is not null
        defaultPendaftaranDumyShouldBeFound("namaPerseorangan.specified=true");

        // Get all the pendaftaranDumyList where namaPerseorangan is null
        defaultPendaftaranDumyShouldNotBeFound("namaPerseorangan.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPerseoranganContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPerseorangan contains DEFAULT_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldBeFound("namaPerseorangan.contains=" + DEFAULT_NAMA_PERSEORANGAN);

        // Get all the pendaftaranDumyList where namaPerseorangan contains UPDATED_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldNotBeFound("namaPerseorangan.contains=" + UPDATED_NAMA_PERSEORANGAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPerseoranganNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPerseorangan does not contain DEFAULT_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldNotBeFound("namaPerseorangan.doesNotContain=" + DEFAULT_NAMA_PERSEORANGAN);

        // Get all the pendaftaranDumyList where namaPerseorangan does not contain UPDATED_NAMA_PERSEORANGAN
        defaultPendaftaranDumyShouldBeFound("namaPerseorangan.doesNotContain=" + UPDATED_NAMA_PERSEORANGAN);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByInstansiIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where instansi equals to DEFAULT_INSTANSI
        defaultPendaftaranDumyShouldBeFound("instansi.equals=" + DEFAULT_INSTANSI);

        // Get all the pendaftaranDumyList where instansi equals to UPDATED_INSTANSI
        defaultPendaftaranDumyShouldNotBeFound("instansi.equals=" + UPDATED_INSTANSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByInstansiIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where instansi not equals to DEFAULT_INSTANSI
        defaultPendaftaranDumyShouldNotBeFound("instansi.notEquals=" + DEFAULT_INSTANSI);

        // Get all the pendaftaranDumyList where instansi not equals to UPDATED_INSTANSI
        defaultPendaftaranDumyShouldBeFound("instansi.notEquals=" + UPDATED_INSTANSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByInstansiIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where instansi in DEFAULT_INSTANSI or UPDATED_INSTANSI
        defaultPendaftaranDumyShouldBeFound("instansi.in=" + DEFAULT_INSTANSI + "," + UPDATED_INSTANSI);

        // Get all the pendaftaranDumyList where instansi equals to UPDATED_INSTANSI
        defaultPendaftaranDumyShouldNotBeFound("instansi.in=" + UPDATED_INSTANSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByInstansiIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where instansi is not null
        defaultPendaftaranDumyShouldBeFound("instansi.specified=true");

        // Get all the pendaftaranDumyList where instansi is null
        defaultPendaftaranDumyShouldNotBeFound("instansi.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByInstansiContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where instansi contains DEFAULT_INSTANSI
        defaultPendaftaranDumyShouldBeFound("instansi.contains=" + DEFAULT_INSTANSI);

        // Get all the pendaftaranDumyList where instansi contains UPDATED_INSTANSI
        defaultPendaftaranDumyShouldNotBeFound("instansi.contains=" + UPDATED_INSTANSI);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByInstansiNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where instansi does not contain DEFAULT_INSTANSI
        defaultPendaftaranDumyShouldNotBeFound("instansi.doesNotContain=" + DEFAULT_INSTANSI);

        // Get all the pendaftaranDumyList where instansi does not contain UPDATED_INSTANSI
        defaultPendaftaranDumyShouldBeFound("instansi.doesNotContain=" + UPDATED_INSTANSI);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUnitIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where unit equals to DEFAULT_UNIT
        defaultPendaftaranDumyShouldBeFound("unit.equals=" + DEFAULT_UNIT);

        // Get all the pendaftaranDumyList where unit equals to UPDATED_UNIT
        defaultPendaftaranDumyShouldNotBeFound("unit.equals=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUnitIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where unit not equals to DEFAULT_UNIT
        defaultPendaftaranDumyShouldNotBeFound("unit.notEquals=" + DEFAULT_UNIT);

        // Get all the pendaftaranDumyList where unit not equals to UPDATED_UNIT
        defaultPendaftaranDumyShouldBeFound("unit.notEquals=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUnitIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where unit in DEFAULT_UNIT or UPDATED_UNIT
        defaultPendaftaranDumyShouldBeFound("unit.in=" + DEFAULT_UNIT + "," + UPDATED_UNIT);

        // Get all the pendaftaranDumyList where unit equals to UPDATED_UNIT
        defaultPendaftaranDumyShouldNotBeFound("unit.in=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUnitIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where unit is not null
        defaultPendaftaranDumyShouldBeFound("unit.specified=true");

        // Get all the pendaftaranDumyList where unit is null
        defaultPendaftaranDumyShouldNotBeFound("unit.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByUnitContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where unit contains DEFAULT_UNIT
        defaultPendaftaranDumyShouldBeFound("unit.contains=" + DEFAULT_UNIT);

        // Get all the pendaftaranDumyList where unit contains UPDATED_UNIT
        defaultPendaftaranDumyShouldNotBeFound("unit.contains=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUnitNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where unit does not contain DEFAULT_UNIT
        defaultPendaftaranDumyShouldNotBeFound("unit.doesNotContain=" + DEFAULT_UNIT);

        // Get all the pendaftaranDumyList where unit does not contain UPDATED_UNIT
        defaultPendaftaranDumyShouldBeFound("unit.doesNotContain=" + UPDATED_UNIT);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJabatanIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jabatan equals to DEFAULT_JABATAN
        defaultPendaftaranDumyShouldBeFound("jabatan.equals=" + DEFAULT_JABATAN);

        // Get all the pendaftaranDumyList where jabatan equals to UPDATED_JABATAN
        defaultPendaftaranDumyShouldNotBeFound("jabatan.equals=" + UPDATED_JABATAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJabatanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jabatan not equals to DEFAULT_JABATAN
        defaultPendaftaranDumyShouldNotBeFound("jabatan.notEquals=" + DEFAULT_JABATAN);

        // Get all the pendaftaranDumyList where jabatan not equals to UPDATED_JABATAN
        defaultPendaftaranDumyShouldBeFound("jabatan.notEquals=" + UPDATED_JABATAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJabatanIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jabatan in DEFAULT_JABATAN or UPDATED_JABATAN
        defaultPendaftaranDumyShouldBeFound("jabatan.in=" + DEFAULT_JABATAN + "," + UPDATED_JABATAN);

        // Get all the pendaftaranDumyList where jabatan equals to UPDATED_JABATAN
        defaultPendaftaranDumyShouldNotBeFound("jabatan.in=" + UPDATED_JABATAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJabatanIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jabatan is not null
        defaultPendaftaranDumyShouldBeFound("jabatan.specified=true");

        // Get all the pendaftaranDumyList where jabatan is null
        defaultPendaftaranDumyShouldNotBeFound("jabatan.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByJabatanContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jabatan contains DEFAULT_JABATAN
        defaultPendaftaranDumyShouldBeFound("jabatan.contains=" + DEFAULT_JABATAN);

        // Get all the pendaftaranDumyList where jabatan contains UPDATED_JABATAN
        defaultPendaftaranDumyShouldNotBeFound("jabatan.contains=" + UPDATED_JABATAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJabatanNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jabatan does not contain DEFAULT_JABATAN
        defaultPendaftaranDumyShouldNotBeFound("jabatan.doesNotContain=" + DEFAULT_JABATAN);

        // Get all the pendaftaranDumyList where jabatan does not contain UPDATED_JABATAN
        defaultPendaftaranDumyShouldBeFound("jabatan.doesNotContain=" + UPDATED_JABATAN);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPejabatIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPejabat equals to DEFAULT_NAMA_PEJABAT
        defaultPendaftaranDumyShouldBeFound("namaPejabat.equals=" + DEFAULT_NAMA_PEJABAT);

        // Get all the pendaftaranDumyList where namaPejabat equals to UPDATED_NAMA_PEJABAT
        defaultPendaftaranDumyShouldNotBeFound("namaPejabat.equals=" + UPDATED_NAMA_PEJABAT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPejabatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPejabat not equals to DEFAULT_NAMA_PEJABAT
        defaultPendaftaranDumyShouldNotBeFound("namaPejabat.notEquals=" + DEFAULT_NAMA_PEJABAT);

        // Get all the pendaftaranDumyList where namaPejabat not equals to UPDATED_NAMA_PEJABAT
        defaultPendaftaranDumyShouldBeFound("namaPejabat.notEquals=" + UPDATED_NAMA_PEJABAT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPejabatIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPejabat in DEFAULT_NAMA_PEJABAT or UPDATED_NAMA_PEJABAT
        defaultPendaftaranDumyShouldBeFound("namaPejabat.in=" + DEFAULT_NAMA_PEJABAT + "," + UPDATED_NAMA_PEJABAT);

        // Get all the pendaftaranDumyList where namaPejabat equals to UPDATED_NAMA_PEJABAT
        defaultPendaftaranDumyShouldNotBeFound("namaPejabat.in=" + UPDATED_NAMA_PEJABAT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPejabatIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPejabat is not null
        defaultPendaftaranDumyShouldBeFound("namaPejabat.specified=true");

        // Get all the pendaftaranDumyList where namaPejabat is null
        defaultPendaftaranDumyShouldNotBeFound("namaPejabat.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPejabatContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPejabat contains DEFAULT_NAMA_PEJABAT
        defaultPendaftaranDumyShouldBeFound("namaPejabat.contains=" + DEFAULT_NAMA_PEJABAT);

        // Get all the pendaftaranDumyList where namaPejabat contains UPDATED_NAMA_PEJABAT
        defaultPendaftaranDumyShouldNotBeFound("namaPejabat.contains=" + UPDATED_NAMA_PEJABAT);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNamaPejabatNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where namaPejabat does not contain DEFAULT_NAMA_PEJABAT
        defaultPendaftaranDumyShouldNotBeFound("namaPejabat.doesNotContain=" + DEFAULT_NAMA_PEJABAT);

        // Get all the pendaftaranDumyList where namaPejabat does not contain UPDATED_NAMA_PEJABAT
        defaultPendaftaranDumyShouldBeFound("namaPejabat.doesNotContain=" + UPDATED_NAMA_PEJABAT);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUntukLayananIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where untukLayanan equals to DEFAULT_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldBeFound("untukLayanan.equals=" + DEFAULT_UNTUK_LAYANAN);

        // Get all the pendaftaranDumyList where untukLayanan equals to UPDATED_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("untukLayanan.equals=" + UPDATED_UNTUK_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUntukLayananIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where untukLayanan not equals to DEFAULT_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("untukLayanan.notEquals=" + DEFAULT_UNTUK_LAYANAN);

        // Get all the pendaftaranDumyList where untukLayanan not equals to UPDATED_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldBeFound("untukLayanan.notEquals=" + UPDATED_UNTUK_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUntukLayananIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where untukLayanan in DEFAULT_UNTUK_LAYANAN or UPDATED_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldBeFound("untukLayanan.in=" + DEFAULT_UNTUK_LAYANAN + "," + UPDATED_UNTUK_LAYANAN);

        // Get all the pendaftaranDumyList where untukLayanan equals to UPDATED_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("untukLayanan.in=" + UPDATED_UNTUK_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUntukLayananIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where untukLayanan is not null
        defaultPendaftaranDumyShouldBeFound("untukLayanan.specified=true");

        // Get all the pendaftaranDumyList where untukLayanan is null
        defaultPendaftaranDumyShouldNotBeFound("untukLayanan.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByUntukLayananContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where untukLayanan contains DEFAULT_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldBeFound("untukLayanan.contains=" + DEFAULT_UNTUK_LAYANAN);

        // Get all the pendaftaranDumyList where untukLayanan contains UPDATED_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("untukLayanan.contains=" + UPDATED_UNTUK_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByUntukLayananNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where untukLayanan does not contain DEFAULT_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("untukLayanan.doesNotContain=" + DEFAULT_UNTUK_LAYANAN);

        // Get all the pendaftaranDumyList where untukLayanan does not contain UPDATED_UNTUK_LAYANAN
        defaultPendaftaranDumyShouldBeFound("untukLayanan.doesNotContain=" + UPDATED_UNTUK_LAYANAN);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNikIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where nik equals to DEFAULT_NIK
        defaultPendaftaranDumyShouldBeFound("nik.equals=" + DEFAULT_NIK);

        // Get all the pendaftaranDumyList where nik equals to UPDATED_NIK
        defaultPendaftaranDumyShouldNotBeFound("nik.equals=" + UPDATED_NIK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNikIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where nik not equals to DEFAULT_NIK
        defaultPendaftaranDumyShouldNotBeFound("nik.notEquals=" + DEFAULT_NIK);

        // Get all the pendaftaranDumyList where nik not equals to UPDATED_NIK
        defaultPendaftaranDumyShouldBeFound("nik.notEquals=" + UPDATED_NIK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNikIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where nik in DEFAULT_NIK or UPDATED_NIK
        defaultPendaftaranDumyShouldBeFound("nik.in=" + DEFAULT_NIK + "," + UPDATED_NIK);

        // Get all the pendaftaranDumyList where nik equals to UPDATED_NIK
        defaultPendaftaranDumyShouldNotBeFound("nik.in=" + UPDATED_NIK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNikIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where nik is not null
        defaultPendaftaranDumyShouldBeFound("nik.specified=true");

        // Get all the pendaftaranDumyList where nik is null
        defaultPendaftaranDumyShouldNotBeFound("nik.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByNikContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where nik contains DEFAULT_NIK
        defaultPendaftaranDumyShouldBeFound("nik.contains=" + DEFAULT_NIK);

        // Get all the pendaftaranDumyList where nik contains UPDATED_NIK
        defaultPendaftaranDumyShouldNotBeFound("nik.contains=" + UPDATED_NIK);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByNikNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where nik does not contain DEFAULT_NIK
        defaultPendaftaranDumyShouldNotBeFound("nik.doesNotContain=" + DEFAULT_NIK);

        // Get all the pendaftaranDumyList where nik does not contain UPDATED_NIK
        defaultPendaftaranDumyShouldBeFound("nik.doesNotContain=" + UPDATED_NIK);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned equals to DEFAULT_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.equals=" + DEFAULT_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned equals to UPDATED_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.equals=" + UPDATED_FLAG_BANNED);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned not equals to DEFAULT_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.notEquals=" + DEFAULT_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned not equals to UPDATED_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.notEquals=" + UPDATED_FLAG_BANNED);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned in DEFAULT_FLAG_BANNED or UPDATED_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.in=" + DEFAULT_FLAG_BANNED + "," + UPDATED_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned equals to UPDATED_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.in=" + UPDATED_FLAG_BANNED);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned is not null
        defaultPendaftaranDumyShouldBeFound("flagBanned.specified=true");

        // Get all the pendaftaranDumyList where flagBanned is null
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned is greater than or equal to DEFAULT_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.greaterThanOrEqual=" + DEFAULT_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned is greater than or equal to UPDATED_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.greaterThanOrEqual=" + UPDATED_FLAG_BANNED);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned is less than or equal to DEFAULT_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.lessThanOrEqual=" + DEFAULT_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned is less than or equal to SMALLER_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.lessThanOrEqual=" + SMALLER_FLAG_BANNED);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsLessThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned is less than DEFAULT_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.lessThan=" + DEFAULT_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned is less than UPDATED_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.lessThan=" + UPDATED_FLAG_BANNED);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByFlagBannedIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where flagBanned is greater than DEFAULT_FLAG_BANNED
        defaultPendaftaranDumyShouldNotBeFound("flagBanned.greaterThan=" + DEFAULT_FLAG_BANNED);

        // Get all the pendaftaranDumyList where flagBanned is greater than SMALLER_FLAG_BANNED
        defaultPendaftaranDumyShouldBeFound("flagBanned.greaterThan=" + SMALLER_FLAG_BANNED);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan equals to DEFAULT_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.equals=" + DEFAULT_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan equals to UPDATED_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.equals=" + UPDATED_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan not equals to DEFAULT_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.notEquals=" + DEFAULT_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan not equals to UPDATED_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.notEquals=" + UPDATED_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan in DEFAULT_ID_SUB_LAYANAN or UPDATED_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.in=" + DEFAULT_ID_SUB_LAYANAN + "," + UPDATED_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan equals to UPDATED_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.in=" + UPDATED_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan is not null
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.specified=true");

        // Get all the pendaftaranDumyList where idSubLayanan is null
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan is greater than or equal to DEFAULT_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.greaterThanOrEqual=" + DEFAULT_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan is greater than or equal to UPDATED_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.greaterThanOrEqual=" + UPDATED_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan is less than or equal to DEFAULT_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.lessThanOrEqual=" + DEFAULT_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan is less than or equal to SMALLER_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.lessThanOrEqual=" + SMALLER_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsLessThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan is less than DEFAULT_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.lessThan=" + DEFAULT_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan is less than UPDATED_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.lessThan=" + UPDATED_ID_SUB_LAYANAN);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByIdSubLayananIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where idSubLayanan is greater than DEFAULT_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldNotBeFound("idSubLayanan.greaterThan=" + DEFAULT_ID_SUB_LAYANAN);

        // Get all the pendaftaranDumyList where idSubLayanan is greater than SMALLER_ID_SUB_LAYANAN
        defaultPendaftaranDumyShouldBeFound("idSubLayanan.greaterThan=" + SMALLER_ID_SUB_LAYANAN);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJenisKendalaIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jenisKendala equals to DEFAULT_JENIS_KENDALA
        defaultPendaftaranDumyShouldBeFound("jenisKendala.equals=" + DEFAULT_JENIS_KENDALA);

        // Get all the pendaftaranDumyList where jenisKendala equals to UPDATED_JENIS_KENDALA
        defaultPendaftaranDumyShouldNotBeFound("jenisKendala.equals=" + UPDATED_JENIS_KENDALA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJenisKendalaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jenisKendala not equals to DEFAULT_JENIS_KENDALA
        defaultPendaftaranDumyShouldNotBeFound("jenisKendala.notEquals=" + DEFAULT_JENIS_KENDALA);

        // Get all the pendaftaranDumyList where jenisKendala not equals to UPDATED_JENIS_KENDALA
        defaultPendaftaranDumyShouldBeFound("jenisKendala.notEquals=" + UPDATED_JENIS_KENDALA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJenisKendalaIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jenisKendala in DEFAULT_JENIS_KENDALA or UPDATED_JENIS_KENDALA
        defaultPendaftaranDumyShouldBeFound("jenisKendala.in=" + DEFAULT_JENIS_KENDALA + "," + UPDATED_JENIS_KENDALA);

        // Get all the pendaftaranDumyList where jenisKendala equals to UPDATED_JENIS_KENDALA
        defaultPendaftaranDumyShouldNotBeFound("jenisKendala.in=" + UPDATED_JENIS_KENDALA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJenisKendalaIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jenisKendala is not null
        defaultPendaftaranDumyShouldBeFound("jenisKendala.specified=true");

        // Get all the pendaftaranDumyList where jenisKendala is null
        defaultPendaftaranDumyShouldNotBeFound("jenisKendala.specified=false");
    }
                @Test
    @Transactional
    public void getAllPendaftaranDumiesByJenisKendalaContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jenisKendala contains DEFAULT_JENIS_KENDALA
        defaultPendaftaranDumyShouldBeFound("jenisKendala.contains=" + DEFAULT_JENIS_KENDALA);

        // Get all the pendaftaranDumyList where jenisKendala contains UPDATED_JENIS_KENDALA
        defaultPendaftaranDumyShouldNotBeFound("jenisKendala.contains=" + UPDATED_JENIS_KENDALA);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByJenisKendalaNotContainsSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where jenisKendala does not contain DEFAULT_JENIS_KENDALA
        defaultPendaftaranDumyShouldNotBeFound("jenisKendala.doesNotContain=" + DEFAULT_JENIS_KENDALA);

        // Get all the pendaftaranDumyList where jenisKendala does not contain UPDATED_JENIS_KENDALA
        defaultPendaftaranDumyShouldBeFound("jenisKendala.doesNotContain=" + UPDATED_JENIS_KENDALA);
    }


    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate equals to DEFAULT_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.equals=" + DEFAULT_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate equals to UPDATED_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.equals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate not equals to DEFAULT_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.notEquals=" + DEFAULT_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate not equals to UPDATED_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.notEquals=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsInShouldWork() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate in DEFAULT_CREATE_DATE or UPDATED_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.in=" + DEFAULT_CREATE_DATE + "," + UPDATED_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate equals to UPDATED_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.in=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate is not null
        defaultPendaftaranDumyShouldBeFound("createDate.specified=true");

        // Get all the pendaftaranDumyList where createDate is null
        defaultPendaftaranDumyShouldNotBeFound("createDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate is greater than or equal to DEFAULT_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.greaterThanOrEqual=" + DEFAULT_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate is greater than or equal to UPDATED_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.greaterThanOrEqual=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate is less than or equal to DEFAULT_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.lessThanOrEqual=" + DEFAULT_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate is less than or equal to SMALLER_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.lessThanOrEqual=" + SMALLER_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsLessThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate is less than DEFAULT_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.lessThan=" + DEFAULT_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate is less than UPDATED_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.lessThan=" + UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void getAllPendaftaranDumiesByCreateDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        // Get all the pendaftaranDumyList where createDate is greater than DEFAULT_CREATE_DATE
        defaultPendaftaranDumyShouldNotBeFound("createDate.greaterThan=" + DEFAULT_CREATE_DATE);

        // Get all the pendaftaranDumyList where createDate is greater than SMALLER_CREATE_DATE
        defaultPendaftaranDumyShouldBeFound("createDate.greaterThan=" + SMALLER_CREATE_DATE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPendaftaranDumyShouldBeFound(String filter) throws Exception {
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pendaftaranDumy.getId().intValue())))
            .andExpect(jsonPath("$.[*].namaBadanUsaha").value(hasItem(DEFAULT_NAMA_BADAN_USAHA)))
            .andExpect(jsonPath("$.[*].namaDireksi").value(hasItem(DEFAULT_NAMA_DIREKSI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].tanggalPengambilan").value(hasItem(DEFAULT_TANGGAL_PENGAMBILAN.toString())))
            .andExpect(jsonPath("$.[*].kodeKonfirmasi").value(hasItem(DEFAULT_KODE_KONFIRMASI)))
            .andExpect(jsonPath("$.[*].idKloter").value(hasItem(DEFAULT_ID_KLOTER.intValue())))
            .andExpect(jsonPath("$.[*].pengambilanStruk").value(hasItem(DEFAULT_PENGAMBILAN_STRUK.booleanValue())))
            .andExpect(jsonPath("$.[*].jamKloter").value(hasItem(DEFAULT_JAM_KLOTER)))
            .andExpect(jsonPath("$.[*].pertanyaan").value(hasItem(DEFAULT_PERTANYAAN.toString())))
            .andExpect(jsonPath("$.[*].jawaban").value(hasItem(DEFAULT_JAWABAN.toString())))
            .andExpect(jsonPath("$.[*].tipeKtp").value(hasItem(DEFAULT_TIPE_KTP)))
            .andExpect(jsonPath("$.[*].statusTiket").value(hasItem(DEFAULT_STATUS_TIKET)))
            .andExpect(jsonPath("$.[*].typeVirtual").value(hasItem(DEFAULT_TYPE_VIRTUAL.booleanValue())))
            .andExpect(jsonPath("$.[*].jamLayanan").value(hasItem(DEFAULT_JAM_LAYANAN)))
            .andExpect(jsonPath("$.[*].noTiket").value(hasItem(DEFAULT_NO_TIKET)))
            .andExpect(jsonPath("$.[*].agent").value(hasItem(DEFAULT_AGENT.booleanValue())))
            .andExpect(jsonPath("$.[*].idDaftar").value(hasItem(DEFAULT_ID_DAFTAR.toString())))
            .andExpect(jsonPath("$.[*].namaPerseorangan").value(hasItem(DEFAULT_NAMA_PERSEORANGAN)))
            .andExpect(jsonPath("$.[*].instansi").value(hasItem(DEFAULT_INSTANSI)))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].jabatan").value(hasItem(DEFAULT_JABATAN)))
            .andExpect(jsonPath("$.[*].namaPejabat").value(hasItem(DEFAULT_NAMA_PEJABAT)))
            .andExpect(jsonPath("$.[*].untukLayanan").value(hasItem(DEFAULT_UNTUK_LAYANAN)))
            .andExpect(jsonPath("$.[*].jwtToken").value(hasItem(DEFAULT_JWT_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].nik").value(hasItem(DEFAULT_NIK)))
            .andExpect(jsonPath("$.[*].flagBanned").value(hasItem(DEFAULT_FLAG_BANNED.intValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].jenisKendala").value(hasItem(DEFAULT_JENIS_KENDALA)))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())));

        // Check, that the count call also returns 1
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPendaftaranDumyShouldNotBeFound(String filter) throws Exception {
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPendaftaranDumy() throws Exception {
        // Get the pendaftaranDumy
        restPendaftaranDumyMockMvc.perform(get("/api/pendaftaran-dumies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePendaftaranDumy() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        int databaseSizeBeforeUpdate = pendaftaranDumyRepository.findAll().size();

        // Update the pendaftaranDumy
        PendaftaranDumy updatedPendaftaranDumy = pendaftaranDumyRepository.findById(pendaftaranDumy.getId()).get();
        // Disconnect from session so that the updates on updatedPendaftaranDumy are not directly saved in db
        em.detach(updatedPendaftaranDumy);
        updatedPendaftaranDumy
            .namaBadanUsaha(UPDATED_NAMA_BADAN_USAHA)
            .namaDireksi(UPDATED_NAMA_DIREKSI)
            .email(UPDATED_EMAIL)
            .tanggalPengambilan(UPDATED_TANGGAL_PENGAMBILAN)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .idKloter(UPDATED_ID_KLOTER)
            .pengambilanStruk(UPDATED_PENGAMBILAN_STRUK)
            .jamKloter(UPDATED_JAM_KLOTER)
            .pertanyaan(UPDATED_PERTANYAAN)
            .jawaban(UPDATED_JAWABAN)
            .tipeKtp(UPDATED_TIPE_KTP)
            .statusTiket(UPDATED_STATUS_TIKET)
            .typeVirtual(UPDATED_TYPE_VIRTUAL)
            .jamLayanan(UPDATED_JAM_LAYANAN)
            .noTiket(UPDATED_NO_TIKET)
            .agent(UPDATED_AGENT)
            .idDaftar(UPDATED_ID_DAFTAR)
            .namaPerseorangan(UPDATED_NAMA_PERSEORANGAN)
            .instansi(UPDATED_INSTANSI)
            .unit(UPDATED_UNIT)
            .jabatan(UPDATED_JABATAN)
            .namaPejabat(UPDATED_NAMA_PEJABAT)
            .untukLayanan(UPDATED_UNTUK_LAYANAN)
            .jwtToken(UPDATED_JWT_TOKEN)
            .nik(UPDATED_NIK)
            .flagBanned(UPDATED_FLAG_BANNED)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .jenisKendala(UPDATED_JENIS_KENDALA)
            .createDate(UPDATED_CREATE_DATE);
        PendaftaranDumyDTO pendaftaranDumyDTO = pendaftaranDumyMapper.toDto(updatedPendaftaranDumy);

        restPendaftaranDumyMockMvc.perform(put("/api/pendaftaran-dumies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDumyDTO)))
            .andExpect(status().isOk());

        // Validate the PendaftaranDumy in the database
        List<PendaftaranDumy> pendaftaranDumyList = pendaftaranDumyRepository.findAll();
        assertThat(pendaftaranDumyList).hasSize(databaseSizeBeforeUpdate);
        PendaftaranDumy testPendaftaranDumy = pendaftaranDumyList.get(pendaftaranDumyList.size() - 1);
        assertThat(testPendaftaranDumy.getNamaBadanUsaha()).isEqualTo(UPDATED_NAMA_BADAN_USAHA);
        assertThat(testPendaftaranDumy.getNamaDireksi()).isEqualTo(UPDATED_NAMA_DIREKSI);
        assertThat(testPendaftaranDumy.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPendaftaranDumy.getTanggalPengambilan()).isEqualTo(UPDATED_TANGGAL_PENGAMBILAN);
        assertThat(testPendaftaranDumy.getKodeKonfirmasi()).isEqualTo(UPDATED_KODE_KONFIRMASI);
        assertThat(testPendaftaranDumy.getIdKloter()).isEqualTo(UPDATED_ID_KLOTER);
        assertThat(testPendaftaranDumy.isPengambilanStruk()).isEqualTo(UPDATED_PENGAMBILAN_STRUK);
        assertThat(testPendaftaranDumy.getJamKloter()).isEqualTo(UPDATED_JAM_KLOTER);
        assertThat(testPendaftaranDumy.getPertanyaan()).isEqualTo(UPDATED_PERTANYAAN);
        assertThat(testPendaftaranDumy.getJawaban()).isEqualTo(UPDATED_JAWABAN);
        assertThat(testPendaftaranDumy.getTipeKtp()).isEqualTo(UPDATED_TIPE_KTP);
        assertThat(testPendaftaranDumy.getStatusTiket()).isEqualTo(UPDATED_STATUS_TIKET);
        assertThat(testPendaftaranDumy.isTypeVirtual()).isEqualTo(UPDATED_TYPE_VIRTUAL);
        assertThat(testPendaftaranDumy.getJamLayanan()).isEqualTo(UPDATED_JAM_LAYANAN);
        assertThat(testPendaftaranDumy.getNoTiket()).isEqualTo(UPDATED_NO_TIKET);
        assertThat(testPendaftaranDumy.isAgent()).isEqualTo(UPDATED_AGENT);
        assertThat(testPendaftaranDumy.getIdDaftar()).isEqualTo(UPDATED_ID_DAFTAR);
        assertThat(testPendaftaranDumy.getNamaPerseorangan()).isEqualTo(UPDATED_NAMA_PERSEORANGAN);
        assertThat(testPendaftaranDumy.getInstansi()).isEqualTo(UPDATED_INSTANSI);
        assertThat(testPendaftaranDumy.getUnit()).isEqualTo(UPDATED_UNIT);
        assertThat(testPendaftaranDumy.getJabatan()).isEqualTo(UPDATED_JABATAN);
        assertThat(testPendaftaranDumy.getNamaPejabat()).isEqualTo(UPDATED_NAMA_PEJABAT);
        assertThat(testPendaftaranDumy.getUntukLayanan()).isEqualTo(UPDATED_UNTUK_LAYANAN);
        assertThat(testPendaftaranDumy.getJwtToken()).isEqualTo(UPDATED_JWT_TOKEN);
        assertThat(testPendaftaranDumy.getNik()).isEqualTo(UPDATED_NIK);
        assertThat(testPendaftaranDumy.getFlagBanned()).isEqualTo(UPDATED_FLAG_BANNED);
        assertThat(testPendaftaranDumy.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testPendaftaranDumy.getJenisKendala()).isEqualTo(UPDATED_JENIS_KENDALA);
        assertThat(testPendaftaranDumy.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPendaftaranDumy() throws Exception {
        int databaseSizeBeforeUpdate = pendaftaranDumyRepository.findAll().size();

        // Create the PendaftaranDumy
        PendaftaranDumyDTO pendaftaranDumyDTO = pendaftaranDumyMapper.toDto(pendaftaranDumy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPendaftaranDumyMockMvc.perform(put("/api/pendaftaran-dumies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDumyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PendaftaranDumy in the database
        List<PendaftaranDumy> pendaftaranDumyList = pendaftaranDumyRepository.findAll();
        assertThat(pendaftaranDumyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePendaftaranDumy() throws Exception {
        // Initialize the database
        pendaftaranDumyRepository.saveAndFlush(pendaftaranDumy);

        int databaseSizeBeforeDelete = pendaftaranDumyRepository.findAll().size();

        // Delete the pendaftaranDumy
        restPendaftaranDumyMockMvc.perform(delete("/api/pendaftaran-dumies/{id}", pendaftaranDumy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PendaftaranDumy> pendaftaranDumyList = pendaftaranDumyRepository.findAll();
        assertThat(pendaftaranDumyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
