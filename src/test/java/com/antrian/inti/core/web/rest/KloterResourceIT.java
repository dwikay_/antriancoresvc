package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Kloter;
import com.antrian.inti.core.repository.KloterRepository;
import com.antrian.inti.core.service.KloterService;
import com.antrian.inti.core.service.dto.KloterDTO;
import com.antrian.inti.core.service.mapper.KloterMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KloterResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class KloterResourceIT {

    private static final String DEFAULT_JAM_MULAI = "AAAAAAAAAA";
    private static final String UPDATED_JAM_MULAI = "BBBBBBBBBB";

    private static final String DEFAULT_JAM_AKHIR = "AAAAAAAAAA";
    private static final String UPDATED_JAM_AKHIR = "BBBBBBBBBB";

    private static final String DEFAULT_QUOTA = "AAAAAAAAAA";
    private static final String UPDATED_QUOTA = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_KLOTER = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_KLOTER = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    @Autowired
    private KloterRepository kloterRepository;

    @Autowired
    private KloterMapper kloterMapper;

    @Autowired
    private KloterService kloterService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restKloterMockMvc;

    private Kloter kloter;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KloterResource kloterResource = new KloterResource(kloterService);
        this.restKloterMockMvc = MockMvcBuilders.standaloneSetup(kloterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kloter createEntity(EntityManager em) {
        Kloter kloter = new Kloter()
            .jamMulai(DEFAULT_JAM_MULAI)
            .jamAkhir(DEFAULT_JAM_AKHIR)
            .quota(DEFAULT_QUOTA)
            .namaKloter(DEFAULT_NAMA_KLOTER)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .createdDate(DEFAULT_CREATED_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID);
        return kloter;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kloter createUpdatedEntity(EntityManager em) {
        Kloter kloter = new Kloter()
            .jamMulai(UPDATED_JAM_MULAI)
            .jamAkhir(UPDATED_JAM_AKHIR)
            .quota(UPDATED_QUOTA)
            .namaKloter(UPDATED_NAMA_KLOTER)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .createdDate(UPDATED_CREATED_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .createUserId(UPDATED_CREATE_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID);
        return kloter;
    }

    @BeforeEach
    public void initTest() {
        kloter = createEntity(em);
    }

    @Test
    @Transactional
    public void createKloter() throws Exception {
        int databaseSizeBeforeCreate = kloterRepository.findAll().size();

        // Create the Kloter
        KloterDTO kloterDTO = kloterMapper.toDto(kloter);
        restKloterMockMvc.perform(post("/api/kloters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kloterDTO)))
            .andExpect(status().isCreated());

        // Validate the Kloter in the database
        List<Kloter> kloterList = kloterRepository.findAll();
        assertThat(kloterList).hasSize(databaseSizeBeforeCreate + 1);
        Kloter testKloter = kloterList.get(kloterList.size() - 1);
        assertThat(testKloter.getJamMulai()).isEqualTo(DEFAULT_JAM_MULAI);
        assertThat(testKloter.getJamAkhir()).isEqualTo(DEFAULT_JAM_AKHIR);
        assertThat(testKloter.getQuota()).isEqualTo(DEFAULT_QUOTA);
        assertThat(testKloter.getNamaKloter()).isEqualTo(DEFAULT_NAMA_KLOTER);
        assertThat(testKloter.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testKloter.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testKloter.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testKloter.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testKloter.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testKloter.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        assertThat(testKloter.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
    }

    @Test
    @Transactional
    public void createKloterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kloterRepository.findAll().size();

        // Create the Kloter with an existing ID
        kloter.setId(1L);
        KloterDTO kloterDTO = kloterMapper.toDto(kloter);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKloterMockMvc.perform(post("/api/kloters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kloterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kloter in the database
        List<Kloter> kloterList = kloterRepository.findAll();
        assertThat(kloterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllKloters() throws Exception {
        // Initialize the database
        kloterRepository.saveAndFlush(kloter);

        // Get all the kloterList
        restKloterMockMvc.perform(get("/api/kloters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kloter.getId().intValue())))
            .andExpect(jsonPath("$.[*].jamMulai").value(hasItem(DEFAULT_JAM_MULAI)))
            .andExpect(jsonPath("$.[*].jamAkhir").value(hasItem(DEFAULT_JAM_AKHIR)))
            .andExpect(jsonPath("$.[*].quota").value(hasItem(DEFAULT_QUOTA)))
            .andExpect(jsonPath("$.[*].namaKloter").value(hasItem(DEFAULT_NAMA_KLOTER)))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())));
    }

    @Test
    @Transactional
    public void getKloter() throws Exception {
        // Initialize the database
        kloterRepository.saveAndFlush(kloter);

        // Get the kloter
        restKloterMockMvc.perform(get("/api/kloters/{id}", kloter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kloter.getId().intValue()))
            .andExpect(jsonPath("$.jamMulai").value(DEFAULT_JAM_MULAI))
            .andExpect(jsonPath("$.jamAkhir").value(DEFAULT_JAM_AKHIR))
            .andExpect(jsonPath("$.quota").value(DEFAULT_QUOTA))
            .andExpect(jsonPath("$.namaKloter").value(DEFAULT_NAMA_KLOTER))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingKloter() throws Exception {
        // Get the kloter
        restKloterMockMvc.perform(get("/api/kloters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKloter() throws Exception {
        // Initialize the database
        kloterRepository.saveAndFlush(kloter);

        int databaseSizeBeforeUpdate = kloterRepository.findAll().size();

        // Update the kloter
        Kloter updatedKloter = kloterRepository.findById(kloter.getId()).get();
        // Disconnect from session so that the updates on updatedKloter are not directly saved in db
        em.detach(updatedKloter);
        updatedKloter
            .jamMulai(UPDATED_JAM_MULAI)
            .jamAkhir(UPDATED_JAM_AKHIR)
            .quota(UPDATED_QUOTA)
            .namaKloter(UPDATED_NAMA_KLOTER)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .createdDate(UPDATED_CREATED_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .createUserId(UPDATED_CREATE_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID);
        KloterDTO kloterDTO = kloterMapper.toDto(updatedKloter);

        restKloterMockMvc.perform(put("/api/kloters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kloterDTO)))
            .andExpect(status().isOk());

        // Validate the Kloter in the database
        List<Kloter> kloterList = kloterRepository.findAll();
        assertThat(kloterList).hasSize(databaseSizeBeforeUpdate);
        Kloter testKloter = kloterList.get(kloterList.size() - 1);
        assertThat(testKloter.getJamMulai()).isEqualTo(UPDATED_JAM_MULAI);
        assertThat(testKloter.getJamAkhir()).isEqualTo(UPDATED_JAM_AKHIR);
        assertThat(testKloter.getQuota()).isEqualTo(UPDATED_QUOTA);
        assertThat(testKloter.getNamaKloter()).isEqualTo(UPDATED_NAMA_KLOTER);
        assertThat(testKloter.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testKloter.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testKloter.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testKloter.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testKloter.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testKloter.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
        assertThat(testKloter.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingKloter() throws Exception {
        int databaseSizeBeforeUpdate = kloterRepository.findAll().size();

        // Create the Kloter
        KloterDTO kloterDTO = kloterMapper.toDto(kloter);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKloterMockMvc.perform(put("/api/kloters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kloterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kloter in the database
        List<Kloter> kloterList = kloterRepository.findAll();
        assertThat(kloterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKloter() throws Exception {
        // Initialize the database
        kloterRepository.saveAndFlush(kloter);

        int databaseSizeBeforeDelete = kloterRepository.findAll().size();

        // Delete the kloter
        restKloterMockMvc.perform(delete("/api/kloters/{id}", kloter.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kloter> kloterList = kloterRepository.findAll();
        assertThat(kloterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
