package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Prosedur;
import com.antrian.inti.core.repository.ProsedurRepository;
import com.antrian.inti.core.service.ProsedurService;
import com.antrian.inti.core.service.dto.ProsedurDTO;
import com.antrian.inti.core.service.mapper.ProsedurMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProsedurResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class ProsedurResourceIT {

    private static final String DEFAULT_TATA_CARA = "AAAAAAAAAA";
    private static final String UPDATED_TATA_CARA = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PROSEDUR = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PROSEDUR = "BBBBBBBBBB";

    @Autowired
    private ProsedurRepository prosedurRepository;

    @Autowired
    private ProsedurMapper prosedurMapper;

    @Autowired
    private ProsedurService prosedurService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProsedurMockMvc;

    private Prosedur prosedur;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProsedurResource prosedurResource = new ProsedurResource(prosedurService);
        this.restProsedurMockMvc = MockMvcBuilders.standaloneSetup(prosedurResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prosedur createEntity(EntityManager em) {
        Prosedur prosedur = new Prosedur()
            .tataCara(DEFAULT_TATA_CARA)
            .namaProsedur(DEFAULT_NAMA_PROSEDUR);
        return prosedur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Prosedur createUpdatedEntity(EntityManager em) {
        Prosedur prosedur = new Prosedur()
            .tataCara(UPDATED_TATA_CARA)
            .namaProsedur(UPDATED_NAMA_PROSEDUR);
        return prosedur;
    }

    @BeforeEach
    public void initTest() {
        prosedur = createEntity(em);
    }

    @Test
    @Transactional
    public void createProsedur() throws Exception {
        int databaseSizeBeforeCreate = prosedurRepository.findAll().size();

        // Create the Prosedur
        ProsedurDTO prosedurDTO = prosedurMapper.toDto(prosedur);
        restProsedurMockMvc.perform(post("/api/prosedurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prosedurDTO)))
            .andExpect(status().isCreated());

        // Validate the Prosedur in the database
        List<Prosedur> prosedurList = prosedurRepository.findAll();
        assertThat(prosedurList).hasSize(databaseSizeBeforeCreate + 1);
        Prosedur testProsedur = prosedurList.get(prosedurList.size() - 1);
        assertThat(testProsedur.getTataCara()).isEqualTo(DEFAULT_TATA_CARA);
        assertThat(testProsedur.getNamaProsedur()).isEqualTo(DEFAULT_NAMA_PROSEDUR);
    }

    @Test
    @Transactional
    public void createProsedurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = prosedurRepository.findAll().size();

        // Create the Prosedur with an existing ID
        prosedur.setId(1L);
        ProsedurDTO prosedurDTO = prosedurMapper.toDto(prosedur);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProsedurMockMvc.perform(post("/api/prosedurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prosedurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Prosedur in the database
        List<Prosedur> prosedurList = prosedurRepository.findAll();
        assertThat(prosedurList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProsedurs() throws Exception {
        // Initialize the database
        prosedurRepository.saveAndFlush(prosedur);

        // Get all the prosedurList
        restProsedurMockMvc.perform(get("/api/prosedurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prosedur.getId().intValue())))
            .andExpect(jsonPath("$.[*].tataCara").value(hasItem(DEFAULT_TATA_CARA.toString())))
            .andExpect(jsonPath("$.[*].namaProsedur").value(hasItem(DEFAULT_NAMA_PROSEDUR)));
    }
    
    @Test
    @Transactional
    public void getProsedur() throws Exception {
        // Initialize the database
        prosedurRepository.saveAndFlush(prosedur);

        // Get the prosedur
        restProsedurMockMvc.perform(get("/api/prosedurs/{id}", prosedur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(prosedur.getId().intValue()))
            .andExpect(jsonPath("$.tataCara").value(DEFAULT_TATA_CARA.toString()))
            .andExpect(jsonPath("$.namaProsedur").value(DEFAULT_NAMA_PROSEDUR));
    }

    @Test
    @Transactional
    public void getNonExistingProsedur() throws Exception {
        // Get the prosedur
        restProsedurMockMvc.perform(get("/api/prosedurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProsedur() throws Exception {
        // Initialize the database
        prosedurRepository.saveAndFlush(prosedur);

        int databaseSizeBeforeUpdate = prosedurRepository.findAll().size();

        // Update the prosedur
        Prosedur updatedProsedur = prosedurRepository.findById(prosedur.getId()).get();
        // Disconnect from session so that the updates on updatedProsedur are not directly saved in db
        em.detach(updatedProsedur);
        updatedProsedur
            .tataCara(UPDATED_TATA_CARA)
            .namaProsedur(UPDATED_NAMA_PROSEDUR);
        ProsedurDTO prosedurDTO = prosedurMapper.toDto(updatedProsedur);

        restProsedurMockMvc.perform(put("/api/prosedurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prosedurDTO)))
            .andExpect(status().isOk());

        // Validate the Prosedur in the database
        List<Prosedur> prosedurList = prosedurRepository.findAll();
        assertThat(prosedurList).hasSize(databaseSizeBeforeUpdate);
        Prosedur testProsedur = prosedurList.get(prosedurList.size() - 1);
        assertThat(testProsedur.getTataCara()).isEqualTo(UPDATED_TATA_CARA);
        assertThat(testProsedur.getNamaProsedur()).isEqualTo(UPDATED_NAMA_PROSEDUR);
    }

    @Test
    @Transactional
    public void updateNonExistingProsedur() throws Exception {
        int databaseSizeBeforeUpdate = prosedurRepository.findAll().size();

        // Create the Prosedur
        ProsedurDTO prosedurDTO = prosedurMapper.toDto(prosedur);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProsedurMockMvc.perform(put("/api/prosedurs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(prosedurDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Prosedur in the database
        List<Prosedur> prosedurList = prosedurRepository.findAll();
        assertThat(prosedurList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProsedur() throws Exception {
        // Initialize the database
        prosedurRepository.saveAndFlush(prosedur);

        int databaseSizeBeforeDelete = prosedurRepository.findAll().size();

        // Delete the prosedur
        restProsedurMockMvc.perform(delete("/api/prosedurs/{id}", prosedur.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Prosedur> prosedurList = prosedurRepository.findAll();
        assertThat(prosedurList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
