package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.DigitalSignage;
import com.antrian.inti.core.repository.DigitalSignageRepository;
import com.antrian.inti.core.service.DigitalSignageService;
import com.antrian.inti.core.service.dto.DigitalSignageDTO;
import com.antrian.inti.core.service.mapper.DigitalSignageMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DigitalSignageResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class DigitalSignageResourceIT {

    private static final String DEFAULT_NAMA_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_DEVICE = "AAAAAAAAAA";
    private static final String UPDATED_DEVICE = "BBBBBBBBBB";

    private static final String DEFAULT_PATH_FILE = "AAAAAAAAAA";
    private static final String UPDATED_PATH_FILE = "BBBBBBBBBB";

    @Autowired
    private DigitalSignageRepository digitalSignageRepository;

    @Autowired
    private DigitalSignageMapper digitalSignageMapper;

    @Autowired
    private DigitalSignageService digitalSignageService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDigitalSignageMockMvc;

    private DigitalSignage digitalSignage;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DigitalSignageResource digitalSignageResource = new DigitalSignageResource(digitalSignageService);
        this.restDigitalSignageMockMvc = MockMvcBuilders.standaloneSetup(digitalSignageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DigitalSignage createEntity(EntityManager em) {
        DigitalSignage digitalSignage = new DigitalSignage()
            .namaVideo(DEFAULT_NAMA_VIDEO)
            .status(DEFAULT_STATUS)
            .device(DEFAULT_DEVICE)
            .pathFile(DEFAULT_PATH_FILE);
        return digitalSignage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DigitalSignage createUpdatedEntity(EntityManager em) {
        DigitalSignage digitalSignage = new DigitalSignage()
            .namaVideo(UPDATED_NAMA_VIDEO)
            .status(UPDATED_STATUS)
            .device(UPDATED_DEVICE)
            .pathFile(UPDATED_PATH_FILE);
        return digitalSignage;
    }

    @BeforeEach
    public void initTest() {
        digitalSignage = createEntity(em);
    }

    @Test
    @Transactional
    public void createDigitalSignage() throws Exception {
        int databaseSizeBeforeCreate = digitalSignageRepository.findAll().size();

        // Create the DigitalSignage
        DigitalSignageDTO digitalSignageDTO = digitalSignageMapper.toDto(digitalSignage);
        restDigitalSignageMockMvc.perform(post("/api/digital-signages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(digitalSignageDTO)))
            .andExpect(status().isCreated());

        // Validate the DigitalSignage in the database
        List<DigitalSignage> digitalSignageList = digitalSignageRepository.findAll();
        assertThat(digitalSignageList).hasSize(databaseSizeBeforeCreate + 1);
        DigitalSignage testDigitalSignage = digitalSignageList.get(digitalSignageList.size() - 1);
        assertThat(testDigitalSignage.getNamaVideo()).isEqualTo(DEFAULT_NAMA_VIDEO);
        assertThat(testDigitalSignage.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDigitalSignage.getDevice()).isEqualTo(DEFAULT_DEVICE);
        assertThat(testDigitalSignage.getPathFile()).isEqualTo(DEFAULT_PATH_FILE);
    }

    @Test
    @Transactional
    public void createDigitalSignageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = digitalSignageRepository.findAll().size();

        // Create the DigitalSignage with an existing ID
        digitalSignage.setId(1L);
        DigitalSignageDTO digitalSignageDTO = digitalSignageMapper.toDto(digitalSignage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDigitalSignageMockMvc.perform(post("/api/digital-signages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(digitalSignageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DigitalSignage in the database
        List<DigitalSignage> digitalSignageList = digitalSignageRepository.findAll();
        assertThat(digitalSignageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDigitalSignages() throws Exception {
        // Initialize the database
        digitalSignageRepository.saveAndFlush(digitalSignage);

        // Get all the digitalSignageList
        restDigitalSignageMockMvc.perform(get("/api/digital-signages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(digitalSignage.getId().intValue())))
            .andExpect(jsonPath("$.[*].namaVideo").value(hasItem(DEFAULT_NAMA_VIDEO)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].device").value(hasItem(DEFAULT_DEVICE)))
            .andExpect(jsonPath("$.[*].pathFile").value(hasItem(DEFAULT_PATH_FILE)));
    }

    @Test
    @Transactional
    public void getDigitalSignage() throws Exception {
        // Initialize the database
        digitalSignageRepository.saveAndFlush(digitalSignage);

        // Get the digitalSignage
        restDigitalSignageMockMvc.perform(get("/api/digital-signages/{id}", digitalSignage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(digitalSignage.getId().intValue()))
            .andExpect(jsonPath("$.namaVideo").value(DEFAULT_NAMA_VIDEO))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.device").value(DEFAULT_DEVICE))
            .andExpect(jsonPath("$.pathFile").value(DEFAULT_PATH_FILE));
    }

    @Test
    @Transactional
    public void getNonExistingDigitalSignage() throws Exception {
        // Get the digitalSignage
        restDigitalSignageMockMvc.perform(get("/api/digital-signages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDigitalSignage() throws Exception {
        // Initialize the database
        digitalSignageRepository.saveAndFlush(digitalSignage);

        int databaseSizeBeforeUpdate = digitalSignageRepository.findAll().size();

        // Update the digitalSignage
        DigitalSignage updatedDigitalSignage = digitalSignageRepository.findById(digitalSignage.getId()).get();
        // Disconnect from session so that the updates on updatedDigitalSignage are not directly saved in db
        em.detach(updatedDigitalSignage);
        updatedDigitalSignage
            .namaVideo(UPDATED_NAMA_VIDEO)
            .status(UPDATED_STATUS)
            .device(UPDATED_DEVICE)
            .pathFile(UPDATED_PATH_FILE);
        DigitalSignageDTO digitalSignageDTO = digitalSignageMapper.toDto(updatedDigitalSignage);

        restDigitalSignageMockMvc.perform(put("/api/digital-signages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(digitalSignageDTO)))
            .andExpect(status().isOk());

        // Validate the DigitalSignage in the database
        List<DigitalSignage> digitalSignageList = digitalSignageRepository.findAll();
        assertThat(digitalSignageList).hasSize(databaseSizeBeforeUpdate);
        DigitalSignage testDigitalSignage = digitalSignageList.get(digitalSignageList.size() - 1);
        assertThat(testDigitalSignage.getNamaVideo()).isEqualTo(UPDATED_NAMA_VIDEO);
        assertThat(testDigitalSignage.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDigitalSignage.getDevice()).isEqualTo(UPDATED_DEVICE);
        assertThat(testDigitalSignage.getPathFile()).isEqualTo(UPDATED_PATH_FILE);
    }

    @Test
    @Transactional
    public void updateNonExistingDigitalSignage() throws Exception {
        int databaseSizeBeforeUpdate = digitalSignageRepository.findAll().size();

        // Create the DigitalSignage
        DigitalSignageDTO digitalSignageDTO = digitalSignageMapper.toDto(digitalSignage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDigitalSignageMockMvc.perform(put("/api/digital-signages")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(digitalSignageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DigitalSignage in the database
        List<DigitalSignage> digitalSignageList = digitalSignageRepository.findAll();
        assertThat(digitalSignageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDigitalSignage() throws Exception {
        // Initialize the database
        digitalSignageRepository.saveAndFlush(digitalSignage);

        int databaseSizeBeforeDelete = digitalSignageRepository.findAll().size();

        // Delete the digitalSignage
        restDigitalSignageMockMvc.perform(delete("/api/digital-signages/{id}", digitalSignage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DigitalSignage> digitalSignageList = digitalSignageRepository.findAll();
        assertThat(digitalSignageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
