package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.TempPanggilan;
import com.antrian.inti.core.repository.TempPanggilanRepository;
import com.antrian.inti.core.service.TempPanggilanService;
import com.antrian.inti.core.service.dto.TempPanggilanDTO;
import com.antrian.inti.core.service.mapper.TempPanggilanMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TempPanggilanResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class TempPanggilanResourceIT {

    private static final UUID DEFAULT_ID_TEMP_PANGGILAN = UUID.randomUUID();
    private static final UUID UPDATED_ID_TEMP_PANGGILAN = UUID.randomUUID();

    private static final String DEFAULT_PANGGILAN = "AAAAAAAAAA";
    private static final String UPDATED_PANGGILAN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final String DEFAULT_NO_URUT = "AAAAAAAAAA";
    private static final String UPDATED_NO_URUT = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final Long DEFAULT_ID_LANTAI = 1L;
    private static final Long UPDATED_ID_LANTAI = 2L;

    @Autowired
    private TempPanggilanRepository tempPanggilanRepository;

    @Autowired
    private TempPanggilanMapper tempPanggilanMapper;

    @Autowired
    private TempPanggilanService tempPanggilanService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTempPanggilanMockMvc;

    private TempPanggilan tempPanggilan;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TempPanggilanResource tempPanggilanResource = new TempPanggilanResource(tempPanggilanService);
        this.restTempPanggilanMockMvc = MockMvcBuilders.standaloneSetup(tempPanggilanResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TempPanggilan createEntity(EntityManager em) {
        TempPanggilan tempPanggilan = new TempPanggilan()
            .idTempPanggilan(DEFAULT_ID_TEMP_PANGGILAN)
            .panggilan(DEFAULT_PANGGILAN)
            .status(DEFAULT_STATUS)
            .noUrut(DEFAULT_NO_URUT)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .idLantai(DEFAULT_ID_LANTAI);
        return tempPanggilan;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TempPanggilan createUpdatedEntity(EntityManager em) {
        TempPanggilan tempPanggilan = new TempPanggilan()
            .idTempPanggilan(UPDATED_ID_TEMP_PANGGILAN)
            .panggilan(UPDATED_PANGGILAN)
            .status(UPDATED_STATUS)
            .noUrut(UPDATED_NO_URUT)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .idLantai(UPDATED_ID_LANTAI);
        return tempPanggilan;
    }

    @BeforeEach
    public void initTest() {
        tempPanggilan = createEntity(em);
    }

    @Test
    @Transactional
    public void createTempPanggilan() throws Exception {
        int databaseSizeBeforeCreate = tempPanggilanRepository.findAll().size();

        // Create the TempPanggilan
        TempPanggilanDTO tempPanggilanDTO = tempPanggilanMapper.toDto(tempPanggilan);
        restTempPanggilanMockMvc.perform(post("/api/temp-panggilans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tempPanggilanDTO)))
            .andExpect(status().isCreated());

        // Validate the TempPanggilan in the database
        List<TempPanggilan> tempPanggilanList = tempPanggilanRepository.findAll();
        assertThat(tempPanggilanList).hasSize(databaseSizeBeforeCreate + 1);
        TempPanggilan testTempPanggilan = tempPanggilanList.get(tempPanggilanList.size() - 1);
        assertThat(testTempPanggilan.getIdTempPanggilan()).isEqualTo(DEFAULT_ID_TEMP_PANGGILAN);
        assertThat(testTempPanggilan.getPanggilan()).isEqualTo(DEFAULT_PANGGILAN);
        assertThat(testTempPanggilan.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTempPanggilan.getNoUrut()).isEqualTo(DEFAULT_NO_URUT);
        assertThat(testTempPanggilan.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testTempPanggilan.getIdLantai()).isEqualTo(DEFAULT_ID_LANTAI);
    }

    @Test
    @Transactional
    public void createTempPanggilanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tempPanggilanRepository.findAll().size();

        // Create the TempPanggilan with an existing ID
        tempPanggilan.setId(1L);
        TempPanggilanDTO tempPanggilanDTO = tempPanggilanMapper.toDto(tempPanggilan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTempPanggilanMockMvc.perform(post("/api/temp-panggilans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tempPanggilanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TempPanggilan in the database
        List<TempPanggilan> tempPanggilanList = tempPanggilanRepository.findAll();
        assertThat(tempPanggilanList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTempPanggilans() throws Exception {
        // Initialize the database
        tempPanggilanRepository.saveAndFlush(tempPanggilan);

        // Get all the tempPanggilanList
        restTempPanggilanMockMvc.perform(get("/api/temp-panggilans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tempPanggilan.getId().intValue())))
            .andExpect(jsonPath("$.[*].idTempPanggilan").value(hasItem(DEFAULT_ID_TEMP_PANGGILAN.toString())))
            .andExpect(jsonPath("$.[*].panggilan").value(hasItem(DEFAULT_PANGGILAN)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].noUrut").value(hasItem(DEFAULT_NO_URUT)))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].idLantai").value(hasItem(DEFAULT_ID_LANTAI.intValue())));
    }

    @Test
    @Transactional
    public void getTempPanggilan() throws Exception {
        // Initialize the database
        tempPanggilanRepository.saveAndFlush(tempPanggilan);

        // Get the tempPanggilan
        restTempPanggilanMockMvc.perform(get("/api/temp-panggilans/{id}", tempPanggilan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tempPanggilan.getId().intValue()))
            .andExpect(jsonPath("$.idTempPanggilan").value(DEFAULT_ID_TEMP_PANGGILAN.toString()))
            .andExpect(jsonPath("$.panggilan").value(DEFAULT_PANGGILAN))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.noUrut").value(DEFAULT_NO_URUT))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.idLantai").value(DEFAULT_ID_LANTAI.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTempPanggilan() throws Exception {
        // Get the tempPanggilan
        restTempPanggilanMockMvc.perform(get("/api/temp-panggilans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTempPanggilan() throws Exception {
        // Initialize the database
        tempPanggilanRepository.saveAndFlush(tempPanggilan);

        int databaseSizeBeforeUpdate = tempPanggilanRepository.findAll().size();

        // Update the tempPanggilan
        TempPanggilan updatedTempPanggilan = tempPanggilanRepository.findById(tempPanggilan.getId()).get();
        // Disconnect from session so that the updates on updatedTempPanggilan are not directly saved in db
        em.detach(updatedTempPanggilan);
        updatedTempPanggilan
            .idTempPanggilan(UPDATED_ID_TEMP_PANGGILAN)
            .panggilan(UPDATED_PANGGILAN)
            .status(UPDATED_STATUS)
            .noUrut(UPDATED_NO_URUT)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .idLantai(UPDATED_ID_LANTAI);
        TempPanggilanDTO tempPanggilanDTO = tempPanggilanMapper.toDto(updatedTempPanggilan);

        restTempPanggilanMockMvc.perform(put("/api/temp-panggilans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tempPanggilanDTO)))
            .andExpect(status().isOk());

        // Validate the TempPanggilan in the database
        List<TempPanggilan> tempPanggilanList = tempPanggilanRepository.findAll();
        assertThat(tempPanggilanList).hasSize(databaseSizeBeforeUpdate);
        TempPanggilan testTempPanggilan = tempPanggilanList.get(tempPanggilanList.size() - 1);
        assertThat(testTempPanggilan.getIdTempPanggilan()).isEqualTo(UPDATED_ID_TEMP_PANGGILAN);
        assertThat(testTempPanggilan.getPanggilan()).isEqualTo(UPDATED_PANGGILAN);
        assertThat(testTempPanggilan.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTempPanggilan.getNoUrut()).isEqualTo(UPDATED_NO_URUT);
        assertThat(testTempPanggilan.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testTempPanggilan.getIdLantai()).isEqualTo(UPDATED_ID_LANTAI);
    }

    @Test
    @Transactional
    public void updateNonExistingTempPanggilan() throws Exception {
        int databaseSizeBeforeUpdate = tempPanggilanRepository.findAll().size();

        // Create the TempPanggilan
        TempPanggilanDTO tempPanggilanDTO = tempPanggilanMapper.toDto(tempPanggilan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTempPanggilanMockMvc.perform(put("/api/temp-panggilans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(tempPanggilanDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TempPanggilan in the database
        List<TempPanggilan> tempPanggilanList = tempPanggilanRepository.findAll();
        assertThat(tempPanggilanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTempPanggilan() throws Exception {
        // Initialize the database
        tempPanggilanRepository.saveAndFlush(tempPanggilan);

        int databaseSizeBeforeDelete = tempPanggilanRepository.findAll().size();

        // Delete the tempPanggilan
        restTempPanggilanMockMvc.perform(delete("/api/temp-panggilans/{id}", tempPanggilan.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TempPanggilan> tempPanggilanList = tempPanggilanRepository.findAll();
        assertThat(tempPanggilanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
