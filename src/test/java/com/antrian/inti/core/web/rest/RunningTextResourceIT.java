package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.RunningText;
import com.antrian.inti.core.repository.RunningTextRepository;
import com.antrian.inti.core.service.RunningTextService;
import com.antrian.inti.core.service.dto.RunningTextDTO;
import com.antrian.inti.core.service.mapper.RunningTextMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RunningTextResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class RunningTextResourceIT {

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAA";
    private static final String UPDATED_STATUS = "BBB";

    private static final String DEFAULT_MONITOR = "AAAAAAAAAA";
    private static final String UPDATED_MONITOR = "BBBBBBBBBB";

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private RunningTextRepository runningTextRepository;

    @Autowired
    private RunningTextMapper runningTextMapper;

    @Autowired
    private RunningTextService runningTextService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRunningTextMockMvc;

    private RunningText runningText;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RunningTextResource runningTextResource = new RunningTextResource(runningTextService);
        this.restRunningTextMockMvc = MockMvcBuilders.standaloneSetup(runningTextResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RunningText createEntity(EntityManager em) {
        RunningText runningText = new RunningText()
            .text(DEFAULT_TEXT)
            .status(DEFAULT_STATUS)
            .monitor(DEFAULT_MONITOR)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        return runningText;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RunningText createUpdatedEntity(EntityManager em) {
        RunningText runningText = new RunningText()
            .text(UPDATED_TEXT)
            .status(UPDATED_STATUS)
            .monitor(UPDATED_MONITOR)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        return runningText;
    }

    @BeforeEach
    public void initTest() {
        runningText = createEntity(em);
    }

    @Test
    @Transactional
    public void createRunningText() throws Exception {
        int databaseSizeBeforeCreate = runningTextRepository.findAll().size();

        // Create the RunningText
        RunningTextDTO runningTextDTO = runningTextMapper.toDto(runningText);
        restRunningTextMockMvc.perform(post("/api/running-texts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(runningTextDTO)))
            .andExpect(status().isCreated());

        // Validate the RunningText in the database
        List<RunningText> runningTextList = runningTextRepository.findAll();
        assertThat(runningTextList).hasSize(databaseSizeBeforeCreate + 1);
        RunningText testRunningText = runningTextList.get(runningTextList.size() - 1);
        assertThat(testRunningText.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testRunningText.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testRunningText.getMonitor()).isEqualTo(DEFAULT_MONITOR);
        assertThat(testRunningText.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testRunningText.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testRunningText.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testRunningText.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testRunningText.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testRunningText.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void createRunningTextWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = runningTextRepository.findAll().size();

        // Create the RunningText with an existing ID
        runningText.setId(1L);
        RunningTextDTO runningTextDTO = runningTextMapper.toDto(runningText);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRunningTextMockMvc.perform(post("/api/running-texts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(runningTextDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RunningText in the database
        List<RunningText> runningTextList = runningTextRepository.findAll();
        assertThat(runningTextList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRunningTexts() throws Exception {
        // Initialize the database
        runningTextRepository.saveAndFlush(runningText);

        // Get all the runningTextList
        restRunningTextMockMvc.perform(get("/api/running-texts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(runningText.getId().intValue())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].monitor").value(hasItem(DEFAULT_MONITOR)))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))));
    }

    @Test
    @Transactional
    public void getRunningText() throws Exception {
        // Initialize the database
        runningTextRepository.saveAndFlush(runningText);

        // Get the runningText
        restRunningTextMockMvc.perform(get("/api/running-texts/{id}", runningText.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(runningText.getId().intValue()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.monitor").value(DEFAULT_MONITOR))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)));
    }

    @Test
    @Transactional
    public void getNonExistingRunningText() throws Exception {
        // Get the runningText
        restRunningTextMockMvc.perform(get("/api/running-texts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRunningText() throws Exception {
        // Initialize the database
        runningTextRepository.saveAndFlush(runningText);

        int databaseSizeBeforeUpdate = runningTextRepository.findAll().size();

        // Update the runningText
        RunningText updatedRunningText = runningTextRepository.findById(runningText.getId()).get();
        // Disconnect from session so that the updates on updatedRunningText are not directly saved in db
        em.detach(updatedRunningText);
        updatedRunningText
            .text(UPDATED_TEXT)
            .status(UPDATED_STATUS)
            .monitor(UPDATED_MONITOR)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        RunningTextDTO runningTextDTO = runningTextMapper.toDto(updatedRunningText);

        restRunningTextMockMvc.perform(put("/api/running-texts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(runningTextDTO)))
            .andExpect(status().isOk());

        // Validate the RunningText in the database
        List<RunningText> runningTextList = runningTextRepository.findAll();
        assertThat(runningTextList).hasSize(databaseSizeBeforeUpdate);
        RunningText testRunningText = runningTextList.get(runningTextList.size() - 1);
        assertThat(testRunningText.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testRunningText.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testRunningText.getMonitor()).isEqualTo(UPDATED_MONITOR);
        assertThat(testRunningText.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testRunningText.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testRunningText.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testRunningText.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testRunningText.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testRunningText.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void updateNonExistingRunningText() throws Exception {
        int databaseSizeBeforeUpdate = runningTextRepository.findAll().size();

        // Create the RunningText
        RunningTextDTO runningTextDTO = runningTextMapper.toDto(runningText);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRunningTextMockMvc.perform(put("/api/running-texts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(runningTextDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RunningText in the database
        List<RunningText> runningTextList = runningTextRepository.findAll();
        assertThat(runningTextList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRunningText() throws Exception {
        // Initialize the database
        runningTextRepository.saveAndFlush(runningText);

        int databaseSizeBeforeDelete = runningTextRepository.findAll().size();

        // Delete the runningText
        restRunningTextMockMvc.perform(delete("/api/running-texts/{id}", runningText.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RunningText> runningTextList = runningTextRepository.findAll();
        assertThat(runningTextList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
