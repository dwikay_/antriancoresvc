package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.AkumulasiKoresponden;
import com.antrian.inti.core.repository.AkumulasiKorespondenRepository;
import com.antrian.inti.core.service.AkumulasiKorespondenService;
import com.antrian.inti.core.service.dto.AkumulasiKorespondenDTO;
import com.antrian.inti.core.service.mapper.AkumulasiKorespondenMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AkumulasiKorespondenResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class AkumulasiKorespondenResourceIT {

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_AKUMULASI_PERSYARATAN = 1D;
    private static final Double UPDATED_AKUMULASI_PERSYARATAN = 2D;

    private static final Double DEFAULT_AKUMULASI_PROSEDUR = 1D;
    private static final Double UPDATED_AKUMULASI_PROSEDUR = 2D;

    private static final Double DEFAULT_AKUMULASI_WAKTU_PELAYANAN = 1D;
    private static final Double UPDATED_AKUMULASI_WAKTU_PELAYANAN = 2D;

    private static final Double DEFAULT_AKUMULASI_BIAYA_TARIF = 1D;
    private static final Double UPDATED_AKUMULASI_BIAYA_TARIF = 2D;

    private static final Double DEFAULT_AKUMULASI_PRODUK_PELAYANAN = 1D;
    private static final Double UPDATED_AKUMULASI_PRODUK_PELAYANAN = 2D;

    private static final Double DEFAULT_AKUMULASI_KOM_PEL = 1D;
    private static final Double UPDATED_AKUMULASI_KOM_PEL = 2D;

    private static final Double DEFAULT_AKUMULASI_PRILAKU_PELAKSANA = 1D;
    private static final Double UPDATED_AKUMULASI_PRILAKU_PELAKSANA = 2D;

    private static final Double DEFAULT_AKUMULASI_SAR_PRAS = 1D;
    private static final Double UPDATED_AKUMULASI_SAR_PRAS = 2D;

    private static final Double DEFAULT_AKUMULASI_SARAN_MASUK = 1D;
    private static final Double UPDATED_AKUMULASI_SARAN_MASUK = 2D;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Double DEFAULT_AKUMULASI_RATA_RATA = 1D;
    private static final Double UPDATED_AKUMULASI_RATA_RATA = 2D;

    @Autowired
    private AkumulasiKorespondenRepository akumulasiKorespondenRepository;

    @Autowired
    private AkumulasiKorespondenMapper akumulasiKorespondenMapper;

    @Autowired
    private AkumulasiKorespondenService akumulasiKorespondenService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAkumulasiKorespondenMockMvc;

    private AkumulasiKoresponden akumulasiKoresponden;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AkumulasiKorespondenResource akumulasiKorespondenResource = new AkumulasiKorespondenResource(akumulasiKorespondenService);
        this.restAkumulasiKorespondenMockMvc = MockMvcBuilders.standaloneSetup(akumulasiKorespondenResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AkumulasiKoresponden createEntity(EntityManager em) {
        AkumulasiKoresponden akumulasiKoresponden = new AkumulasiKoresponden()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .akumulasiPersyaratan(DEFAULT_AKUMULASI_PERSYARATAN)
            .akumulasiProsedur(DEFAULT_AKUMULASI_PROSEDUR)
            .akumulasiWaktuPelayanan(DEFAULT_AKUMULASI_WAKTU_PELAYANAN)
            .akumulasiBiayaTarif(DEFAULT_AKUMULASI_BIAYA_TARIF)
            .akumulasiProdukPelayanan(DEFAULT_AKUMULASI_PRODUK_PELAYANAN)
            .akumulasiKomPel(DEFAULT_AKUMULASI_KOM_PEL)
            .akumulasiPrilakuPelaksana(DEFAULT_AKUMULASI_PRILAKU_PELAKSANA)
            .akumulasiSarPras(DEFAULT_AKUMULASI_SAR_PRAS)
            .akumulasiSaranMasuk(DEFAULT_AKUMULASI_SARAN_MASUK)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .akumulasiRataRata(DEFAULT_AKUMULASI_RATA_RATA);
        return akumulasiKoresponden;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AkumulasiKoresponden createUpdatedEntity(EntityManager em) {
        AkumulasiKoresponden akumulasiKoresponden = new AkumulasiKoresponden()
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .akumulasiPersyaratan(UPDATED_AKUMULASI_PERSYARATAN)
            .akumulasiProsedur(UPDATED_AKUMULASI_PROSEDUR)
            .akumulasiWaktuPelayanan(UPDATED_AKUMULASI_WAKTU_PELAYANAN)
            .akumulasiBiayaTarif(UPDATED_AKUMULASI_BIAYA_TARIF)
            .akumulasiProdukPelayanan(UPDATED_AKUMULASI_PRODUK_PELAYANAN)
            .akumulasiKomPel(UPDATED_AKUMULASI_KOM_PEL)
            .akumulasiPrilakuPelaksana(UPDATED_AKUMULASI_PRILAKU_PELAKSANA)
            .akumulasiSarPras(UPDATED_AKUMULASI_SAR_PRAS)
            .akumulasiSaranMasuk(UPDATED_AKUMULASI_SARAN_MASUK)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .akumulasiRataRata(UPDATED_AKUMULASI_RATA_RATA);
        return akumulasiKoresponden;
    }

    @BeforeEach
    public void initTest() {
        akumulasiKoresponden = createEntity(em);
    }

    @Test
    @Transactional
    public void createAkumulasiKoresponden() throws Exception {
        int databaseSizeBeforeCreate = akumulasiKorespondenRepository.findAll().size();

        // Create the AkumulasiKoresponden
        AkumulasiKorespondenDTO akumulasiKorespondenDTO = akumulasiKorespondenMapper.toDto(akumulasiKoresponden);
        restAkumulasiKorespondenMockMvc.perform(post("/api/akumulasi-korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(akumulasiKorespondenDTO)))
            .andExpect(status().isCreated());

        // Validate the AkumulasiKoresponden in the database
        List<AkumulasiKoresponden> akumulasiKorespondenList = akumulasiKorespondenRepository.findAll();
        assertThat(akumulasiKorespondenList).hasSize(databaseSizeBeforeCreate + 1);
        AkumulasiKoresponden testAkumulasiKoresponden = akumulasiKorespondenList.get(akumulasiKorespondenList.size() - 1);
        assertThat(testAkumulasiKoresponden.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testAkumulasiKoresponden.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testAkumulasiKoresponden.getAkumulasiPersyaratan()).isEqualTo(DEFAULT_AKUMULASI_PERSYARATAN);
        assertThat(testAkumulasiKoresponden.getAkumulasiProsedur()).isEqualTo(DEFAULT_AKUMULASI_PROSEDUR);
        assertThat(testAkumulasiKoresponden.getAkumulasiWaktuPelayanan()).isEqualTo(DEFAULT_AKUMULASI_WAKTU_PELAYANAN);
        assertThat(testAkumulasiKoresponden.getAkumulasiBiayaTarif()).isEqualTo(DEFAULT_AKUMULASI_BIAYA_TARIF);
        assertThat(testAkumulasiKoresponden.getAkumulasiProdukPelayanan()).isEqualTo(DEFAULT_AKUMULASI_PRODUK_PELAYANAN);
        assertThat(testAkumulasiKoresponden.getAkumulasiKomPel()).isEqualTo(DEFAULT_AKUMULASI_KOM_PEL);
        assertThat(testAkumulasiKoresponden.getAkumulasiPrilakuPelaksana()).isEqualTo(DEFAULT_AKUMULASI_PRILAKU_PELAKSANA);
        assertThat(testAkumulasiKoresponden.getAkumulasiSarPras()).isEqualTo(DEFAULT_AKUMULASI_SAR_PRAS);
        assertThat(testAkumulasiKoresponden.getAkumulasiSaranMasuk()).isEqualTo(DEFAULT_AKUMULASI_SARAN_MASUK);
        assertThat(testAkumulasiKoresponden.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testAkumulasiKoresponden.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testAkumulasiKoresponden.getAkumulasiRataRata()).isEqualTo(DEFAULT_AKUMULASI_RATA_RATA);
    }

    @Test
    @Transactional
    public void createAkumulasiKorespondenWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = akumulasiKorespondenRepository.findAll().size();

        // Create the AkumulasiKoresponden with an existing ID
        akumulasiKoresponden.setId(1L);
        AkumulasiKorespondenDTO akumulasiKorespondenDTO = akumulasiKorespondenMapper.toDto(akumulasiKoresponden);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAkumulasiKorespondenMockMvc.perform(post("/api/akumulasi-korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(akumulasiKorespondenDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AkumulasiKoresponden in the database
        List<AkumulasiKoresponden> akumulasiKorespondenList = akumulasiKorespondenRepository.findAll();
        assertThat(akumulasiKorespondenList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAkumulasiKorespondens() throws Exception {
        // Initialize the database
        akumulasiKorespondenRepository.saveAndFlush(akumulasiKoresponden);

        // Get all the akumulasiKorespondenList
        restAkumulasiKorespondenMockMvc.perform(get("/api/akumulasi-korespondens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(akumulasiKoresponden.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].akumulasiPersyaratan").value(hasItem(DEFAULT_AKUMULASI_PERSYARATAN.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiProsedur").value(hasItem(DEFAULT_AKUMULASI_PROSEDUR.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiWaktuPelayanan").value(hasItem(DEFAULT_AKUMULASI_WAKTU_PELAYANAN.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiBiayaTarif").value(hasItem(DEFAULT_AKUMULASI_BIAYA_TARIF.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiProdukPelayanan").value(hasItem(DEFAULT_AKUMULASI_PRODUK_PELAYANAN.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiKomPel").value(hasItem(DEFAULT_AKUMULASI_KOM_PEL.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiPrilakuPelaksana").value(hasItem(DEFAULT_AKUMULASI_PRILAKU_PELAKSANA.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiSarPras").value(hasItem(DEFAULT_AKUMULASI_SAR_PRAS.doubleValue())))
            .andExpect(jsonPath("$.[*].akumulasiSaranMasuk").value(hasItem(DEFAULT_AKUMULASI_SARAN_MASUK.doubleValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].akumulasiRataRata").value(hasItem(DEFAULT_AKUMULASI_RATA_RATA.doubleValue())));
    }

    @Test
    @Transactional
    public void getAkumulasiKoresponden() throws Exception {
        // Initialize the database
        akumulasiKorespondenRepository.saveAndFlush(akumulasiKoresponden);

        // Get the akumulasiKoresponden
        restAkumulasiKorespondenMockMvc.perform(get("/api/akumulasi-korespondens/{id}", akumulasiKoresponden.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(akumulasiKoresponden.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.akumulasiPersyaratan").value(DEFAULT_AKUMULASI_PERSYARATAN.doubleValue()))
            .andExpect(jsonPath("$.akumulasiProsedur").value(DEFAULT_AKUMULASI_PROSEDUR.doubleValue()))
            .andExpect(jsonPath("$.akumulasiWaktuPelayanan").value(DEFAULT_AKUMULASI_WAKTU_PELAYANAN.doubleValue()))
            .andExpect(jsonPath("$.akumulasiBiayaTarif").value(DEFAULT_AKUMULASI_BIAYA_TARIF.doubleValue()))
            .andExpect(jsonPath("$.akumulasiProdukPelayanan").value(DEFAULT_AKUMULASI_PRODUK_PELAYANAN.doubleValue()))
            .andExpect(jsonPath("$.akumulasiKomPel").value(DEFAULT_AKUMULASI_KOM_PEL.doubleValue()))
            .andExpect(jsonPath("$.akumulasiPrilakuPelaksana").value(DEFAULT_AKUMULASI_PRILAKU_PELAKSANA.doubleValue()))
            .andExpect(jsonPath("$.akumulasiSarPras").value(DEFAULT_AKUMULASI_SAR_PRAS.doubleValue()))
            .andExpect(jsonPath("$.akumulasiSaranMasuk").value(DEFAULT_AKUMULASI_SARAN_MASUK.doubleValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.akumulasiRataRata").value(DEFAULT_AKUMULASI_RATA_RATA.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAkumulasiKoresponden() throws Exception {
        // Get the akumulasiKoresponden
        restAkumulasiKorespondenMockMvc.perform(get("/api/akumulasi-korespondens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAkumulasiKoresponden() throws Exception {
        // Initialize the database
        akumulasiKorespondenRepository.saveAndFlush(akumulasiKoresponden);

        int databaseSizeBeforeUpdate = akumulasiKorespondenRepository.findAll().size();

        // Update the akumulasiKoresponden
        AkumulasiKoresponden updatedAkumulasiKoresponden = akumulasiKorespondenRepository.findById(akumulasiKoresponden.getId()).get();
        // Disconnect from session so that the updates on updatedAkumulasiKoresponden are not directly saved in db
        em.detach(updatedAkumulasiKoresponden);
        updatedAkumulasiKoresponden
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .akumulasiPersyaratan(UPDATED_AKUMULASI_PERSYARATAN)
            .akumulasiProsedur(UPDATED_AKUMULASI_PROSEDUR)
            .akumulasiWaktuPelayanan(UPDATED_AKUMULASI_WAKTU_PELAYANAN)
            .akumulasiBiayaTarif(UPDATED_AKUMULASI_BIAYA_TARIF)
            .akumulasiProdukPelayanan(UPDATED_AKUMULASI_PRODUK_PELAYANAN)
            .akumulasiKomPel(UPDATED_AKUMULASI_KOM_PEL)
            .akumulasiPrilakuPelaksana(UPDATED_AKUMULASI_PRILAKU_PELAKSANA)
            .akumulasiSarPras(UPDATED_AKUMULASI_SAR_PRAS)
            .akumulasiSaranMasuk(UPDATED_AKUMULASI_SARAN_MASUK)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .akumulasiRataRata(UPDATED_AKUMULASI_RATA_RATA);
        AkumulasiKorespondenDTO akumulasiKorespondenDTO = akumulasiKorespondenMapper.toDto(updatedAkumulasiKoresponden);

        restAkumulasiKorespondenMockMvc.perform(put("/api/akumulasi-korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(akumulasiKorespondenDTO)))
            .andExpect(status().isOk());

        // Validate the AkumulasiKoresponden in the database
        List<AkumulasiKoresponden> akumulasiKorespondenList = akumulasiKorespondenRepository.findAll();
        assertThat(akumulasiKorespondenList).hasSize(databaseSizeBeforeUpdate);
        AkumulasiKoresponden testAkumulasiKoresponden = akumulasiKorespondenList.get(akumulasiKorespondenList.size() - 1);
        assertThat(testAkumulasiKoresponden.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testAkumulasiKoresponden.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testAkumulasiKoresponden.getAkumulasiPersyaratan()).isEqualTo(UPDATED_AKUMULASI_PERSYARATAN);
        assertThat(testAkumulasiKoresponden.getAkumulasiProsedur()).isEqualTo(UPDATED_AKUMULASI_PROSEDUR);
        assertThat(testAkumulasiKoresponden.getAkumulasiWaktuPelayanan()).isEqualTo(UPDATED_AKUMULASI_WAKTU_PELAYANAN);
        assertThat(testAkumulasiKoresponden.getAkumulasiBiayaTarif()).isEqualTo(UPDATED_AKUMULASI_BIAYA_TARIF);
        assertThat(testAkumulasiKoresponden.getAkumulasiProdukPelayanan()).isEqualTo(UPDATED_AKUMULASI_PRODUK_PELAYANAN);
        assertThat(testAkumulasiKoresponden.getAkumulasiKomPel()).isEqualTo(UPDATED_AKUMULASI_KOM_PEL);
        assertThat(testAkumulasiKoresponden.getAkumulasiPrilakuPelaksana()).isEqualTo(UPDATED_AKUMULASI_PRILAKU_PELAKSANA);
        assertThat(testAkumulasiKoresponden.getAkumulasiSarPras()).isEqualTo(UPDATED_AKUMULASI_SAR_PRAS);
        assertThat(testAkumulasiKoresponden.getAkumulasiSaranMasuk()).isEqualTo(UPDATED_AKUMULASI_SARAN_MASUK);
        assertThat(testAkumulasiKoresponden.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testAkumulasiKoresponden.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testAkumulasiKoresponden.getAkumulasiRataRata()).isEqualTo(UPDATED_AKUMULASI_RATA_RATA);
    }

    @Test
    @Transactional
    public void updateNonExistingAkumulasiKoresponden() throws Exception {
        int databaseSizeBeforeUpdate = akumulasiKorespondenRepository.findAll().size();

        // Create the AkumulasiKoresponden
        AkumulasiKorespondenDTO akumulasiKorespondenDTO = akumulasiKorespondenMapper.toDto(akumulasiKoresponden);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAkumulasiKorespondenMockMvc.perform(put("/api/akumulasi-korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(akumulasiKorespondenDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AkumulasiKoresponden in the database
        List<AkumulasiKoresponden> akumulasiKorespondenList = akumulasiKorespondenRepository.findAll();
        assertThat(akumulasiKorespondenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAkumulasiKoresponden() throws Exception {
        // Initialize the database
        akumulasiKorespondenRepository.saveAndFlush(akumulasiKoresponden);

        int databaseSizeBeforeDelete = akumulasiKorespondenRepository.findAll().size();

        // Delete the akumulasiKoresponden
        restAkumulasiKorespondenMockMvc.perform(delete("/api/akumulasi-korespondens/{id}", akumulasiKoresponden.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AkumulasiKoresponden> akumulasiKorespondenList = akumulasiKorespondenRepository.findAll();
        assertThat(akumulasiKorespondenList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
