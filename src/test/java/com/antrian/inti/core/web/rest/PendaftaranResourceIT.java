package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Pendaftaran;
import com.antrian.inti.core.repository.PendaftaranRepository;
import com.antrian.inti.core.service.PendaftaranService;
import com.antrian.inti.core.service.dto.PendaftaranDTO;
import com.antrian.inti.core.service.mapper.PendaftaranMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PendaftaranResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class PendaftaranResourceIT {

    private static final String DEFAULT_NAMA_BADAN_USAHA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_BADAN_USAHA = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_DIREKSI = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_DIREKSI = "BBBBBBBBBB";

    private static final String DEFAULT_NIK_DIREKSI = "AAAAAAAAAA";
    private static final String UPDATED_NIK_DIREKSI = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_PENGAMBILAN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_PENGAMBILAN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_KODE_KONFIRMASI = "AAAAAAAAAA";
    private static final String UPDATED_KODE_KONFIRMASI = "BBBBBBBBBB";

    private static final Long DEFAULT_ID_KLOTER = 1L;
    private static final Long UPDATED_ID_KLOTER = 2L;

    private static final Boolean DEFAULT_PENGAMBILAN_STRUK = false;
    private static final Boolean UPDATED_PENGAMBILAN_STRUK = true;

    private static final String DEFAULT_JAM_KLOTER = "AAAAAAAAAA";
    private static final String UPDATED_JAM_KLOTER = "BBBBBBBBBB";

    private static final String DEFAULT_PERTANYAAN = "AAAAAAAAAA";
    private static final String UPDATED_PERTANYAAN = "BBBBBBBBBB";

    private static final String DEFAULT_JAWABAN = "AAAAAAAAAA";
    private static final String UPDATED_JAWABAN = "BBBBBBBBBB";

    private static final String DEFAULT_TIPE_KTP = "AAAAAAAAAA";
    private static final String UPDATED_TIPE_KTP = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS_TIKET = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_TIKET = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TYPE_VIRTUAL = false;
    private static final Boolean UPDATED_TYPE_VIRTUAL = true;

    private static final String DEFAULT_JAM_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_JAM_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_TIKET = "AAAAAAAAAA";
    private static final String UPDATED_NO_TIKET = "BBBBBBBBBB";

    private static final Boolean DEFAULT_AGENT = false;
    private static final Boolean UPDATED_AGENT = true;

    private static final UUID DEFAULT_ID_DAFTAR = UUID.randomUUID();
    private static final UUID UPDATED_ID_DAFTAR = UUID.randomUUID();

    private static final String DEFAULT_NAMA_PERSEORANGAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PERSEORANGAN = "BBBBBBBBBB";

    private static final String DEFAULT_NIK_PERSEORANGAN = "AAAAAAAAAA";
    private static final String UPDATED_NIK_PERSEORANGAN = "BBBBBBBBBB";

    private static final String DEFAULT_INSTANSI = "AAAAAAAAAA";
    private static final String UPDATED_INSTANSI = "BBBBBBBBBB";

    private static final String DEFAULT_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_UNIT = "BBBBBBBBBB";

    private static final String DEFAULT_JABATAN = "AAAAAAAAAA";
    private static final String UPDATED_JABATAN = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PEJABAT = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PEJABAT = "BBBBBBBBBB";

    private static final String DEFAULT_UNTUK_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_UNTUK_LAYANAN = "BBBBBBBBBB";

    private static final String DEFAULT_JWT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_JWT_TOKEN = "BBBBBBBBBB";

    @Autowired
    private PendaftaranRepository pendaftaranRepository;

    @Autowired
    private PendaftaranMapper pendaftaranMapper;

    @Autowired
    private PendaftaranService pendaftaranService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPendaftaranMockMvc;

    private Pendaftaran pendaftaran;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PendaftaranResource pendaftaranResource = new PendaftaranResource(pendaftaranService);
        this.restPendaftaranMockMvc = MockMvcBuilders.standaloneSetup(pendaftaranResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pendaftaran createEntity(EntityManager em) {
        Pendaftaran pendaftaran = new Pendaftaran()
            .namaBadanUsaha(DEFAULT_NAMA_BADAN_USAHA)
            .namaDireksi(DEFAULT_NAMA_DIREKSI)
            .nikDireksi(DEFAULT_NIK_DIREKSI)
            .email(DEFAULT_EMAIL)
            .tanggalPengambilan(DEFAULT_TANGGAL_PENGAMBILAN)
            .kodeKonfirmasi(DEFAULT_KODE_KONFIRMASI)
            .idKloter(DEFAULT_ID_KLOTER)
            .pengambilanStruk(DEFAULT_PENGAMBILAN_STRUK)
            .jamKloter(DEFAULT_JAM_KLOTER)
            .pertanyaan(DEFAULT_PERTANYAAN)
            .jawaban(DEFAULT_JAWABAN)
            .tipeKtp(DEFAULT_TIPE_KTP)
            .statusTiket(DEFAULT_STATUS_TIKET)
            .typeVirtual(DEFAULT_TYPE_VIRTUAL)
            .jamLayanan(DEFAULT_JAM_LAYANAN)
            .noTiket(DEFAULT_NO_TIKET)
            .agent(DEFAULT_AGENT)
            .idDaftar(DEFAULT_ID_DAFTAR)
            .namaPerseorangan(DEFAULT_NAMA_PERSEORANGAN)
            .nikPerseorangan(DEFAULT_NIK_PERSEORANGAN)
            .instansi(DEFAULT_INSTANSI)
            .unit(DEFAULT_UNIT)
            .jabatan(DEFAULT_JABATAN)
            .namaPejabat(DEFAULT_NAMA_PEJABAT)
            .untukLayanan(DEFAULT_UNTUK_LAYANAN)
            .jwtToken(DEFAULT_JWT_TOKEN);
        return pendaftaran;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pendaftaran createUpdatedEntity(EntityManager em) {
        Pendaftaran pendaftaran = new Pendaftaran()
            .namaBadanUsaha(UPDATED_NAMA_BADAN_USAHA)
            .namaDireksi(UPDATED_NAMA_DIREKSI)
            .nikDireksi(UPDATED_NIK_DIREKSI)
            .email(UPDATED_EMAIL)
            .tanggalPengambilan(UPDATED_TANGGAL_PENGAMBILAN)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .idKloter(UPDATED_ID_KLOTER)
            .pengambilanStruk(UPDATED_PENGAMBILAN_STRUK)
            .jamKloter(UPDATED_JAM_KLOTER)
            .pertanyaan(UPDATED_PERTANYAAN)
            .jawaban(UPDATED_JAWABAN)
            .tipeKtp(UPDATED_TIPE_KTP)
            .statusTiket(UPDATED_STATUS_TIKET)
            .typeVirtual(UPDATED_TYPE_VIRTUAL)
            .jamLayanan(UPDATED_JAM_LAYANAN)
            .noTiket(UPDATED_NO_TIKET)
            .agent(UPDATED_AGENT)
            .idDaftar(UPDATED_ID_DAFTAR)
            .namaPerseorangan(UPDATED_NAMA_PERSEORANGAN)
            .nikPerseorangan(UPDATED_NIK_PERSEORANGAN)
            .instansi(UPDATED_INSTANSI)
            .unit(UPDATED_UNIT)
            .jabatan(UPDATED_JABATAN)
            .namaPejabat(UPDATED_NAMA_PEJABAT)
            .untukLayanan(UPDATED_UNTUK_LAYANAN)
            .jwtToken(UPDATED_JWT_TOKEN);
        return pendaftaran;
    }

    @BeforeEach
    public void initTest() {
        pendaftaran = createEntity(em);
    }

    @Test
    @Transactional
    public void createPendaftaran() throws Exception {
        int databaseSizeBeforeCreate = pendaftaranRepository.findAll().size();

        // Create the Pendaftaran
        PendaftaranDTO pendaftaranDTO = pendaftaranMapper.toDto(pendaftaran);
        restPendaftaranMockMvc.perform(post("/api/pendaftarans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDTO)))
            .andExpect(status().isCreated());

        // Validate the Pendaftaran in the database
        List<Pendaftaran> pendaftaranList = pendaftaranRepository.findAll();
        assertThat(pendaftaranList).hasSize(databaseSizeBeforeCreate + 1);
        Pendaftaran testPendaftaran = pendaftaranList.get(pendaftaranList.size() - 1);
        assertThat(testPendaftaran.getNamaBadanUsaha()).isEqualTo(DEFAULT_NAMA_BADAN_USAHA);
        assertThat(testPendaftaran.getNamaDireksi()).isEqualTo(DEFAULT_NAMA_DIREKSI);
        assertThat(testPendaftaran.getNikDireksi()).isEqualTo(DEFAULT_NIK_DIREKSI);
        assertThat(testPendaftaran.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPendaftaran.getTanggalPengambilan()).isEqualTo(DEFAULT_TANGGAL_PENGAMBILAN);
        assertThat(testPendaftaran.getKodeKonfirmasi()).isEqualTo(DEFAULT_KODE_KONFIRMASI);
        assertThat(testPendaftaran.getIdKloter()).isEqualTo(DEFAULT_ID_KLOTER);
        assertThat(testPendaftaran.isPengambilanStruk()).isEqualTo(DEFAULT_PENGAMBILAN_STRUK);
        assertThat(testPendaftaran.getJamKloter()).isEqualTo(DEFAULT_JAM_KLOTER);
        assertThat(testPendaftaran.getPertanyaan()).isEqualTo(DEFAULT_PERTANYAAN);
        assertThat(testPendaftaran.getJawaban()).isEqualTo(DEFAULT_JAWABAN);
        assertThat(testPendaftaran.getTipeKtp()).isEqualTo(DEFAULT_TIPE_KTP);
        assertThat(testPendaftaran.getStatusTiket()).isEqualTo(DEFAULT_STATUS_TIKET);
        assertThat(testPendaftaran.isTypeVirtual()).isEqualTo(DEFAULT_TYPE_VIRTUAL);
        assertThat(testPendaftaran.getJamLayanan()).isEqualTo(DEFAULT_JAM_LAYANAN);
        assertThat(testPendaftaran.getNoTiket()).isEqualTo(DEFAULT_NO_TIKET);
        assertThat(testPendaftaran.isAgent()).isEqualTo(DEFAULT_AGENT);
        assertThat(testPendaftaran.getIdDaftar()).isEqualTo(DEFAULT_ID_DAFTAR);
        assertThat(testPendaftaran.getNamaPerseorangan()).isEqualTo(DEFAULT_NAMA_PERSEORANGAN);
        assertThat(testPendaftaran.getNikPerseorangan()).isEqualTo(DEFAULT_NIK_PERSEORANGAN);
        assertThat(testPendaftaran.getInstansi()).isEqualTo(DEFAULT_INSTANSI);
        assertThat(testPendaftaran.getUnit()).isEqualTo(DEFAULT_UNIT);
        assertThat(testPendaftaran.getJabatan()).isEqualTo(DEFAULT_JABATAN);
        assertThat(testPendaftaran.getNamaPejabat()).isEqualTo(DEFAULT_NAMA_PEJABAT);
        assertThat(testPendaftaran.getUntukLayanan()).isEqualTo(DEFAULT_UNTUK_LAYANAN);
        assertThat(testPendaftaran.getJwtToken()).isEqualTo(DEFAULT_JWT_TOKEN);
    }

    @Test
    @Transactional
    public void createPendaftaranWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pendaftaranRepository.findAll().size();

        // Create the Pendaftaran with an existing ID
        pendaftaran.setId(1L);
        PendaftaranDTO pendaftaranDTO = pendaftaranMapper.toDto(pendaftaran);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPendaftaranMockMvc.perform(post("/api/pendaftarans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pendaftaran in the database
        List<Pendaftaran> pendaftaranList = pendaftaranRepository.findAll();
        assertThat(pendaftaranList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPendaftarans() throws Exception {
        // Initialize the database
        pendaftaranRepository.saveAndFlush(pendaftaran);

        // Get all the pendaftaranList
        restPendaftaranMockMvc.perform(get("/api/pendaftarans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pendaftaran.getId().intValue())))
            .andExpect(jsonPath("$.[*].namaBadanUsaha").value(hasItem(DEFAULT_NAMA_BADAN_USAHA)))
            .andExpect(jsonPath("$.[*].namaDireksi").value(hasItem(DEFAULT_NAMA_DIREKSI)))
            .andExpect(jsonPath("$.[*].nikDireksi").value(hasItem(DEFAULT_NIK_DIREKSI)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].tanggalPengambilan").value(hasItem(DEFAULT_TANGGAL_PENGAMBILAN.toString())))
            .andExpect(jsonPath("$.[*].kodeKonfirmasi").value(hasItem(DEFAULT_KODE_KONFIRMASI)))
            .andExpect(jsonPath("$.[*].idKloter").value(hasItem(DEFAULT_ID_KLOTER.intValue())))
            .andExpect(jsonPath("$.[*].pengambilanStruk").value(hasItem(DEFAULT_PENGAMBILAN_STRUK.booleanValue())))
            .andExpect(jsonPath("$.[*].jamKloter").value(hasItem(DEFAULT_JAM_KLOTER)))
            .andExpect(jsonPath("$.[*].pertanyaan").value(hasItem(DEFAULT_PERTANYAAN.toString())))
            .andExpect(jsonPath("$.[*].jawaban").value(hasItem(DEFAULT_JAWABAN.toString())))
            .andExpect(jsonPath("$.[*].tipeKtp").value(hasItem(DEFAULT_TIPE_KTP)))
            .andExpect(jsonPath("$.[*].statusTiket").value(hasItem(DEFAULT_STATUS_TIKET)))
            .andExpect(jsonPath("$.[*].typeVirtual").value(hasItem(DEFAULT_TYPE_VIRTUAL.booleanValue())))
            .andExpect(jsonPath("$.[*].jamLayanan").value(hasItem(DEFAULT_JAM_LAYANAN)))
            .andExpect(jsonPath("$.[*].noTiket").value(hasItem(DEFAULT_NO_TIKET)))
            .andExpect(jsonPath("$.[*].agent").value(hasItem(DEFAULT_AGENT.booleanValue())))
            .andExpect(jsonPath("$.[*].idDaftar").value(hasItem(DEFAULT_ID_DAFTAR.toString())))
            .andExpect(jsonPath("$.[*].namaPerseorangan").value(hasItem(DEFAULT_NAMA_PERSEORANGAN)))
            .andExpect(jsonPath("$.[*].nikPerseorangan").value(hasItem(DEFAULT_NIK_PERSEORANGAN)))
            .andExpect(jsonPath("$.[*].instansi").value(hasItem(DEFAULT_INSTANSI)))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].jabatan").value(hasItem(DEFAULT_JABATAN)))
            .andExpect(jsonPath("$.[*].namaPejabat").value(hasItem(DEFAULT_NAMA_PEJABAT)))
            .andExpect(jsonPath("$.[*].untukLayanan").value(hasItem(DEFAULT_UNTUK_LAYANAN)))
            .andExpect(jsonPath("$.[*].jwtToken").value(hasItem(DEFAULT_JWT_TOKEN.toString())));
    }
    
    @Test
    @Transactional
    public void getPendaftaran() throws Exception {
        // Initialize the database
        pendaftaranRepository.saveAndFlush(pendaftaran);

        // Get the pendaftaran
        restPendaftaranMockMvc.perform(get("/api/pendaftarans/{id}", pendaftaran.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pendaftaran.getId().intValue()))
            .andExpect(jsonPath("$.namaBadanUsaha").value(DEFAULT_NAMA_BADAN_USAHA))
            .andExpect(jsonPath("$.namaDireksi").value(DEFAULT_NAMA_DIREKSI))
            .andExpect(jsonPath("$.nikDireksi").value(DEFAULT_NIK_DIREKSI))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.tanggalPengambilan").value(DEFAULT_TANGGAL_PENGAMBILAN.toString()))
            .andExpect(jsonPath("$.kodeKonfirmasi").value(DEFAULT_KODE_KONFIRMASI))
            .andExpect(jsonPath("$.idKloter").value(DEFAULT_ID_KLOTER.intValue()))
            .andExpect(jsonPath("$.pengambilanStruk").value(DEFAULT_PENGAMBILAN_STRUK.booleanValue()))
            .andExpect(jsonPath("$.jamKloter").value(DEFAULT_JAM_KLOTER))
            .andExpect(jsonPath("$.pertanyaan").value(DEFAULT_PERTANYAAN.toString()))
            .andExpect(jsonPath("$.jawaban").value(DEFAULT_JAWABAN.toString()))
            .andExpect(jsonPath("$.tipeKtp").value(DEFAULT_TIPE_KTP))
            .andExpect(jsonPath("$.statusTiket").value(DEFAULT_STATUS_TIKET))
            .andExpect(jsonPath("$.typeVirtual").value(DEFAULT_TYPE_VIRTUAL.booleanValue()))
            .andExpect(jsonPath("$.jamLayanan").value(DEFAULT_JAM_LAYANAN))
            .andExpect(jsonPath("$.noTiket").value(DEFAULT_NO_TIKET))
            .andExpect(jsonPath("$.agent").value(DEFAULT_AGENT.booleanValue()))
            .andExpect(jsonPath("$.idDaftar").value(DEFAULT_ID_DAFTAR.toString()))
            .andExpect(jsonPath("$.namaPerseorangan").value(DEFAULT_NAMA_PERSEORANGAN))
            .andExpect(jsonPath("$.nikPerseorangan").value(DEFAULT_NIK_PERSEORANGAN))
            .andExpect(jsonPath("$.instansi").value(DEFAULT_INSTANSI))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT))
            .andExpect(jsonPath("$.jabatan").value(DEFAULT_JABATAN))
            .andExpect(jsonPath("$.namaPejabat").value(DEFAULT_NAMA_PEJABAT))
            .andExpect(jsonPath("$.untukLayanan").value(DEFAULT_UNTUK_LAYANAN))
            .andExpect(jsonPath("$.jwtToken").value(DEFAULT_JWT_TOKEN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPendaftaran() throws Exception {
        // Get the pendaftaran
        restPendaftaranMockMvc.perform(get("/api/pendaftarans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePendaftaran() throws Exception {
        // Initialize the database
        pendaftaranRepository.saveAndFlush(pendaftaran);

        int databaseSizeBeforeUpdate = pendaftaranRepository.findAll().size();

        // Update the pendaftaran
        Pendaftaran updatedPendaftaran = pendaftaranRepository.findById(pendaftaran.getId()).get();
        // Disconnect from session so that the updates on updatedPendaftaran are not directly saved in db
        em.detach(updatedPendaftaran);
        updatedPendaftaran
            .namaBadanUsaha(UPDATED_NAMA_BADAN_USAHA)
            .namaDireksi(UPDATED_NAMA_DIREKSI)
            .nikDireksi(UPDATED_NIK_DIREKSI)
            .email(UPDATED_EMAIL)
            .tanggalPengambilan(UPDATED_TANGGAL_PENGAMBILAN)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .idKloter(UPDATED_ID_KLOTER)
            .pengambilanStruk(UPDATED_PENGAMBILAN_STRUK)
            .jamKloter(UPDATED_JAM_KLOTER)
            .pertanyaan(UPDATED_PERTANYAAN)
            .jawaban(UPDATED_JAWABAN)
            .tipeKtp(UPDATED_TIPE_KTP)
            .statusTiket(UPDATED_STATUS_TIKET)
            .typeVirtual(UPDATED_TYPE_VIRTUAL)
            .jamLayanan(UPDATED_JAM_LAYANAN)
            .noTiket(UPDATED_NO_TIKET)
            .agent(UPDATED_AGENT)
            .idDaftar(UPDATED_ID_DAFTAR)
            .namaPerseorangan(UPDATED_NAMA_PERSEORANGAN)
            .nikPerseorangan(UPDATED_NIK_PERSEORANGAN)
            .instansi(UPDATED_INSTANSI)
            .unit(UPDATED_UNIT)
            .jabatan(UPDATED_JABATAN)
            .namaPejabat(UPDATED_NAMA_PEJABAT)
            .untukLayanan(UPDATED_UNTUK_LAYANAN)
            .jwtToken(UPDATED_JWT_TOKEN);
        PendaftaranDTO pendaftaranDTO = pendaftaranMapper.toDto(updatedPendaftaran);

        restPendaftaranMockMvc.perform(put("/api/pendaftarans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDTO)))
            .andExpect(status().isOk());

        // Validate the Pendaftaran in the database
        List<Pendaftaran> pendaftaranList = pendaftaranRepository.findAll();
        assertThat(pendaftaranList).hasSize(databaseSizeBeforeUpdate);
        Pendaftaran testPendaftaran = pendaftaranList.get(pendaftaranList.size() - 1);
        assertThat(testPendaftaran.getNamaBadanUsaha()).isEqualTo(UPDATED_NAMA_BADAN_USAHA);
        assertThat(testPendaftaran.getNamaDireksi()).isEqualTo(UPDATED_NAMA_DIREKSI);
        assertThat(testPendaftaran.getNikDireksi()).isEqualTo(UPDATED_NIK_DIREKSI);
        assertThat(testPendaftaran.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPendaftaran.getTanggalPengambilan()).isEqualTo(UPDATED_TANGGAL_PENGAMBILAN);
        assertThat(testPendaftaran.getKodeKonfirmasi()).isEqualTo(UPDATED_KODE_KONFIRMASI);
        assertThat(testPendaftaran.getIdKloter()).isEqualTo(UPDATED_ID_KLOTER);
        assertThat(testPendaftaran.isPengambilanStruk()).isEqualTo(UPDATED_PENGAMBILAN_STRUK);
        assertThat(testPendaftaran.getJamKloter()).isEqualTo(UPDATED_JAM_KLOTER);
        assertThat(testPendaftaran.getPertanyaan()).isEqualTo(UPDATED_PERTANYAAN);
        assertThat(testPendaftaran.getJawaban()).isEqualTo(UPDATED_JAWABAN);
        assertThat(testPendaftaran.getTipeKtp()).isEqualTo(UPDATED_TIPE_KTP);
        assertThat(testPendaftaran.getStatusTiket()).isEqualTo(UPDATED_STATUS_TIKET);
        assertThat(testPendaftaran.isTypeVirtual()).isEqualTo(UPDATED_TYPE_VIRTUAL);
        assertThat(testPendaftaran.getJamLayanan()).isEqualTo(UPDATED_JAM_LAYANAN);
        assertThat(testPendaftaran.getNoTiket()).isEqualTo(UPDATED_NO_TIKET);
        assertThat(testPendaftaran.isAgent()).isEqualTo(UPDATED_AGENT);
        assertThat(testPendaftaran.getIdDaftar()).isEqualTo(UPDATED_ID_DAFTAR);
        assertThat(testPendaftaran.getNamaPerseorangan()).isEqualTo(UPDATED_NAMA_PERSEORANGAN);
        assertThat(testPendaftaran.getNikPerseorangan()).isEqualTo(UPDATED_NIK_PERSEORANGAN);
        assertThat(testPendaftaran.getInstansi()).isEqualTo(UPDATED_INSTANSI);
        assertThat(testPendaftaran.getUnit()).isEqualTo(UPDATED_UNIT);
        assertThat(testPendaftaran.getJabatan()).isEqualTo(UPDATED_JABATAN);
        assertThat(testPendaftaran.getNamaPejabat()).isEqualTo(UPDATED_NAMA_PEJABAT);
        assertThat(testPendaftaran.getUntukLayanan()).isEqualTo(UPDATED_UNTUK_LAYANAN);
        assertThat(testPendaftaran.getJwtToken()).isEqualTo(UPDATED_JWT_TOKEN);
    }

    @Test
    @Transactional
    public void updateNonExistingPendaftaran() throws Exception {
        int databaseSizeBeforeUpdate = pendaftaranRepository.findAll().size();

        // Create the Pendaftaran
        PendaftaranDTO pendaftaranDTO = pendaftaranMapper.toDto(pendaftaran);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPendaftaranMockMvc.perform(put("/api/pendaftarans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pendaftaranDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pendaftaran in the database
        List<Pendaftaran> pendaftaranList = pendaftaranRepository.findAll();
        assertThat(pendaftaranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePendaftaran() throws Exception {
        // Initialize the database
        pendaftaranRepository.saveAndFlush(pendaftaran);

        int databaseSizeBeforeDelete = pendaftaranRepository.findAll().size();

        // Delete the pendaftaran
        restPendaftaranMockMvc.perform(delete("/api/pendaftarans/{id}", pendaftaran.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pendaftaran> pendaftaranList = pendaftaranRepository.findAll();
        assertThat(pendaftaranList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
