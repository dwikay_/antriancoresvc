package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Banned;
import com.antrian.inti.core.repository.BannedRepository;
import com.antrian.inti.core.service.BannedService;
import com.antrian.inti.core.service.dto.BannedDTO;
import com.antrian.inti.core.service.mapper.BannedMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BannedResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class BannedResourceIT {

    private static final String DEFAULT_NIK = "AAAAAAAAAA";
    private static final String UPDATED_NIK = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_BANNED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_BANNED = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FINISH_BANNED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FINISH_BANNED = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATUS_BANNED = "AAA";
    private static final String UPDATED_STATUS_BANNED = "BBB";

    private static final LocalDate DEFAULT_CREATE_SYSTEM_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_SYSTEM_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_MODIFICATION_SYSTEM_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFICATION_SYSTEM_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_JUMLAH = 1;
    private static final Integer UPDATED_JUMLAH = 2;

    @Autowired
    private BannedRepository bannedRepository;

    @Autowired
    private BannedMapper bannedMapper;

    @Autowired
    private BannedService bannedService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBannedMockMvc;

    private Banned banned;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BannedResource bannedResource = new BannedResource(bannedService);
        this.restBannedMockMvc = MockMvcBuilders.standaloneSetup(bannedResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Banned createEntity(EntityManager em) {
        Banned banned = new Banned()
            .nik(DEFAULT_NIK)
            .email(DEFAULT_EMAIL)
            .tanggal_banned(DEFAULT_TANGGAL_BANNED)
            .finish_banned(DEFAULT_FINISH_BANNED)
            .status_banned(DEFAULT_STATUS_BANNED)
            .createSystemDate(DEFAULT_CREATE_SYSTEM_DATE)
            .modificationSystemDate(DEFAULT_MODIFICATION_SYSTEM_DATE)
            .jumlah(DEFAULT_JUMLAH);
        return banned;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Banned createUpdatedEntity(EntityManager em) {
        Banned banned = new Banned()
            .nik(UPDATED_NIK)
            .email(UPDATED_EMAIL)
            .tanggal_banned(UPDATED_TANGGAL_BANNED)
            .finish_banned(UPDATED_FINISH_BANNED)
            .status_banned(UPDATED_STATUS_BANNED)
            .createSystemDate(UPDATED_CREATE_SYSTEM_DATE)
            .modificationSystemDate(UPDATED_MODIFICATION_SYSTEM_DATE)
            .jumlah(UPDATED_JUMLAH);
        return banned;
    }

    @BeforeEach
    public void initTest() {
        banned = createEntity(em);
    }

    @Test
    @Transactional
    public void createBanned() throws Exception {
        int databaseSizeBeforeCreate = bannedRepository.findAll().size();

        // Create the Banned
        BannedDTO bannedDTO = bannedMapper.toDto(banned);
        restBannedMockMvc.perform(post("/api/banneds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bannedDTO)))
            .andExpect(status().isCreated());

        // Validate the Banned in the database
        List<Banned> bannedList = bannedRepository.findAll();
        assertThat(bannedList).hasSize(databaseSizeBeforeCreate + 1);
        Banned testBanned = bannedList.get(bannedList.size() - 1);
        assertThat(testBanned.getNik()).isEqualTo(DEFAULT_NIK);
        assertThat(testBanned.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testBanned.getTanggal_banned()).isEqualTo(DEFAULT_TANGGAL_BANNED);
        assertThat(testBanned.getFinish_banned()).isEqualTo(DEFAULT_FINISH_BANNED);
        assertThat(testBanned.getStatus_banned()).isEqualTo(DEFAULT_STATUS_BANNED);
        assertThat(testBanned.getCreateSystemDate()).isEqualTo(DEFAULT_CREATE_SYSTEM_DATE);
        assertThat(testBanned.getModificationSystemDate()).isEqualTo(DEFAULT_MODIFICATION_SYSTEM_DATE);
        assertThat(testBanned.getJumlah()).isEqualTo(DEFAULT_JUMLAH);
    }

    @Test
    @Transactional
    public void createBannedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bannedRepository.findAll().size();

        // Create the Banned with an existing ID
        banned.setId(1L);
        BannedDTO bannedDTO = bannedMapper.toDto(banned);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBannedMockMvc.perform(post("/api/banneds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bannedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Banned in the database
        List<Banned> bannedList = bannedRepository.findAll();
        assertThat(bannedList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBanneds() throws Exception {
        // Initialize the database
        bannedRepository.saveAndFlush(banned);

        // Get all the bannedList
        restBannedMockMvc.perform(get("/api/banneds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(banned.getId().intValue())))
            .andExpect(jsonPath("$.[*].nik").value(hasItem(DEFAULT_NIK)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].tanggal_banned").value(hasItem(DEFAULT_TANGGAL_BANNED.toString())))
            .andExpect(jsonPath("$.[*].finish_banned").value(hasItem(DEFAULT_FINISH_BANNED.toString())))
            .andExpect(jsonPath("$.[*].status_banned").value(hasItem(DEFAULT_STATUS_BANNED)))
            .andExpect(jsonPath("$.[*].createSystemDate").value(hasItem(DEFAULT_CREATE_SYSTEM_DATE.toString())))
            .andExpect(jsonPath("$.[*].modificationSystemDate").value(hasItem(DEFAULT_MODIFICATION_SYSTEM_DATE.toString())))
            .andExpect(jsonPath("$.[*].jumlah").value(hasItem(DEFAULT_JUMLAH)));
    }

    @Test
    @Transactional
    public void getBanned() throws Exception {
        // Initialize the database
        bannedRepository.saveAndFlush(banned);

        // Get the banned
        restBannedMockMvc.perform(get("/api/banneds/{id}", banned.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(banned.getId().intValue()))
            .andExpect(jsonPath("$.nik").value(DEFAULT_NIK))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.tanggal_banned").value(DEFAULT_TANGGAL_BANNED.toString()))
            .andExpect(jsonPath("$.finish_banned").value(DEFAULT_FINISH_BANNED.toString()))
            .andExpect(jsonPath("$.status_banned").value(DEFAULT_STATUS_BANNED))
            .andExpect(jsonPath("$.createSystemDate").value(DEFAULT_CREATE_SYSTEM_DATE.toString()))
            .andExpect(jsonPath("$.modificationSystemDate").value(DEFAULT_MODIFICATION_SYSTEM_DATE.toString()))
            .andExpect(jsonPath("$.jumlah").value(DEFAULT_JUMLAH));
    }

    @Test
    @Transactional
    public void getNonExistingBanned() throws Exception {
        // Get the banned
        restBannedMockMvc.perform(get("/api/banneds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBanned() throws Exception {
        // Initialize the database
        bannedRepository.saveAndFlush(banned);

        int databaseSizeBeforeUpdate = bannedRepository.findAll().size();

        // Update the banned
        Banned updatedBanned = bannedRepository.findById(banned.getId()).get();
        // Disconnect from session so that the updates on updatedBanned are not directly saved in db
        em.detach(updatedBanned);
        updatedBanned
            .nik(UPDATED_NIK)
            .email(UPDATED_EMAIL)
            .tanggal_banned(UPDATED_TANGGAL_BANNED)
            .finish_banned(UPDATED_FINISH_BANNED)
            .status_banned(UPDATED_STATUS_BANNED)
            .createSystemDate(UPDATED_CREATE_SYSTEM_DATE)
            .modificationSystemDate(UPDATED_MODIFICATION_SYSTEM_DATE)
            .jumlah(UPDATED_JUMLAH);
        BannedDTO bannedDTO = bannedMapper.toDto(updatedBanned);

        restBannedMockMvc.perform(put("/api/banneds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bannedDTO)))
            .andExpect(status().isOk());

        // Validate the Banned in the database
        List<Banned> bannedList = bannedRepository.findAll();
        assertThat(bannedList).hasSize(databaseSizeBeforeUpdate);
        Banned testBanned = bannedList.get(bannedList.size() - 1);
        assertThat(testBanned.getNik()).isEqualTo(UPDATED_NIK);
        assertThat(testBanned.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testBanned.getTanggal_banned()).isEqualTo(UPDATED_TANGGAL_BANNED);
        assertThat(testBanned.getFinish_banned()).isEqualTo(UPDATED_FINISH_BANNED);
        assertThat(testBanned.getStatus_banned()).isEqualTo(UPDATED_STATUS_BANNED);
        assertThat(testBanned.getCreateSystemDate()).isEqualTo(UPDATED_CREATE_SYSTEM_DATE);
        assertThat(testBanned.getModificationSystemDate()).isEqualTo(UPDATED_MODIFICATION_SYSTEM_DATE);
        assertThat(testBanned.getJumlah()).isEqualTo(UPDATED_JUMLAH);
    }

    @Test
    @Transactional
    public void updateNonExistingBanned() throws Exception {
        int databaseSizeBeforeUpdate = bannedRepository.findAll().size();

        // Create the Banned
        BannedDTO bannedDTO = bannedMapper.toDto(banned);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBannedMockMvc.perform(put("/api/banneds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bannedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Banned in the database
        List<Banned> bannedList = bannedRepository.findAll();
        assertThat(bannedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBanned() throws Exception {
        // Initialize the database
        bannedRepository.saveAndFlush(banned);

        int databaseSizeBeforeDelete = bannedRepository.findAll().size();

        // Delete the banned
        restBannedMockMvc.perform(delete("/api/banneds/{id}", banned.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Banned> bannedList = bannedRepository.findAll();
        assertThat(bannedList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
