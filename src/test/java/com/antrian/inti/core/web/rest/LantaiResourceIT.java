package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Lantai;
import com.antrian.inti.core.repository.LantaiRepository;
import com.antrian.inti.core.service.LantaiService;
import com.antrian.inti.core.service.dto.LantaiDTO;
import com.antrian.inti.core.service.mapper.LantaiMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LantaiResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class LantaiResourceIT {

    private static final Long DEFAULT_LANTAI = 1L;
    private static final Long UPDATED_LANTAI = 2L;

    private static final String DEFAULT_DESKRIPSI = "AAAAAAAAAA";
    private static final String UPDATED_DESKRIPSI = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAA";
    private static final String UPDATED_STATUS = "BBB";

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private LantaiRepository lantaiRepository;

    @Autowired
    private LantaiMapper lantaiMapper;

    @Autowired
    private LantaiService lantaiService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLantaiMockMvc;

    private Lantai lantai;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LantaiResource lantaiResource = new LantaiResource(lantaiService);
        this.restLantaiMockMvc = MockMvcBuilders.standaloneSetup(lantaiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lantai createEntity(EntityManager em) {
        Lantai lantai = new Lantai()
            .lantai(DEFAULT_LANTAI)
            .deskripsi(DEFAULT_DESKRIPSI)
            .status(DEFAULT_STATUS)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        return lantai;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Lantai createUpdatedEntity(EntityManager em) {
        Lantai lantai = new Lantai()
            .lantai(UPDATED_LANTAI)
            .deskripsi(UPDATED_DESKRIPSI)
            .status(UPDATED_STATUS)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        return lantai;
    }

    @BeforeEach
    public void initTest() {
        lantai = createEntity(em);
    }

    @Test
    @Transactional
    public void createLantai() throws Exception {
        int databaseSizeBeforeCreate = lantaiRepository.findAll().size();

        // Create the Lantai
        LantaiDTO lantaiDTO = lantaiMapper.toDto(lantai);
        restLantaiMockMvc.perform(post("/api/lantais")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lantaiDTO)))
            .andExpect(status().isCreated());

        // Validate the Lantai in the database
        List<Lantai> lantaiList = lantaiRepository.findAll();
        assertThat(lantaiList).hasSize(databaseSizeBeforeCreate + 1);
        Lantai testLantai = lantaiList.get(lantaiList.size() - 1);
        assertThat(testLantai.getLantai()).isEqualTo(DEFAULT_LANTAI);
        assertThat(testLantai.getDeskripsi()).isEqualTo(DEFAULT_DESKRIPSI);
        assertThat(testLantai.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testLantai.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testLantai.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testLantai.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testLantai.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testLantai.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testLantai.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void createLantaiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lantaiRepository.findAll().size();

        // Create the Lantai with an existing ID
        lantai.setId(1L);
        LantaiDTO lantaiDTO = lantaiMapper.toDto(lantai);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLantaiMockMvc.perform(post("/api/lantais")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lantaiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Lantai in the database
        List<Lantai> lantaiList = lantaiRepository.findAll();
        assertThat(lantaiList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLantais() throws Exception {
        // Initialize the database
        lantaiRepository.saveAndFlush(lantai);

        // Get all the lantaiList
        restLantaiMockMvc.perform(get("/api/lantais?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lantai.getId().intValue())))
            .andExpect(jsonPath("$.[*].lantai").value(hasItem(DEFAULT_LANTAI.intValue())))
            .andExpect(jsonPath("$.[*].deskripsi").value(hasItem(DEFAULT_DESKRIPSI)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))));
    }

    @Test
    @Transactional
    public void getLantai() throws Exception {
        // Initialize the database
        lantaiRepository.saveAndFlush(lantai);

        // Get the lantai
        restLantaiMockMvc.perform(get("/api/lantais/{id}", lantai.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(lantai.getId().intValue()))
            .andExpect(jsonPath("$.lantai").value(DEFAULT_LANTAI.intValue()))
            .andExpect(jsonPath("$.deskripsi").value(DEFAULT_DESKRIPSI))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)));
    }

    @Test
    @Transactional
    public void getNonExistingLantai() throws Exception {
        // Get the lantai
        restLantaiMockMvc.perform(get("/api/lantais/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLantai() throws Exception {
        // Initialize the database
        lantaiRepository.saveAndFlush(lantai);

        int databaseSizeBeforeUpdate = lantaiRepository.findAll().size();

        // Update the lantai
        Lantai updatedLantai = lantaiRepository.findById(lantai.getId()).get();
        // Disconnect from session so that the updates on updatedLantai are not directly saved in db
        em.detach(updatedLantai);
        updatedLantai
            .lantai(UPDATED_LANTAI)
            .deskripsi(UPDATED_DESKRIPSI)
            .status(UPDATED_STATUS)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        LantaiDTO lantaiDTO = lantaiMapper.toDto(updatedLantai);

        restLantaiMockMvc.perform(put("/api/lantais")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lantaiDTO)))
            .andExpect(status().isOk());

        // Validate the Lantai in the database
        List<Lantai> lantaiList = lantaiRepository.findAll();
        assertThat(lantaiList).hasSize(databaseSizeBeforeUpdate);
        Lantai testLantai = lantaiList.get(lantaiList.size() - 1);
        assertThat(testLantai.getLantai()).isEqualTo(UPDATED_LANTAI);
        assertThat(testLantai.getDeskripsi()).isEqualTo(UPDATED_DESKRIPSI);
        assertThat(testLantai.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testLantai.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testLantai.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testLantai.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testLantai.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testLantai.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testLantai.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void updateNonExistingLantai() throws Exception {
        int databaseSizeBeforeUpdate = lantaiRepository.findAll().size();

        // Create the Lantai
        LantaiDTO lantaiDTO = lantaiMapper.toDto(lantai);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLantaiMockMvc.perform(put("/api/lantais")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lantaiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Lantai in the database
        List<Lantai> lantaiList = lantaiRepository.findAll();
        assertThat(lantaiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLantai() throws Exception {
        // Initialize the database
        lantaiRepository.saveAndFlush(lantai);

        int databaseSizeBeforeDelete = lantaiRepository.findAll().size();

        // Delete the lantai
        restLantaiMockMvc.perform(delete("/api/lantais/{id}", lantai.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Lantai> lantaiList = lantaiRepository.findAll();
        assertThat(lantaiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
