package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Kiosk;
import com.antrian.inti.core.repository.KioskRepository;
import com.antrian.inti.core.service.KioskService;
import com.antrian.inti.core.service.dto.KioskDTO;
import com.antrian.inti.core.service.mapper.KioskMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KioskResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class KioskResourceIT {

    private static final String DEFAULT_LOKASI = "AAAAAAAAAA";
    private static final String UPDATED_LOKASI = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD_KIOSK = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD_KIOSK = "BBBBBBBBBB";

    private static final String DEFAULT_DESKRIPSI = "AAAAAAAAAA";
    private static final String UPDATED_DESKRIPSI = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAA";
    private static final String UPDATED_STATUS = "BBB";

    private static final Long DEFAULT_CREATE_USER_ID = 1L;
    private static final Long UPDATED_CREATE_USER_ID = 2L;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LAST_MODIFICATION_USER_ID = 1L;
    private static final Long UPDATED_LAST_MODIFICATION_USER_ID = 2L;

    private static final LocalDate DEFAULT_LAST_MODIFICATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFICATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_MODIFICATION_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private KioskRepository kioskRepository;

    @Autowired
    private KioskMapper kioskMapper;

    @Autowired
    private KioskService kioskService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restKioskMockMvc;

    private Kiosk kiosk;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KioskResource kioskResource = new KioskResource(kioskService);
        this.restKioskMockMvc = MockMvcBuilders.standaloneSetup(kioskResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kiosk createEntity(EntityManager em) {
        Kiosk kiosk = new Kiosk()
            .lokasi(DEFAULT_LOKASI)
            .username(DEFAULT_USERNAME)
            .passwordKiosk(DEFAULT_PASSWORD_KIOSK)
            .deskripsi(DEFAULT_DESKRIPSI)
            .status(DEFAULT_STATUS)
            .createUserId(DEFAULT_CREATE_USER_ID)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .lastModificationUserId(DEFAULT_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(DEFAULT_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(DEFAULT_LAST_MODIFICATION_DATE_TIME);
        return kiosk;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kiosk createUpdatedEntity(EntityManager em) {
        Kiosk kiosk = new Kiosk()
            .lokasi(UPDATED_LOKASI)
            .username(UPDATED_USERNAME)
            .passwordKiosk(UPDATED_PASSWORD_KIOSK)
            .deskripsi(UPDATED_DESKRIPSI)
            .status(UPDATED_STATUS)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        return kiosk;
    }

    @BeforeEach
    public void initTest() {
        kiosk = createEntity(em);
    }

    @Test
    @Transactional
    public void createKiosk() throws Exception {
        int databaseSizeBeforeCreate = kioskRepository.findAll().size();

        // Create the Kiosk
        KioskDTO kioskDTO = kioskMapper.toDto(kiosk);
        restKioskMockMvc.perform(post("/api/kiosks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kioskDTO)))
            .andExpect(status().isCreated());

        // Validate the Kiosk in the database
        List<Kiosk> kioskList = kioskRepository.findAll();
        assertThat(kioskList).hasSize(databaseSizeBeforeCreate + 1);
        Kiosk testKiosk = kioskList.get(kioskList.size() - 1);
        assertThat(testKiosk.getLokasi()).isEqualTo(DEFAULT_LOKASI);
        assertThat(testKiosk.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testKiosk.getPasswordKiosk()).isEqualTo(DEFAULT_PASSWORD_KIOSK);
        assertThat(testKiosk.getDeskripsi()).isEqualTo(DEFAULT_DESKRIPSI);
        assertThat(testKiosk.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testKiosk.getCreateUserId()).isEqualTo(DEFAULT_CREATE_USER_ID);
        assertThat(testKiosk.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testKiosk.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testKiosk.getLastModificationUserId()).isEqualTo(DEFAULT_LAST_MODIFICATION_USER_ID);
        assertThat(testKiosk.getLastModificationDate()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE);
        assertThat(testKiosk.getLastModificationDateTime()).isEqualTo(DEFAULT_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void createKioskWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kioskRepository.findAll().size();

        // Create the Kiosk with an existing ID
        kiosk.setId(1L);
        KioskDTO kioskDTO = kioskMapper.toDto(kiosk);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKioskMockMvc.perform(post("/api/kiosks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kioskDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kiosk in the database
        List<Kiosk> kioskList = kioskRepository.findAll();
        assertThat(kioskList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllKiosks() throws Exception {
        // Initialize the database
        kioskRepository.saveAndFlush(kiosk);

        // Get all the kioskList
        restKioskMockMvc.perform(get("/api/kiosks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kiosk.getId().intValue())))
            .andExpect(jsonPath("$.[*].lokasi").value(hasItem(DEFAULT_LOKASI)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].passwordKiosk").value(hasItem(DEFAULT_PASSWORD_KIOSK)))
            .andExpect(jsonPath("$.[*].deskripsi").value(hasItem(DEFAULT_DESKRIPSI)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].createUserId").value(hasItem(DEFAULT_CREATE_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].lastModificationUserId").value(hasItem(DEFAULT_LAST_MODIFICATION_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].lastModificationDate").value(hasItem(DEFAULT_LAST_MODIFICATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModificationDateTime").value(hasItem(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME))));
    }

    @Test
    @Transactional
    public void getKiosk() throws Exception {
        // Initialize the database
        kioskRepository.saveAndFlush(kiosk);

        // Get the kiosk
        restKioskMockMvc.perform(get("/api/kiosks/{id}", kiosk.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kiosk.getId().intValue()))
            .andExpect(jsonPath("$.lokasi").value(DEFAULT_LOKASI))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.passwordKiosk").value(DEFAULT_PASSWORD_KIOSK))
            .andExpect(jsonPath("$.deskripsi").value(DEFAULT_DESKRIPSI))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.createUserId").value(DEFAULT_CREATE_USER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.lastModificationUserId").value(DEFAULT_LAST_MODIFICATION_USER_ID.intValue()))
            .andExpect(jsonPath("$.lastModificationDate").value(DEFAULT_LAST_MODIFICATION_DATE.toString()))
            .andExpect(jsonPath("$.lastModificationDateTime").value(sameInstant(DEFAULT_LAST_MODIFICATION_DATE_TIME)));
    }

    @Test
    @Transactional
    public void getNonExistingKiosk() throws Exception {
        // Get the kiosk
        restKioskMockMvc.perform(get("/api/kiosks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKiosk() throws Exception {
        // Initialize the database
        kioskRepository.saveAndFlush(kiosk);

        int databaseSizeBeforeUpdate = kioskRepository.findAll().size();

        // Update the kiosk
        Kiosk updatedKiosk = kioskRepository.findById(kiosk.getId()).get();
        // Disconnect from session so that the updates on updatedKiosk are not directly saved in db
        em.detach(updatedKiosk);
        updatedKiosk
            .lokasi(UPDATED_LOKASI)
            .username(UPDATED_USERNAME)
            .passwordKiosk(UPDATED_PASSWORD_KIOSK)
            .deskripsi(UPDATED_DESKRIPSI)
            .status(UPDATED_STATUS)
            .createUserId(UPDATED_CREATE_USER_ID)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .lastModificationUserId(UPDATED_LAST_MODIFICATION_USER_ID)
            .lastModificationDate(UPDATED_LAST_MODIFICATION_DATE)
            .lastModificationDateTime(UPDATED_LAST_MODIFICATION_DATE_TIME);
        KioskDTO kioskDTO = kioskMapper.toDto(updatedKiosk);

        restKioskMockMvc.perform(put("/api/kiosks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kioskDTO)))
            .andExpect(status().isOk());

        // Validate the Kiosk in the database
        List<Kiosk> kioskList = kioskRepository.findAll();
        assertThat(kioskList).hasSize(databaseSizeBeforeUpdate);
        Kiosk testKiosk = kioskList.get(kioskList.size() - 1);
        assertThat(testKiosk.getLokasi()).isEqualTo(UPDATED_LOKASI);
        assertThat(testKiosk.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testKiosk.getPasswordKiosk()).isEqualTo(UPDATED_PASSWORD_KIOSK);
        assertThat(testKiosk.getDeskripsi()).isEqualTo(UPDATED_DESKRIPSI);
        assertThat(testKiosk.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testKiosk.getCreateUserId()).isEqualTo(UPDATED_CREATE_USER_ID);
        assertThat(testKiosk.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testKiosk.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testKiosk.getLastModificationUserId()).isEqualTo(UPDATED_LAST_MODIFICATION_USER_ID);
        assertThat(testKiosk.getLastModificationDate()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE);
        assertThat(testKiosk.getLastModificationDateTime()).isEqualTo(UPDATED_LAST_MODIFICATION_DATE_TIME);
    }

    @Test
    @Transactional
    public void updateNonExistingKiosk() throws Exception {
        int databaseSizeBeforeUpdate = kioskRepository.findAll().size();

        // Create the Kiosk
        KioskDTO kioskDTO = kioskMapper.toDto(kiosk);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKioskMockMvc.perform(put("/api/kiosks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kioskDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kiosk in the database
        List<Kiosk> kioskList = kioskRepository.findAll();
        assertThat(kioskList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKiosk() throws Exception {
        // Initialize the database
        kioskRepository.saveAndFlush(kiosk);

        int databaseSizeBeforeDelete = kioskRepository.findAll().size();

        // Delete the kiosk
        restKioskMockMvc.perform(delete("/api/kiosks/{id}", kiosk.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kiosk> kioskList = kioskRepository.findAll();
        assertThat(kioskList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
