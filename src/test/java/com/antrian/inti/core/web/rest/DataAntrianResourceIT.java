package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.DataAntrian;
import com.antrian.inti.core.repository.DataAntrianRepository;
import com.antrian.inti.core.service.DataAntrianService;
import com.antrian.inti.core.service.dto.DataAntrianDTO;
import com.antrian.inti.core.service.mapper.DataAntrianMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DataAntrianResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class DataAntrianResourceIT {

    private static final LocalDate DEFAULT_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final Long DEFAULT_NO_URUT = 1L;
    private static final Long UPDATED_NO_URUT = 2L;

    private static final Boolean DEFAULT_STATUS_PANGGILAN = false;
    private static final Boolean UPDATED_STATUS_PANGGILAN = true;

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final String DEFAULT_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_LAYANAN = "BBBBBBBBBB";

    private static final Long DEFAULT_LOKET = 1L;
    private static final Long UPDATED_LOKET = 2L;

    private static final String DEFAULT_PREFIX_SUARA = "AAAAAAAAAA";
    private static final String UPDATED_PREFIX_SUARA = "BBBBBBBBBB";

    private static final Long DEFAULT_LANTAI = 1L;
    private static final Long UPDATED_LANTAI = 2L;

    private static final Boolean DEFAULT_STATUS_CURRENT = false;
    private static final Boolean UPDATED_STATUS_CURRENT = true;

    private static final String DEFAULT_KODE_KONFIRMASI = "AAAAAAAAAA";
    private static final String UPDATED_KODE_KONFIRMASI = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE_WEBCAM = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_WEBCAM = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_WEBCAM_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_WEBCAM_CONTENT_TYPE = "image/png";

    private static final Boolean DEFAULT_LAYANAN_ONLINE = false;
    private static final Boolean UPDATED_LAYANAN_ONLINE = true;

    private static final String DEFAULT_DURASI = "AAAAAAAAAA";
    private static final String UPDATED_DURASI = "BBBBBBBBBB";

    private static final String DEFAULT_SISA_DURASI = "AAAAAAAAAA";
    private static final String UPDATED_SISA_DURASI = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS_KONSELING = false;
    private static final Boolean UPDATED_STATUS_KONSELING = true;

    private static final String DEFAULT_TANGGAL_WAKTU = "AAAAAAAAAA";
    private static final String UPDATED_TANGGAL_WAKTU = "BBBBBBBBBB";

    @Autowired
    private DataAntrianRepository dataAntrianRepository;

    @Autowired
    private DataAntrianMapper dataAntrianMapper;

    @Autowired
    private DataAntrianService dataAntrianService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDataAntrianMockMvc;

    private DataAntrian dataAntrian;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataAntrianResource dataAntrianResource = new DataAntrianResource(dataAntrianService);
        this.restDataAntrianMockMvc = MockMvcBuilders.standaloneSetup(dataAntrianResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataAntrian createEntity(EntityManager em) {
        DataAntrian dataAntrian = new DataAntrian()
            .tanggal(DEFAULT_TANGGAL)
            .noUrut(DEFAULT_NO_URUT)
            .statusPanggilan(DEFAULT_STATUS_PANGGILAN)
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .layanan(DEFAULT_LAYANAN)
            .loket(DEFAULT_LOKET)
            .prefixSuara(DEFAULT_PREFIX_SUARA)
            .lantai(DEFAULT_LANTAI)
            .statusCurrent(DEFAULT_STATUS_CURRENT)
            .kodeKonfirmasi(DEFAULT_KODE_KONFIRMASI)
            .imageWebcam(DEFAULT_IMAGE_WEBCAM)
            .imageWebcamContentType(DEFAULT_IMAGE_WEBCAM_CONTENT_TYPE)
            .layananOnline(DEFAULT_LAYANAN_ONLINE)
            .durasi(DEFAULT_DURASI)
            .sisaDurasi(DEFAULT_SISA_DURASI)
            .statusKonseling(DEFAULT_STATUS_KONSELING)
            .tanggalWaktu(DEFAULT_TANGGAL_WAKTU);
        return dataAntrian;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataAntrian createUpdatedEntity(EntityManager em) {
        DataAntrian dataAntrian = new DataAntrian()
            .tanggal(UPDATED_TANGGAL)
            .noUrut(UPDATED_NO_URUT)
            .statusPanggilan(UPDATED_STATUS_PANGGILAN)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .layanan(UPDATED_LAYANAN)
            .loket(UPDATED_LOKET)
            .prefixSuara(UPDATED_PREFIX_SUARA)
            .lantai(UPDATED_LANTAI)
            .statusCurrent(UPDATED_STATUS_CURRENT)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .imageWebcam(UPDATED_IMAGE_WEBCAM)
            .imageWebcamContentType(UPDATED_IMAGE_WEBCAM_CONTENT_TYPE)
            .layananOnline(UPDATED_LAYANAN_ONLINE)
            .durasi(UPDATED_DURASI)
            .sisaDurasi(UPDATED_SISA_DURASI)
            .statusKonseling(UPDATED_STATUS_KONSELING)
            .tanggalWaktu(UPDATED_TANGGAL_WAKTU);
        return dataAntrian;
    }

    @BeforeEach
    public void initTest() {
        dataAntrian = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataAntrian() throws Exception {
        int databaseSizeBeforeCreate = dataAntrianRepository.findAll().size();

        // Create the DataAntrian
        DataAntrianDTO dataAntrianDTO = dataAntrianMapper.toDto(dataAntrian);
        restDataAntrianMockMvc.perform(post("/api/data-antrians")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dataAntrianDTO)))
            .andExpect(status().isCreated());

        // Validate the DataAntrian in the database
        List<DataAntrian> dataAntrianList = dataAntrianRepository.findAll();
        assertThat(dataAntrianList).hasSize(databaseSizeBeforeCreate + 1);
        DataAntrian testDataAntrian = dataAntrianList.get(dataAntrianList.size() - 1);
        assertThat(testDataAntrian.getTanggal()).isEqualTo(DEFAULT_TANGGAL);
        assertThat(testDataAntrian.getNoUrut()).isEqualTo(DEFAULT_NO_URUT);
        assertThat(testDataAntrian.isStatusPanggilan()).isEqualTo(DEFAULT_STATUS_PANGGILAN);
        assertThat(testDataAntrian.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testDataAntrian.getLayanan()).isEqualTo(DEFAULT_LAYANAN);
        assertThat(testDataAntrian.getLoket()).isEqualTo(DEFAULT_LOKET);
        assertThat(testDataAntrian.getPrefixSuara()).isEqualTo(DEFAULT_PREFIX_SUARA);
        assertThat(testDataAntrian.getLantai()).isEqualTo(DEFAULT_LANTAI);
        assertThat(testDataAntrian.isStatusCurrent()).isEqualTo(DEFAULT_STATUS_CURRENT);
        assertThat(testDataAntrian.getKodeKonfirmasi()).isEqualTo(DEFAULT_KODE_KONFIRMASI);
        assertThat(testDataAntrian.getImageWebcam()).isEqualTo(DEFAULT_IMAGE_WEBCAM);
        assertThat(testDataAntrian.getImageWebcamContentType()).isEqualTo(DEFAULT_IMAGE_WEBCAM_CONTENT_TYPE);
        assertThat(testDataAntrian.isLayananOnline()).isEqualTo(DEFAULT_LAYANAN_ONLINE);
        assertThat(testDataAntrian.getDurasi()).isEqualTo(DEFAULT_DURASI);
        assertThat(testDataAntrian.getSisaDurasi()).isEqualTo(DEFAULT_SISA_DURASI);
        assertThat(testDataAntrian.isStatusKonseling()).isEqualTo(DEFAULT_STATUS_KONSELING);
        assertThat(testDataAntrian.getTanggalWaktu()).isEqualTo(DEFAULT_TANGGAL_WAKTU);
    }

    @Test
    @Transactional
    public void createDataAntrianWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataAntrianRepository.findAll().size();

        // Create the DataAntrian with an existing ID
        dataAntrian.setId(1L);
        DataAntrianDTO dataAntrianDTO = dataAntrianMapper.toDto(dataAntrian);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataAntrianMockMvc.perform(post("/api/data-antrians")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dataAntrianDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DataAntrian in the database
        List<DataAntrian> dataAntrianList = dataAntrianRepository.findAll();
        assertThat(dataAntrianList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDataAntrians() throws Exception {
        // Initialize the database
        dataAntrianRepository.saveAndFlush(dataAntrian);

        // Get all the dataAntrianList
        restDataAntrianMockMvc.perform(get("/api/data-antrians?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataAntrian.getId().intValue())))
            .andExpect(jsonPath("$.[*].tanggal").value(hasItem(DEFAULT_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].noUrut").value(hasItem(DEFAULT_NO_URUT.intValue())))
            .andExpect(jsonPath("$.[*].statusPanggilan").value(hasItem(DEFAULT_STATUS_PANGGILAN.booleanValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].layanan").value(hasItem(DEFAULT_LAYANAN)))
            .andExpect(jsonPath("$.[*].loket").value(hasItem(DEFAULT_LOKET.intValue())))
            .andExpect(jsonPath("$.[*].prefixSuara").value(hasItem(DEFAULT_PREFIX_SUARA)))
            .andExpect(jsonPath("$.[*].lantai").value(hasItem(DEFAULT_LANTAI.intValue())))
            .andExpect(jsonPath("$.[*].statusCurrent").value(hasItem(DEFAULT_STATUS_CURRENT.booleanValue())))
            .andExpect(jsonPath("$.[*].kodeKonfirmasi").value(hasItem(DEFAULT_KODE_KONFIRMASI)))
            .andExpect(jsonPath("$.[*].imageWebcamContentType").value(hasItem(DEFAULT_IMAGE_WEBCAM_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageWebcam").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_WEBCAM))))
            .andExpect(jsonPath("$.[*].layananOnline").value(hasItem(DEFAULT_LAYANAN_ONLINE.booleanValue())))
            .andExpect(jsonPath("$.[*].durasi").value(hasItem(DEFAULT_DURASI)))
            .andExpect(jsonPath("$.[*].sisaDurasi").value(hasItem(DEFAULT_SISA_DURASI)))
            .andExpect(jsonPath("$.[*].statusKonseling").value(hasItem(DEFAULT_STATUS_KONSELING.booleanValue())))
            .andExpect(jsonPath("$.[*].tanggalWaktu").value(hasItem(DEFAULT_TANGGAL_WAKTU)));
    }

    @Test
    @Transactional
    public void getDataAntrian() throws Exception {
        // Initialize the database
        dataAntrianRepository.saveAndFlush(dataAntrian);

        // Get the dataAntrian
        restDataAntrianMockMvc.perform(get("/api/data-antrians/{id}", dataAntrian.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dataAntrian.getId().intValue()))
            .andExpect(jsonPath("$.tanggal").value(DEFAULT_TANGGAL.toString()))
            .andExpect(jsonPath("$.noUrut").value(DEFAULT_NO_URUT.intValue()))
            .andExpect(jsonPath("$.statusPanggilan").value(DEFAULT_STATUS_PANGGILAN.booleanValue()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.layanan").value(DEFAULT_LAYANAN))
            .andExpect(jsonPath("$.loket").value(DEFAULT_LOKET.intValue()))
            .andExpect(jsonPath("$.prefixSuara").value(DEFAULT_PREFIX_SUARA))
            .andExpect(jsonPath("$.lantai").value(DEFAULT_LANTAI.intValue()))
            .andExpect(jsonPath("$.statusCurrent").value(DEFAULT_STATUS_CURRENT.booleanValue()))
            .andExpect(jsonPath("$.kodeKonfirmasi").value(DEFAULT_KODE_KONFIRMASI))
            .andExpect(jsonPath("$.imageWebcamContentType").value(DEFAULT_IMAGE_WEBCAM_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageWebcam").value(Base64Utils.encodeToString(DEFAULT_IMAGE_WEBCAM)))
            .andExpect(jsonPath("$.layananOnline").value(DEFAULT_LAYANAN_ONLINE.booleanValue()))
            .andExpect(jsonPath("$.durasi").value(DEFAULT_DURASI))
            .andExpect(jsonPath("$.sisaDurasi").value(DEFAULT_SISA_DURASI))
            .andExpect(jsonPath("$.statusKonseling").value(DEFAULT_STATUS_KONSELING.booleanValue()))
            .andExpect(jsonPath("$.tanggalWaktu").value(DEFAULT_TANGGAL_WAKTU));
    }

    @Test
    @Transactional
    public void getNonExistingDataAntrian() throws Exception {
        // Get the dataAntrian
        restDataAntrianMockMvc.perform(get("/api/data-antrians/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataAntrian() throws Exception {
        // Initialize the database
        dataAntrianRepository.saveAndFlush(dataAntrian);

        int databaseSizeBeforeUpdate = dataAntrianRepository.findAll().size();

        // Update the dataAntrian
        DataAntrian updatedDataAntrian = dataAntrianRepository.findById(dataAntrian.getId()).get();
        // Disconnect from session so that the updates on updatedDataAntrian are not directly saved in db
        em.detach(updatedDataAntrian);
        updatedDataAntrian
            .tanggal(UPDATED_TANGGAL)
            .noUrut(UPDATED_NO_URUT)
            .statusPanggilan(UPDATED_STATUS_PANGGILAN)
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .layanan(UPDATED_LAYANAN)
            .loket(UPDATED_LOKET)
            .prefixSuara(UPDATED_PREFIX_SUARA)
            .lantai(UPDATED_LANTAI)
            .statusCurrent(UPDATED_STATUS_CURRENT)
            .kodeKonfirmasi(UPDATED_KODE_KONFIRMASI)
            .imageWebcam(UPDATED_IMAGE_WEBCAM)
            .imageWebcamContentType(UPDATED_IMAGE_WEBCAM_CONTENT_TYPE)
            .layananOnline(UPDATED_LAYANAN_ONLINE)
            .durasi(UPDATED_DURASI)
            .sisaDurasi(UPDATED_SISA_DURASI)
            .statusKonseling(UPDATED_STATUS_KONSELING)
            .tanggalWaktu(UPDATED_TANGGAL_WAKTU);
        DataAntrianDTO dataAntrianDTO = dataAntrianMapper.toDto(updatedDataAntrian);

        restDataAntrianMockMvc.perform(put("/api/data-antrians")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dataAntrianDTO)))
            .andExpect(status().isOk());

        // Validate the DataAntrian in the database
        List<DataAntrian> dataAntrianList = dataAntrianRepository.findAll();
        assertThat(dataAntrianList).hasSize(databaseSizeBeforeUpdate);
        DataAntrian testDataAntrian = dataAntrianList.get(dataAntrianList.size() - 1);
        assertThat(testDataAntrian.getTanggal()).isEqualTo(UPDATED_TANGGAL);
        assertThat(testDataAntrian.getNoUrut()).isEqualTo(UPDATED_NO_URUT);
        assertThat(testDataAntrian.isStatusPanggilan()).isEqualTo(UPDATED_STATUS_PANGGILAN);
        assertThat(testDataAntrian.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testDataAntrian.getLayanan()).isEqualTo(UPDATED_LAYANAN);
        assertThat(testDataAntrian.getLoket()).isEqualTo(UPDATED_LOKET);
        assertThat(testDataAntrian.getPrefixSuara()).isEqualTo(UPDATED_PREFIX_SUARA);
        assertThat(testDataAntrian.getLantai()).isEqualTo(UPDATED_LANTAI);
        assertThat(testDataAntrian.isStatusCurrent()).isEqualTo(UPDATED_STATUS_CURRENT);
        assertThat(testDataAntrian.getKodeKonfirmasi()).isEqualTo(UPDATED_KODE_KONFIRMASI);
        assertThat(testDataAntrian.getImageWebcam()).isEqualTo(UPDATED_IMAGE_WEBCAM);
        assertThat(testDataAntrian.getImageWebcamContentType()).isEqualTo(UPDATED_IMAGE_WEBCAM_CONTENT_TYPE);
        assertThat(testDataAntrian.isLayananOnline()).isEqualTo(UPDATED_LAYANAN_ONLINE);
        assertThat(testDataAntrian.getDurasi()).isEqualTo(UPDATED_DURASI);
        assertThat(testDataAntrian.getSisaDurasi()).isEqualTo(UPDATED_SISA_DURASI);
        assertThat(testDataAntrian.isStatusKonseling()).isEqualTo(UPDATED_STATUS_KONSELING);
        assertThat(testDataAntrian.getTanggalWaktu()).isEqualTo(UPDATED_TANGGAL_WAKTU);
    }

    @Test
    @Transactional
    public void updateNonExistingDataAntrian() throws Exception {
        int databaseSizeBeforeUpdate = dataAntrianRepository.findAll().size();

        // Create the DataAntrian
        DataAntrianDTO dataAntrianDTO = dataAntrianMapper.toDto(dataAntrian);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDataAntrianMockMvc.perform(put("/api/data-antrians")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(dataAntrianDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DataAntrian in the database
        List<DataAntrian> dataAntrianList = dataAntrianRepository.findAll();
        assertThat(dataAntrianList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDataAntrian() throws Exception {
        // Initialize the database
        dataAntrianRepository.saveAndFlush(dataAntrian);

        int databaseSizeBeforeDelete = dataAntrianRepository.findAll().size();

        // Delete the dataAntrian
        restDataAntrianMockMvc.perform(delete("/api/data-antrians/{id}", dataAntrian.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DataAntrian> dataAntrianList = dataAntrianRepository.findAll();
        assertThat(dataAntrianList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
