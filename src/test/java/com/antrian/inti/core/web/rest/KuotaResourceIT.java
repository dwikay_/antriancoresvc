package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Kuota;
import com.antrian.inti.core.repository.KuotaRepository;
import com.antrian.inti.core.service.KuotaService;
import com.antrian.inti.core.service.dto.KuotaDTO;
import com.antrian.inti.core.service.mapper.KuotaMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import javax.print.attribute.standard.Media;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KuotaResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class KuotaResourceIT {

    private static final Long DEFAULT_ID_SUB_LAYANAN = 1L;
    private static final Long UPDATED_ID_SUB_LAYANAN = 2L;

    private static final String DEFAULT_QUOTA = "AAAAAAAAAA";
    private static final String UPDATED_QUOTA = "BBBBBBBBBB";

    private static final String DEFAULT_JENIS_LAYANAN = "AAAAAAAAAA";
    private static final String UPDATED_JENIS_LAYANAN = "BBBBBBBBBB";

    @Autowired
    private KuotaRepository kuotaRepository;

    @Autowired
    private KuotaMapper kuotaMapper;

    @Autowired
    private KuotaService kuotaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restKuotaMockMvc;

    private Kuota kuota;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KuotaResource kuotaResource = new KuotaResource(kuotaService);
        this.restKuotaMockMvc = MockMvcBuilders.standaloneSetup(kuotaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kuota createEntity(EntityManager em) {
        Kuota kuota = new Kuota()
            .idSubLayanan(DEFAULT_ID_SUB_LAYANAN)
            .quota(DEFAULT_QUOTA)
            .jenisLayanan(DEFAULT_JENIS_LAYANAN);
        return kuota;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kuota createUpdatedEntity(EntityManager em) {
        Kuota kuota = new Kuota()
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .quota(UPDATED_QUOTA)
            .jenisLayanan(UPDATED_JENIS_LAYANAN);
        return kuota;
    }

    @BeforeEach
    public void initTest() {
        kuota = createEntity(em);
    }

    @Test
    @Transactional
    public void createKuota() throws Exception {
        int databaseSizeBeforeCreate = kuotaRepository.findAll().size();

        // Create the Kuota
        KuotaDTO kuotaDTO = kuotaMapper.toDto(kuota);
        restKuotaMockMvc.perform(post("/api/kuotas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kuotaDTO)))
            .andExpect(status().isCreated());

        // Validate the Kuota in the database
        List<Kuota> kuotaList = kuotaRepository.findAll();
        assertThat(kuotaList).hasSize(databaseSizeBeforeCreate + 1);
        Kuota testKuota = kuotaList.get(kuotaList.size() - 1);
        assertThat(testKuota.getIdSubLayanan()).isEqualTo(DEFAULT_ID_SUB_LAYANAN);
        assertThat(testKuota.getQuota()).isEqualTo(DEFAULT_QUOTA);
        assertThat(testKuota.getJenisLayanan()).isEqualTo(DEFAULT_JENIS_LAYANAN);
    }

    @Test
    @Transactional
    public void createKuotaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = kuotaRepository.findAll().size();

        // Create the Kuota with an existing ID
        kuota.setId(1L);
        KuotaDTO kuotaDTO = kuotaMapper.toDto(kuota);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKuotaMockMvc.perform(post("/api/kuotas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kuotaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kuota in the database
        List<Kuota> kuotaList = kuotaRepository.findAll();
        assertThat(kuotaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllKuotas() throws Exception {
        // Initialize the database
        kuotaRepository.saveAndFlush(kuota);

        // Get all the kuotaList
        restKuotaMockMvc.perform(get("/api/kuotas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kuota.getId().intValue())))
            .andExpect(jsonPath("$.[*].idSubLayanan").value(hasItem(DEFAULT_ID_SUB_LAYANAN.intValue())))
            .andExpect(jsonPath("$.[*].quota").value(hasItem(DEFAULT_QUOTA)))
            .andExpect(jsonPath("$.[*].jenisLayanan").value(hasItem(DEFAULT_JENIS_LAYANAN)));
    }

    @Test
    @Transactional
    public void getKuota() throws Exception {
        // Initialize the database
        kuotaRepository.saveAndFlush(kuota);

        // Get the kuota
        restKuotaMockMvc.perform(get("/api/kuotas/{id}", kuota.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kuota.getId().intValue()))
            .andExpect(jsonPath("$.idSubLayanan").value(DEFAULT_ID_SUB_LAYANAN.intValue()))
            .andExpect(jsonPath("$.quota").value(DEFAULT_QUOTA))
            .andExpect(jsonPath("$.jenisLayanan").value(DEFAULT_JENIS_LAYANAN));
    }

    @Test
    @Transactional
    public void getNonExistingKuota() throws Exception {
        // Get the kuota
        restKuotaMockMvc.perform(get("/api/kuotas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKuota() throws Exception {
        // Initialize the database
        kuotaRepository.saveAndFlush(kuota);

        int databaseSizeBeforeUpdate = kuotaRepository.findAll().size();

        // Update the kuota
        Kuota updatedKuota = kuotaRepository.findById(kuota.getId()).get();
        // Disconnect from session so that the updates on updatedKuota are not directly saved in db
        em.detach(updatedKuota);
        updatedKuota
            .idSubLayanan(UPDATED_ID_SUB_LAYANAN)
            .quota(UPDATED_QUOTA)
            .jenisLayanan(UPDATED_JENIS_LAYANAN);
        KuotaDTO kuotaDTO = kuotaMapper.toDto(updatedKuota);

        restKuotaMockMvc.perform(put("/api/kuotas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kuotaDTO)))
            .andExpect(status().isOk());

        // Validate the Kuota in the database
        List<Kuota> kuotaList = kuotaRepository.findAll();
        assertThat(kuotaList).hasSize(databaseSizeBeforeUpdate);
        Kuota testKuota = kuotaList.get(kuotaList.size() - 1);
        assertThat(testKuota.getIdSubLayanan()).isEqualTo(UPDATED_ID_SUB_LAYANAN);
        assertThat(testKuota.getQuota()).isEqualTo(UPDATED_QUOTA);
        assertThat(testKuota.getJenisLayanan()).isEqualTo(UPDATED_JENIS_LAYANAN);
    }

    @Test
    @Transactional
    public void updateNonExistingKuota() throws Exception {
        int databaseSizeBeforeUpdate = kuotaRepository.findAll().size();

        // Create the Kuota
        KuotaDTO kuotaDTO = kuotaMapper.toDto(kuota);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKuotaMockMvc.perform(put("/api/kuotas")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(kuotaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kuota in the database
        List<Kuota> kuotaList = kuotaRepository.findAll();
        assertThat(kuotaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKuota() throws Exception {
        // Initialize the database
        kuotaRepository.saveAndFlush(kuota);

        int databaseSizeBeforeDelete = kuotaRepository.findAll().size();

        // Delete the kuota
        restKuotaMockMvc.perform(delete("/api/kuotas/{id}", kuota.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kuota> kuotaList = kuotaRepository.findAll();
        assertThat(kuotaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
