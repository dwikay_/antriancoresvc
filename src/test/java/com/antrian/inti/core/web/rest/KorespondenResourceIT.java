package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.AntriancoresvcApp;
import com.antrian.inti.core.domain.Koresponden;
import com.antrian.inti.core.repository.KorespondenRepository;
import com.antrian.inti.core.service.KorespondenService;
import com.antrian.inti.core.service.dto.KorespondenDTO;
import com.antrian.inti.core.service.mapper.KorespondenMapper;
import com.antrian.inti.core.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.antrian.inti.core.web.rest.TestUtil.sameInstant;
import static com.antrian.inti.core.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KorespondenResource} REST controller.
 */
@SpringBootTest(classes = AntriancoresvcApp.class)
public class KorespondenResourceIT {

    private static final String DEFAULT_NIK = "AAAAAAAAAA";
    private static final String UPDATED_NIK = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA = "AAAAAAAAAA";
    private static final String UPDATED_NAMA = "BBBBBBBBBB";

    private static final String DEFAULT_NAMA_PERUSAHAAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_PERUSAHAAN = "BBBBBBBBBB";

    private static final String DEFAULT_NO_TELP = "AAAAAAAAAA";
    private static final String UPDATED_NO_TELP = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_KODE_VERIFIKASI = "AAAAAAAAAA";
    private static final String UPDATED_KODE_VERIFIKASI = "BBBBBBBBBB";

    private static final Double DEFAULT_NILAI_PERSYARATAN = 1D;
    private static final Double UPDATED_NILAI_PERSYARATAN = 2D;

    private static final Double DEFAULT_NILAI_PROSEDUR = 1D;
    private static final Double UPDATED_NILAI_PROSEDUR = 2D;

    private static final Double DEFAULT_NILAI_WAKTU_PELAYANAN = 1D;
    private static final Double UPDATED_NILAI_WAKTU_PELAYANAN = 2D;

    private static final Double DEFAULT_NILAI_BIAYA_TARIF = 1D;
    private static final Double UPDATED_NILAI_BIAYA_TARIF = 2D;

    private static final Double DEFAULT_NILAI_PRODUK_PELAYANAN_1 = 1D;
    private static final Double UPDATED_NILAI_PRODUK_PELAYANAN_1 = 2D;

    private static final Double DEFAULT_NILAI_PRODUK_PELAYANAN_2 = 1D;
    private static final Double UPDATED_NILAI_PRODUK_PELAYANAN_2 = 2D;

    private static final Double DEFAULT_NILAI_KOM_PEL_1 = 1D;
    private static final Double UPDATED_NILAI_KOM_PEL_1 = 2D;

    private static final Double DEFAULT_NILAI_KOM_PEL_2 = 1D;
    private static final Double UPDATED_NILAI_KOM_PEL_2 = 2D;

    private static final Double DEFAULT_NILAI_KOM_PEL_3 = 1D;
    private static final Double UPDATED_NILAI_KOM_PEL_3 = 2D;

    private static final Double DEFAULT_NILAI_KOM_PEL_4 = 1D;
    private static final Double UPDATED_NILAI_KOM_PEL_4 = 2D;

    private static final Double DEFAULT_NILAI_PERILAKU_PELAKSANA_1 = 1D;
    private static final Double UPDATED_NILAI_PERILAKU_PELAKSANA_1 = 2D;

    private static final Double DEFAULT_NILAI_PRILAKU_PELAKSANA_2 = 1D;
    private static final Double UPDATED_NILAI_PRILAKU_PELAKSANA_2 = 2D;

    private static final Double DEFAULT_NILAI_SAR_PRAS = 1D;
    private static final Double UPDATED_NILAI_SAR_PRAS = 2D;

    private static final Double DEFAULT_NILAI_SARAN_MASUK = 1D;
    private static final Double UPDATED_NILAI_SARAN_MASUK = 2D;

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_JENIS_KELAMIN = false;
    private static final Boolean UPDATED_JENIS_KELAMIN = true;

    private static final String DEFAULT_PEKERJAAN = "AAAAAAAAAA";
    private static final String UPDATED_PEKERJAAN = "BBBBBBBBBB";

    private static final String DEFAULT_PENDIDIKAN = "AAAAAAAAAA";
    private static final String UPDATED_PENDIDIKAN = "BBBBBBBBBB";

    @Autowired
    private KorespondenRepository korespondenRepository;

    @Autowired
    private KorespondenMapper korespondenMapper;

    @Autowired
    private KorespondenService korespondenService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restKorespondenMockMvc;

    private Koresponden koresponden;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KorespondenResource korespondenResource = new KorespondenResource(korespondenService);
        this.restKorespondenMockMvc = MockMvcBuilders.standaloneSetup(korespondenResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Koresponden createEntity(EntityManager em) {
        Koresponden koresponden = new Koresponden()
            .nik(DEFAULT_NIK)
            .nama(DEFAULT_NAMA)
            .namaPerusahaan(DEFAULT_NAMA_PERUSAHAAN)
            .noTelp(DEFAULT_NO_TELP)
            .email(DEFAULT_EMAIL)
            .kodeVerifikasi(DEFAULT_KODE_VERIFIKASI)
            .nilaiPersyaratan(DEFAULT_NILAI_PERSYARATAN)
            .nilaiProsedur(DEFAULT_NILAI_PROSEDUR)
            .nilaiWaktuPelayanan(DEFAULT_NILAI_WAKTU_PELAYANAN)
            .nilaiBiayaTarif(DEFAULT_NILAI_BIAYA_TARIF)
            .nilaiProdukPelayanan1(DEFAULT_NILAI_PRODUK_PELAYANAN_1)
            .nilaiProdukPelayanan2(DEFAULT_NILAI_PRODUK_PELAYANAN_2)
            .nilaiKomPel1(DEFAULT_NILAI_KOM_PEL_1)
            .nilaiKomPel2(DEFAULT_NILAI_KOM_PEL_2)
            .nilaiKomPel3(DEFAULT_NILAI_KOM_PEL_3)
            .nilaiKomPel4(DEFAULT_NILAI_KOM_PEL_4)
            .nilaiPerilakuPelaksana1(DEFAULT_NILAI_PERILAKU_PELAKSANA_1)
            .nilaiPrilakuPelaksana2(DEFAULT_NILAI_PRILAKU_PELAKSANA_2)
            .nilaiSarPras(DEFAULT_NILAI_SAR_PRAS)
            .nilaiSaranMasuk(DEFAULT_NILAI_SARAN_MASUK)
            .createDate(DEFAULT_CREATE_DATE)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .jenisKelamin(DEFAULT_JENIS_KELAMIN)
            .pekerjaan(DEFAULT_PEKERJAAN)
            .pendidikan(DEFAULT_PENDIDIKAN);
        return koresponden;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Koresponden createUpdatedEntity(EntityManager em) {
        Koresponden koresponden = new Koresponden()
            .nik(UPDATED_NIK)
            .nama(UPDATED_NAMA)
            .namaPerusahaan(UPDATED_NAMA_PERUSAHAAN)
            .noTelp(UPDATED_NO_TELP)
            .email(UPDATED_EMAIL)
            .kodeVerifikasi(UPDATED_KODE_VERIFIKASI)
            .nilaiPersyaratan(UPDATED_NILAI_PERSYARATAN)
            .nilaiProsedur(UPDATED_NILAI_PROSEDUR)
            .nilaiWaktuPelayanan(UPDATED_NILAI_WAKTU_PELAYANAN)
            .nilaiBiayaTarif(UPDATED_NILAI_BIAYA_TARIF)
            .nilaiProdukPelayanan1(UPDATED_NILAI_PRODUK_PELAYANAN_1)
            .nilaiProdukPelayanan2(UPDATED_NILAI_PRODUK_PELAYANAN_2)
            .nilaiKomPel1(UPDATED_NILAI_KOM_PEL_1)
            .nilaiKomPel2(UPDATED_NILAI_KOM_PEL_2)
            .nilaiKomPel3(UPDATED_NILAI_KOM_PEL_3)
            .nilaiKomPel4(UPDATED_NILAI_KOM_PEL_4)
            .nilaiPerilakuPelaksana1(UPDATED_NILAI_PERILAKU_PELAKSANA_1)
            .nilaiPrilakuPelaksana2(UPDATED_NILAI_PRILAKU_PELAKSANA_2)
            .nilaiSarPras(UPDATED_NILAI_SAR_PRAS)
            .nilaiSaranMasuk(UPDATED_NILAI_SARAN_MASUK)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .pekerjaan(UPDATED_PEKERJAAN)
            .pendidikan(UPDATED_PENDIDIKAN);
        return koresponden;
    }

    @BeforeEach
    public void initTest() {
        koresponden = createEntity(em);
    }

    @Test
    @Transactional
    public void createKoresponden() throws Exception {
        int databaseSizeBeforeCreate = korespondenRepository.findAll().size();

        // Create the Koresponden
        KorespondenDTO korespondenDTO = korespondenMapper.toDto(koresponden);
        restKorespondenMockMvc.perform(post("/api/korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(korespondenDTO)))
            .andExpect(status().isCreated());

        // Validate the Koresponden in the database
        List<Koresponden> korespondenList = korespondenRepository.findAll();
        assertThat(korespondenList).hasSize(databaseSizeBeforeCreate + 1);
        Koresponden testKoresponden = korespondenList.get(korespondenList.size() - 1);
        assertThat(testKoresponden.getNik()).isEqualTo(DEFAULT_NIK);
        assertThat(testKoresponden.getNama()).isEqualTo(DEFAULT_NAMA);
        assertThat(testKoresponden.getNamaPerusahaan()).isEqualTo(DEFAULT_NAMA_PERUSAHAAN);
        assertThat(testKoresponden.getNoTelp()).isEqualTo(DEFAULT_NO_TELP);
        assertThat(testKoresponden.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testKoresponden.getKodeVerifikasi()).isEqualTo(DEFAULT_KODE_VERIFIKASI);
        assertThat(testKoresponden.getNilaiPersyaratan()).isEqualTo(DEFAULT_NILAI_PERSYARATAN);
        assertThat(testKoresponden.getNilaiProsedur()).isEqualTo(DEFAULT_NILAI_PROSEDUR);
        assertThat(testKoresponden.getNilaiWaktuPelayanan()).isEqualTo(DEFAULT_NILAI_WAKTU_PELAYANAN);
        assertThat(testKoresponden.getNilaiBiayaTarif()).isEqualTo(DEFAULT_NILAI_BIAYA_TARIF);
        assertThat(testKoresponden.getNilaiProdukPelayanan1()).isEqualTo(DEFAULT_NILAI_PRODUK_PELAYANAN_1);
        assertThat(testKoresponden.getNilaiProdukPelayanan2()).isEqualTo(DEFAULT_NILAI_PRODUK_PELAYANAN_2);
        assertThat(testKoresponden.getNilaiKomPel1()).isEqualTo(DEFAULT_NILAI_KOM_PEL_1);
        assertThat(testKoresponden.getNilaiKomPel2()).isEqualTo(DEFAULT_NILAI_KOM_PEL_2);
        assertThat(testKoresponden.getNilaiKomPel3()).isEqualTo(DEFAULT_NILAI_KOM_PEL_3);
        assertThat(testKoresponden.getNilaiKomPel4()).isEqualTo(DEFAULT_NILAI_KOM_PEL_4);
        assertThat(testKoresponden.getNilaiPerilakuPelaksana1()).isEqualTo(DEFAULT_NILAI_PERILAKU_PELAKSANA_1);
        assertThat(testKoresponden.getNilaiPrilakuPelaksana2()).isEqualTo(DEFAULT_NILAI_PRILAKU_PELAKSANA_2);
        assertThat(testKoresponden.getNilaiSarPras()).isEqualTo(DEFAULT_NILAI_SAR_PRAS);
        assertThat(testKoresponden.getNilaiSaranMasuk()).isEqualTo(DEFAULT_NILAI_SARAN_MASUK);
        assertThat(testKoresponden.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testKoresponden.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testKoresponden.isJenisKelamin()).isEqualTo(DEFAULT_JENIS_KELAMIN);
        assertThat(testKoresponden.getPekerjaan()).isEqualTo(DEFAULT_PEKERJAAN);
        assertThat(testKoresponden.getPendidikan()).isEqualTo(DEFAULT_PENDIDIKAN);
    }

    @Test
    @Transactional
    public void createKorespondenWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = korespondenRepository.findAll().size();

        // Create the Koresponden with an existing ID
        koresponden.setId(1L);
        KorespondenDTO korespondenDTO = korespondenMapper.toDto(koresponden);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKorespondenMockMvc.perform(post("/api/korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(korespondenDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Koresponden in the database
        List<Koresponden> korespondenList = korespondenRepository.findAll();
        assertThat(korespondenList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllKorespondens() throws Exception {
        // Initialize the database
        korespondenRepository.saveAndFlush(koresponden);

        // Get all the korespondenList
        restKorespondenMockMvc.perform(get("/api/korespondens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(koresponden.getId().intValue())))
            .andExpect(jsonPath("$.[*].nik").value(hasItem(DEFAULT_NIK)))
            .andExpect(jsonPath("$.[*].nama").value(hasItem(DEFAULT_NAMA)))
            .andExpect(jsonPath("$.[*].namaPerusahaan").value(hasItem(DEFAULT_NAMA_PERUSAHAAN)))
            .andExpect(jsonPath("$.[*].noTelp").value(hasItem(DEFAULT_NO_TELP)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].kodeVerifikasi").value(hasItem(DEFAULT_KODE_VERIFIKASI.toString())))
            .andExpect(jsonPath("$.[*].nilaiPersyaratan").value(hasItem(DEFAULT_NILAI_PERSYARATAN.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiProsedur").value(hasItem(DEFAULT_NILAI_PROSEDUR.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiWaktuPelayanan").value(hasItem(DEFAULT_NILAI_WAKTU_PELAYANAN.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiBiayaTarif").value(hasItem(DEFAULT_NILAI_BIAYA_TARIF.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiProdukPelayanan1").value(hasItem(DEFAULT_NILAI_PRODUK_PELAYANAN_1.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiProdukPelayanan2").value(hasItem(DEFAULT_NILAI_PRODUK_PELAYANAN_2.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiKomPel1").value(hasItem(DEFAULT_NILAI_KOM_PEL_1.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiKomPel2").value(hasItem(DEFAULT_NILAI_KOM_PEL_2.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiKomPel3").value(hasItem(DEFAULT_NILAI_KOM_PEL_3.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiKomPel4").value(hasItem(DEFAULT_NILAI_KOM_PEL_4.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiPerilakuPelaksana1").value(hasItem(DEFAULT_NILAI_PERILAKU_PELAKSANA_1.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiPrilakuPelaksana2").value(hasItem(DEFAULT_NILAI_PRILAKU_PELAKSANA_2.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiSarPras").value(hasItem(DEFAULT_NILAI_SAR_PRAS.doubleValue())))
            .andExpect(jsonPath("$.[*].nilaiSaranMasuk").value(hasItem(DEFAULT_NILAI_SARAN_MASUK.doubleValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].jenisKelamin").value(hasItem(DEFAULT_JENIS_KELAMIN.booleanValue())))
            .andExpect(jsonPath("$.[*].pekerjaan").value(hasItem(DEFAULT_PEKERJAAN)))
            .andExpect(jsonPath("$.[*].pendidikan").value(hasItem(DEFAULT_PENDIDIKAN)));
    }

    @Test
    @Transactional
    public void getKoresponden() throws Exception {
        // Initialize the database
        korespondenRepository.saveAndFlush(koresponden);

        // Get the koresponden
        restKorespondenMockMvc.perform(get("/api/korespondens/{id}", koresponden.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(koresponden.getId().intValue()))
            .andExpect(jsonPath("$.nik").value(DEFAULT_NIK))
            .andExpect(jsonPath("$.nama").value(DEFAULT_NAMA))
            .andExpect(jsonPath("$.namaPerusahaan").value(DEFAULT_NAMA_PERUSAHAAN))
            .andExpect(jsonPath("$.noTelp").value(DEFAULT_NO_TELP))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.kodeVerifikasi").value(DEFAULT_KODE_VERIFIKASI.toString()))
            .andExpect(jsonPath("$.nilaiPersyaratan").value(DEFAULT_NILAI_PERSYARATAN.doubleValue()))
            .andExpect(jsonPath("$.nilaiProsedur").value(DEFAULT_NILAI_PROSEDUR.doubleValue()))
            .andExpect(jsonPath("$.nilaiWaktuPelayanan").value(DEFAULT_NILAI_WAKTU_PELAYANAN.doubleValue()))
            .andExpect(jsonPath("$.nilaiBiayaTarif").value(DEFAULT_NILAI_BIAYA_TARIF.doubleValue()))
            .andExpect(jsonPath("$.nilaiProdukPelayanan1").value(DEFAULT_NILAI_PRODUK_PELAYANAN_1.doubleValue()))
            .andExpect(jsonPath("$.nilaiProdukPelayanan2").value(DEFAULT_NILAI_PRODUK_PELAYANAN_2.doubleValue()))
            .andExpect(jsonPath("$.nilaiKomPel1").value(DEFAULT_NILAI_KOM_PEL_1.doubleValue()))
            .andExpect(jsonPath("$.nilaiKomPel2").value(DEFAULT_NILAI_KOM_PEL_2.doubleValue()))
            .andExpect(jsonPath("$.nilaiKomPel3").value(DEFAULT_NILAI_KOM_PEL_3.doubleValue()))
            .andExpect(jsonPath("$.nilaiKomPel4").value(DEFAULT_NILAI_KOM_PEL_4.doubleValue()))
            .andExpect(jsonPath("$.nilaiPerilakuPelaksana1").value(DEFAULT_NILAI_PERILAKU_PELAKSANA_1.doubleValue()))
            .andExpect(jsonPath("$.nilaiPrilakuPelaksana2").value(DEFAULT_NILAI_PRILAKU_PELAKSANA_2.doubleValue()))
            .andExpect(jsonPath("$.nilaiSarPras").value(DEFAULT_NILAI_SAR_PRAS.doubleValue()))
            .andExpect(jsonPath("$.nilaiSaranMasuk").value(DEFAULT_NILAI_SARAN_MASUK.doubleValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.jenisKelamin").value(DEFAULT_JENIS_KELAMIN.booleanValue()))
            .andExpect(jsonPath("$.pekerjaan").value(DEFAULT_PEKERJAAN))
            .andExpect(jsonPath("$.pendidikan").value(DEFAULT_PENDIDIKAN));
    }

    @Test
    @Transactional
    public void getNonExistingKoresponden() throws Exception {
        // Get the koresponden
        restKorespondenMockMvc.perform(get("/api/korespondens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKoresponden() throws Exception {
        // Initialize the database
        korespondenRepository.saveAndFlush(koresponden);

        int databaseSizeBeforeUpdate = korespondenRepository.findAll().size();

        // Update the koresponden
        Koresponden updatedKoresponden = korespondenRepository.findById(koresponden.getId()).get();
        // Disconnect from session so that the updates on updatedKoresponden are not directly saved in db
        em.detach(updatedKoresponden);
        updatedKoresponden
            .nik(UPDATED_NIK)
            .nama(UPDATED_NAMA)
            .namaPerusahaan(UPDATED_NAMA_PERUSAHAAN)
            .noTelp(UPDATED_NO_TELP)
            .email(UPDATED_EMAIL)
            .kodeVerifikasi(UPDATED_KODE_VERIFIKASI)
            .nilaiPersyaratan(UPDATED_NILAI_PERSYARATAN)
            .nilaiProsedur(UPDATED_NILAI_PROSEDUR)
            .nilaiWaktuPelayanan(UPDATED_NILAI_WAKTU_PELAYANAN)
            .nilaiBiayaTarif(UPDATED_NILAI_BIAYA_TARIF)
            .nilaiProdukPelayanan1(UPDATED_NILAI_PRODUK_PELAYANAN_1)
            .nilaiProdukPelayanan2(UPDATED_NILAI_PRODUK_PELAYANAN_2)
            .nilaiKomPel1(UPDATED_NILAI_KOM_PEL_1)
            .nilaiKomPel2(UPDATED_NILAI_KOM_PEL_2)
            .nilaiKomPel3(UPDATED_NILAI_KOM_PEL_3)
            .nilaiKomPel4(UPDATED_NILAI_KOM_PEL_4)
            .nilaiPerilakuPelaksana1(UPDATED_NILAI_PERILAKU_PELAKSANA_1)
            .nilaiPrilakuPelaksana2(UPDATED_NILAI_PRILAKU_PELAKSANA_2)
            .nilaiSarPras(UPDATED_NILAI_SAR_PRAS)
            .nilaiSaranMasuk(UPDATED_NILAI_SARAN_MASUK)
            .createDate(UPDATED_CREATE_DATE)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .jenisKelamin(UPDATED_JENIS_KELAMIN)
            .pekerjaan(UPDATED_PEKERJAAN)
            .pendidikan(UPDATED_PENDIDIKAN);
        KorespondenDTO korespondenDTO = korespondenMapper.toDto(updatedKoresponden);

        restKorespondenMockMvc.perform(put("/api/korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(korespondenDTO)))
            .andExpect(status().isOk());

        // Validate the Koresponden in the database
        List<Koresponden> korespondenList = korespondenRepository.findAll();
        assertThat(korespondenList).hasSize(databaseSizeBeforeUpdate);
        Koresponden testKoresponden = korespondenList.get(korespondenList.size() - 1);
        assertThat(testKoresponden.getNik()).isEqualTo(UPDATED_NIK);
        assertThat(testKoresponden.getNama()).isEqualTo(UPDATED_NAMA);
        assertThat(testKoresponden.getNamaPerusahaan()).isEqualTo(UPDATED_NAMA_PERUSAHAAN);
        assertThat(testKoresponden.getNoTelp()).isEqualTo(UPDATED_NO_TELP);
        assertThat(testKoresponden.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKoresponden.getKodeVerifikasi()).isEqualTo(UPDATED_KODE_VERIFIKASI);
        assertThat(testKoresponden.getNilaiPersyaratan()).isEqualTo(UPDATED_NILAI_PERSYARATAN);
        assertThat(testKoresponden.getNilaiProsedur()).isEqualTo(UPDATED_NILAI_PROSEDUR);
        assertThat(testKoresponden.getNilaiWaktuPelayanan()).isEqualTo(UPDATED_NILAI_WAKTU_PELAYANAN);
        assertThat(testKoresponden.getNilaiBiayaTarif()).isEqualTo(UPDATED_NILAI_BIAYA_TARIF);
        assertThat(testKoresponden.getNilaiProdukPelayanan1()).isEqualTo(UPDATED_NILAI_PRODUK_PELAYANAN_1);
        assertThat(testKoresponden.getNilaiProdukPelayanan2()).isEqualTo(UPDATED_NILAI_PRODUK_PELAYANAN_2);
        assertThat(testKoresponden.getNilaiKomPel1()).isEqualTo(UPDATED_NILAI_KOM_PEL_1);
        assertThat(testKoresponden.getNilaiKomPel2()).isEqualTo(UPDATED_NILAI_KOM_PEL_2);
        assertThat(testKoresponden.getNilaiKomPel3()).isEqualTo(UPDATED_NILAI_KOM_PEL_3);
        assertThat(testKoresponden.getNilaiKomPel4()).isEqualTo(UPDATED_NILAI_KOM_PEL_4);
        assertThat(testKoresponden.getNilaiPerilakuPelaksana1()).isEqualTo(UPDATED_NILAI_PERILAKU_PELAKSANA_1);
        assertThat(testKoresponden.getNilaiPrilakuPelaksana2()).isEqualTo(UPDATED_NILAI_PRILAKU_PELAKSANA_2);
        assertThat(testKoresponden.getNilaiSarPras()).isEqualTo(UPDATED_NILAI_SAR_PRAS);
        assertThat(testKoresponden.getNilaiSaranMasuk()).isEqualTo(UPDATED_NILAI_SARAN_MASUK);
        assertThat(testKoresponden.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testKoresponden.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testKoresponden.isJenisKelamin()).isEqualTo(UPDATED_JENIS_KELAMIN);
        assertThat(testKoresponden.getPekerjaan()).isEqualTo(UPDATED_PEKERJAAN);
        assertThat(testKoresponden.getPendidikan()).isEqualTo(UPDATED_PENDIDIKAN);
    }

    @Test
    @Transactional
    public void updateNonExistingKoresponden() throws Exception {
        int databaseSizeBeforeUpdate = korespondenRepository.findAll().size();

        // Create the Koresponden
        KorespondenDTO korespondenDTO = korespondenMapper.toDto(koresponden);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKorespondenMockMvc.perform(put("/api/korespondens")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(korespondenDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Koresponden in the database
        List<Koresponden> korespondenList = korespondenRepository.findAll();
        assertThat(korespondenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKoresponden() throws Exception {
        // Initialize the database
        korespondenRepository.saveAndFlush(koresponden);

        int databaseSizeBeforeDelete = korespondenRepository.findAll().size();

        // Delete the koresponden
        restKorespondenMockMvc.perform(delete("/api/korespondens/{id}", koresponden.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Koresponden> korespondenList = korespondenRepository.findAll();
        assertThat(korespondenList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
