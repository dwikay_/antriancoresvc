package com.antrian.inti.core.business.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

public class SimpleLayanan implements Serializable {

    private Long id;

    private String kodeLayanan;

    private String namaLayanan;

    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;

    private Long lastModificationUserId;

    private String imageLayanan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKodeLayanan() {
        return kodeLayanan;
    }

    public void setKodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public String getImageLayanan() {
        return imageLayanan;
    }

    public void setImageLayanan(String imageLayanan) {
        this.imageLayanan = imageLayanan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleLayanan that = (SimpleLayanan) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(kodeLayanan, that.kodeLayanan) &&
            Objects.equals(namaLayanan, that.namaLayanan) &&
            Objects.equals(createUserId, that.createUserId) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(createDateTime, that.createDateTime) &&
            Objects.equals(lastModificationDate, that.lastModificationDate) &&
            Objects.equals(lastModificationDateTime, that.lastModificationDateTime) &&
            Objects.equals(lastModificationUserId, that.lastModificationUserId) &&
            Objects.equals(imageLayanan, that.imageLayanan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, kodeLayanan, namaLayanan, createUserId, createDate, createDateTime, lastModificationDate, lastModificationDateTime, lastModificationUserId, imageLayanan);
    }

    @Override
    public String toString() {
        return "SimpleLayanan{" +
            "id=" + id +
            ", kodeLayanan='" + kodeLayanan + '\'' +
            ", namaLayanan='" + namaLayanan + '\'' +
            ", createUserId=" + createUserId +
            ", createDate=" + createDate +
            ", createDateTime=" + createDateTime +
            ", lastModificationDate=" + lastModificationDate +
            ", lastModificationDateTime=" + lastModificationDateTime +
            ", lastModificationUserId=" + lastModificationUserId +
            ", imageLayanan='" + imageLayanan + '\'' +
            '}';
    }
}
