package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.DigitalSignage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DigitalSignage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DigitalSignageRepository extends JpaRepository<DigitalSignage, Long> {

}
