package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Loket;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Loket entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoketRepository extends JpaRepository<Loket, Long> {
    Optional<Loket> findByIdSubLayanan(Long idSubLayanan);
    Optional<Loket> findByIpController(String ipController);

}
