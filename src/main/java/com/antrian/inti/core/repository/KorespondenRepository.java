package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Koresponden;
import com.antrian.inti.core.domain.Registrasi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;


/**
 * Spring Data  repository for the Koresponden entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KorespondenRepository extends JpaRepository<Koresponden, Long> {
    Page<Koresponden> findByCreateDateBetween(Pageable pageable, LocalDate startDate, LocalDate endDate);
}
