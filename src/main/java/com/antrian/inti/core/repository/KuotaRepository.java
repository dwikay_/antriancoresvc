package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Kuota;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Kuota entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KuotaRepository extends JpaRepository<Kuota, Long> {

}
