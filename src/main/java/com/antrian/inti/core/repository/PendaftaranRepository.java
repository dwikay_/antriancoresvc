package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Pendaftaran;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Pendaftaran entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PendaftaranRepository extends JpaRepository<Pendaftaran, Long> {

}
