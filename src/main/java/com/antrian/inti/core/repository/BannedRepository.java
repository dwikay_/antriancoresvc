package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Banned;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Banned entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BannedRepository extends JpaRepository<Banned, Long> {
    Page<Banned> findByJumlahGreaterThan(Pageable pageable, Integer jumlah);
    Optional<Banned> findOneByNikAndJumlahGreaterThan(String nik, Integer jumlah);
}
