package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Registrasi;
import com.antrian.inti.core.domain.SubLayanan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Registrasi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegistrasiRepository extends JpaRepository<Registrasi, Long> {
    Optional<Registrasi> findOneByNikAndEmailAndVerifikasiEmail(String nik, String email, boolean verifikasiEmail);
    Optional<Registrasi> findOneByKodeVerifikasiAndVerifikasiEmail(String kodeVerifikasi, boolean verifikasiEmail);
    Optional<Registrasi> findOneByNikAndEmailAndVerifikasiEmailAndPengambilanStrukAndTanggalPengambilanGreaterThanEqual(String nik, String email, boolean verifikasiEmail, boolean pengambilanStruk, LocalDate tanggalPengambilan);
    Optional<Registrasi> findOneByKodeKonfirmasiAndIdSubLayananAndTanggalPengambilan(String kodeKonfirmasi, Long idSubLayanan, LocalDate dateNow);
    Optional<Registrasi> findOneByKodeVerifikasi(String kodeVerifikasi);
    Page<Registrasi> findByTanggalPengambilanBetween(Pageable pageable, LocalDate startDate,LocalDate endDate);
    Page<Registrasi> findByTanggalPengambilan(Pageable pageable, LocalDate dateNow);

    @Query(value = "SELECT " +
        "COUNT(tanggal_pengambilan) AS jumlah_data," +
        "tanggal_pengambilan as waktunya FROM registrasi " +
        "GROUP BY tanggal_pengambilan " +
        "HAVING COUNT(tanggal_pengambilan) >= ?1 "
        , nativeQuery = true)
    List<Registrasi> havingCountRegistrasi(Long limitQuota);

    Integer countByTanggalPengambilanAndIdSubLayananAndIdKloterNot(LocalDate dateRegistrasi, Long idSubLayanan, Long idKloter);
    Integer countByTanggalPengambilanAndIdSubLayananAndTypeVirtualAndIdKloterNot(LocalDate dateRegistrasi, Long idSubLayanan,boolean typeVirtual, Long idKloter);

}
