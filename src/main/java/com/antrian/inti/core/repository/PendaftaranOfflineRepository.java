package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.PendaftaranOffline;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;


/**
 * Spring Data  repository for the PendaftaranOffline entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PendaftaranOfflineRepository extends JpaRepository<PendaftaranOffline, Long> {
    Optional<PendaftaranOffline> findByTokenValidasi(String token);
    Optional<PendaftaranOffline> findByNikAndEmailAndIdSubLayananAndTanggal(String nik, String email,Long idSubLayanan, LocalDate tanggal);
    Optional<PendaftaranOffline> findByNoUrutAndIdSubLayananAndTanggal(String noUrut, Long idSubLayanan, LocalDate tanggal);
    Page<PendaftaranOffline> findAllByTanggalBetween(Pageable pageable, LocalDate startDate, LocalDate endDate);
}
