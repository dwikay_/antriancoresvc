package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.ImageRepository;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ImageRepository entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageRepositoryRepository extends JpaRepository<ImageRepository, Long> {

}
