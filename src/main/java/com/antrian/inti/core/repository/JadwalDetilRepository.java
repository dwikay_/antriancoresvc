package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.JadwalDetil;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;


/**
 * Spring Data  repository for the JadwalDetil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JadwalDetilRepository extends JpaRepository<JadwalDetil, Long> {
    List<JadwalDetil> findAllByIdMasterOrderByKloterAsc(Long idMaster);
    List<JadwalDetil> findAllByIdMasterAndVirtualOrderByKloterAsc(Long idMaster, boolean virtual);

    Integer countByVirtual(boolean virtual);

}
