package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.PendaftaranDumy;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PendaftaranDumy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PendaftaranDumyRepository extends JpaRepository<PendaftaranDumy, Long>, JpaSpecificationExecutor<PendaftaranDumy> {

}
