package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Layanan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Layanan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LayananRepository extends JpaRepository<Layanan, Long> {
    List<Layanan> findAllByIdLantai(Long idLantai);
}
