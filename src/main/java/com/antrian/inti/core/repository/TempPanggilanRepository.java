package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.TempPanggilan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the TempPanggilan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TempPanggilanRepository extends JpaRepository<TempPanggilan, Long> {
    Optional<TempPanggilan> findFirstByStatusAndIdLantaiOrderByIdAsc(boolean status, Long idLantai);
    Integer countByIdSubLayananAndNoUrut(Long idSubLayanan,String noUrut);
    Optional<TempPanggilan> findByIdSubLayananAndNoUrutAndStatus(Long idSubLayanan,String noUrut, boolean statusPanggilan);
}
