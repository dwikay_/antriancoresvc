package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.PelayananLoket;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PelayananLoket entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PelayananLoketRepository extends JpaRepository<PelayananLoket, Long> {

}
