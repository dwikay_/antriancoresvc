package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.AkumulasiKoresponden;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AkumulasiKoresponden entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AkumulasiKorespondenRepository extends JpaRepository<AkumulasiKoresponden, Long> {

}
