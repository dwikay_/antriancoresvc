package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Kloter;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Kloter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KloterRepository extends JpaRepository<Kloter, Long> {

}
