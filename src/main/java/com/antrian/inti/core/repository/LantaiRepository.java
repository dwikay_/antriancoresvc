package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Lantai;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Lantai entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LantaiRepository extends JpaRepository<Lantai, Long> {

}
