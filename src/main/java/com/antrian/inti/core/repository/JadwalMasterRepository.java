package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.JadwalMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the JadwalMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JadwalMasterRepository extends JpaRepository<JadwalMaster, Long> {
    Optional<JadwalMaster> findFirstByOrderByIdDesc();
    Optional<JadwalMaster> findByIdSubLayanan(Long idSubLayanan);
}
