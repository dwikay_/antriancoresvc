package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.HariLibur;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HariLibur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HariLiburRepository extends JpaRepository<HariLibur, Long> {

}
