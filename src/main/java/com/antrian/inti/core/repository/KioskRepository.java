package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Kiosk;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Kiosk entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KioskRepository extends JpaRepository<Kiosk, Long> {

}
