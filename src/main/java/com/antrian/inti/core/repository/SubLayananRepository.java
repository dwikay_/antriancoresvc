package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.SubLayanan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the SubLayanan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubLayananRepository extends JpaRepository<SubLayanan, Long> {
    List<SubLayanan> findAllByIdLayanan(Long idLayanan);
    List<SubLayanan> findAllByIdLayananAndStatus(Long idLayanan, String status);
    List<SubLayanan> findAllByLayananOnlineOrderByPrefixSuaraAsc(boolean layananOnline);
    Page<SubLayanan> findAllByStatus(Pageable pageable,String status);
    List<SubLayanan> findAllById(Long id);
    List<SubLayanan> findAllByIdLantaiOrderByPrefixSuaraAsc(Long idLantai);

    List<SubLayanan> findAllByIdLantaiAndStatusOrderByPrefixSuaraAsc(Long idLantai, String status);

    @Query(value = "SELECT *FROM sub_layanan where " +
        "status like %?1% "
        , nativeQuery = true)
    List<SubLayanan> findAllByStatusList(String status);
    List<SubLayanan> findAllByLayananOnlineAndStatusOrderByPrefixSuaraAsc(boolean layananOnline, String status);


}
