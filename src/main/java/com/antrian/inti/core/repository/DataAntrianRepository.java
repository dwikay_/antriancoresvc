package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.DataAntrian;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the DataAntrian entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataAntrianRepository extends JpaRepository<DataAntrian, Long> {
    Optional<DataAntrian> findByLantaiAndIdSubLayananAndNoUrutAndTanggal(Long idLantai, Long idSubLayanan, Long noUrut, LocalDate tanggal);
    Optional<DataAntrian> findFirstByIdSubLayananAndTanggalOrderByIdAsc(Long idSubLayanan, LocalDate tanggal);
    Optional<DataAntrian> findFirstByIdSubLayananAndStatusCurrentAndTanggalAndStatusPanggilanOrderByNoUrutAsc(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, boolean statusPanggilan);
    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndStatusCurrentAndTanggalOrderByNoUrutAsc(Long loket,Long idSubLayanan, boolean statusCurrent, LocalDate tanggal);
    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndStatusCurrentAndTanggalOrderByNoUrutDesc(Long loket,Long idSubLayanan, boolean statusCurrent, LocalDate tanggal);
    Optional<DataAntrian> findByIdSubLayananAndNoUrutAndStatusKonselingAndTanggal(Long idSubLayanan,Long noUrut,boolean statusKonseling, LocalDate tanggal);
    Integer countByTanggalAndIdSubLayanan(LocalDate dateNow, Long idSubLayanan);
    Integer countByTanggalAndIdSubLayananAndStatusPanggilan(LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan);
    Page<DataAntrian> findAllByTanggalAndIdSubLayananAndStatusPanggilanOrderByNoUrutAsc(Pageable pageable, LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan);
    Page<DataAntrian> findAllByTanggalAndIdSubLayananAndStatusPanggilanAndLoketOrderByNoUrutAsc(Pageable pageable, LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan,Long loket);
    Optional<DataAntrian> findByIdAndStatusPanggilanAndStatusCurrent(Long idAntrian, boolean statusPanggilan, boolean statusCurrent);
    Page<DataAntrian> findAllByTanggalAndIdSubLayananAndStatusCurrentOrderByNoUrutAsc(Pageable pageable, LocalDate dateNow, Long idSubLayanan, boolean statusCurrent);
    Optional<DataAntrian> findByIdSubLayananAndTanggalAndNoUrutLessThan(Long idSubLayanan, LocalDate tanggal, Long noUrut);
//    Optional<DataAntrian> findFirstByIdSubLayananAndStatusCurrentAndTanggalAndLantaiAndStatusKonselingOrderByTanggalWaktuDesc(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, Long idLantai,boolean statusKonseling);
    Optional<DataAntrian> findFirstByIdSubLayananAndStatusCurrentAndTanggalAndLantaiAndStatusKonselingOrderByIdDesc(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, Long idLantai,boolean statusKonseling);
    Optional<DataAntrian> findFirstByIdSubLayananAndStatusCurrentAndTanggalAndLantaiOrderByIdDesc(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, Long idLantai);

    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndTanggalOrderByNoUrutDesc(Long loket,Long idSubLayanan, LocalDate tanggal);
    Optional<DataAntrian> findFirstByNoUrutAndIdSubLayananAndTanggal(Long noUrut, Long idSubLayanan, LocalDate tanggal);

    //NEW UPDATE MAKASAR

    //NEXT
    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndTanggalAndStatusPanggilanAndStatusKonselingAndStatusCurrentOrderByNoUrutAsc(
        Long loket,Long idSubLayanan,LocalDate tanggalNow,boolean statusPanggilan,boolean statusKonseling,boolean statusCurrent
    );

    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndTanggalAndStatusKonselingOrderByNoUrutAsc(
        Long loket, Long idSubLayanan, LocalDate tanggalNow, boolean statusKonseling
    );
    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndTanggalAndStatusCurrentAndStatusKonseling(
        Long loket,Long idSubLayanan, LocalDate tanggal, boolean statusCurrent, boolean statusKonseling
    );

    //PREV
    Optional<DataAntrian> findFirstByLoketAndIdSubLayananAndTanggalAndStatusKonselingOrderByNoUrutDesc(
        Long loket,Long idSubLayanan, LocalDate tanggal, boolean statusKonseling
    );

    List<DataAntrian> findAllByLoketAndIdSubLayananAndTanggalAndStatusCurrent(
      Long loket, Long idSubLayanan, LocalDate tanggal, boolean statusCurrent
    );
    //FAST CALL

    //EXIST

    //REFR
    Optional<DataAntrian> findFirstByIdSubLayananAndLoketAndTanggalAndStatusCurrentAndStatusPanggilanAndStatusKonselingOrderByNoUrutAsc(
        Long idSubLayanan,Long loket, LocalDate tanggal,boolean statusPanggilan, boolean statusCurrent, boolean statusKonseling
    );

}
