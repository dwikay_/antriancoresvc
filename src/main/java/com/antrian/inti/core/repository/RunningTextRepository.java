package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.RunningText;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RunningText entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RunningTextRepository extends JpaRepository<RunningText, Long> {

}
