package com.antrian.inti.core.repository;

import com.antrian.inti.core.domain.Prosedur;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Prosedur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProsedurRepository extends JpaRepository<Prosedur, Long> {

}
