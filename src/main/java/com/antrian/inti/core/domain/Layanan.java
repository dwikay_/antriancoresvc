package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A Layanan.
 */
@Entity
@Table(name = "layanan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Layanan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "kode_layanan")
    private String kodeLayanan;

    @Column(name = "nama_layanan")
    private String namaLayanan;

    @Lob
    @Column(name = "gambar")
    private byte[] gambar;

    @Column(name = "gambar_content_type")
    private String gambarContentType;

    @Column(name = "create_user_id")
    private Long createUserId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "last_modification_date")
    private LocalDate lastModificationDate;

    @Column(name = "last_modification_date_time")
    private ZonedDateTime lastModificationDateTime;

    @Column(name = "last_modification_user_id")
    private Long lastModificationUserId;

    @Column(name = "id_lantai")
    private Long idLantai;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKodeLayanan() {
        return kodeLayanan;
    }

    public Layanan kodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
        return this;
    }

    public void setKodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public Layanan namaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
        return this;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public byte[] getGambar() {
        return gambar;
    }

    public Layanan gambar(byte[] gambar) {
        this.gambar = gambar;
        return this;
    }

    public void setGambar(byte[] gambar) {
        this.gambar = gambar;
    }

    public String getGambarContentType() {
        return gambarContentType;
    }

    public Layanan gambarContentType(String gambarContentType) {
        this.gambarContentType = gambarContentType;
        return this;
    }

    public void setGambarContentType(String gambarContentType) {
        this.gambarContentType = gambarContentType;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Layanan createUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Layanan createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public Layanan createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public Layanan lastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
        return this;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public Layanan lastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
        return this;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public Layanan lastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
        return this;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public Layanan idLantai(Long idLantai) {
        this.idLantai = idLantai;
        return this;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Layanan)) {
            return false;
        }
        return id != null && id.equals(((Layanan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Layanan{" +
            "id=" + getId() +
            ", kodeLayanan='" + getKodeLayanan() + "'" +
            ", namaLayanan='" + getNamaLayanan() + "'" +
            ", gambar='" + getGambar() + "'" +
            ", gambarContentType='" + getGambarContentType() + "'" +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", idLantai=" + getIdLantai() +
            "}";
    }
}
