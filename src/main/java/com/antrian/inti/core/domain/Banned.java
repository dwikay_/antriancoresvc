package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A Banned.
 */
@Entity
@Table(name = "banned")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Banned implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nik")
    private String nik;

    @Column(name = "email")
    private String email;

    @Column(name = "tanggal_banned")
    private LocalDate tanggal_banned;

    @Column(name = "finish_banned")
    private LocalDate finish_banned;

    @Size(max = 3)
    @Column(name = "status_banned", length = 3)
    private String status_banned;

    @Column(name = "create_system_date")
    private LocalDate createSystemDate;

    @Column(name = "modification_system_date")
    private LocalDate modificationSystemDate;

    @Column(name = "jumlah")
    private Integer jumlah;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public Banned nik(String nik) {
        this.nik = nik;
        return this;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getEmail() {
        return email;
    }

    public Banned email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getTanggal_banned() {
        return tanggal_banned;
    }

    public Banned tanggal_banned(LocalDate tanggal_banned) {
        this.tanggal_banned = tanggal_banned;
        return this;
    }

    public void setTanggal_banned(LocalDate tanggal_banned) {
        this.tanggal_banned = tanggal_banned;
    }

    public LocalDate getFinish_banned() {
        return finish_banned;
    }

    public Banned finish_banned(LocalDate finish_banned) {
        this.finish_banned = finish_banned;
        return this;
    }

    public void setFinish_banned(LocalDate finish_banned) {
        this.finish_banned = finish_banned;
    }

    public String getStatus_banned() {
        return status_banned;
    }

    public Banned status_banned(String status_banned) {
        this.status_banned = status_banned;
        return this;
    }

    public void setStatus_banned(String status_banned) {
        this.status_banned = status_banned;
    }

    public LocalDate getCreateSystemDate() {
        return createSystemDate;
    }

    public Banned createSystemDate(LocalDate createSystemDate) {
        this.createSystemDate = createSystemDate;
        return this;
    }

    public void setCreateSystemDate(LocalDate createSystemDate) {
        this.createSystemDate = createSystemDate;
    }

    public LocalDate getModificationSystemDate() {
        return modificationSystemDate;
    }

    public Banned modificationSystemDate(LocalDate modificationSystemDate) {
        this.modificationSystemDate = modificationSystemDate;
        return this;
    }

    public void setModificationSystemDate(LocalDate modificationSystemDate) {
        this.modificationSystemDate = modificationSystemDate;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public Banned jumlah(Integer jumlah) {
        this.jumlah = jumlah;
        return this;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Banned)) {
            return false;
        }
        return id != null && id.equals(((Banned) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Banned{" +
            "id=" + getId() +
            ", nik='" + getNik() + "'" +
            ", email='" + getEmail() + "'" +
            ", tanggal_banned='" + getTanggal_banned() + "'" +
            ", finish_banned='" + getFinish_banned() + "'" +
            ", status_banned='" + getStatus_banned() + "'" +
            ", createSystemDate='" + getCreateSystemDate() + "'" +
            ", modificationSystemDate='" + getModificationSystemDate() + "'" +
            ", jumlah=" + getJumlah() +
            "}";
    }
}
