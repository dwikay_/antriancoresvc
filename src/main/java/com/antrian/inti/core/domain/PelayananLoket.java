package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A PelayananLoket.
 */
@Entity
@Table(name = "pelayanan_loket")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PelayananLoket implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_layanan")
    private Long idLayanan;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "id_loket")
    private Long idLoket;

    @Column(name = "create_user_id")
    private Long createUserId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "last_modification_user_id")
    private Long lastModificationUserId;

    @Column(name = "last_modification_date_time")
    private ZonedDateTime lastModificationDateTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdLayanan() {
        return idLayanan;
    }

    public PelayananLoket idLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
        return this;
    }

    public void setIdLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public PelayananLoket idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public Long getIdLoket() {
        return idLoket;
    }

    public PelayananLoket idLoket(Long idLoket) {
        this.idLoket = idLoket;
        return this;
    }

    public void setIdLoket(Long idLoket) {
        this.idLoket = idLoket;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public PelayananLoket createUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public PelayananLoket createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public PelayananLoket createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public PelayananLoket lastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
        return this;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public PelayananLoket lastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
        return this;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PelayananLoket)) {
            return false;
        }
        return id != null && id.equals(((PelayananLoket) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PelayananLoket{" +
            "id=" + getId() +
            ", idLayanan=" + getIdLayanan() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", idLoket=" + getIdLoket() +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            "}";
    }
}
