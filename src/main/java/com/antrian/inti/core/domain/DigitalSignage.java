package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A DigitalSignage.
 */
@Entity
@Table(name = "digital_signage")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DigitalSignage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nama_video")
    private String namaVideo;

    @Column(name = "status")
    private String status;

    @Column(name = "device")
    private String device;

    @Column(name = "path_file")
    private String pathFile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaVideo() {
        return namaVideo;
    }

    public DigitalSignage namaVideo(String namaVideo) {
        this.namaVideo = namaVideo;
        return this;
    }

    public void setNamaVideo(String namaVideo) {
        this.namaVideo = namaVideo;
    }

    public String getStatus() {
        return status;
    }

    public DigitalSignage status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public DigitalSignage device(String device) {
        this.device = device;
        return this;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getPathFile() {
        return pathFile;
    }

    public DigitalSignage pathFile(String pathFile) {
        this.pathFile = pathFile;
        return this;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DigitalSignage)) {
            return false;
        }
        return id != null && id.equals(((DigitalSignage) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DigitalSignage{" +
            "id=" + getId() +
            ", namaVideo='" + getNamaVideo() + "'" +
            ", status='" + getStatus() + "'" +
            ", device='" + getDevice() + "'" +
            ", pathFile='" + getPathFile() + "'" +
            "}";
    }
}
