package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A Registrasi.
 */
@Entity
@Table(name = "registrasi")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Registrasi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_daftar")
    private String idDaftar;

    @Column(name = "email")
    private String email;

    @Column(name = "hubungan_perusahaan")
    private String hubunganPerusahaan;

    @Column(name = "nama_perusahaan")
    private String namaPerusahaan;

    @Column(name = "tanggal_pengambilan")
    private LocalDate tanggalPengambilan;

    @Column(name = "tanggal_waktu_pengambilan")
    private String tanggalWaktuPengambilan;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "pertanyaan")
    private String pertanyaan;

    @Column(name = "telepon")
    private String telepon;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "kode_verifikasi")
    private String kodeVerifikasi;

    @Column(name = "nama_lengkap")
    private String namaLengkap;

    @Column(name = "verifikasi_email")
    private Boolean verifikasiEmail;

    @Size(max = 16)
    @Column(name = "nik", length = 16)
    private String nik;

    @Column(name = "nama_perusahaan_pendaftar")
    private String namaPerusahaanPendaftar;

    @Column(name = "kode_konfirmasi")
    private String kodeKonfirmasi;

    @Column(name = "id_kloter")
    private Long idKloter;

    @Column(name = "pengambilan_struk")
    private Boolean pengambilanStruk;

    @Column(name = "jam_kloter")
    private String jamKloter;

    @Column(name = "waktu_jam_kloter")
    private String waktuJamKloter;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "modification_date")
    private LocalDate modificationDate;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Lob
    @Column(name = "ktp")
    private byte[] ktp;

    @Column(name = "ktp_content_type")
    private String ktpContentType;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "jawaban")
    private String jawaban;

    @Column(name = "tipe_ktp")
    private String tipe_ktp;

    @Column(name = "path_files")
    private String path_files;

    @Column(name = "status_tiket")
    private String statusTiket;

    @Column(name = "jam_layanan")
    private String jamLayanan;

    @Column(name = "no_tiket")
    private String noTiket;

    @Column(name = "type_virtual")
    private Boolean typeVirtual;

    @Column(name = "agent")
    private Boolean agent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDaftar() {
        return idDaftar;
    }

    public Registrasi idDaftar(String idDaftar) {
        this.idDaftar = idDaftar;
        return this;
    }

    public void setIdDaftar(String idDaftar) {
        this.idDaftar = idDaftar;
    }

    public String getEmail() {
        return email;
    }

    public Registrasi email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHubunganPerusahaan() {
        return hubunganPerusahaan;
    }

    public Registrasi hubunganPerusahaan(String hubunganPerusahaan) {
        this.hubunganPerusahaan = hubunganPerusahaan;
        return this;
    }

    public void setHubunganPerusahaan(String hubunganPerusahaan) {
        this.hubunganPerusahaan = hubunganPerusahaan;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public Registrasi namaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
        return this;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public LocalDate getTanggalPengambilan() {
        return tanggalPengambilan;
    }

    public Registrasi tanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
        return this;
    }

    public void setTanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
    }

    public String getTanggalWaktuPengambilan() {
        return tanggalWaktuPengambilan;
    }

    public Registrasi tanggalWaktuPengambilan(String tanggalWaktuPengambilan) {
        this.tanggalWaktuPengambilan = tanggalWaktuPengambilan;
        return this;
    }

    public void setTanggalWaktuPengambilan(String tanggalWaktuPengambilan) {
        this.tanggalWaktuPengambilan = tanggalWaktuPengambilan;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public Registrasi pertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
        return this;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getTelepon() {
        return telepon;
    }

    public Registrasi telepon(String telepon) {
        this.telepon = telepon;
        return this;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getKodeVerifikasi() {
        return kodeVerifikasi;
    }

    public Registrasi kodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
        return this;
    }

    public void setKodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public Registrasi namaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
        return this;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public Boolean isVerifikasiEmail() {
        return verifikasiEmail;
    }

    public Registrasi verifikasiEmail(Boolean verifikasiEmail) {
        this.verifikasiEmail = verifikasiEmail;
        return this;
    }

    public void setVerifikasiEmail(Boolean verifikasiEmail) {
        this.verifikasiEmail = verifikasiEmail;
    }

    public String getNik() {
        return nik;
    }

    public Registrasi nik(String nik) {
        this.nik = nik;
        return this;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaPerusahaanPendaftar() {
        return namaPerusahaanPendaftar;
    }

    public Registrasi namaPerusahaanPendaftar(String namaPerusahaanPendaftar) {
        this.namaPerusahaanPendaftar = namaPerusahaanPendaftar;
        return this;
    }

    public void setNamaPerusahaanPendaftar(String namaPerusahaanPendaftar) {
        this.namaPerusahaanPendaftar = namaPerusahaanPendaftar;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public Registrasi kodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
        return this;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public Long getIdKloter() {
        return idKloter;
    }

    public Registrasi idKloter(Long idKloter) {
        this.idKloter = idKloter;
        return this;
    }

    public void setIdKloter(Long idKloter) {
        this.idKloter = idKloter;
    }

    public Boolean isPengambilanStruk() {
        return pengambilanStruk;
    }

    public Registrasi pengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
        return this;
    }

    public void setPengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
    }

    public String getJamKloter() {
        return jamKloter;
    }

    public Registrasi jamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
        return this;
    }

    public void setJamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
    }

    public String getWaktuJamKloter() {
        return waktuJamKloter;
    }

    public Registrasi waktuJamKloter(String waktuJamKloter) {
        this.waktuJamKloter = waktuJamKloter;
        return this;
    }

    public void setWaktuJamKloter(String waktuJamKloter) {
        this.waktuJamKloter = waktuJamKloter;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Registrasi createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public Registrasi createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public Registrasi modificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
        return this;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public Registrasi idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public byte[] getKtp() {
        return ktp;
    }

    public Registrasi ktp(byte[] ktp) {
        this.ktp = ktp;
        return this;
    }

    public void setKtp(byte[] ktp) {
        this.ktp = ktp;
    }

    public String getKtpContentType() {
        return ktpContentType;
    }

    public Registrasi ktpContentType(String ktpContentType) {
        this.ktpContentType = ktpContentType;
        return this;
    }

    public void setKtpContentType(String ktpContentType) {
        this.ktpContentType = ktpContentType;
    }

    public String getJawaban() {
        return jawaban;
    }

    public Registrasi jawaban(String jawaban) {
        this.jawaban = jawaban;
        return this;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getTipe_ktp() {
        return tipe_ktp;
    }

    public Registrasi tipe_ktp(String tipe_ktp) {
        this.tipe_ktp = tipe_ktp;
        return this;
    }

    public void setTipe_ktp(String tipe_ktp) {
        this.tipe_ktp = tipe_ktp;
    }

    public String getPath_files() {
        return path_files;
    }

    public Registrasi path_files(String path_files) {
        this.path_files = path_files;
        return this;
    }

    public void setPath_files(String path_files) {
        this.path_files = path_files;
    }

    public String getStatusTiket() {
        return statusTiket;
    }

    public Registrasi statusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
        return this;
    }

    public void setStatusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
    }

    public String getJamLayanan() {
        return jamLayanan;
    }

    public Registrasi jamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
        return this;
    }

    public void setJamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
    }

    public String getNoTiket() {
        return noTiket;
    }

    public Registrasi noTiket(String noTiket) {
        this.noTiket = noTiket;
        return this;
    }

    public void setNoTiket(String noTiket) {
        this.noTiket = noTiket;
    }

    public Boolean isTypeVirtual() {
        return typeVirtual;
    }

    public Registrasi typeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
        return this;
    }

    public void setTypeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
    }

    public Boolean isAgent() {
        return agent;
    }

    public Registrasi agent(Boolean agent) {
        this.agent = agent;
        return this;
    }

    public void setAgent(Boolean agent) {
        this.agent = agent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Registrasi)) {
            return false;
        }
        return id != null && id.equals(((Registrasi) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Registrasi{" +
            "id=" + getId() +
            ", idDaftar='" + getIdDaftar() + "'" +
            ", email='" + getEmail() + "'" +
            ", hubunganPerusahaan='" + getHubunganPerusahaan() + "'" +
            ", namaPerusahaan='" + getNamaPerusahaan() + "'" +
            ", tanggalPengambilan='" + getTanggalPengambilan() + "'" +
            ", tanggalWaktuPengambilan='" + getTanggalWaktuPengambilan() + "'" +
            ", pertanyaan='" + getPertanyaan() + "'" +
            ", telepon='" + getTelepon() + "'" +
            ", kodeVerifikasi='" + getKodeVerifikasi() + "'" +
            ", namaLengkap='" + getNamaLengkap() + "'" +
            ", verifikasiEmail='" + isVerifikasiEmail() + "'" +
            ", nik='" + getNik() + "'" +
            ", namaPerusahaanPendaftar='" + getNamaPerusahaanPendaftar() + "'" +
            ", kodeKonfirmasi='" + getKodeKonfirmasi() + "'" +
            ", idKloter=" + getIdKloter() +
            ", pengambilanStruk='" + isPengambilanStruk() + "'" +
            ", jamKloter='" + getJamKloter() + "'" +
            ", waktuJamKloter='" + getWaktuJamKloter() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", modificationDate='" + getModificationDate() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", ktp='" + getKtp() + "'" +
            ", ktpContentType='" + getKtpContentType() + "'" +
            ", jawaban='" + getJawaban() + "'" +
            ", tipe_ktp='" + getTipe_ktp() + "'" +
            ", path_files='" + getPath_files() + "'" +
            ", statusTiket='" + getStatusTiket() + "'" +
            ", jamLayanan='" + getJamLayanan() + "'" +
            ", noTiket='" + getNoTiket() + "'" +
            ", typeVirtual='" + isTypeVirtual() + "'" +
            ", agent='" + isAgent() + "'" +
            "}";
    }
}
