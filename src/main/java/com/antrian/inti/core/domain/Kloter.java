package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A Kloter.
 */
@Entity
@Table(name = "kloter")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Kloter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_akhir")
    private String jamAkhir;

    @Column(name = "quota")
    private String quota;

    @Column(name = "nama_kloter")
    private String namaKloter;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "create_user_id")
    private Long createUserId;

    @Column(name = "last_modification_date")
    private LocalDate lastModificationDate;

    @Column(name = "last_modification_date_time")
    private ZonedDateTime lastModificationDateTime;

    @Column(name = "last_modification_user_id")
    private Long lastModificationUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJamMulai() {
        return jamMulai;
    }

    public Kloter jamMulai(String jamMulai) {
        this.jamMulai = jamMulai;
        return this;
    }

    public void setJamMulai(String jamMulai) {
        this.jamMulai = jamMulai;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public Kloter jamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
        return this;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getQuota() {
        return quota;
    }

    public Kloter quota(String quota) {
        this.quota = quota;
        return this;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getNamaKloter() {
        return namaKloter;
    }

    public Kloter namaKloter(String namaKloter) {
        this.namaKloter = namaKloter;
        return this;
    }

    public void setNamaKloter(String namaKloter) {
        this.namaKloter = namaKloter;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public Kloter idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public Kloter createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public Kloter createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Kloter createUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public Kloter lastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
        return this;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public Kloter lastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
        return this;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public Kloter lastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
        return this;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kloter)) {
            return false;
        }
        return id != null && id.equals(((Kloter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Kloter{" +
            "id=" + getId() +
            ", jamMulai='" + getJamMulai() + "'" +
            ", jamAkhir='" + getJamAkhir() + "'" +
            ", quota='" + getQuota() + "'" +
            ", namaKloter='" + getNamaKloter() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", createUserId=" + getCreateUserId() +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            "}";
    }
}
