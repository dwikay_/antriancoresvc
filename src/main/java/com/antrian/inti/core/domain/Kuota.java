package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Kuota.
 */
@Entity
@Table(name = "kuota")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Kuota implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "quota")
    private String quota;

    @Column(name = "jenis_layanan")
    private String jenisLayanan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public Kuota idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getQuota() {
        return quota;
    }

    public Kuota quota(String quota) {
        this.quota = quota;
        return this;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getJenisLayanan() {
        return jenisLayanan;
    }

    public Kuota jenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
        return this;
    }

    public void setJenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kuota)) {
            return false;
        }
        return id != null && id.equals(((Kuota) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Kuota{" +
            "id=" + getId() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", quota='" + getQuota() + "'" +
            ", jenisLayanan='" + getJenisLayanan() + "'" +
            "}";
    }
}
