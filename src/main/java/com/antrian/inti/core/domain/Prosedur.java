package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Prosedur.
 */
@Entity
@Table(name = "prosedur")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Prosedur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "tata_cara")
    private String tataCara;

    @Column(name = "nama_prosedur")
    private String namaProsedur;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTataCara() {
        return tataCara;
    }

    public Prosedur tataCara(String tataCara) {
        this.tataCara = tataCara;
        return this;
    }

    public void setTataCara(String tataCara) {
        this.tataCara = tataCara;
    }

    public String getNamaProsedur() {
        return namaProsedur;
    }

    public Prosedur namaProsedur(String namaProsedur) {
        this.namaProsedur = namaProsedur;
        return this;
    }

    public void setNamaProsedur(String namaProsedur) {
        this.namaProsedur = namaProsedur;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Prosedur)) {
            return false;
        }
        return id != null && id.equals(((Prosedur) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Prosedur{" +
            "id=" + getId() +
            ", tataCara='" + getTataCara() + "'" +
            ", namaProsedur='" + getNamaProsedur() + "'" +
            "}";
    }
}
