package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A HariLibur.
 */
@Entity
@Table(name = "hari_libur")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HariLibur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "keterangan")
    private String keterangan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public HariLibur tanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public HariLibur keterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HariLibur)) {
            return false;
        }
        return id != null && id.equals(((HariLibur) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HariLibur{" +
            "id=" + getId() +
            ", tanggal='" + getTanggal() + "'" +
            ", keterangan='" + getKeterangan() + "'" +
            "}";
    }
}
