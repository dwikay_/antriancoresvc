package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A Koresponden.
 */
@Entity
@Table(name = "koresponden")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Koresponden implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nik")
    private String nik;

    @Column(name = "nama")
    private String nama;

    @Column(name = "nama_perusahaan")
    private String namaPerusahaan;

    @Column(name = "no_telp")
    private String noTelp;

    @Column(name = "email")
    private String email;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "kode_verifikasi")
    private String kodeVerifikasi;

    @Column(name = "nilai_persyaratan")
    private Double nilaiPersyaratan;

    @Column(name = "nilai_prosedur")
    private Double nilaiProsedur;

    @Column(name = "nilai_waktu_pelayanan")
    private Double nilaiWaktuPelayanan;

    @Column(name = "nilai_biaya_tarif")
    private Double nilaiBiayaTarif;

    @Column(name = "nilai_produk_pelayanan_1")
    private Double nilaiProdukPelayanan1;

    @Column(name = "nilai_produk_pelayanan_2")
    private Double nilaiProdukPelayanan2;

    @Column(name = "nilai_kom_pel_1")
    private Double nilaiKomPel1;

    @Column(name = "nilai_kom_pel_2")
    private Double nilaiKomPel2;

    @Column(name = "nilai_kom_pel_3")
    private Double nilaiKomPel3;

    @Column(name = "nilai_kom_pel_4")
    private Double nilaiKomPel4;

    @Column(name = "nilai_perilaku_pelaksana_1")
    private Double nilaiPerilakuPelaksana1;

    @Column(name = "nilai_prilaku_pelaksana_2")
    private Double nilaiPrilakuPelaksana2;

    @Column(name = "nilai_sar_pras")
    private Double nilaiSarPras;

    @Column(name = "nilai_saran_masuk")
    private Double nilaiSaranMasuk;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "jenis_kelamin")
    private Boolean jenisKelamin;

    @Column(name = "pekerjaan")
    private String pekerjaan;

    @Column(name = "pendidikan")
    private String pendidikan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public Koresponden nik(String nik) {
        this.nik = nik;
        return this;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public Koresponden nama(String nama) {
        this.nama = nama;
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public Koresponden namaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
        return this;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public Koresponden noTelp(String noTelp) {
        this.noTelp = noTelp;
        return this;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getEmail() {
        return email;
    }

    public Koresponden email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKodeVerifikasi() {
        return kodeVerifikasi;
    }

    public Koresponden kodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
        return this;
    }

    public void setKodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
    }

    public Double getNilaiPersyaratan() {
        return nilaiPersyaratan;
    }

    public Koresponden nilaiPersyaratan(Double nilaiPersyaratan) {
        this.nilaiPersyaratan = nilaiPersyaratan;
        return this;
    }

    public void setNilaiPersyaratan(Double nilaiPersyaratan) {
        this.nilaiPersyaratan = nilaiPersyaratan;
    }

    public Double getNilaiProsedur() {
        return nilaiProsedur;
    }

    public Koresponden nilaiProsedur(Double nilaiProsedur) {
        this.nilaiProsedur = nilaiProsedur;
        return this;
    }

    public void setNilaiProsedur(Double nilaiProsedur) {
        this.nilaiProsedur = nilaiProsedur;
    }

    public Double getNilaiWaktuPelayanan() {
        return nilaiWaktuPelayanan;
    }

    public Koresponden nilaiWaktuPelayanan(Double nilaiWaktuPelayanan) {
        this.nilaiWaktuPelayanan = nilaiWaktuPelayanan;
        return this;
    }

    public void setNilaiWaktuPelayanan(Double nilaiWaktuPelayanan) {
        this.nilaiWaktuPelayanan = nilaiWaktuPelayanan;
    }

    public Double getNilaiBiayaTarif() {
        return nilaiBiayaTarif;
    }

    public Koresponden nilaiBiayaTarif(Double nilaiBiayaTarif) {
        this.nilaiBiayaTarif = nilaiBiayaTarif;
        return this;
    }

    public void setNilaiBiayaTarif(Double nilaiBiayaTarif) {
        this.nilaiBiayaTarif = nilaiBiayaTarif;
    }

    public Double getNilaiProdukPelayanan1() {
        return nilaiProdukPelayanan1;
    }

    public Koresponden nilaiProdukPelayanan1(Double nilaiProdukPelayanan1) {
        this.nilaiProdukPelayanan1 = nilaiProdukPelayanan1;
        return this;
    }

    public void setNilaiProdukPelayanan1(Double nilaiProdukPelayanan1) {
        this.nilaiProdukPelayanan1 = nilaiProdukPelayanan1;
    }

    public Double getNilaiProdukPelayanan2() {
        return nilaiProdukPelayanan2;
    }

    public Koresponden nilaiProdukPelayanan2(Double nilaiProdukPelayanan2) {
        this.nilaiProdukPelayanan2 = nilaiProdukPelayanan2;
        return this;
    }

    public void setNilaiProdukPelayanan2(Double nilaiProdukPelayanan2) {
        this.nilaiProdukPelayanan2 = nilaiProdukPelayanan2;
    }

    public Double getNilaiKomPel1() {
        return nilaiKomPel1;
    }

    public Koresponden nilaiKomPel1(Double nilaiKomPel1) {
        this.nilaiKomPel1 = nilaiKomPel1;
        return this;
    }

    public void setNilaiKomPel1(Double nilaiKomPel1) {
        this.nilaiKomPel1 = nilaiKomPel1;
    }

    public Double getNilaiKomPel2() {
        return nilaiKomPel2;
    }

    public Koresponden nilaiKomPel2(Double nilaiKomPel2) {
        this.nilaiKomPel2 = nilaiKomPel2;
        return this;
    }

    public void setNilaiKomPel2(Double nilaiKomPel2) {
        this.nilaiKomPel2 = nilaiKomPel2;
    }

    public Double getNilaiKomPel3() {
        return nilaiKomPel3;
    }

    public Koresponden nilaiKomPel3(Double nilaiKomPel3) {
        this.nilaiKomPel3 = nilaiKomPel3;
        return this;
    }

    public void setNilaiKomPel3(Double nilaiKomPel3) {
        this.nilaiKomPel3 = nilaiKomPel3;
    }

    public Double getNilaiKomPel4() {
        return nilaiKomPel4;
    }

    public Koresponden nilaiKomPel4(Double nilaiKomPel4) {
        this.nilaiKomPel4 = nilaiKomPel4;
        return this;
    }

    public void setNilaiKomPel4(Double nilaiKomPel4) {
        this.nilaiKomPel4 = nilaiKomPel4;
    }

    public Double getNilaiPerilakuPelaksana1() {
        return nilaiPerilakuPelaksana1;
    }

    public Koresponden nilaiPerilakuPelaksana1(Double nilaiPerilakuPelaksana1) {
        this.nilaiPerilakuPelaksana1 = nilaiPerilakuPelaksana1;
        return this;
    }

    public void setNilaiPerilakuPelaksana1(Double nilaiPerilakuPelaksana1) {
        this.nilaiPerilakuPelaksana1 = nilaiPerilakuPelaksana1;
    }

    public Double getNilaiPrilakuPelaksana2() {
        return nilaiPrilakuPelaksana2;
    }

    public Koresponden nilaiPrilakuPelaksana2(Double nilaiPrilakuPelaksana2) {
        this.nilaiPrilakuPelaksana2 = nilaiPrilakuPelaksana2;
        return this;
    }

    public void setNilaiPrilakuPelaksana2(Double nilaiPrilakuPelaksana2) {
        this.nilaiPrilakuPelaksana2 = nilaiPrilakuPelaksana2;
    }

    public Double getNilaiSarPras() {
        return nilaiSarPras;
    }

    public Koresponden nilaiSarPras(Double nilaiSarPras) {
        this.nilaiSarPras = nilaiSarPras;
        return this;
    }

    public void setNilaiSarPras(Double nilaiSarPras) {
        this.nilaiSarPras = nilaiSarPras;
    }

    public Double getNilaiSaranMasuk() {
        return nilaiSaranMasuk;
    }

    public Koresponden nilaiSaranMasuk(Double nilaiSaranMasuk) {
        this.nilaiSaranMasuk = nilaiSaranMasuk;
        return this;
    }

    public void setNilaiSaranMasuk(Double nilaiSaranMasuk) {
        this.nilaiSaranMasuk = nilaiSaranMasuk;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Koresponden createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public Koresponden createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Boolean isJenisKelamin() {
        return jenisKelamin;
    }

    public Koresponden jenisKelamin(Boolean jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
        return this;
    }

    public void setJenisKelamin(Boolean jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public Koresponden pekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
        return this;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public Koresponden pendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
        return this;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Koresponden)) {
            return false;
        }
        return id != null && id.equals(((Koresponden) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Koresponden{" +
            "id=" + getId() +
            ", nik='" + getNik() + "'" +
            ", nama='" + getNama() + "'" +
            ", namaPerusahaan='" + getNamaPerusahaan() + "'" +
            ", noTelp='" + getNoTelp() + "'" +
            ", email='" + getEmail() + "'" +
            ", kodeVerifikasi='" + getKodeVerifikasi() + "'" +
            ", nilaiPersyaratan=" + getNilaiPersyaratan() +
            ", nilaiProsedur=" + getNilaiProsedur() +
            ", nilaiWaktuPelayanan=" + getNilaiWaktuPelayanan() +
            ", nilaiBiayaTarif=" + getNilaiBiayaTarif() +
            ", nilaiProdukPelayanan1=" + getNilaiProdukPelayanan1() +
            ", nilaiProdukPelayanan2=" + getNilaiProdukPelayanan2() +
            ", nilaiKomPel1=" + getNilaiKomPel1() +
            ", nilaiKomPel2=" + getNilaiKomPel2() +
            ", nilaiKomPel3=" + getNilaiKomPel3() +
            ", nilaiKomPel4=" + getNilaiKomPel4() +
            ", nilaiPerilakuPelaksana1=" + getNilaiPerilakuPelaksana1() +
            ", nilaiPrilakuPelaksana2=" + getNilaiPrilakuPelaksana2() +
            ", nilaiSarPras=" + getNilaiSarPras() +
            ", nilaiSaranMasuk=" + getNilaiSaranMasuk() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", jenisKelamin='" + isJenisKelamin() + "'" +
            ", pekerjaan='" + getPekerjaan() + "'" +
            ", pendidikan='" + getPendidikan() + "'" +
            "}";
    }
}
