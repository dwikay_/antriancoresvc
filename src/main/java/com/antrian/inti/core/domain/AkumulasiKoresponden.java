package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A AkumulasiKoresponden.
 */
@Entity
@Table(name = "akumulasi_koresponden")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AkumulasiKoresponden implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "akumulasi_persyaratan")
    private Double akumulasiPersyaratan;

    @Column(name = "akumulasi_prosedur")
    private Double akumulasiProsedur;

    @Column(name = "akumulasi_waktu_pelayanan")
    private Double akumulasiWaktuPelayanan;

    @Column(name = "akumulasi_biaya_tarif")
    private Double akumulasiBiayaTarif;

    @Column(name = "akumulasi_produk_pelayanan")
    private Double akumulasiProdukPelayanan;

    @Column(name = "akumulasi_kom_pel")
    private Double akumulasiKomPel;

    @Column(name = "akumulasi_prilaku_pelaksana")
    private Double akumulasiPrilakuPelaksana;

    @Column(name = "akumulasi_sar_pras")
    private Double akumulasiSarPras;

    @Column(name = "akumulasi_saran_masuk")
    private Double akumulasiSaranMasuk;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "akumulasi_rata_rata")
    private Double akumulasiRataRata;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public AkumulasiKoresponden startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public AkumulasiKoresponden endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getAkumulasiPersyaratan() {
        return akumulasiPersyaratan;
    }

    public AkumulasiKoresponden akumulasiPersyaratan(Double akumulasiPersyaratan) {
        this.akumulasiPersyaratan = akumulasiPersyaratan;
        return this;
    }

    public void setAkumulasiPersyaratan(Double akumulasiPersyaratan) {
        this.akumulasiPersyaratan = akumulasiPersyaratan;
    }

    public Double getAkumulasiProsedur() {
        return akumulasiProsedur;
    }

    public AkumulasiKoresponden akumulasiProsedur(Double akumulasiProsedur) {
        this.akumulasiProsedur = akumulasiProsedur;
        return this;
    }

    public void setAkumulasiProsedur(Double akumulasiProsedur) {
        this.akumulasiProsedur = akumulasiProsedur;
    }

    public Double getAkumulasiWaktuPelayanan() {
        return akumulasiWaktuPelayanan;
    }

    public AkumulasiKoresponden akumulasiWaktuPelayanan(Double akumulasiWaktuPelayanan) {
        this.akumulasiWaktuPelayanan = akumulasiWaktuPelayanan;
        return this;
    }

    public void setAkumulasiWaktuPelayanan(Double akumulasiWaktuPelayanan) {
        this.akumulasiWaktuPelayanan = akumulasiWaktuPelayanan;
    }

    public Double getAkumulasiBiayaTarif() {
        return akumulasiBiayaTarif;
    }

    public AkumulasiKoresponden akumulasiBiayaTarif(Double akumulasiBiayaTarif) {
        this.akumulasiBiayaTarif = akumulasiBiayaTarif;
        return this;
    }

    public void setAkumulasiBiayaTarif(Double akumulasiBiayaTarif) {
        this.akumulasiBiayaTarif = akumulasiBiayaTarif;
    }

    public Double getAkumulasiProdukPelayanan() {
        return akumulasiProdukPelayanan;
    }

    public AkumulasiKoresponden akumulasiProdukPelayanan(Double akumulasiProdukPelayanan) {
        this.akumulasiProdukPelayanan = akumulasiProdukPelayanan;
        return this;
    }

    public void setAkumulasiProdukPelayanan(Double akumulasiProdukPelayanan) {
        this.akumulasiProdukPelayanan = akumulasiProdukPelayanan;
    }

    public Double getAkumulasiKomPel() {
        return akumulasiKomPel;
    }

    public AkumulasiKoresponden akumulasiKomPel(Double akumulasiKomPel) {
        this.akumulasiKomPel = akumulasiKomPel;
        return this;
    }

    public void setAkumulasiKomPel(Double akumulasiKomPel) {
        this.akumulasiKomPel = akumulasiKomPel;
    }

    public Double getAkumulasiPrilakuPelaksana() {
        return akumulasiPrilakuPelaksana;
    }

    public AkumulasiKoresponden akumulasiPrilakuPelaksana(Double akumulasiPrilakuPelaksana) {
        this.akumulasiPrilakuPelaksana = akumulasiPrilakuPelaksana;
        return this;
    }

    public void setAkumulasiPrilakuPelaksana(Double akumulasiPrilakuPelaksana) {
        this.akumulasiPrilakuPelaksana = akumulasiPrilakuPelaksana;
    }

    public Double getAkumulasiSarPras() {
        return akumulasiSarPras;
    }

    public AkumulasiKoresponden akumulasiSarPras(Double akumulasiSarPras) {
        this.akumulasiSarPras = akumulasiSarPras;
        return this;
    }

    public void setAkumulasiSarPras(Double akumulasiSarPras) {
        this.akumulasiSarPras = akumulasiSarPras;
    }

    public Double getAkumulasiSaranMasuk() {
        return akumulasiSaranMasuk;
    }

    public AkumulasiKoresponden akumulasiSaranMasuk(Double akumulasiSaranMasuk) {
        this.akumulasiSaranMasuk = akumulasiSaranMasuk;
        return this;
    }

    public void setAkumulasiSaranMasuk(Double akumulasiSaranMasuk) {
        this.akumulasiSaranMasuk = akumulasiSaranMasuk;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public AkumulasiKoresponden createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public AkumulasiKoresponden createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Double getAkumulasiRataRata() {
        return akumulasiRataRata;
    }

    public AkumulasiKoresponden akumulasiRataRata(Double akumulasiRataRata) {
        this.akumulasiRataRata = akumulasiRataRata;
        return this;
    }

    public void setAkumulasiRataRata(Double akumulasiRataRata) {
        this.akumulasiRataRata = akumulasiRataRata;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AkumulasiKoresponden)) {
            return false;
        }
        return id != null && id.equals(((AkumulasiKoresponden) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AkumulasiKoresponden{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", akumulasiPersyaratan=" + getAkumulasiPersyaratan() +
            ", akumulasiProsedur=" + getAkumulasiProsedur() +
            ", akumulasiWaktuPelayanan=" + getAkumulasiWaktuPelayanan() +
            ", akumulasiBiayaTarif=" + getAkumulasiBiayaTarif() +
            ", akumulasiProdukPelayanan=" + getAkumulasiProdukPelayanan() +
            ", akumulasiKomPel=" + getAkumulasiKomPel() +
            ", akumulasiPrilakuPelaksana=" + getAkumulasiPrilakuPelaksana() +
            ", akumulasiSarPras=" + getAkumulasiSarPras() +
            ", akumulasiSaranMasuk=" + getAkumulasiSaranMasuk() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", akumulasiRataRata=" + getAkumulasiRataRata() +
            "}";
    }
}
