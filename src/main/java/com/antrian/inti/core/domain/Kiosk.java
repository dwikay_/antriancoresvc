package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A Kiosk.
 */
@Entity
@Table(name = "kiosk")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Kiosk implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "lokasi")
    private String lokasi;

    @Column(name = "username")
    private String username;

    @Column(name = "password_kiosk")
    private String passwordKiosk;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Size(max = 3)
    @Column(name = "status", length = 3)
    private String status;

    @Column(name = "create_user_id")
    private Long createUserId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "last_modification_user_id")
    private Long lastModificationUserId;

    @Column(name = "last_modification_date")
    private LocalDate lastModificationDate;

    @Column(name = "last_modification_date_time")
    private ZonedDateTime lastModificationDateTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLokasi() {
        return lokasi;
    }

    public Kiosk lokasi(String lokasi) {
        this.lokasi = lokasi;
        return this;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getUsername() {
        return username;
    }

    public Kiosk username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordKiosk() {
        return passwordKiosk;
    }

    public Kiosk passwordKiosk(String passwordKiosk) {
        this.passwordKiosk = passwordKiosk;
        return this;
    }

    public void setPasswordKiosk(String passwordKiosk) {
        this.passwordKiosk = passwordKiosk;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public Kiosk deskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
        return this;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStatus() {
        return status;
    }

    public Kiosk status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Kiosk createUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public Kiosk createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public Kiosk createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public Kiosk lastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
        return this;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public Kiosk lastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
        return this;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public Kiosk lastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
        return this;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kiosk)) {
            return false;
        }
        return id != null && id.equals(((Kiosk) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Kiosk{" +
            "id=" + getId() +
            ", lokasi='" + getLokasi() + "'" +
            ", username='" + getUsername() + "'" +
            ", passwordKiosk='" + getPasswordKiosk() + "'" +
            ", deskripsi='" + getDeskripsi() + "'" +
            ", status='" + getStatus() + "'" +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            "}";
    }
}
