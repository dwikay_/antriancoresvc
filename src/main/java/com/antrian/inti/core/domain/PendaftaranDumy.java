package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

/**
 * A PendaftaranDumy.
 */
@Entity
@Table(name = "pendaftaran_dumy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PendaftaranDumy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nama_badan_usaha")
    private String namaBadanUsaha;

    @Column(name = "nama_direksi")
    private String namaDireksi;

    @Column(name = "email")
    private String email;

    @Column(name = "tanggal_pengambilan")
    private LocalDate tanggalPengambilan;

    @Column(name = "kode_konfirmasi")
    private String kodeKonfirmasi;

    @Column(name = "id_kloter")
    private Long idKloter;

    @Column(name = "pengambilan_struk")
    private Boolean pengambilanStruk;

    @Column(name = "jam_kloter")
    private String jamKloter;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "pertanyaan")
    private String pertanyaan;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "jawaban")
    private String jawaban;

    @Column(name = "tipe_ktp")
    private String tipeKtp;

    @Column(name = "status_tiket")
    private String statusTiket;

    @Column(name = "type_virtual")
    private Boolean typeVirtual;

    @Column(name = "jam_layanan")
    private String jamLayanan;

    @Column(name = "no_tiket")
    private String noTiket;

    @Column(name = "agent")
    private Boolean agent;

    @Column(name = "id_daftar")
    private UUID idDaftar;

    @Column(name = "nama_perseorangan")
    private String namaPerseorangan;

    @Column(name = "instansi")
    private String instansi;

    @Column(name = "unit")
    private String unit;

    @Column(name = "jabatan")
    private String jabatan;

    @Column(name = "nama_pejabat")
    private String namaPejabat;

    @Column(name = "untuk_layanan")
    private String untukLayanan;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "jwt_token")
    private String jwtToken;

    @Column(name = "nik")
    private String nik;

    @Column(name = "flag_banned")
    private Long flagBanned;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "jenis_kendala")
    private String jenisKendala;

    @Column(name = "create_date")
    private LocalDate createDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaBadanUsaha() {
        return namaBadanUsaha;
    }

    public PendaftaranDumy namaBadanUsaha(String namaBadanUsaha) {
        this.namaBadanUsaha = namaBadanUsaha;
        return this;
    }

    public void setNamaBadanUsaha(String namaBadanUsaha) {
        this.namaBadanUsaha = namaBadanUsaha;
    }

    public String getNamaDireksi() {
        return namaDireksi;
    }

    public PendaftaranDumy namaDireksi(String namaDireksi) {
        this.namaDireksi = namaDireksi;
        return this;
    }

    public void setNamaDireksi(String namaDireksi) {
        this.namaDireksi = namaDireksi;
    }

    public String getEmail() {
        return email;
    }

    public PendaftaranDumy email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getTanggalPengambilan() {
        return tanggalPengambilan;
    }

    public PendaftaranDumy tanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
        return this;
    }

    public void setTanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public PendaftaranDumy kodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
        return this;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public Long getIdKloter() {
        return idKloter;
    }

    public PendaftaranDumy idKloter(Long idKloter) {
        this.idKloter = idKloter;
        return this;
    }

    public void setIdKloter(Long idKloter) {
        this.idKloter = idKloter;
    }

    public Boolean isPengambilanStruk() {
        return pengambilanStruk;
    }

    public PendaftaranDumy pengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
        return this;
    }

    public void setPengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
    }

    public String getJamKloter() {
        return jamKloter;
    }

    public PendaftaranDumy jamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
        return this;
    }

    public void setJamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public PendaftaranDumy pertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
        return this;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getJawaban() {
        return jawaban;
    }

    public PendaftaranDumy jawaban(String jawaban) {
        this.jawaban = jawaban;
        return this;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getTipeKtp() {
        return tipeKtp;
    }

    public PendaftaranDumy tipeKtp(String tipeKtp) {
        this.tipeKtp = tipeKtp;
        return this;
    }

    public void setTipeKtp(String tipeKtp) {
        this.tipeKtp = tipeKtp;
    }

    public String getStatusTiket() {
        return statusTiket;
    }

    public PendaftaranDumy statusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
        return this;
    }

    public void setStatusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
    }

    public Boolean isTypeVirtual() {
        return typeVirtual;
    }

    public PendaftaranDumy typeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
        return this;
    }

    public void setTypeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
    }

    public String getJamLayanan() {
        return jamLayanan;
    }

    public PendaftaranDumy jamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
        return this;
    }

    public void setJamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
    }

    public String getNoTiket() {
        return noTiket;
    }

    public PendaftaranDumy noTiket(String noTiket) {
        this.noTiket = noTiket;
        return this;
    }

    public void setNoTiket(String noTiket) {
        this.noTiket = noTiket;
    }

    public Boolean isAgent() {
        return agent;
    }

    public PendaftaranDumy agent(Boolean agent) {
        this.agent = agent;
        return this;
    }

    public void setAgent(Boolean agent) {
        this.agent = agent;
    }

    public UUID getIdDaftar() {
        return idDaftar;
    }

    public PendaftaranDumy idDaftar(UUID idDaftar) {
        this.idDaftar = idDaftar;
        return this;
    }

    public void setIdDaftar(UUID idDaftar) {
        this.idDaftar = idDaftar;
    }

    public String getNamaPerseorangan() {
        return namaPerseorangan;
    }

    public PendaftaranDumy namaPerseorangan(String namaPerseorangan) {
        this.namaPerseorangan = namaPerseorangan;
        return this;
    }

    public void setNamaPerseorangan(String namaPerseorangan) {
        this.namaPerseorangan = namaPerseorangan;
    }

    public String getInstansi() {
        return instansi;
    }

    public PendaftaranDumy instansi(String instansi) {
        this.instansi = instansi;
        return this;
    }

    public void setInstansi(String instansi) {
        this.instansi = instansi;
    }

    public String getUnit() {
        return unit;
    }

    public PendaftaranDumy unit(String unit) {
        this.unit = unit;
        return this;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getJabatan() {
        return jabatan;
    }

    public PendaftaranDumy jabatan(String jabatan) {
        this.jabatan = jabatan;
        return this;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getNamaPejabat() {
        return namaPejabat;
    }

    public PendaftaranDumy namaPejabat(String namaPejabat) {
        this.namaPejabat = namaPejabat;
        return this;
    }

    public void setNamaPejabat(String namaPejabat) {
        this.namaPejabat = namaPejabat;
    }

    public String getUntukLayanan() {
        return untukLayanan;
    }

    public PendaftaranDumy untukLayanan(String untukLayanan) {
        this.untukLayanan = untukLayanan;
        return this;
    }

    public void setUntukLayanan(String untukLayanan) {
        this.untukLayanan = untukLayanan;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public PendaftaranDumy jwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
        return this;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getNik() {
        return nik;
    }

    public PendaftaranDumy nik(String nik) {
        this.nik = nik;
        return this;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public Long getFlagBanned() {
        return flagBanned;
    }

    public PendaftaranDumy flagBanned(Long flagBanned) {
        this.flagBanned = flagBanned;
        return this;
    }

    public void setFlagBanned(Long flagBanned) {
        this.flagBanned = flagBanned;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public PendaftaranDumy idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getJenisKendala() {
        return jenisKendala;
    }

    public PendaftaranDumy jenisKendala(String jenisKendala) {
        this.jenisKendala = jenisKendala;
        return this;
    }

    public void setJenisKendala(String jenisKendala) {
        this.jenisKendala = jenisKendala;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public PendaftaranDumy createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PendaftaranDumy)) {
            return false;
        }
        return id != null && id.equals(((PendaftaranDumy) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PendaftaranDumy{" +
            "id=" + getId() +
            ", namaBadanUsaha='" + getNamaBadanUsaha() + "'" +
            ", namaDireksi='" + getNamaDireksi() + "'" +
            ", email='" + getEmail() + "'" +
            ", tanggalPengambilan='" + getTanggalPengambilan() + "'" +
            ", kodeKonfirmasi='" + getKodeKonfirmasi() + "'" +
            ", idKloter=" + getIdKloter() +
            ", pengambilanStruk='" + isPengambilanStruk() + "'" +
            ", jamKloter='" + getJamKloter() + "'" +
            ", pertanyaan='" + getPertanyaan() + "'" +
            ", jawaban='" + getJawaban() + "'" +
            ", tipeKtp='" + getTipeKtp() + "'" +
            ", statusTiket='" + getStatusTiket() + "'" +
            ", typeVirtual='" + isTypeVirtual() + "'" +
            ", jamLayanan='" + getJamLayanan() + "'" +
            ", noTiket='" + getNoTiket() + "'" +
            ", agent='" + isAgent() + "'" +
            ", idDaftar='" + getIdDaftar() + "'" +
            ", namaPerseorangan='" + getNamaPerseorangan() + "'" +
            ", instansi='" + getInstansi() + "'" +
            ", unit='" + getUnit() + "'" +
            ", jabatan='" + getJabatan() + "'" +
            ", namaPejabat='" + getNamaPejabat() + "'" +
            ", untukLayanan='" + getUntukLayanan() + "'" +
            ", jwtToken='" + getJwtToken() + "'" +
            ", nik='" + getNik() + "'" +
            ", flagBanned=" + getFlagBanned() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", jenisKendala='" + getJenisKendala() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
