package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A SubLayanan.
 */
@Entity
@Table(name = "sub_layanan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SubLayanan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_layanan")
    private Long idLayanan;

    @Column(name = "kode_layanan")
    private String kodeLayanan;

    @Column(name = "nama_layanan")
    private String namaLayanan;

    @Column(name = "id_lantai")
    private Long idLantai;

    @Column(name = "kuota")
    private Long kuota;

    @Column(name = "jam_awal")
    private String jamAwal;

    @Column(name = "jam_akhir")
    private String jamAkhir;

    @Column(name = "durasi")
    private Long durasi;

    @Size(max = 3)
    @Column(name = "status", length = 3)
    private String status;

    @Size(max = 5)
    @Column(name = "prefix_suara", length = 5)
    private String prefixSuara;

    @Column(name = "layanan_online")
    private Boolean layananOnline;

    @Column(name = "id_kiosk")
    private Long idKiosk;

    @Column(name = "create_user_id")
    private Long createUserId;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "last_modification_user_id")
    private Long lastModificationUserId;

    @Column(name = "last_modification_date")
    private LocalDate lastModificationDate;

    @Column(name = "last_modification_date_time")
    private ZonedDateTime lastModificationDateTime;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "sk")
    private String sk;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdLayanan() {
        return idLayanan;
    }

    public SubLayanan idLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
        return this;
    }

    public void setIdLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
    }

    public String getKodeLayanan() {
        return kodeLayanan;
    }

    public SubLayanan kodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
        return this;
    }

    public void setKodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public SubLayanan namaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
        return this;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public SubLayanan idLantai(Long idLantai) {
        this.idLantai = idLantai;
        return this;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }

    public Long getKuota() {
        return kuota;
    }

    public SubLayanan kuota(Long kuota) {
        this.kuota = kuota;
        return this;
    }

    public void setKuota(Long kuota) {
        this.kuota = kuota;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public SubLayanan jamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
        return this;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public SubLayanan jamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
        return this;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public Long getDurasi() {
        return durasi;
    }

    public SubLayanan durasi(Long durasi) {
        this.durasi = durasi;
        return this;
    }

    public void setDurasi(Long durasi) {
        this.durasi = durasi;
    }

    public String getStatus() {
        return status;
    }

    public SubLayanan status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public SubLayanan prefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
        return this;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    public Boolean isLayananOnline() {
        return layananOnline;
    }

    public SubLayanan layananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
        return this;
    }

    public void setLayananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
    }

    public Long getIdKiosk() {
        return idKiosk;
    }

    public SubLayanan idKiosk(Long idKiosk) {
        this.idKiosk = idKiosk;
        return this;
    }

    public void setIdKiosk(Long idKiosk) {
        this.idKiosk = idKiosk;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public SubLayanan createUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public SubLayanan createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public SubLayanan createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public SubLayanan lastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
        return this;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public SubLayanan lastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
        return this;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public SubLayanan lastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
        return this;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public String getSk() {
        return sk;
    }

    public SubLayanan sk(String sk) {
        this.sk = sk;
        return this;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubLayanan)) {
            return false;
        }
        return id != null && id.equals(((SubLayanan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SubLayanan{" +
            "id=" + getId() +
            ", idLayanan=" + getIdLayanan() +
            ", kodeLayanan='" + getKodeLayanan() + "'" +
            ", namaLayanan='" + getNamaLayanan() + "'" +
            ", idLantai=" + getIdLantai() +
            ", kuota=" + getKuota() +
            ", jamAwal='" + getJamAwal() + "'" +
            ", jamAkhir='" + getJamAkhir() + "'" +
            ", durasi=" + getDurasi() +
            ", status='" + getStatus() + "'" +
            ", prefixSuara='" + getPrefixSuara() + "'" +
            ", layananOnline='" + isLayananOnline() + "'" +
            ", idKiosk=" + getIdKiosk() +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            ", sk='" + getSk() + "'" +
            "}";
    }
}
