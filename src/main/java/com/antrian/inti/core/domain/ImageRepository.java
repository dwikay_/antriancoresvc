package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ImageRepository.
 */
@Entity
@Table(name = "image_repository")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ImageRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nama_file")
    private String namaFile;

    @Column(name = "relasi_id")
    private Long relasiId;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "document")
    private String document;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public ImageRepository namaFile(String namaFile) {
        this.namaFile = namaFile;
        return this;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public Long getRelasiId() {
        return relasiId;
    }

    public ImageRepository relasiId(Long relasiId) {
        this.relasiId = relasiId;
        return this;
    }

    public void setRelasiId(Long relasiId) {
        this.relasiId = relasiId;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public ImageRepository idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getDocument() {
        return document;
    }

    public ImageRepository document(String document) {
        this.document = document;
        return this;
    }

    public void setDocument(String document) {
        this.document = document;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageRepository)) {
            return false;
        }
        return id != null && id.equals(((ImageRepository) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ImageRepository{" +
            "id=" + getId() +
            ", namaFile='" + getNamaFile() + "'" +
            ", relasiId=" + getRelasiId() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", document='" + getDocument() + "'" +
            "}";
    }
}
