package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A JadwalDetil.
 */
@Entity
@Table(name = "jadwal_detil")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class JadwalDetil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_master")
    private Long idMaster;

    @Column(name = "jam_awal")
    private String jamAwal;

    @Column(name = "jam_akhir")
    private String jamAkhir;

    @Column(name = "kloter")
    private String kloter;

    @Column(name = "jumlah_kuota")
    private String jumlahKuota;

    @Column(name = "virtual")
    private Boolean virtual;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdMaster() {
        return idMaster;
    }

    public JadwalDetil idMaster(Long idMaster) {
        this.idMaster = idMaster;
        return this;
    }

    public void setIdMaster(Long idMaster) {
        this.idMaster = idMaster;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public JadwalDetil jamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
        return this;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public JadwalDetil jamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
        return this;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getKloter() {
        return kloter;
    }

    public JadwalDetil kloter(String kloter) {
        this.kloter = kloter;
        return this;
    }

    public void setKloter(String kloter) {
        this.kloter = kloter;
    }

    public String getJumlahKuota() {
        return jumlahKuota;
    }

    public JadwalDetil jumlahKuota(String jumlahKuota) {
        this.jumlahKuota = jumlahKuota;
        return this;
    }

    public void setJumlahKuota(String jumlahKuota) {
        this.jumlahKuota = jumlahKuota;
    }

    public Boolean isVirtual() {
        return virtual;
    }

    public JadwalDetil virtual(Boolean virtual) {
        this.virtual = virtual;
        return this;
    }

    public void setVirtual(Boolean virtual) {
        this.virtual = virtual;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JadwalDetil)) {
            return false;
        }
        return id != null && id.equals(((JadwalDetil) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "JadwalDetil{" +
            "id=" + getId() +
            ", idMaster=" + getIdMaster() +
            ", jamAwal='" + getJamAwal() + "'" +
            ", jamAkhir='" + getJamAkhir() + "'" +
            ", kloter='" + getKloter() + "'" +
            ", jumlahKuota='" + getJumlahKuota() + "'" +
            ", virtual='" + isVirtual() + "'" +
            "}";
    }
}
