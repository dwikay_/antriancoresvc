package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A PendaftaranOffline.
 */
@Entity
@Table(name = "pendaftaran_offline")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PendaftaranOffline implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nama_pelaku")
    private String namaPelaku;

    @Column(name = "email")
    private String email;

    @Column(name = "nik")
    private String nik;

    @Column(name = "nama_perusahaan")
    private String namaPerusahaan;

    @Column(name = "telepon")
    private String telepon;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "pertanyaan")
    private String pertanyaan;

    @Column(name = "no_urut")
    private String noUrut;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "token_validasi")
    private String tokenValidasi;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPelaku() {
        return namaPelaku;
    }

    public PendaftaranOffline namaPelaku(String namaPelaku) {
        this.namaPelaku = namaPelaku;
        return this;
    }

    public void setNamaPelaku(String namaPelaku) {
        this.namaPelaku = namaPelaku;
    }

    public String getEmail() {
        return email;
    }

    public PendaftaranOffline email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNik() {
        return nik;
    }

    public PendaftaranOffline nik(String nik) {
        this.nik = nik;
        return this;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public PendaftaranOffline namaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
        return this;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String getTelepon() {
        return telepon;
    }

    public PendaftaranOffline telepon(String telepon) {
        this.telepon = telepon;
        return this;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public PendaftaranOffline pertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
        return this;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public PendaftaranOffline noUrut(String noUrut) {
        this.noUrut = noUrut;
        return this;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public PendaftaranOffline idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getTokenValidasi() {
        return tokenValidasi;
    }

    public PendaftaranOffline tokenValidasi(String tokenValidasi) {
        this.tokenValidasi = tokenValidasi;
        return this;
    }

    public void setTokenValidasi(String tokenValidasi) {
        this.tokenValidasi = tokenValidasi;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public PendaftaranOffline tanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PendaftaranOffline)) {
            return false;
        }
        return id != null && id.equals(((PendaftaranOffline) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PendaftaranOffline{" +
            "id=" + getId() +
            ", namaPelaku='" + getNamaPelaku() + "'" +
            ", email='" + getEmail() + "'" +
            ", nik='" + getNik() + "'" +
            ", namaPerusahaan='" + getNamaPerusahaan() + "'" +
            ", telepon='" + getTelepon() + "'" +
            ", pertanyaan='" + getPertanyaan() + "'" +
            ", noUrut='" + getNoUrut() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", tokenValidasi='" + getTokenValidasi() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            "}";
    }
}
