package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A DataAntrian.
 */
@Entity
@Table(name = "data_antrian")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataAntrian implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "no_urut")
    private Long noUrut;

    @Column(name = "status_panggilan")
    private Boolean statusPanggilan;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "layanan")
    private String layanan;

    @Column(name = "loket")
    private Long loket;

    @Column(name = "prefix_suara")
    private String prefixSuara;

    @Column(name = "lantai")
    private Long lantai;

    @Column(name = "status_current")
    private Boolean statusCurrent;

    @Column(name = "kode_konfirmasi")
    private String kodeKonfirmasi;

    @Lob
    @Column(name = "image_webcam")
    private byte[] imageWebcam;

    @Column(name = "image_webcam_content_type")
    private String imageWebcamContentType;

    @Column(name = "layanan_online")
    private Boolean layananOnline;

    @Column(name = "durasi")
    private String durasi;

    @Column(name = "sisa_durasi")
    private String sisaDurasi;

    @Column(name = "status_konseling")
    private Boolean statusKonseling;

    @Column(name = "tanggal_waktu")
    private String tanggalWaktu;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public DataAntrian tanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Long getNoUrut() {
        return noUrut;
    }

    public DataAntrian noUrut(Long noUrut) {
        this.noUrut = noUrut;
        return this;
    }

    public void setNoUrut(Long noUrut) {
        this.noUrut = noUrut;
    }

    public Boolean isStatusPanggilan() {
        return statusPanggilan;
    }

    public DataAntrian statusPanggilan(Boolean statusPanggilan) {
        this.statusPanggilan = statusPanggilan;
        return this;
    }

    public void setStatusPanggilan(Boolean statusPanggilan) {
        this.statusPanggilan = statusPanggilan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public DataAntrian idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getLayanan() {
        return layanan;
    }

    public DataAntrian layanan(String layanan) {
        this.layanan = layanan;
        return this;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public Long getLoket() {
        return loket;
    }

    public DataAntrian loket(Long loket) {
        this.loket = loket;
        return this;
    }

    public void setLoket(Long loket) {
        this.loket = loket;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public DataAntrian prefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
        return this;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    public Long getLantai() {
        return lantai;
    }

    public DataAntrian lantai(Long lantai) {
        this.lantai = lantai;
        return this;
    }

    public void setLantai(Long lantai) {
        this.lantai = lantai;
    }

    public Boolean isStatusCurrent() {
        return statusCurrent;
    }

    public DataAntrian statusCurrent(Boolean statusCurrent) {
        this.statusCurrent = statusCurrent;
        return this;
    }

    public void setStatusCurrent(Boolean statusCurrent) {
        this.statusCurrent = statusCurrent;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public DataAntrian kodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
        return this;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public byte[] getImageWebcam() {
        return imageWebcam;
    }

    public DataAntrian imageWebcam(byte[] imageWebcam) {
        this.imageWebcam = imageWebcam;
        return this;
    }

    public void setImageWebcam(byte[] imageWebcam) {
        this.imageWebcam = imageWebcam;
    }

    public String getImageWebcamContentType() {
        return imageWebcamContentType;
    }

    public DataAntrian imageWebcamContentType(String imageWebcamContentType) {
        this.imageWebcamContentType = imageWebcamContentType;
        return this;
    }

    public void setImageWebcamContentType(String imageWebcamContentType) {
        this.imageWebcamContentType = imageWebcamContentType;
    }

    public Boolean isLayananOnline() {
        return layananOnline;
    }

    public DataAntrian layananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
        return this;
    }

    public void setLayananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
    }

    public String getDurasi() {
        return durasi;
    }

    public DataAntrian durasi(String durasi) {
        this.durasi = durasi;
        return this;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getSisaDurasi() {
        return sisaDurasi;
    }

    public DataAntrian sisaDurasi(String sisaDurasi) {
        this.sisaDurasi = sisaDurasi;
        return this;
    }

    public void setSisaDurasi(String sisaDurasi) {
        this.sisaDurasi = sisaDurasi;
    }

    public Boolean isStatusKonseling() {
        return statusKonseling;
    }

    public DataAntrian statusKonseling(Boolean statusKonseling) {
        this.statusKonseling = statusKonseling;
        return this;
    }

    public void setStatusKonseling(Boolean statusKonseling) {
        this.statusKonseling = statusKonseling;
    }

    public String getTanggalWaktu() {
        return tanggalWaktu;
    }

    public DataAntrian tanggalWaktu(String tanggalWaktu) {
        this.tanggalWaktu = tanggalWaktu;
        return this;
    }

    public void setTanggalWaktu(String tanggalWaktu) {
        this.tanggalWaktu = tanggalWaktu;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DataAntrian)) {
            return false;
        }
        return id != null && id.equals(((DataAntrian) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DataAntrian{" +
            "id=" + getId() +
            ", tanggal='" + getTanggal() + "'" +
            ", noUrut=" + getNoUrut() +
            ", statusPanggilan='" + isStatusPanggilan() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", layanan='" + getLayanan() + "'" +
            ", loket=" + getLoket() +
            ", prefixSuara='" + getPrefixSuara() + "'" +
            ", lantai=" + getLantai() +
            ", statusCurrent='" + isStatusCurrent() + "'" +
            ", kodeKonfirmasi='" + getKodeKonfirmasi() + "'" +
            ", imageWebcam='" + getImageWebcam() + "'" +
            ", imageWebcamContentType='" + getImageWebcamContentType() + "'" +
            ", layananOnline='" + isLayananOnline() + "'" +
            ", durasi='" + getDurasi() + "'" +
            ", sisaDurasi='" + getSisaDurasi() + "'" +
            ", statusKonseling='" + isStatusKonseling() + "'" +
            ", tanggalWaktu='" + getTanggalWaktu() + "'" +
            "}";
    }
}
