package com.antrian.inti.core.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * A TempPanggilan.
 */
@Entity
@Table(name = "temp_panggilan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TempPanggilan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "id_temp_panggilan")
    private UUID idTempPanggilan;

    @Column(name = "panggilan")
    private String panggilan;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "no_urut")
    private String noUrut;

    @Column(name = "id_sub_layanan")
    private Long idSubLayanan;

    @Column(name = "id_lantai")
    private Long idLantai;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getIdTempPanggilan() {
        return idTempPanggilan;
    }

    public TempPanggilan idTempPanggilan(UUID idTempPanggilan) {
        this.idTempPanggilan = idTempPanggilan;
        return this;
    }

    public void setIdTempPanggilan(UUID idTempPanggilan) {
        this.idTempPanggilan = idTempPanggilan;
    }

    public String getPanggilan() {
        return panggilan;
    }

    public TempPanggilan panggilan(String panggilan) {
        this.panggilan = panggilan;
        return this;
    }

    public void setPanggilan(String panggilan) {
        this.panggilan = panggilan;
    }

    public Boolean isStatus() {
        return status;
    }

    public TempPanggilan status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public TempPanggilan noUrut(String noUrut) {
        this.noUrut = noUrut;
        return this;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public TempPanggilan idSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
        return this;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public TempPanggilan idLantai(Long idLantai) {
        this.idLantai = idLantai;
        return this;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TempPanggilan)) {
            return false;
        }
        return id != null && id.equals(((TempPanggilan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TempPanggilan{" +
            "id=" + getId() +
            ", idTempPanggilan='" + getIdTempPanggilan() + "'" +
            ", panggilan='" + getPanggilan() + "'" +
            ", status='" + isStatus() + "'" +
            ", noUrut='" + getNoUrut() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", idLantai=" + getIdLantai() +
            "}";
    }
}
