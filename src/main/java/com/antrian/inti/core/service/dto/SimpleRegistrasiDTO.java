package com.antrian.inti.core.service.dto;

import javax.persistence.Lob;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

public class SimpleRegistrasiDTO implements Serializable {

    private Long id;
    private String idDaftar;
    private String email;
    private String hubunganPerusahaan;
    private String namaPerusahaan;
    private LocalDate tanggalPengambilan;
    private String tanggalWaktuPengambilan;
    @Lob
    private String pertanyaan;
    private String telepon;
    @Lob
    private String kodeVerifikasi;
    private String namaLengkap;
    private Boolean verifikasiEmail;
    @Size(max = 16)
    private String nik;
    private String namaPerusahaanPendaftar;
    private String kodeKonfirmasi;
    private Long idKloter;
    private Boolean pengambilanStruk;
    private String jamKloter;
    private String waktuJamKloter;
    private LocalDate createDate;
    private ZonedDateTime createDateTime;
    private LocalDate modificationDate;
    private Long idSubLayanan;
    private String urlKtp;
    private String statusTiket;
    private String jamLayanan;
    private String noTiket;
    private Boolean typeVirtual;
    private Boolean agent;
    Optional<SimpleSubLayananDTO> simpleSubLayananDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDaftar() {
        return idDaftar;
    }

    public void setIdDaftar(String idDaftar) {
        this.idDaftar = idDaftar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHubunganPerusahaan() {
        return hubunganPerusahaan;
    }

    public void setHubunganPerusahaan(String hubunganPerusahaan) {
        this.hubunganPerusahaan = hubunganPerusahaan;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public LocalDate getTanggalPengambilan() {
        return tanggalPengambilan;
    }

    public void setTanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
    }

    public String getTanggalWaktuPengambilan() {
        return tanggalWaktuPengambilan;
    }

    public void setTanggalWaktuPengambilan(String tanggalWaktuPengambilan) {
        this.tanggalWaktuPengambilan = tanggalWaktuPengambilan;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getKodeVerifikasi() {
        return kodeVerifikasi;
    }

    public void setKodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public Boolean getVerifikasiEmail() {
        return verifikasiEmail;
    }

    public void setVerifikasiEmail(Boolean verifikasiEmail) {
        this.verifikasiEmail = verifikasiEmail;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaPerusahaanPendaftar() {
        return namaPerusahaanPendaftar;
    }

    public void setNamaPerusahaanPendaftar(String namaPerusahaanPendaftar) {
        this.namaPerusahaanPendaftar = namaPerusahaanPendaftar;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public Long getIdKloter() {
        return idKloter;
    }

    public void setIdKloter(Long idKloter) {
        this.idKloter = idKloter;
    }

    public Boolean getPengambilanStruk() {
        return pengambilanStruk;
    }

    public void setPengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
    }

    public String getJamKloter() {
        return jamKloter;
    }

    public void setJamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
    }

    public String getWaktuJamKloter() {
        return waktuJamKloter;
    }

    public void setWaktuJamKloter(String waktuJamKloter) {
        this.waktuJamKloter = waktuJamKloter;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getUrlKtp() {
        return urlKtp;
    }

    public void setUrlKtp(String urlKtp) {
        this.urlKtp = urlKtp;
    }

    public String getStatusTiket() {
        return statusTiket;
    }

    public void setStatusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
    }

    public String getJamLayanan() {
        return jamLayanan;
    }

    public void setJamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
    }

    public String getNoTiket() {
        return noTiket;
    }

    public void setNoTiket(String noTiket) {
        this.noTiket = noTiket;
    }

    public Boolean getTypeVirtual() {
        return typeVirtual;
    }

    public void setTypeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
    }

    public Boolean getAgent() {
        return agent;
    }

    public void setAgent(Boolean agent) {
        this.agent = agent;
    }

    public Optional<SimpleSubLayananDTO> getSimpleSubLayananDTO() {
        return simpleSubLayananDTO;
    }

    public void setSimpleSubLayananDTO(Optional<SimpleSubLayananDTO> simpleSubLayananDTO) {
        this.simpleSubLayananDTO = simpleSubLayananDTO;
    }

    @Override
    public String toString() {
        return "SimpleRegistrasiDTO{" +
            "id=" + id +
            ", idDaftar='" + idDaftar + '\'' +
            ", email='" + email + '\'' +
            ", hubunganPerusahaan='" + hubunganPerusahaan + '\'' +
            ", namaPerusahaan='" + namaPerusahaan + '\'' +
            ", tanggalPengambilan=" + tanggalPengambilan +
            ", tanggalWaktuPengambilan='" + tanggalWaktuPengambilan + '\'' +
            ", pertanyaan='" + pertanyaan + '\'' +
            ", telepon='" + telepon + '\'' +
            ", kodeVerifikasi='" + kodeVerifikasi + '\'' +
            ", namaLengkap='" + namaLengkap + '\'' +
            ", verifikasiEmail=" + verifikasiEmail +
            ", nik='" + nik + '\'' +
            ", namaPerusahaanPendaftar='" + namaPerusahaanPendaftar + '\'' +
            ", kodeKonfirmasi='" + kodeKonfirmasi + '\'' +
            ", idKloter=" + idKloter +
            ", pengambilanStruk=" + pengambilanStruk +
            ", jamKloter='" + jamKloter + '\'' +
            ", waktuJamKloter='" + waktuJamKloter + '\'' +
            ", createDate=" + createDate +
            ", createDateTime=" + createDateTime +
            ", modificationDate=" + modificationDate +
            ", idSubLayanan=" + idSubLayanan +
            ", urlKtp='" + urlKtp + '\'' +
            ", statusTiket='" + statusTiket + '\'' +
            ", jamLayanan='" + jamLayanan + '\'' +
            ", noTiket='" + noTiket + '\'' +
            ", typeVirtual=" + typeVirtual +
            ", agent=" + agent +
            ", simpleSubLayananDTO=" + simpleSubLayananDTO +
            '}';
    }
}
