package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.KorespondenDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Koresponden} and its DTO {@link KorespondenDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KorespondenMapper extends EntityMapper<KorespondenDTO, Koresponden> {



    default Koresponden fromId(Long id) {
        if (id == null) {
            return null;
        }
        Koresponden koresponden = new Koresponden();
        koresponden.setId(id);
        return koresponden;
    }
}
