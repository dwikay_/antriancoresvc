package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.domain.DataAntrian;
import com.antrian.inti.core.domain.Loket;
import com.antrian.inti.core.domain.SubLayanan;
import com.antrian.inti.core.service.RegistrasiService;
import com.antrian.inti.core.domain.Registrasi;
import com.antrian.inti.core.repository.RegistrasiRepository;
import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.*;
import com.antrian.inti.core.service.mapper.RegistrasiMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Registrasi}.
 */
@Service
@Transactional
public class RegistrasiServiceImpl implements RegistrasiService {

    private final Logger log = LoggerFactory.getLogger(RegistrasiServiceImpl.class);

    private final RegistrasiRepository registrasiRepository;

    private final RegistrasiMapper registrasiMapper;

    @Autowired
    SubLayananService subLayananService;

    public RegistrasiServiceImpl(RegistrasiRepository registrasiRepository, RegistrasiMapper registrasiMapper) {
        this.registrasiRepository = registrasiRepository;
        this.registrasiMapper = registrasiMapper;
    }

    /**
     * Save a registrasi.
     *
     * @param registrasiDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RegistrasiDTO save(RegistrasiDTO registrasiDTO) {
        log.debug("Request to save Registrasi : {}", registrasiDTO);
        Registrasi registrasi = registrasiMapper.toEntity(registrasiDTO);
        registrasi = registrasiRepository.save(registrasi);
        return registrasiMapper.toDto(registrasi);
    }

    /**
     * Get all the registrasis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RegistrasiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Registrasis");
        return registrasiRepository.findAll(pageable)
            .map(registrasiMapper::toDto);
    }


    /**
     * Get one registrasi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RegistrasiDTO> findOne(Long id) {
        log.debug("Request to get Registrasi : {}", id);
        return registrasiRepository.findById(id)
            .map(registrasiMapper::toDto);
    }

    /**
     * Delete the registrasi by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Registrasi : {}", id);
        registrasiRepository.deleteById(id);
    }
    /*
        Tambahan
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RegistrasiDTO> checkingRegistrasi(String nik, String email, boolean verifikasiEmail) {
        log.debug("Request to get Registrasi by nik, email, verifikasiEmail: {}", nik, email, verifikasiEmail);
        return registrasiRepository.findOneByNikAndEmailAndVerifikasiEmail(nik,email,verifikasiEmail)
            .map(registrasiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RegistrasiDTO> verifikasi(String kodeVerifikasi) {
        log.debug("Request to get Registrasi by kodeVerifikasi: {}", kodeVerifikasi);
        return registrasiRepository.findOneByKodeVerifikasi(kodeVerifikasi)
            .map(registrasiMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleRegistrasiDTO> checkingVerifikasiEmail(String kodeVerifikasi, boolean verifikasiEmail) {
        log.debug("Request to get Registrasi by kodeVerifikasi, verifikasiEmail: {}",kodeVerifikasi, verifikasiEmail);
        String imageUrl = "/services/antriancoresvc/api/imageKtp/%1$s";
        return registrasiRepository.findOneByKodeVerifikasiAndVerifikasiEmail(kodeVerifikasi,verifikasiEmail)
            .map(registrasiMapper::toDto).map( e -> {

                SimpleRegistrasiDTO simpleRegistrasiDTO = new SimpleRegistrasiDTO();
                simpleRegistrasiDTO.setId(e.getId());
                simpleRegistrasiDTO.setCreateDate(e.getCreateDate());
                simpleRegistrasiDTO.setCreateDateTime(e.getCreateDateTime());
                simpleRegistrasiDTO.setIdDaftar(e.getIdDaftar());
                simpleRegistrasiDTO.setEmail(e.getEmail());
                simpleRegistrasiDTO.setHubunganPerusahaan(e.getHubunganPerusahaan());
                simpleRegistrasiDTO.setNamaPerusahaan(e.getNamaPerusahaan());
                simpleRegistrasiDTO.setTanggalPengambilan(e.getTanggalPengambilan());
                simpleRegistrasiDTO.setPertanyaan(e.getPertanyaan());
                simpleRegistrasiDTO.setTelepon(e.getTelepon());
                simpleRegistrasiDTO.setKodeVerifikasi(e.getKodeVerifikasi());
                simpleRegistrasiDTO.setNamaLengkap(e.getNamaLengkap());
                simpleRegistrasiDTO.setVerifikasiEmail(e.isVerifikasiEmail());
                simpleRegistrasiDTO.setNik(e.getNik());
                simpleRegistrasiDTO.setNamaPerusahaanPendaftar(e.getNamaPerusahaanPendaftar());
                simpleRegistrasiDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleRegistrasiDTO.setIdKloter(e.getIdKloter());
                simpleRegistrasiDTO.setPengambilanStruk(e.isPengambilanStruk());
                simpleRegistrasiDTO.setJamKloter(e.getJamKloter());
                simpleRegistrasiDTO.setWaktuJamKloter(e.getWaktuJamKloter());
                simpleRegistrasiDTO.setModificationDate(e.getModificationDate());
                simpleRegistrasiDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleRegistrasiDTO.setJamLayanan(e.getJamLayanan());
                simpleRegistrasiDTO.setAgent(e.isAgent());
                simpleRegistrasiDTO.setStatusTiket(e.getStatusTiket());
                simpleRegistrasiDTO.setTypeVirtual(e.isTypeVirtual());
                simpleRegistrasiDTO.setUrlKtp(String.format(imageUrl, e.getKodeVerifikasi()));
                return  simpleRegistrasiDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RegistrasiDTO> checkingAvailable(String nik, String email, boolean verifikasiEmail, boolean pengambilanStruk, LocalDate tanggalPengambilan) {
        log.debug("Request to get Registrasi by nik, email, verifikasiEmail, pengambilanStruk, tanggalPengambilan: {}", nik, email, verifikasiEmail, pengambilanStruk, tanggalPengambilan);
        return registrasiRepository.findOneByNikAndEmailAndVerifikasiEmailAndPengambilanStrukAndTanggalPengambilanGreaterThanEqual(nik,email,verifikasiEmail,pengambilanStruk,tanggalPengambilan)
            .map(registrasiMapper::toDto);
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<RegistrasiDTO> checkingKodeKonfirmasi(String kodeKonfirmasi, Long idSubLayanan) {
        log.debug("Request to get Registrasi by kodeKonfirmasi, idSubLayanan: {}",kodeKonfirmasi, idSubLayanan);
        LocalDate dateNow = LocalDate.now();
        return registrasiRepository.findOneByKodeKonfirmasiAndIdSubLayananAndTanggalPengambilan(kodeKonfirmasi,idSubLayanan,dateNow)
            .map(registrasiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleRegistrasiDTO> findSimpleAll(Pageable pageable) {
        log.debug("Request to get all Registrasi");
        String imageUrl = "/services/antriancoresvc/api/imageKtp/%1$s";
        return registrasiRepository.findAll(pageable)
            .map(registrasiMapper::toDto).map( e -> {

                Long idSubs;
                if(e.getIdSubLayanan() == null){
                    idSubs = new Long(12);
                }else{
                    idSubs = e.getIdSubLayanan();
                }

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(idSubs);
                SimpleRegistrasiDTO simpleRegistrasiDTO = new SimpleRegistrasiDTO();
                simpleRegistrasiDTO.setId(e.getId());
                simpleRegistrasiDTO.setCreateDate(e.getCreateDate());
                simpleRegistrasiDTO.setCreateDateTime(e.getCreateDateTime());
                simpleRegistrasiDTO.setIdDaftar(e.getIdDaftar());
                simpleRegistrasiDTO.setEmail(e.getEmail());
                simpleRegistrasiDTO.setHubunganPerusahaan(e.getHubunganPerusahaan());
                simpleRegistrasiDTO.setNamaPerusahaan(e.getNamaPerusahaan());
                simpleRegistrasiDTO.setTanggalPengambilan(e.getTanggalPengambilan());
                simpleRegistrasiDTO.setPertanyaan(e.getPertanyaan());
                simpleRegistrasiDTO.setTelepon(e.getTelepon());
                simpleRegistrasiDTO.setKodeVerifikasi(e.getKodeVerifikasi());
                simpleRegistrasiDTO.setNamaLengkap(e.getNamaLengkap());
                simpleRegistrasiDTO.setVerifikasiEmail(e.isVerifikasiEmail());
                simpleRegistrasiDTO.setNik(e.getNik());
                simpleRegistrasiDTO.setNamaPerusahaanPendaftar(e.getNamaPerusahaanPendaftar());
                simpleRegistrasiDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleRegistrasiDTO.setIdKloter(e.getIdKloter());
                simpleRegistrasiDTO.setPengambilanStruk(e.isPengambilanStruk());
                simpleRegistrasiDTO.setJamKloter(e.getJamKloter());
                simpleRegistrasiDTO.setWaktuJamKloter(e.getWaktuJamKloter());
                simpleRegistrasiDTO.setModificationDate(e.getModificationDate());
                simpleRegistrasiDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleRegistrasiDTO.setUrlKtp(String.format(imageUrl, e.getKodeVerifikasi()));
                simpleRegistrasiDTO.setSimpleSubLayananDTO(simpleSubLayananDTO);
                simpleRegistrasiDTO.setJamLayanan(e.getJamLayanan());
                simpleRegistrasiDTO.setAgent(e.isAgent());
                simpleRegistrasiDTO.setStatusTiket(e.getStatusTiket());
                simpleRegistrasiDTO.setTypeVirtual(e.isTypeVirtual());
                return  simpleRegistrasiDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleRegistrasiDTO> findSimpleAllrangeDate(Pageable pageable, LocalDate startDate, LocalDate endDate) {
        log.debug("Request to get all Registrasi by range ",startDate,endDate);
        String imageUrl = "/services/antriancoresvc/api/imageKtp/%1$s";
        return registrasiRepository.findByTanggalPengambilanBetween(pageable,startDate,endDate)
            .map(registrasiMapper::toDto).map( e -> {

                Long idSubs;
                if(e.getIdSubLayanan() == null){
                    idSubs = new Long(12);
                }else{
                    idSubs = e.getIdSubLayanan();
                }

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(idSubs);
                SimpleRegistrasiDTO simpleRegistrasiDTO = new SimpleRegistrasiDTO();
                simpleRegistrasiDTO.setId(e.getId());
                simpleRegistrasiDTO.setCreateDate(e.getCreateDate());
                simpleRegistrasiDTO.setCreateDateTime(e.getCreateDateTime());
                simpleRegistrasiDTO.setIdDaftar(e.getIdDaftar());
                simpleRegistrasiDTO.setEmail(e.getEmail());
                simpleRegistrasiDTO.setHubunganPerusahaan(e.getHubunganPerusahaan());
                simpleRegistrasiDTO.setNamaPerusahaan(e.getNamaPerusahaan());
                simpleRegistrasiDTO.setTanggalPengambilan(e.getTanggalPengambilan());
                simpleRegistrasiDTO.setPertanyaan(e.getPertanyaan());
                simpleRegistrasiDTO.setTelepon(e.getTelepon());
                simpleRegistrasiDTO.setKodeVerifikasi(e.getKodeVerifikasi());
                simpleRegistrasiDTO.setNamaLengkap(e.getNamaLengkap());
                simpleRegistrasiDTO.setVerifikasiEmail(e.isVerifikasiEmail());
                simpleRegistrasiDTO.setNik(e.getNik());
                simpleRegistrasiDTO.setNamaPerusahaanPendaftar(e.getNamaPerusahaanPendaftar());
                simpleRegistrasiDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleRegistrasiDTO.setIdKloter(e.getIdKloter());
                simpleRegistrasiDTO.setPengambilanStruk(e.isPengambilanStruk());
                simpleRegistrasiDTO.setJamKloter(e.getJamKloter());
                simpleRegistrasiDTO.setWaktuJamKloter(e.getWaktuJamKloter());
                simpleRegistrasiDTO.setModificationDate(e.getModificationDate());
                simpleRegistrasiDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleRegistrasiDTO.setUrlKtp(String.format(imageUrl, e.getKodeVerifikasi()));
                simpleRegistrasiDTO.setSimpleSubLayananDTO(simpleSubLayananDTO);
                simpleRegistrasiDTO.setJamLayanan(e.getJamLayanan());
                simpleRegistrasiDTO.setAgent(e.isAgent());
                simpleRegistrasiDTO.setStatusTiket(e.getStatusTiket());
                simpleRegistrasiDTO.setTypeVirtual(e.isTypeVirtual());
                return  simpleRegistrasiDTO;
            });
    }
    @Override
    @Transactional(readOnly = true)
    public Page<SimpleRegistrasiDTO> findSimpleAllselectByDate(Pageable pageable, LocalDate dateNow) {
        log.debug("Request to get all Registrasi by dateNow ",dateNow);
        String imageUrl = "/services/antriancoresvc/api/imageKtp/%1$s";
        return registrasiRepository.findByTanggalPengambilan(pageable,dateNow)
            .map(registrasiMapper::toDto).map( e -> {

                Long idSubs;
                if(e.getIdSubLayanan() == null){
                    idSubs = new Long(12);
                }else{
                    idSubs = e.getIdSubLayanan();
                }

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(idSubs);
                SimpleRegistrasiDTO simpleRegistrasiDTO = new SimpleRegistrasiDTO();
                simpleRegistrasiDTO.setId(e.getId());
                simpleRegistrasiDTO.setCreateDate(e.getCreateDate());
                simpleRegistrasiDTO.setCreateDateTime(e.getCreateDateTime());
                simpleRegistrasiDTO.setIdDaftar(e.getIdDaftar());
                simpleRegistrasiDTO.setEmail(e.getEmail());
                simpleRegistrasiDTO.setHubunganPerusahaan(e.getHubunganPerusahaan());
                simpleRegistrasiDTO.setNamaPerusahaan(e.getNamaPerusahaan());
                simpleRegistrasiDTO.setTanggalPengambilan(e.getTanggalPengambilan());
                simpleRegistrasiDTO.setPertanyaan(e.getPertanyaan());
                simpleRegistrasiDTO.setTelepon(e.getTelepon());
                simpleRegistrasiDTO.setKodeVerifikasi(e.getKodeVerifikasi());
                simpleRegistrasiDTO.setNamaLengkap(e.getNamaLengkap());
                simpleRegistrasiDTO.setVerifikasiEmail(e.isVerifikasiEmail());
                simpleRegistrasiDTO.setNik(e.getNik());
                simpleRegistrasiDTO.setNamaPerusahaanPendaftar(e.getNamaPerusahaanPendaftar());
                simpleRegistrasiDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleRegistrasiDTO.setIdKloter(e.getIdKloter());
                simpleRegistrasiDTO.setPengambilanStruk(e.isPengambilanStruk());
                simpleRegistrasiDTO.setJamKloter(e.getJamKloter());
                simpleRegistrasiDTO.setWaktuJamKloter(e.getWaktuJamKloter());
                simpleRegistrasiDTO.setModificationDate(e.getModificationDate());
                simpleRegistrasiDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleRegistrasiDTO.setUrlKtp(String.format(imageUrl, e.getKodeVerifikasi()));
                simpleRegistrasiDTO.setSimpleSubLayananDTO(simpleSubLayananDTO);
                simpleRegistrasiDTO.setJamLayanan(e.getJamLayanan());
                simpleRegistrasiDTO.setAgent(e.isAgent());
                simpleRegistrasiDTO.setStatusTiket(e.getStatusTiket());
                simpleRegistrasiDTO.setTypeVirtual(e.isTypeVirtual());
                return  simpleRegistrasiDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public List<HavinRegistrasiDTO> getDate(Long limitQuota) {
        log.debug("Request to get all TempPanggilans");
        List<Registrasi> havingCount = registrasiRepository.havingCountRegistrasi(limitQuota);

        log.info("HavingData " +havingCount.toString());
        if(havingCount.size() > 0){

        }else{

        }
        return null;
    }

    @Override
    public Integer totalRegistrasi(LocalDate dateRegistrasi, Long idSubLayanan) {
        log.debug("Request count all DataAntrian by dateNow and idSubLayanan and statusPanggilan",dateRegistrasi,idSubLayanan);
        return registrasiRepository.countByTanggalPengambilanAndIdSubLayananAndIdKloterNot(dateRegistrasi,idSubLayanan,new Long(0));
    }
    @Override
    public Integer totalRegistrasiWithTypeVirtual(LocalDate dateRegistrasi, Long idSubLayanan, boolean typeVirtual) {
        log.debug("Request count all DataAntrian by dateNow and idSubLayanan and idSubLayanan, typeVirtual : {},{},{}",dateRegistrasi,idSubLayanan,typeVirtual);
        return registrasiRepository.countByTanggalPengambilanAndIdSubLayananAndTypeVirtualAndIdKloterNot(dateRegistrasi,idSubLayanan,typeVirtual,new Long(0));
    }
}
