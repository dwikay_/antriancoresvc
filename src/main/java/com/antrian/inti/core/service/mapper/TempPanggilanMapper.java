package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.TempPanggilanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TempPanggilan} and its DTO {@link TempPanggilanDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TempPanggilanMapper extends EntityMapper<TempPanggilanDTO, TempPanggilan> {



    default TempPanggilan fromId(Long id) {
        if (id == null) {
            return null;
        }
        TempPanggilan tempPanggilan = new TempPanggilan();
        tempPanggilan.setId(id);
        return tempPanggilan;
    }
}
