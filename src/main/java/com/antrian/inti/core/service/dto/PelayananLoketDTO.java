package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.PelayananLoket} entity.
 */
public class PelayananLoketDTO implements Serializable {

    private Long id;

    private Long idLayanan;

    private Long idSubLayanan;

    private Long idLoket;

    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Long lastModificationUserId;

    private ZonedDateTime lastModificationDateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdLayanan() {
        return idLayanan;
    }

    public void setIdLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public Long getIdLoket() {
        return idLoket;
    }

    public void setIdLoket(Long idLoket) {
        this.idLoket = idLoket;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PelayananLoketDTO pelayananLoketDTO = (PelayananLoketDTO) o;
        if (pelayananLoketDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pelayananLoketDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PelayananLoketDTO{" +
            "id=" + getId() +
            ", idLayanan=" + getIdLayanan() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", idLoket=" + getIdLoket() +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            "}";
    }
}
