package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.KuotaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Kuota} and its DTO {@link KuotaDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KuotaMapper extends EntityMapper<KuotaDTO, Kuota> {



    default Kuota fromId(Long id) {
        if (id == null) {
            return null;
        }
        Kuota kuota = new Kuota();
        kuota.setId(id);
        return kuota;
    }
}
