package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.ImageRepositoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImageRepository} and its DTO {@link ImageRepositoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ImageRepositoryMapper extends EntityMapper<ImageRepositoryDTO, ImageRepository> {



    default ImageRepository fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImageRepository imageRepository = new ImageRepository();
        imageRepository.setId(id);
        return imageRepository;
    }
}
