package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.JadwalMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link JadwalMaster} and its DTO {@link JadwalMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JadwalMasterMapper extends EntityMapper<JadwalMasterDTO, JadwalMaster> {



    default JadwalMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        JadwalMaster jadwalMaster = new JadwalMaster();
        jadwalMaster.setId(id);
        return jadwalMaster;
    }
}
