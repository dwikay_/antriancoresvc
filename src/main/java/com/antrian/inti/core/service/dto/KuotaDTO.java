package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Kuota} entity.
 */
public class KuotaDTO implements Serializable {

    private Long id;

    private Long idSubLayanan;

    private String quota;

    private String jenisLayanan;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getJenisLayanan() {
        return jenisLayanan;
    }

    public void setJenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KuotaDTO kuotaDTO = (KuotaDTO) o;
        if (kuotaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kuotaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KuotaDTO{" +
            "id=" + getId() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", quota='" + getQuota() + "'" +
            ", jenisLayanan='" + getJenisLayanan() + "'" +
            "}";
    }
}
