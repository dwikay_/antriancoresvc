package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.RunningTextService;
import com.antrian.inti.core.domain.RunningText;
import com.antrian.inti.core.repository.RunningTextRepository;
import com.antrian.inti.core.service.dto.RunningTextDTO;
import com.antrian.inti.core.service.mapper.RunningTextMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RunningText}.
 */
@Service
@Transactional
public class RunningTextServiceImpl implements RunningTextService {

    private final Logger log = LoggerFactory.getLogger(RunningTextServiceImpl.class);

    private final RunningTextRepository runningTextRepository;

    private final RunningTextMapper runningTextMapper;

    public RunningTextServiceImpl(RunningTextRepository runningTextRepository, RunningTextMapper runningTextMapper) {
        this.runningTextRepository = runningTextRepository;
        this.runningTextMapper = runningTextMapper;
    }

    /**
     * Save a runningText.
     *
     * @param runningTextDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RunningTextDTO save(RunningTextDTO runningTextDTO) {
        log.debug("Request to save RunningText : {}", runningTextDTO);
        RunningText runningText = runningTextMapper.toEntity(runningTextDTO);
        runningText = runningTextRepository.save(runningText);
        return runningTextMapper.toDto(runningText);
    }

    /**
     * Get all the runningTexts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RunningTextDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RunningTexts");
        return runningTextRepository.findAll(pageable)
            .map(runningTextMapper::toDto);
    }


    /**
     * Get one runningText by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RunningTextDTO> findOne(Long id) {
        log.debug("Request to get RunningText : {}", id);
        return runningTextRepository.findById(id)
            .map(runningTextMapper::toDto);
    }

    /**
     * Delete the runningText by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RunningText : {}", id);
        runningTextRepository.deleteById(id);
    }
}
