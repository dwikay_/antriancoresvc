package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.KioskDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Kiosk}.
 */
public interface KioskService {

    /**
     * Save a kiosk.
     *
     * @param kioskDTO the entity to save.
     * @return the persisted entity.
     */
    KioskDTO save(KioskDTO kioskDTO);

    /**
     * Get all the kiosks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KioskDTO> findAll(Pageable pageable);


    /**
     * Get the "id" kiosk.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KioskDTO> findOne(Long id);

    /**
     * Delete the "id" kiosk.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
