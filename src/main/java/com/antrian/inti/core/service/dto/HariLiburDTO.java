package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.HariLibur} entity.
 */
public class HariLiburDTO implements Serializable {

    private Long id;

    private LocalDate tanggal;

    private String keterangan;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HariLiburDTO hariLiburDTO = (HariLiburDTO) o;
        if (hariLiburDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hariLiburDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HariLiburDTO{" +
            "id=" + getId() +
            ", tanggal='" + getTanggal() + "'" +
            ", keterangan='" + getKeterangan() + "'" +
            "}";
    }
}
