package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.HariLiburService;
import com.antrian.inti.core.domain.HariLibur;
import com.antrian.inti.core.repository.HariLiburRepository;
import com.antrian.inti.core.service.dto.HariLiburDTO;
import com.antrian.inti.core.service.mapper.HariLiburMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link HariLibur}.
 */
@Service
@Transactional
public class HariLiburServiceImpl implements HariLiburService {

    private final Logger log = LoggerFactory.getLogger(HariLiburServiceImpl.class);

    private final HariLiburRepository hariLiburRepository;

    private final HariLiburMapper hariLiburMapper;

    public HariLiburServiceImpl(HariLiburRepository hariLiburRepository, HariLiburMapper hariLiburMapper) {
        this.hariLiburRepository = hariLiburRepository;
        this.hariLiburMapper = hariLiburMapper;
    }

    /**
     * Save a hariLibur.
     *
     * @param hariLiburDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HariLiburDTO save(HariLiburDTO hariLiburDTO) {
        log.debug("Request to save HariLibur : {}", hariLiburDTO);
        HariLibur hariLibur = hariLiburMapper.toEntity(hariLiburDTO);
        hariLibur = hariLiburRepository.save(hariLibur);
        return hariLiburMapper.toDto(hariLibur);
    }

    /**
     * Get all the hariLiburs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HariLiburDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HariLiburs");
        return hariLiburRepository.findAll(pageable)
            .map(hariLiburMapper::toDto);
    }


    /**
     * Get one hariLibur by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HariLiburDTO> findOne(Long id) {
        log.debug("Request to get HariLibur : {}", id);
        return hariLiburRepository.findById(id)
            .map(hariLiburMapper::toDto);
    }

    /**
     * Delete the hariLibur by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HariLibur : {}", id);
        hariLiburRepository.deleteById(id);
    }
}
