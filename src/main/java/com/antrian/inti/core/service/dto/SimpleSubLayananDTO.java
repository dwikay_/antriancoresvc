package com.antrian.inti.core.service.dto;

import com.antrian.inti.core.domain.Kiosk;
import com.antrian.inti.core.domain.Lantai;
import com.antrian.inti.core.domain.Layanan;

import javax.persistence.Lob;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

public class SimpleSubLayananDTO implements Serializable {

    private Long id;

    private Long idLayanan;

    private String kodeLayanan;

    private String namaLayanan;

    private Long idLantai;

    private Long kuota;

    private String jamAwal;

    private String jamAkhir;

    private Long durasi;

    @Size(max = 3)
    private String status;

    @Size(max = 5)
    private String prefixSuara;

    private Boolean layananOnline;

    private Long idKiosk;

    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Long lastModificationUserId;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;

    @Lob
    private String sk;

    private Optional<Kiosk> kiosk;

    private Optional<Lantai> lantai;

    private Optional<SimpleLayananDTO> layanan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdLayanan() {
        return idLayanan;
    }

    public void setIdLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
    }

    public String getKodeLayanan() {
        return kodeLayanan;
    }

    public void setKodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }

    public Long getKuota() {
        return kuota;
    }

    public void setKuota(Long kuota) {
        this.kuota = kuota;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public Long getDurasi() {
        return durasi;
    }

    public void setDurasi(Long durasi) {
        this.durasi = durasi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    public Boolean getLayananOnline() {
        return layananOnline;
    }

    public void setLayananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
    }

    public Long getIdKiosk() {
        return idKiosk;
    }

    public void setIdKiosk(Long idKiosk) {
        this.idKiosk = idKiosk;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public Optional<Kiosk> getKiosk() {
        return kiosk;
    }

    public void setKiosk(Optional<Kiosk> kiosk) {
        this.kiosk = kiosk;
    }

    public Optional<Lantai> getLantai() {
        return lantai;
    }

    public void setLantai(Optional<Lantai> lantai) {
        this.lantai = lantai;
    }

    public Optional<SimpleLayananDTO> getLayanan() {
        return layanan;
    }

    public void setLayanan(Optional<SimpleLayananDTO> layanan) {
        this.layanan = layanan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleSubLayananDTO that = (SimpleSubLayananDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(idLayanan, that.idLayanan) &&
            Objects.equals(kodeLayanan, that.kodeLayanan) &&
            Objects.equals(namaLayanan, that.namaLayanan) &&
            Objects.equals(idLantai, that.idLantai) &&
            Objects.equals(kuota, that.kuota) &&
            Objects.equals(jamAwal, that.jamAwal) &&
            Objects.equals(jamAkhir, that.jamAkhir) &&
            Objects.equals(durasi, that.durasi) &&
            Objects.equals(status, that.status) &&
            Objects.equals(prefixSuara, that.prefixSuara) &&
            Objects.equals(layananOnline, that.layananOnline) &&
            Objects.equals(idKiosk, that.idKiosk) &&
            Objects.equals(createUserId, that.createUserId) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(createDateTime, that.createDateTime) &&
            Objects.equals(lastModificationUserId, that.lastModificationUserId) &&
            Objects.equals(lastModificationDate, that.lastModificationDate) &&
            Objects.equals(lastModificationDateTime, that.lastModificationDateTime) &&
            Objects.equals(sk, that.sk) &&
            Objects.equals(kiosk, that.kiosk) &&
            Objects.equals(lantai, that.lantai) &&
            Objects.equals(layanan, that.layanan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idLayanan, kodeLayanan, namaLayanan, idLantai, kuota, jamAwal, jamAkhir, durasi, status, prefixSuara, layananOnline, idKiosk, createUserId, createDate, createDateTime, lastModificationUserId, lastModificationDate, lastModificationDateTime, sk, kiosk, lantai, layanan);
    }

    @Override
    public String toString() {
        return "SimpleSubLayananDTO{" +
            "id=" + id +
            ", idLayanan=" + idLayanan +
            ", kodeLayanan='" + kodeLayanan + '\'' +
            ", namaLayanan='" + namaLayanan + '\'' +
            ", idLantai=" + idLantai +
            ", kuota=" + kuota +
            ", jamAwal='" + jamAwal + '\'' +
            ", jamAkhir='" + jamAkhir + '\'' +
            ", durasi=" + durasi +
            ", status='" + status + '\'' +
            ", prefixSuara='" + prefixSuara + '\'' +
            ", layananOnline=" + layananOnline +
            ", idKiosk=" + idKiosk +
            ", createUserId=" + createUserId +
            ", createDate=" + createDate +
            ", createDateTime=" + createDateTime +
            ", lastModificationUserId=" + lastModificationUserId +
            ", lastModificationDate=" + lastModificationDate +
            ", lastModificationDateTime=" + lastModificationDateTime +
            ", sk='" + sk + '\'' +
            ", kiosk=" + kiosk +
            ", lantai=" + lantai +
            ", layanan=" + layanan +
            '}';
    }
}
