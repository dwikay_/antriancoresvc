package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.PendaftaranDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Pendaftaran}.
 */
public interface PendaftaranService {

    /**
     * Save a pendaftaran.
     *
     * @param pendaftaranDTO the entity to save.
     * @return the persisted entity.
     */
    PendaftaranDTO save(PendaftaranDTO pendaftaranDTO);

    /**
     * Get all the pendaftarans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PendaftaranDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pendaftaran.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PendaftaranDTO> findOne(Long id);

    /**
     * Delete the "id" pendaftaran.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
