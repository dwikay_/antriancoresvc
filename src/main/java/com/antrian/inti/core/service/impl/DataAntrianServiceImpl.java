package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.business.domain.Plasma;
import com.antrian.inti.core.domain.Loket;
import com.antrian.inti.core.domain.SubLayanan;
import com.antrian.inti.core.repository.LoketRepository;
import com.antrian.inti.core.repository.SubLayananRepository;
import com.antrian.inti.core.repository.TempPanggilanRepository;
import com.antrian.inti.core.service.DataAntrianService;
import com.antrian.inti.core.domain.DataAntrian;
import com.antrian.inti.core.repository.DataAntrianRepository;
import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.*;
import com.antrian.inti.core.service.mapper.DataAntrianMapper;
import org.checkerframework.checker.nullness.Opt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link DataAntrian}.
 */
@Service
@Transactional
public class DataAntrianServiceImpl implements DataAntrianService {

    private final Logger log = LoggerFactory.getLogger(DataAntrianServiceImpl.class);

    @Autowired
    SubLayananService subLayananService;
    @Autowired
    SubLayananRepository subLayananRepository;
    @Autowired
    LoketRepository loketRepository;
    @Autowired
    TempPanggilanRepository tempPanggilanRepository;

    private final DataAntrianRepository dataAntrianRepository;

    private final DataAntrianMapper dataAntrianMapper;

    public DataAntrianServiceImpl(DataAntrianRepository dataAntrianRepository, DataAntrianMapper dataAntrianMapper) {
        this.dataAntrianRepository = dataAntrianRepository;
        this.dataAntrianMapper = dataAntrianMapper;
    }

    /**
     * Save a dataAntrian.
     *
     * @param dataAntrianDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DataAntrianDTO save(DataAntrianDTO dataAntrianDTO) {
        log.debug("Request to save DataAntrian : {}", dataAntrianDTO);
        DataAntrian dataAntrian = dataAntrianMapper.toEntity(dataAntrianDTO);
        dataAntrian = dataAntrianRepository.save(dataAntrian);
        return dataAntrianMapper.toDto(dataAntrian);
    }

    /**
     * Get all the dataAntrians.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DataAntrianDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DataAntrians");
        return dataAntrianRepository.findAll(pageable)
            .map(dataAntrianMapper::toDto);
    }


    /**
     * Get one dataAntrian by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> findOne(Long id) {
        log.debug("Request to get DataAntrian : {}", id);
        return dataAntrianRepository.findById(id)
            .map(dataAntrianMapper::toDto);
    }

//    @Override
//    @Transactional(readOnly = true)
//    public Optional<DataAntrianDTO> findOneByIdAndStatusPanggilanAndStatusCurrent(Long idAntrian, boolean statusPanggilan, boolean statusCurrent) {
//        log.debug("Request to get DataAntrian panggilan Berikutnya : {}", idAntrian);
//        return dataAntrianRepository.findByIdAndStatusPanggilanAndStatusCurrent(idAntrian,statusPanggilan,statusCurrent);
//            .map(dataAntrianMapper::toDto);
//    }

    /**
     * Delete the dataAntrian by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DataAntrian : {}", id);
        dataAntrianRepository.deleteById(id);
    }

    /*
        Tambahan
     */

    @Override
    @Transactional(readOnly = true)
    public Optional<LostFoundDTO> foundAndLost(Long idLantai, Long idSubLayanan, Long noUrut) {
        log.debug("Request to get Registrasi by idSubLayanan and tanggalLayanan: {}",idLantai, idSubLayanan, noUrut);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findByLantaiAndIdSubLayananAndNoUrutAndTanggal(idLantai, idSubLayanan,noUrut,tanggal)
            .map(dataAntrianMapper::toDto).map(e -> {

                Optional<Loket> loket = loketRepository.findById(e.getLoket());
                if(loket.isPresent()){
                    Optional<SubLayanan> subLayanan = subLayananRepository.findById(e.getIdSubLayanan());
                    Integer countNow = tempPanggilanRepository.countByIdSubLayananAndNoUrut(e.getIdSubLayanan(), noUrut.toString());
                    LostFoundDTO lostFoundDTO = new LostFoundDTO();
                    lostFoundDTO.setIdLoket(loket.get().getId());
                    lostFoundDTO.setNamaLoket(loket.get().getDeskripsi());
                    lostFoundDTO.setIdSubLayanan(e.getIdSubLayanan());
                    lostFoundDTO.setNamaLayanan(subLayanan.get().getNamaLayanan());
                    lostFoundDTO.setNoUrut(e.getNoUrut());
                    lostFoundDTO.setPrefix(e.getPrefixSuara());
                    lostFoundDTO.setJumlahPanggilan(countNow);

                    return lostFoundDTO;
                }else{
                    Optional<SubLayanan> subLayanan = subLayananRepository.findById(e.getIdSubLayanan());
                    Integer countNow = tempPanggilanRepository.countByIdSubLayananAndNoUrut(e.getIdSubLayanan(), noUrut.toString());
                    LostFoundDTO lostFoundDTO = new LostFoundDTO();
                    lostFoundDTO.setIdLoket(new Long(0));
                    lostFoundDTO.setNamaLoket("-");
                    lostFoundDTO.setIdSubLayanan(e.getIdSubLayanan());
                    lostFoundDTO.setNamaLayanan(subLayanan.get().getNamaLayanan());
                    lostFoundDTO.setNoUrut(e.getNoUrut());
                    lostFoundDTO.setPrefix(e.getPrefixSuara());
                    lostFoundDTO.setJumlahPanggilan(countNow);

                    return lostFoundDTO;
                }
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleDataAntrianDTO> getDataAntrianLast(Long idSubLayanan, LocalDate tanggalLayanan) {
        log.debug("Request to get Registrasi by idSubLayanan and tanggalLayanan: {}",idSubLayanan, tanggalLayanan);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findFirstByIdSubLayananAndTanggalOrderByIdAsc(idSubLayanan,tanggalLayanan)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setDurasi(e.getDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcam(e.getImageWebcam());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;
            });
    }

    @Override
    public Integer getTotalAntrianNow(LocalDate dateNow, Long idSubLayanan) {
        log.debug("Request count all DataAntrian by dateNow and idSubLayanan",dateNow,idSubLayanan);
        return dataAntrianRepository.countByTanggalAndIdSubLayanan(dateNow,idSubLayanan);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleDataAntrianDTO> panggilanBerikutnya(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, boolean statusPanggilan) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by idSubLayanan,statusCurrent,tanggal : {}",idSubLayanan,statusCurrent,tanggal,statusPanggilan);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findFirstByIdSubLayananAndStatusCurrentAndTanggalAndStatusPanggilanOrderByNoUrutAsc(idSubLayanan,statusCurrent,tanggal,statusPanggilan)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setDurasi(e.getDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcam(e.getImageWebcam());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));
                return simpleDataAntrianDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> panggilanBerikutnya2(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, boolean statusPanggilan) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by idSubLayanan,statusCurrent,tanggal : {}",idSubLayanan,statusCurrent,tanggal,statusPanggilan);
        return dataAntrianRepository.findFirstByIdSubLayananAndStatusCurrentAndTanggalAndStatusPanggilanOrderByNoUrutAsc(idSubLayanan,statusCurrent,tanggal,statusPanggilan)
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getPanggilanSebelumnya(Long loket,Long idSubLayanan, LocalDate tanggal) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by idSubLayanan,statusCurrent,tanggal : {}",idSubLayanan,loket,tanggal);
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndTanggalOrderByNoUrutDesc(idSubLayanan,loket,tanggal)
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getByNoUrutAndIdSubLayanan(Long noUrut, Long idSubLayanan) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by noUrut,idSubLayanan : {}",noUrut,idSubLayanan);

        return dataAntrianRepository.findFirstByNoUrutAndIdSubLayananAndTanggal(noUrut,idSubLayanan,LocalDate.now())
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getDataPrev(Long idSubLayanan, Long noUrut) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by noUrut,idSubLayanan : {}",noUrut,idSubLayanan);

        return dataAntrianRepository.findByIdSubLayananAndTanggalAndNoUrutLessThan(idSubLayanan,LocalDate.now(),noUrut)
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleDataAntrianDTO> fastCall(Long idSubLayanan,Long noUrut,boolean statusKonseling, LocalDate tanggal) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by idSubLayanan,statusCurrent,tanggal : {}",idSubLayanan,noUrut,statusKonseling,tanggal);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findByIdSubLayananAndNoUrutAndStatusKonselingAndTanggal(idSubLayanan,noUrut,statusKonseling,tanggal)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setDurasi(e.getDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcam(e.getImageWebcam());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleDataAntrianDTO> panggilanSaatIni(Long loket,Long idSubLayanan, boolean statusCurrent, LocalDate tanggal) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by idSubLayanan,statusCurrent,tanggal : {}",loket,idSubLayanan,statusCurrent,tanggal);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndStatusCurrentAndTanggalOrderByNoUrutAsc(loket,idSubLayanan,statusCurrent,tanggal)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setDurasi(e.getDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcam(e.getImageWebcam());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;

            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleDataAntrianDTO> lastPanggilan(Long loket,Long idSubLayanan, boolean statusCurrent, LocalDate tanggal) {
        log.debug("Request to get DataAntrian Panggilan Berikutnya by idSubLayanan,statusCurrent,tanggal : {}",loket,idSubLayanan,statusCurrent,tanggal);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndStatusCurrentAndTanggalOrderByNoUrutDesc(loket,idSubLayanan,statusCurrent,tanggal)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setDurasi(e.getDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcam(e.getImageWebcam());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;

            });
    }
    @Override
    public Integer totalAntrianSisa(LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan) {
        log.debug("Request count all DataAntrian by dateNow and idSubLayanan and statusPanggilan",dateNow,idSubLayanan,statusPanggilan);
        return dataAntrianRepository.countByTanggalAndIdSubLayananAndStatusPanggilan(dateNow,idSubLayanan,statusPanggilan);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleDataAntrianDTO> antrianSisaList(Pageable pageable, LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan) {
        log.debug("REST request to get a page of DataAntrians by dateNow, idSubLayanan ",dateNow,idSubLayanan,statusPanggilan);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findAllByTanggalAndIdSubLayananAndStatusPanggilanOrderByNoUrutAsc(pageable,dateNow,idSubLayanan,statusPanggilan)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcam(e.getImageWebcam());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;

            });
    }


    @Override
    @Transactional(readOnly = true)
    public Page<SimpleDataAntrianDTO> historyAntrian(Pageable pageable, LocalDate dateNow, Long idSubLayanan, Long idLoket) {
        log.debug("REST request to get a page of DataAntrians by dateNow, idSubLayanan, idLoket ",dateNow,idSubLayanan,idLoket);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findAllByTanggalAndIdSubLayananAndStatusPanggilanAndLoketOrderByNoUrutAsc(pageable,dateNow,idSubLayanan,true,idLoket)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;

            });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleDataAntrianDTO> antreanLayanan(Pageable pageable, LocalDate dateNow, Long idSubLayanan) {
        log.debug("REST request to get a page of DataAntrians-antreanLayanan by dateNow, idSubLayanan ",dateNow,idSubLayanan);
        String imageUrl = "/services/antriancoresvc/api/imageWebcam/%1$s";
        return dataAntrianRepository.findAllByTanggalAndIdSubLayananAndStatusCurrentOrderByNoUrutAsc(pageable,dateNow,idSubLayanan, true)
            .map(dataAntrianMapper::toDto).map(e -> {

                SimpleDataAntrianDTO simpleDataAntrianDTO = new SimpleDataAntrianDTO();
                simpleDataAntrianDTO.setId(e.getId());
                simpleDataAntrianDTO.setTanggal(e.getTanggal());
                simpleDataAntrianDTO.setNoUrut(e.getNoUrut());
                simpleDataAntrianDTO.setStatusPanggilan(e.isStatusPanggilan());
                simpleDataAntrianDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleDataAntrianDTO.setLayanan(e.getLayanan());
                simpleDataAntrianDTO.setLoket(e.getLoket());
                simpleDataAntrianDTO.setPrefixSuara(e.getPrefixSuara());
                simpleDataAntrianDTO.setLantai(e.getLantai());
                simpleDataAntrianDTO.setStatusCurrent(e.isStatusCurrent());
                simpleDataAntrianDTO.setKodeKonfirmasi(e.getKodeKonfirmasi());
                simpleDataAntrianDTO.setLayananOnline(e.isLayananOnline());
                simpleDataAntrianDTO.setSisaDurasi(e.getSisaDurasi());
                simpleDataAntrianDTO.setStatusKonseling(e.isStatusKonseling());
                simpleDataAntrianDTO.setImageWebcamContentType(e.getImageWebcamContentType());
                simpleDataAntrianDTO.setUrlWebcam(String.format(imageUrl, e.getId()));

                return simpleDataAntrianDTO;

            });
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlasmaDTO> findAllPlasma(LocalDate tanggalSekarang, Long idLantai) {
        log.debug("Request to get all TempPanggilans");
        List<SubLayanan> subLayanan = subLayananRepository.findAllByIdLantaiAndStatusOrderByPrefixSuaraAsc(idLantai, "1");
        log.info("sizeenya" + subLayanan.size());

            List<PlasmaDTO> listPlasma = new ArrayList<>();
            for (SubLayanan listShowPlasma : subLayanan) {
                Integer countNow = dataAntrianRepository.countByTanggalAndIdSubLayanan(LocalDate.now(), listShowPlasma.getId());
                PlasmaDTO plasmaDTO = new PlasmaDTO();
                log.info("countRows" + countNow);
                if (countNow > 0) {
                    LocalDate dateNow = LocalDate.now();
                    Optional<DataAntrian> dataAntrianDTO = dataAntrianRepository.findFirstByIdSubLayananAndStatusCurrentAndTanggalAndLantaiOrderByIdDesc(listShowPlasma.getId(), true, LocalDate.now(), idLantai);

                    Integer totalAntrianSisa = dataAntrianRepository.countByTanggalAndIdSubLayananAndStatusPanggilan(dateNow, listShowPlasma.getId(), false);

                    if (dataAntrianDTO.isPresent()) {

                        Optional<Loket> loket = loketRepository.findById(dataAntrianDTO.get().getLoket());
                        if (loket.isPresent()) {

                            plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                            plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                            plasmaDTO.setSisaAntrian(new Long(totalAntrianSisa));
                            plasmaDTO.setIdLoket(dataAntrianDTO.get().getLoket());
                            plasmaDTO.setLoket(loket.get().getDeskripsi());
                            plasmaDTO.setNoUrut(dataAntrianDTO.get().getNoUrut());
                            plasmaDTO.setPrefixSuara(dataAntrianDTO.get().getPrefixSuara());
                        } else {

                            plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                            plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                            plasmaDTO.setSisaAntrian(new Long(totalAntrianSisa));
                            plasmaDTO.setIdLoket(dataAntrianDTO.get().getLoket());
                            plasmaDTO.setLoket("-");
                            plasmaDTO.setNoUrut(dataAntrianDTO.get().getNoUrut());
                            plasmaDTO.setPrefixSuara(dataAntrianDTO.get().getPrefixSuara());
                        }
                    } else {

                        plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                        plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                        plasmaDTO.setSisaAntrian(new Long(totalAntrianSisa));
                        plasmaDTO.setIdLoket(new Long(0));
                        plasmaDTO.setLoket("-");
                        plasmaDTO.setNoUrut(new Long(0));
                        plasmaDTO.setPrefixSuara(listShowPlasma.getPrefixSuara());
                    }
                } else {

                    plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                    plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                    plasmaDTO.setSisaAntrian(new Long(0));
                    plasmaDTO.setIdLoket(new Long(0));
                    plasmaDTO.setLoket("-");
                    plasmaDTO.setNoUrut(new Long(0));
                    plasmaDTO.setPrefixSuara(listShowPlasma.getPrefixSuara());
                }
                listPlasma.add(plasmaDTO);
            }
            return listPlasma;

    }

    @Override
    public void deleteDataAntrian() {
        log.debug("Request to clean DataAntrian : {}", LocalDate.now());
        dataAntrianRepository.deleteAll();
    }


    @Override
    @Transactional(readOnly = true)
    public List<PlasmaDTO> findAllPlasma2(LocalDate tanggalSekarang, Long idLantai) {
        log.debug("Request to get all TempPanggilans");
        List<SubLayanan> subLayanan = subLayananRepository.findAllByIdLantaiAndStatusOrderByPrefixSuaraAsc(idLantai,"1");
        log.info("sizeenya"+subLayanan.size());
        if(subLayanan.size() > 0){
            List<PlasmaDTO> listPlasma = new ArrayList<>();
            for (SubLayanan listShowPlasma:subLayanan){
                Integer countNow = dataAntrianRepository.countByTanggalAndIdSubLayanan(LocalDate.now(), listShowPlasma.getId());
                PlasmaDTO plasmaDTO = new PlasmaDTO();
                log.info("countRow"+countNow);
                if(countNow > 0){
                    LocalDate dateNow = LocalDate.now();
                    Optional<DataAntrian> dataAntrianDTO = dataAntrianRepository.findFirstByIdSubLayananAndStatusCurrentAndTanggalAndLantaiAndStatusKonselingOrderByIdDesc(listShowPlasma.getId(),true,LocalDate.now(), idLantai,true);
                    Integer totalAntrianSisa = dataAntrianRepository.countByTanggalAndIdSubLayananAndStatusPanggilan(dateNow,listShowPlasma.getId(),false);
                    if(dataAntrianDTO.isPresent()){
                        Optional<Loket> loket = loketRepository.findById(dataAntrianDTO.get().getLoket());
                        if(loket.isPresent()){
                            plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                            plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                            plasmaDTO.setSisaAntrian(new Long(totalAntrianSisa));
                            plasmaDTO.setIdLoket(dataAntrianDTO.get().getLoket());
                            plasmaDTO.setLoket(loket.get().getDeskripsi());
                            plasmaDTO.setNoUrut(dataAntrianDTO.get().getNoUrut());
                            plasmaDTO.setPrefixSuara(dataAntrianDTO.get().getPrefixSuara());
                        }else{
                            plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                            plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                            plasmaDTO.setSisaAntrian(new Long(totalAntrianSisa));
                            plasmaDTO.setIdLoket(dataAntrianDTO.get().getLoket());
                            plasmaDTO.setLoket("-");
                            plasmaDTO.setNoUrut(dataAntrianDTO.get().getNoUrut());
                            plasmaDTO.setPrefixSuara(dataAntrianDTO.get().getPrefixSuara());
                        }
                    }else{
                        plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                        plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                        plasmaDTO.setSisaAntrian(new Long(totalAntrianSisa));
                        plasmaDTO.setIdLoket(new Long(0));
                        plasmaDTO.setLoket("-");
                        plasmaDTO.setNoUrut(new Long(0));
                        plasmaDTO.setPrefixSuara(listShowPlasma.getPrefixSuara());
                    }
                }else{
                    plasmaDTO.setNamaLayanan(listShowPlasma.getNamaLayanan());
                    plasmaDTO.setIdSubLayanan(listShowPlasma.getId());
                    plasmaDTO.setSisaAntrian(new Long(0));
                    plasmaDTO.setIdLoket(new Long(0));
                    plasmaDTO.setLoket("-");
                    plasmaDTO.setNoUrut(new Long(0));
                    plasmaDTO.setPrefixSuara(listShowPlasma.getPrefixSuara());
                }

                listPlasma.add(plasmaDTO);
            }
            return listPlasma;
        }else{

        }
        return null;

    }

    //NEW UPDATE MAKASAR

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getNewPanggilanSebelumnya(Long loket,Long idSubLayanan) {
        log.debug("Request to get getNewPanggilanSebelumnya by loket, idSubLayanan: {},{}",idSubLayanan,loket);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndTanggalAndStatusPanggilanAndStatusKonselingAndStatusCurrentOrderByNoUrutAsc(
            loket,idSubLayanan,tanggal,true,true,true
        )
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getNewPanggilanSaatIni(Long loket,Long idSubLayanan) {
        log.debug("Request to get getNewPanggilanSaatIni by loket, idSubLayanan: {},{}",idSubLayanan,loket);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndTanggalAndStatusKonselingOrderByNoUrutAsc(
            loket,idSubLayanan,tanggal,false
        )
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getFastCallBelumDipanggil(Long loket,Long idSubLayanan) {
        log.debug("Request to get getFastCallBelumDipanggil by loket, idSubLayanan: {},{}",idSubLayanan,loket);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndTanggalAndStatusCurrentAndStatusKonseling(
            loket,idSubLayanan,tanggal,true,false
        )
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getFastCallSesudahDipanggil(Long loket,Long idSubLayanan) {
        log.debug("Request to get getFastCallSesudahDipanggil by loket, idSubLayanan: {},{}",idSubLayanan,loket);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndTanggalAndStatusCurrentAndStatusKonseling(
            loket,idSubLayanan,tanggal,true,true
        )
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DataAntrianDTO> getAllDataAntrianPerloket(Long loket, Long idSubLayanan) {
        log.debug("Request to get all getAllDataAntrianPerloket ");
        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findAllByLoketAndIdSubLayananAndTanggalAndStatusCurrent(loket,idSubLayanan,tanggal,true).stream()
            .map(dataAntrianMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getPrevPanggilanSebelumnya(Long loket,Long idSubLayanan) {
        log.debug("Request to get getPrevPanggilanSebelumnya by loket, idSubLayanan: {},{}",idSubLayanan,loket);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByLoketAndIdSubLayananAndTanggalAndStatusKonselingOrderByNoUrutDesc(
            loket,idSubLayanan,tanggal,true
        )
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> getNewFastCall(Long noUrut,Long idSubLayanan) {
        log.debug("Request to get getPrevPanggilanSebelumnya by idSubLayanan: {},{}",idSubLayanan);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByNoUrutAndIdSubLayananAndTanggal(
            noUrut,idSubLayanan,tanggal
        )
            .map(dataAntrianMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DataAntrianDTO> newRfr(Long loket,Long idSubLayanan) {
        log.debug("Request to get getPrevPanggilanSebelumnya by loket, idSubLayanan: {},{}",idSubLayanan,loket);

        LocalDate tanggal = LocalDate.now();
        return dataAntrianRepository.findFirstByIdSubLayananAndLoketAndTanggalAndStatusCurrentAndStatusPanggilanAndStatusKonselingOrderByNoUrutAsc(
            idSubLayanan,loket,tanggal,true,true,false
        )
            .map(dataAntrianMapper::toDto);
    }
}
