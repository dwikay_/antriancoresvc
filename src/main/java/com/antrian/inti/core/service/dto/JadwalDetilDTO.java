package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.JadwalDetil} entity.
 */
public class JadwalDetilDTO implements Serializable {

    private Long id;

    private Long idMaster;

    private String jamAwal;

    private String jamAkhir;

    private String kloter;

    private String jumlahKuota;

    private Boolean virtual;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdMaster() {
        return idMaster;
    }

    public void setIdMaster(Long idMaster) {
        this.idMaster = idMaster;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getKloter() {
        return kloter;
    }

    public void setKloter(String kloter) {
        this.kloter = kloter;
    }

    public String getJumlahKuota() {
        return jumlahKuota;
    }

    public void setJumlahKuota(String jumlahKuota) {
        this.jumlahKuota = jumlahKuota;
    }

    public Boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(Boolean virtual) {
        this.virtual = virtual;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JadwalDetilDTO jadwalDetilDTO = (JadwalDetilDTO) o;
        if (jadwalDetilDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jadwalDetilDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JadwalDetilDTO{" +
            "id=" + getId() +
            ", idMaster=" + getIdMaster() +
            ", jamAwal='" + getJamAwal() + "'" +
            ", jamAkhir='" + getJamAkhir() + "'" +
            ", kloter='" + getKloter() + "'" +
            ", jumlahKuota='" + getJumlahKuota() + "'" +
            ", virtual='" + isVirtual() + "'" +
            "}";
    }
}
