package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.JadwalDetilDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link JadwalDetil} and its DTO {@link JadwalDetilDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JadwalDetilMapper extends EntityMapper<JadwalDetilDTO, JadwalDetil> {



    default JadwalDetil fromId(Long id) {
        if (id == null) {
            return null;
        }
        JadwalDetil jadwalDetil = new JadwalDetil();
        jadwalDetil.setId(id);
        return jadwalDetil;
    }
}
