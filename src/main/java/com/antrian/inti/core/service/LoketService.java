package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.LoketDTO;

import com.antrian.inti.core.service.dto.SimpleLoketDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Loket}.
 */
public interface LoketService {

    /**
     * Save a loket.
     *
     * @param loketDTO the entity to save.
     * @return the persisted entity.
     */
    LoketDTO save(LoketDTO loketDTO);

    /**
     * Get all the lokets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoketDTO> findAll(Pageable pageable);
    Page<SimpleLoketDTO> findSimpleAll(Pageable pageable);


    /**
     * Get the "id" loket.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoketDTO> findOne(Long id);
    Optional<SimpleLoketDTO> findSimpleOne(Long id);
    Optional<SimpleLoketDTO> findSimpleOneIdSubLayanan(Long idSubLayanan);
    Optional<SimpleLoketDTO> findSimpleOneIpController(String ipController);

    /**
     * Delete the "id" loket.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
