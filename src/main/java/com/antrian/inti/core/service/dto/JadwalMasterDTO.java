package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.JadwalMaster} entity.
 */
public class JadwalMasterDTO implements Serializable {

    private Long id;

    private Long idSubLayanan;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JadwalMasterDTO jadwalMasterDTO = (JadwalMasterDTO) o;
        if (jadwalMasterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jadwalMasterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JadwalMasterDTO{" +
            "id=" + getId() +
            ", idSubLayanan=" + getIdSubLayanan() +
            "}";
    }
}
