package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.KloterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Kloter}.
 */
public interface KloterService {

    /**
     * Save a kloter.
     *
     * @param kloterDTO the entity to save.
     * @return the persisted entity.
     */
    KloterDTO save(KloterDTO kloterDTO);

    /**
     * Get all the kloters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KloterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" kloter.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KloterDTO> findOne(Long id);

    /**
     * Delete the "id" kloter.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
