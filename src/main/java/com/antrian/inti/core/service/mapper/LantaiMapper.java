package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.LantaiDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Lantai} and its DTO {@link LantaiDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LantaiMapper extends EntityMapper<LantaiDTO, Lantai> {



    default Lantai fromId(Long id) {
        if (id == null) {
            return null;
        }
        Lantai lantai = new Lantai();
        lantai.setId(id);
        return lantai;
    }
}
