package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Prosedur} entity.
 */
public class ProsedurDTO implements Serializable {

    private Long id;

    @Lob
    private String tataCara;

    private String namaProsedur;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTataCara() {
        return tataCara;
    }

    public void setTataCara(String tataCara) {
        this.tataCara = tataCara;
    }

    public String getNamaProsedur() {
        return namaProsedur;
    }

    public void setNamaProsedur(String namaProsedur) {
        this.namaProsedur = namaProsedur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProsedurDTO prosedurDTO = (ProsedurDTO) o;
        if (prosedurDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), prosedurDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProsedurDTO{" +
            "id=" + getId() +
            ", tataCara='" + getTataCara() + "'" +
            ", namaProsedur='" + getNamaProsedur() + "'" +
            "}";
    }
}
