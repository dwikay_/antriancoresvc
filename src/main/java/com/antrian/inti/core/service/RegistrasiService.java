package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.HavinRegistrasiDTO;
import com.antrian.inti.core.service.dto.RegistrasiDTO;

import com.antrian.inti.core.service.dto.SimpleRegistrasiDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Registrasi}.
 */
public interface RegistrasiService {

    /**
     * Save a registrasi.
     *
     * @param registrasiDTO the entity to save.
     * @return the persisted entity.
     */
    RegistrasiDTO save(RegistrasiDTO registrasiDTO);

    /**
     * Get all the registrasis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RegistrasiDTO> findAll(Pageable pageable);
    Page<SimpleRegistrasiDTO> findSimpleAll(Pageable pageable);
    Page<SimpleRegistrasiDTO> findSimpleAllrangeDate(Pageable pageable, LocalDate startDate, LocalDate endDate);
    Page<SimpleRegistrasiDTO> findSimpleAllselectByDate(Pageable pageable, LocalDate dateNow);
    List<HavinRegistrasiDTO> getDate(Long limitQuota);

    /**
     * Get the "id" registrasi.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RegistrasiDTO> findOne(Long id);

    Optional<RegistrasiDTO> checkingRegistrasi(String nik, String email, boolean verifikasiEmail);
    Optional<SimpleRegistrasiDTO> checkingVerifikasiEmail(String kodeVerifikasi, boolean verifikasiEmail);
    Optional<RegistrasiDTO> verifikasi(String kodeVerifikasi);
    Optional<RegistrasiDTO> checkingKodeKonfirmasi(String kodeKonfirmasi, Long idSubLayanan);
    Optional<RegistrasiDTO> checkingAvailable(String nik, String email, boolean verifikasiEmail, boolean pengambilanStruk, LocalDate tanggalPengambilan);

    /**
     * Delete the "id" registrasi.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Integer totalRegistrasi(LocalDate dateRegistrasi, Long idSubLayanan);
    Integer totalRegistrasiWithTypeVirtual(LocalDate dateRegistrasi, Long idSubLayanan, boolean typeVirtual);


}
