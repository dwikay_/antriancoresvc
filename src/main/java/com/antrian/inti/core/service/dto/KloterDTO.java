package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Kloter} entity.
 */
public class KloterDTO implements Serializable {

    private Long id;

    private String jamMulai;

    private String jamAkhir;

    private String quota;

    private String namaKloter;

    private Long idSubLayanan;

    private LocalDate createdDate;

    private ZonedDateTime createDateTime;

    private Long createUserId;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;

    private Long lastModificationUserId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJamMulai() {
        return jamMulai;
    }

    public void setJamMulai(String jamMulai) {
        this.jamMulai = jamMulai;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getNamaKloter() {
        return namaKloter;
    }

    public void setNamaKloter(String namaKloter) {
        this.namaKloter = namaKloter;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KloterDTO kloterDTO = (KloterDTO) o;
        if (kloterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), kloterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KloterDTO{" +
            "id=" + getId() +
            ", jamMulai='" + getJamMulai() + "'" +
            ", jamAkhir='" + getJamAkhir() + "'" +
            ", quota='" + getQuota() + "'" +
            ", namaKloter='" + getNamaKloter() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", createUserId=" + getCreateUserId() +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            "}";
    }
}
