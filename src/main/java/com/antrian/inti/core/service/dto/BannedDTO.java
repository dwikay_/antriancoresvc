package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Banned} entity.
 */
public class BannedDTO implements Serializable {

    private Long id;

    private String nik;

    private String email;

    private LocalDate tanggal_banned;

    private LocalDate finish_banned;

    @Size(max = 3)
    private String status_banned;

    private LocalDate createSystemDate;

    private LocalDate modificationSystemDate;

    private Integer jumlah;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getTanggal_banned() {
        return tanggal_banned;
    }

    public void setTanggal_banned(LocalDate tanggal_banned) {
        this.tanggal_banned = tanggal_banned;
    }

    public LocalDate getFinish_banned() {
        return finish_banned;
    }

    public void setFinish_banned(LocalDate finish_banned) {
        this.finish_banned = finish_banned;
    }

    public String getStatus_banned() {
        return status_banned;
    }

    public void setStatus_banned(String status_banned) {
        this.status_banned = status_banned;
    }

    public LocalDate getCreateSystemDate() {
        return createSystemDate;
    }

    public void setCreateSystemDate(LocalDate createSystemDate) {
        this.createSystemDate = createSystemDate;
    }

    public LocalDate getModificationSystemDate() {
        return modificationSystemDate;
    }

    public void setModificationSystemDate(LocalDate modificationSystemDate) {
        this.modificationSystemDate = modificationSystemDate;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BannedDTO bannedDTO = (BannedDTO) o;
        if (bannedDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bannedDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BannedDTO{" +
            "id=" + getId() +
            ", nik='" + getNik() + "'" +
            ", email='" + getEmail() + "'" +
            ", tanggal_banned='" + getTanggal_banned() + "'" +
            ", finish_banned='" + getFinish_banned() + "'" +
            ", status_banned='" + getStatus_banned() + "'" +
            ", createSystemDate='" + getCreateSystemDate() + "'" +
            ", modificationSystemDate='" + getModificationSystemDate() + "'" +
            ", jumlah=" + getJumlah() +
            "}";
    }
}
