package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.JadwalDetilDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.JadwalDetil}.
 */
public interface JadwalDetilService {

    /**
     * Save a jadwalDetil.
     *
     * @param jadwalDetilDTO the entity to save.
     * @return the persisted entity.
     */
    JadwalDetilDTO save(JadwalDetilDTO jadwalDetilDTO);

    /**
     * Get all the jadwalDetils.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JadwalDetilDTO> findAll(Pageable pageable);
    List<JadwalDetilDTO> findAllByIdMaster(Long idMaster);
    List<JadwalDetilDTO> findAllByIdMasterAndTipeVirtual(Long idMaster, boolean tipeVirtual);

    Integer countByTipeVirtual(boolean tipeVirtual);

    /**
     * Get the "id" jadwalDetil.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JadwalDetilDTO> findOne(Long id);

    /**
     * Delete the "id" jadwalDetil.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
