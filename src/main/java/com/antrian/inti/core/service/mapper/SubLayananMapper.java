package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.SubLayananDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SubLayanan} and its DTO {@link SubLayananDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubLayananMapper extends EntityMapper<SubLayananDTO, SubLayanan> {



    default SubLayanan fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubLayanan subLayanan = new SubLayanan();
        subLayanan.setId(id);
        return subLayanan;
    }
}
