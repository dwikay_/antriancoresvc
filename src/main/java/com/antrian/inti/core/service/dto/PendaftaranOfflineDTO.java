package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.PendaftaranOffline} entity.
 */
public class PendaftaranOfflineDTO implements Serializable {

    private Long id;

    private String namaPelaku;

    private String email;

    private String nik;

    private String namaPerusahaan;

    private String telepon;

    @Lob
    private String pertanyaan;

    private String noUrut;

    private Long idSubLayanan;

    private String tokenValidasi;

    private LocalDate tanggal;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPelaku() {
        return namaPelaku;
    }

    public void setNamaPelaku(String namaPelaku) {
        this.namaPelaku = namaPelaku;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getTokenValidasi() {
        return tokenValidasi;
    }

    public void setTokenValidasi(String tokenValidasi) {
        this.tokenValidasi = tokenValidasi;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PendaftaranOfflineDTO pendaftaranOfflineDTO = (PendaftaranOfflineDTO) o;
        if (pendaftaranOfflineDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pendaftaranOfflineDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PendaftaranOfflineDTO{" +
            "id=" + getId() +
            ", namaPelaku='" + getNamaPelaku() + "'" +
            ", email='" + getEmail() + "'" +
            ", nik='" + getNik() + "'" +
            ", namaPerusahaan='" + getNamaPerusahaan() + "'" +
            ", telepon='" + getTelepon() + "'" +
            ", pertanyaan='" + getPertanyaan() + "'" +
            ", noUrut='" + getNoUrut() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", tokenValidasi='" + getTokenValidasi() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            "}";
    }
}
