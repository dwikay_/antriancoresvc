package com.antrian.inti.core.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class HavinRegistrasiDTO implements Serializable {

    LocalDate tanggal;
    Long jumlah;

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Long getJumlah() {
        return jumlah;
    }

    public void setJumlah(Long jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HavinRegistrasiDTO that = (HavinRegistrasiDTO) o;
        return Objects.equals(tanggal, that.tanggal) &&
            Objects.equals(jumlah, that.jumlah);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tanggal, jumlah);
    }

    @Override
    public String toString() {
        return "HavinRegistrasiDTO{" +
            "tanggal=" + tanggal +
            ", jumlah=" + jumlah +
            '}';
    }
}
