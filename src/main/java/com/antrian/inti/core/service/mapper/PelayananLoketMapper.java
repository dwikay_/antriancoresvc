package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.PelayananLoketDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PelayananLoket} and its DTO {@link PelayananLoketDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PelayananLoketMapper extends EntityMapper<PelayananLoketDTO, PelayananLoket> {



    default PelayananLoket fromId(Long id) {
        if (id == null) {
            return null;
        }
        PelayananLoket pelayananLoket = new PelayananLoket();
        pelayananLoket.setId(id);
        return pelayananLoket;
    }
}
