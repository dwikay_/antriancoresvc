package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Lantai} entity.
 */
public class LantaiDTO implements Serializable {

    private Long id;

    private Long lantai;

    private String deskripsi;

    @Size(max = 3)
    private String status;

    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Long lastModificationUserId;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLantai() {
        return lantai;
    }

    public void setLantai(Long lantai) {
        this.lantai = lantai;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LantaiDTO lantaiDTO = (LantaiDTO) o;
        if (lantaiDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lantaiDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LantaiDTO{" +
            "id=" + getId() +
            ", lantai=" + getLantai() +
            ", deskripsi='" + getDeskripsi() + "'" +
            ", status='" + getStatus() + "'" +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            "}";
    }
}
