package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.KorespondenService;
import com.antrian.inti.core.domain.Koresponden;
import com.antrian.inti.core.repository.KorespondenRepository;
import com.antrian.inti.core.service.dto.KorespondenDTO;
import com.antrian.inti.core.service.mapper.KorespondenMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Koresponden}.
 */
@Service
@Transactional
public class KorespondenServiceImpl implements KorespondenService {

    private final Logger log = LoggerFactory.getLogger(KorespondenServiceImpl.class);

    private final KorespondenRepository korespondenRepository;

    private final KorespondenMapper korespondenMapper;

    public KorespondenServiceImpl(KorespondenRepository korespondenRepository, KorespondenMapper korespondenMapper) {
        this.korespondenRepository = korespondenRepository;
        this.korespondenMapper = korespondenMapper;
    }

    /**
     * Save a koresponden.
     *
     * @param korespondenDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public KorespondenDTO save(KorespondenDTO korespondenDTO) {
        log.debug("Request to save Koresponden : {}", korespondenDTO);
        Koresponden koresponden = korespondenMapper.toEntity(korespondenDTO);
        koresponden = korespondenRepository.save(koresponden);
        return korespondenMapper.toDto(koresponden);
    }

    /**
     * Get all the korespondens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KorespondenDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Korespondens");
        return korespondenRepository.findAll(pageable)
            .map(korespondenMapper::toDto);
    }


    /**
     * Get one koresponden by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KorespondenDTO> findOne(Long id) {
        log.debug("Request to get Koresponden : {}", id);
        return korespondenRepository.findById(id)
            .map(korespondenMapper::toDto);
    }

    /**
     * Delete the koresponden by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Koresponden : {}", id);
        korespondenRepository.deleteById(id);
    }
    /*
        Tambahan
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KorespondenDTO> findAllRangeDate(Pageable pageable, LocalDate startDate, LocalDate endDate) {
        log.debug("Request to get all Korespondens by date ",startDate,endDate);
        return korespondenRepository.findByCreateDateBetween(pageable,startDate,endDate)
            .map(korespondenMapper::toDto);
    }
}
