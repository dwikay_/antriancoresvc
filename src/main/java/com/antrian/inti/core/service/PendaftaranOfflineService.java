package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.PendaftaranOfflineDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.PendaftaranOffline}.
 */
public interface PendaftaranOfflineService {

    /**
     * Save a pendaftaranOffline.
     *
     * @param pendaftaranOfflineDTO the entity to save.
     * @return the persisted entity.
     */
    PendaftaranOfflineDTO save(PendaftaranOfflineDTO pendaftaranOfflineDTO);

    /**
     * Get all the pendaftaranOfflines.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PendaftaranOfflineDTO> findAll(Pageable pageable);
    Page<PendaftaranOfflineDTO> getByDate(Pageable pageable, LocalDate startDate, LocalDate endDate);


    /**
     * Get the "id" pendaftaranOffline.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PendaftaranOfflineDTO> findOne(Long id);
    Optional<PendaftaranOfflineDTO> validToken(String token);
    Optional<PendaftaranOfflineDTO> getByNikAndEmailAndIdSubLayanan(String nik, String email, Long idSubLayanan);
    Optional<PendaftaranOfflineDTO> getByNoUrutAndIdSubLayanan(String noUrut, Long idSubLayanan);
    /**
     * Delete the "id" pendaftaranOffline.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
