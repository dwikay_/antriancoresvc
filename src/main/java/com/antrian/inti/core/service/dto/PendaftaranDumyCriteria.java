package com.antrian.inti.core.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;
import io.github.jhipster.service.filter.UUIDFilter;

/**
 * Criteria class for the {@link com.antrian.inti.core.domain.PendaftaranDumy} entity. This class is used
 * in {@link com.antrian.inti.core.web.rest.PendaftaranDumyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /pendaftaran-dumies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PendaftaranDumyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter namaBadanUsaha;

    private StringFilter namaDireksi;

    private StringFilter email;

    private LocalDateFilter tanggalPengambilan;

    private StringFilter kodeKonfirmasi;

    private LongFilter idKloter;

    private BooleanFilter pengambilanStruk;

    private StringFilter jamKloter;

    private StringFilter tipeKtp;

    private StringFilter statusTiket;

    private BooleanFilter typeVirtual;

    private StringFilter jamLayanan;

    private StringFilter noTiket;

    private BooleanFilter agent;

    private UUIDFilter idDaftar;

    private StringFilter namaPerseorangan;

    private StringFilter instansi;

    private StringFilter unit;

    private StringFilter jabatan;

    private StringFilter namaPejabat;

    private StringFilter untukLayanan;

    private StringFilter nik;

    private LongFilter flagBanned;

    private LongFilter idSubLayanan;

    private StringFilter jenisKendala;

    private LocalDateFilter createDate;

    public PendaftaranDumyCriteria(){
    }

    public PendaftaranDumyCriteria(PendaftaranDumyCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.namaBadanUsaha = other.namaBadanUsaha == null ? null : other.namaBadanUsaha.copy();
        this.namaDireksi = other.namaDireksi == null ? null : other.namaDireksi.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.tanggalPengambilan = other.tanggalPengambilan == null ? null : other.tanggalPengambilan.copy();
        this.kodeKonfirmasi = other.kodeKonfirmasi == null ? null : other.kodeKonfirmasi.copy();
        this.idKloter = other.idKloter == null ? null : other.idKloter.copy();
        this.pengambilanStruk = other.pengambilanStruk == null ? null : other.pengambilanStruk.copy();
        this.jamKloter = other.jamKloter == null ? null : other.jamKloter.copy();
        this.tipeKtp = other.tipeKtp == null ? null : other.tipeKtp.copy();
        this.statusTiket = other.statusTiket == null ? null : other.statusTiket.copy();
        this.typeVirtual = other.typeVirtual == null ? null : other.typeVirtual.copy();
        this.jamLayanan = other.jamLayanan == null ? null : other.jamLayanan.copy();
        this.noTiket = other.noTiket == null ? null : other.noTiket.copy();
        this.agent = other.agent == null ? null : other.agent.copy();
        this.idDaftar = other.idDaftar == null ? null : other.idDaftar.copy();
        this.namaPerseorangan = other.namaPerseorangan == null ? null : other.namaPerseorangan.copy();
        this.instansi = other.instansi == null ? null : other.instansi.copy();
        this.unit = other.unit == null ? null : other.unit.copy();
        this.jabatan = other.jabatan == null ? null : other.jabatan.copy();
        this.namaPejabat = other.namaPejabat == null ? null : other.namaPejabat.copy();
        this.untukLayanan = other.untukLayanan == null ? null : other.untukLayanan.copy();
        this.nik = other.nik == null ? null : other.nik.copy();
        this.flagBanned = other.flagBanned == null ? null : other.flagBanned.copy();
        this.idSubLayanan = other.idSubLayanan == null ? null : other.idSubLayanan.copy();
        this.jenisKendala = other.jenisKendala == null ? null : other.jenisKendala.copy();
        this.createDate = other.createDate == null ? null : other.createDate.copy();
    }

    @Override
    public PendaftaranDumyCriteria copy() {
        return new PendaftaranDumyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNamaBadanUsaha() {
        return namaBadanUsaha;
    }

    public void setNamaBadanUsaha(StringFilter namaBadanUsaha) {
        this.namaBadanUsaha = namaBadanUsaha;
    }

    public StringFilter getNamaDireksi() {
        return namaDireksi;
    }

    public void setNamaDireksi(StringFilter namaDireksi) {
        this.namaDireksi = namaDireksi;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public LocalDateFilter getTanggalPengambilan() {
        return tanggalPengambilan;
    }

    public void setTanggalPengambilan(LocalDateFilter tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
    }

    public StringFilter getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public void setKodeKonfirmasi(StringFilter kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public LongFilter getIdKloter() {
        return idKloter;
    }

    public void setIdKloter(LongFilter idKloter) {
        this.idKloter = idKloter;
    }

    public BooleanFilter getPengambilanStruk() {
        return pengambilanStruk;
    }

    public void setPengambilanStruk(BooleanFilter pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
    }

    public StringFilter getJamKloter() {
        return jamKloter;
    }

    public void setJamKloter(StringFilter jamKloter) {
        this.jamKloter = jamKloter;
    }

    public StringFilter getTipeKtp() {
        return tipeKtp;
    }

    public void setTipeKtp(StringFilter tipeKtp) {
        this.tipeKtp = tipeKtp;
    }

    public StringFilter getStatusTiket() {
        return statusTiket;
    }

    public void setStatusTiket(StringFilter statusTiket) {
        this.statusTiket = statusTiket;
    }

    public BooleanFilter getTypeVirtual() {
        return typeVirtual;
    }

    public void setTypeVirtual(BooleanFilter typeVirtual) {
        this.typeVirtual = typeVirtual;
    }

    public StringFilter getJamLayanan() {
        return jamLayanan;
    }

    public void setJamLayanan(StringFilter jamLayanan) {
        this.jamLayanan = jamLayanan;
    }

    public StringFilter getNoTiket() {
        return noTiket;
    }

    public void setNoTiket(StringFilter noTiket) {
        this.noTiket = noTiket;
    }

    public BooleanFilter getAgent() {
        return agent;
    }

    public void setAgent(BooleanFilter agent) {
        this.agent = agent;
    }

    public UUIDFilter getIdDaftar() {
        return idDaftar;
    }

    public void setIdDaftar(UUIDFilter idDaftar) {
        this.idDaftar = idDaftar;
    }

    public StringFilter getNamaPerseorangan() {
        return namaPerseorangan;
    }

    public void setNamaPerseorangan(StringFilter namaPerseorangan) {
        this.namaPerseorangan = namaPerseorangan;
    }

    public StringFilter getInstansi() {
        return instansi;
    }

    public void setInstansi(StringFilter instansi) {
        this.instansi = instansi;
    }

    public StringFilter getUnit() {
        return unit;
    }

    public void setUnit(StringFilter unit) {
        this.unit = unit;
    }

    public StringFilter getJabatan() {
        return jabatan;
    }

    public void setJabatan(StringFilter jabatan) {
        this.jabatan = jabatan;
    }

    public StringFilter getNamaPejabat() {
        return namaPejabat;
    }

    public void setNamaPejabat(StringFilter namaPejabat) {
        this.namaPejabat = namaPejabat;
    }

    public StringFilter getUntukLayanan() {
        return untukLayanan;
    }

    public void setUntukLayanan(StringFilter untukLayanan) {
        this.untukLayanan = untukLayanan;
    }

    public StringFilter getNik() {
        return nik;
    }

    public void setNik(StringFilter nik) {
        this.nik = nik;
    }

    public LongFilter getFlagBanned() {
        return flagBanned;
    }

    public void setFlagBanned(LongFilter flagBanned) {
        this.flagBanned = flagBanned;
    }

    public LongFilter getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(LongFilter idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public StringFilter getJenisKendala() {
        return jenisKendala;
    }

    public void setJenisKendala(StringFilter jenisKendala) {
        this.jenisKendala = jenisKendala;
    }

    public LocalDateFilter getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateFilter createDate) {
        this.createDate = createDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PendaftaranDumyCriteria that = (PendaftaranDumyCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(namaBadanUsaha, that.namaBadanUsaha) &&
            Objects.equals(namaDireksi, that.namaDireksi) &&
            Objects.equals(email, that.email) &&
            Objects.equals(tanggalPengambilan, that.tanggalPengambilan) &&
            Objects.equals(kodeKonfirmasi, that.kodeKonfirmasi) &&
            Objects.equals(idKloter, that.idKloter) &&
            Objects.equals(pengambilanStruk, that.pengambilanStruk) &&
            Objects.equals(jamKloter, that.jamKloter) &&
            Objects.equals(tipeKtp, that.tipeKtp) &&
            Objects.equals(statusTiket, that.statusTiket) &&
            Objects.equals(typeVirtual, that.typeVirtual) &&
            Objects.equals(jamLayanan, that.jamLayanan) &&
            Objects.equals(noTiket, that.noTiket) &&
            Objects.equals(agent, that.agent) &&
            Objects.equals(idDaftar, that.idDaftar) &&
            Objects.equals(namaPerseorangan, that.namaPerseorangan) &&
            Objects.equals(instansi, that.instansi) &&
            Objects.equals(unit, that.unit) &&
            Objects.equals(jabatan, that.jabatan) &&
            Objects.equals(namaPejabat, that.namaPejabat) &&
            Objects.equals(untukLayanan, that.untukLayanan) &&
            Objects.equals(nik, that.nik) &&
            Objects.equals(flagBanned, that.flagBanned) &&
            Objects.equals(idSubLayanan, that.idSubLayanan) &&
            Objects.equals(jenisKendala, that.jenisKendala) &&
            Objects.equals(createDate, that.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        namaBadanUsaha,
        namaDireksi,
        email,
        tanggalPengambilan,
        kodeKonfirmasi,
        idKloter,
        pengambilanStruk,
        jamKloter,
        tipeKtp,
        statusTiket,
        typeVirtual,
        jamLayanan,
        noTiket,
        agent,
        idDaftar,
        namaPerseorangan,
        instansi,
        unit,
        jabatan,
        namaPejabat,
        untukLayanan,
        nik,
        flagBanned,
        idSubLayanan,
        jenisKendala,
        createDate
        );
    }

    @Override
    public String toString() {
        return "PendaftaranDumyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (namaBadanUsaha != null ? "namaBadanUsaha=" + namaBadanUsaha + ", " : "") +
                (namaDireksi != null ? "namaDireksi=" + namaDireksi + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (tanggalPengambilan != null ? "tanggalPengambilan=" + tanggalPengambilan + ", " : "") +
                (kodeKonfirmasi != null ? "kodeKonfirmasi=" + kodeKonfirmasi + ", " : "") +
                (idKloter != null ? "idKloter=" + idKloter + ", " : "") +
                (pengambilanStruk != null ? "pengambilanStruk=" + pengambilanStruk + ", " : "") +
                (jamKloter != null ? "jamKloter=" + jamKloter + ", " : "") +
                (tipeKtp != null ? "tipeKtp=" + tipeKtp + ", " : "") +
                (statusTiket != null ? "statusTiket=" + statusTiket + ", " : "") +
                (typeVirtual != null ? "typeVirtual=" + typeVirtual + ", " : "") +
                (jamLayanan != null ? "jamLayanan=" + jamLayanan + ", " : "") +
                (noTiket != null ? "noTiket=" + noTiket + ", " : "") +
                (agent != null ? "agent=" + agent + ", " : "") +
                (idDaftar != null ? "idDaftar=" + idDaftar + ", " : "") +
                (namaPerseorangan != null ? "namaPerseorangan=" + namaPerseorangan + ", " : "") +
                (instansi != null ? "instansi=" + instansi + ", " : "") +
                (unit != null ? "unit=" + unit + ", " : "") +
                (jabatan != null ? "jabatan=" + jabatan + ", " : "") +
                (namaPejabat != null ? "namaPejabat=" + namaPejabat + ", " : "") +
                (untukLayanan != null ? "untukLayanan=" + untukLayanan + ", " : "") +
                (nik != null ? "nik=" + nik + ", " : "") +
                (flagBanned != null ? "flagBanned=" + flagBanned + ", " : "") +
                (idSubLayanan != null ? "idSubLayanan=" + idSubLayanan + ", " : "") +
                (jenisKendala != null ? "jenisKendala=" + jenisKendala + ", " : "") +
                (createDate != null ? "createDate=" + createDate + ", " : "") +
            "}";
    }

}
