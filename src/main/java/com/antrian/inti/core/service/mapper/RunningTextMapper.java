package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.RunningTextDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RunningText} and its DTO {@link RunningTextDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RunningTextMapper extends EntityMapper<RunningTextDTO, RunningText> {



    default RunningText fromId(Long id) {
        if (id == null) {
            return null;
        }
        RunningText runningText = new RunningText();
        runningText.setId(id);
        return runningText;
    }
}
