package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.repository.LantaiRepository;
import com.antrian.inti.core.repository.SubLayananRepository;
import com.antrian.inti.core.service.LantaiService;
import com.antrian.inti.core.service.LayananService;
import com.antrian.inti.core.repository.LayananRepository;
import com.antrian.inti.core.service.dto.*;
import com.antrian.inti.core.service.mapper.LayananMapper;
import com.antrian.inti.core.service.mapper.SimpleLayananMapper;
import com.antrian.inti.core.service.mapper.SubLayananMapper;
import com.netflix.discovery.converters.Auto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Layanan}.
 */
@Service
@Transactional
public class LayananServiceImpl implements LayananService {

    private final Logger log = LoggerFactory.getLogger(LayananServiceImpl.class);

    private final LayananRepository layananRepository;

    private final SubLayananRepository subLayananRepository;

    @Autowired
    LayananService layananService;

    @Autowired
    LantaiRepository lantaiRepository;

    @Autowired
    SimpleLayananMapper simpleLayananMapper;

    private final SubLayananMapper subLayananMapper;

    private final LayananMapper layananMapper;

    @Autowired
    LantaiService lantaiService;

    public LayananServiceImpl(SubLayananMapper subLayananMapper,SubLayananRepository subLayananRepository, LayananRepository layananRepository, LayananMapper layananMapper) {
        this.layananRepository = layananRepository;
        this.layananMapper = layananMapper;
        this.subLayananRepository = subLayananRepository;
        this.subLayananMapper = subLayananMapper;
    }

    /**
     * Save a layanan.
     *
     * @param layananDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LayananDTO save(LayananDTO layananDTO) {
        log.debug("Request to save Layanan : {}", layananDTO);
        Layanan layanan = layananMapper.toEntity(layananDTO);
        layanan = layananRepository.save(layanan);
        return layananMapper.toDto(layanan);
    }

    /**
     * Get all the layanans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LayananDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Layanans");
        return layananRepository.findAll(pageable)
            .map(layananMapper::toDto);
    }


    /**
     * Get one layanan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LayananDTO> findOne(Long id) {
        log.debug("Request to get Layanan : {}", id);
        return layananRepository.findById(id)
            .map(layananMapper::toDto);
    }

    /**
     * Delete the layanan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Layanan : {}", id);
        layananRepository.deleteById(id);
    }

    /*
        Tambahan
     */

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleLayananDTO> findSimpleAll(Pageable pageable) {
        log.debug("Request to get all Layanan");
        String imageUrl = "/services/antriancoresvc/api/imageLayanan/%1$s";
        return layananRepository.findAll(pageable)
            .map(layananMapper::toDto).map( e -> {
                SimpleLayananDTO simpleLayananDTO = new SimpleLayananDTO();
                List<SubLayanan> subLayanan = subLayananRepository.findAllByIdLayananAndStatus(e.getId(),"1");
                Optional<LantaiDTO> lantai = lantaiService.findOne(e.getIdLantai());
                simpleLayananDTO.setId(e.getId());
                simpleLayananDTO.setNamaLayanan(e.getNamaLayanan());
                simpleLayananDTO.setKodeLayanan(e.getKodeLayanan());
                simpleLayananDTO.setCreateUserId(e.getCreateUserId());
                simpleLayananDTO.setCreateDate(e.getCreateDate());
                simpleLayananDTO.setIdLantai(e.getIdLantai());
                simpleLayananDTO.setCreateDateTime(e.getCreateDateTime());
                simpleLayananDTO.setLastModificationUserId(e.getLastModificationUserId());
                simpleLayananDTO.setLastModificationDate(e.getLastModificationDate());
                simpleLayananDTO.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleLayananDTO.setImageLayanan(String.format(imageUrl, e.getId().toString()));
                simpleLayananDTO.setSubLayanan(subLayananMapper.toDto(subLayanan));
                simpleLayananDTO.setLantai(lantai);
                return  simpleLayananDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleLayananDTO> findOneSimple(Long id) {
        log.debug("Request to get Layanan : {}", id);
        String imageUrl = "/services/antriancoresvc/api/imageLayanan/%1$s";
        return layananRepository.findById(id)
            .map(layananMapper::toDto).map(e -> {

                SimpleLayananDTO simpleLayananDTO = new SimpleLayananDTO();
                List<SubLayanan> subLayanan = subLayananRepository.findAllByIdLayanan(e.getId());
                Optional<LantaiDTO> lantai = lantaiService.findOne(e.getIdLantai());
                simpleLayananDTO.setId(e.getId());
                simpleLayananDTO.setNamaLayanan(e.getNamaLayanan());
                simpleLayananDTO.setKodeLayanan(e.getKodeLayanan());
                simpleLayananDTO.setCreateUserId(e.getCreateUserId());
                simpleLayananDTO.setCreateDate(e.getCreateDate());
                simpleLayananDTO.setIdLantai(e.getIdLantai());
                simpleLayananDTO.setCreateDateTime(e.getCreateDateTime());
                simpleLayananDTO.setLastModificationUserId(e.getLastModificationUserId());
                simpleLayananDTO.setLastModificationDate(e.getLastModificationDate());
                simpleLayananDTO.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleLayananDTO.setImageLayanan(String.format(imageUrl, e.getId().toString()));
                simpleLayananDTO.setSubLayanan(subLayananMapper.toDto(subLayanan));
                simpleLayananDTO.setLantai(lantai);
                return  simpleLayananDTO;

            });
    }

    @Override
    @Transactional(readOnly = true)
    public List<SimpleLayananDTO> findSimpleAllByIdLantai(Long idLantai) {
        log.debug("Request to get all Layanan");
        String imageUrl = "/services/antriancoresvc/api/imageLayanan/%1$s";

        List<Layanan> layanan = layananRepository.findAllByIdLantai(idLantai);

        if(layanan.size() > 0){
            List<SimpleLayananDTO> listLayanan = new ArrayList<>();
            for (Layanan listShowLayanan:layanan) {
                SimpleLayananDTO simpleLayananDTO = new SimpleLayananDTO();
                List<SubLayanan> subLayanan = subLayananRepository.findAllByIdLayananAndStatus(listShowLayanan.getId(), "1");
                Optional<LantaiDTO> lantai = lantaiService.findOne(listShowLayanan.getIdLantai());
                simpleLayananDTO.setId(listShowLayanan.getId());
                simpleLayananDTO.setNamaLayanan(listShowLayanan.getNamaLayanan());
                simpleLayananDTO.setKodeLayanan(listShowLayanan.getKodeLayanan());
                simpleLayananDTO.setCreateUserId(listShowLayanan.getCreateUserId());
                simpleLayananDTO.setCreateDate(listShowLayanan.getCreateDate());
                simpleLayananDTO.setIdLantai(listShowLayanan.getIdLantai());
                simpleLayananDTO.setCreateDateTime(listShowLayanan.getCreateDateTime());
                simpleLayananDTO.setLastModificationUserId(listShowLayanan.getLastModificationUserId());
                simpleLayananDTO.setLastModificationDate(listShowLayanan.getLastModificationDate());
                simpleLayananDTO.setLastModificationDateTime(listShowLayanan.getLastModificationDateTime());
                simpleLayananDTO.setImageLayanan(String.format(imageUrl, listShowLayanan.getId().toString()));
                simpleLayananDTO.setSubLayanan(subLayananMapper.toDto(subLayanan));
                simpleLayananDTO.setLantai(lantai);
                listLayanan.add(simpleLayananDTO);
            }
            return listLayanan;
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SimpleLantaiDTO> getLayananByLantai() {
        log.debug("Request to get all Layanan relation with lantai");
        List<Lantai> lantai = lantaiRepository.findAll();
        String imageUrl = "/services/antriancoresvc/api/imageLayanan/%1$s";
        if(lantai.size() > 0){
            List<SimpleLantaiDTO> listLantai = new ArrayList<>();

            for (Lantai listShowLayanan:lantai){

                List<SimpleLayananDTO> layanan = layananService.findSimpleAllByIdLantai(listShowLayanan.getId());

                SimpleLantaiDTO simpleLantaiDTO = new SimpleLantaiDTO();
                simpleLantaiDTO.setId(listShowLayanan.getId());
                simpleLantaiDTO.setLantai(listShowLayanan.getLantai());
                simpleLantaiDTO.setDeskripsi(listShowLayanan.getDeskripsi());
                simpleLantaiDTO.setStatus(listShowLayanan.getStatus());
                simpleLantaiDTO.setCreateDate(listShowLayanan.getCreateDate());
                simpleLantaiDTO.setCreateDateTime(listShowLayanan.getCreateDateTime());
                simpleLantaiDTO.setCreateUserId(listShowLayanan.getCreateUserId());
                simpleLantaiDTO.setLastModificationDate(listShowLayanan.getLastModificationDate());
                simpleLantaiDTO.setLastModificationDateTime(listShowLayanan.getLastModificationDateTime());
                simpleLantaiDTO.setLastModificationUserId(listShowLayanan.getLastModificationUserId());
                simpleLantaiDTO.setLayanan(layanan);

                listLantai.add(simpleLantaiDTO);
           }
            return listLantai;
        }

        return null;
    }

}
