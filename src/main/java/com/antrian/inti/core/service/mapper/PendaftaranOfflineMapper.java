package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.PendaftaranOfflineDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PendaftaranOffline} and its DTO {@link PendaftaranOfflineDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PendaftaranOfflineMapper extends EntityMapper<PendaftaranOfflineDTO, PendaftaranOffline> {



    default PendaftaranOffline fromId(Long id) {
        if (id == null) {
            return null;
        }
        PendaftaranOffline pendaftaranOffline = new PendaftaranOffline();
        pendaftaranOffline.setId(id);
        return pendaftaranOffline;
    }
}
