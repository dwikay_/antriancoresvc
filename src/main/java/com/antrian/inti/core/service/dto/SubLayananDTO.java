package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.SubLayanan} entity.
 */
public class SubLayananDTO implements Serializable {

    private Long id;

    private Long idLayanan;

    private String kodeLayanan;

    private String namaLayanan;

    private Long idLantai;

    private Long kuota;

    private String jamAwal;

    private String jamAkhir;

    private Long durasi;

    @Size(max = 3)
    private String status;

    @Size(max = 5)
    private String prefixSuara;

    private Boolean layananOnline;

    private Long idKiosk;

    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Long lastModificationUserId;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;

    @Lob
    private String sk;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdLayanan() {
        return idLayanan;
    }

    public void setIdLayanan(Long idLayanan) {
        this.idLayanan = idLayanan;
    }

    public String getKodeLayanan() {
        return kodeLayanan;
    }

    public void setKodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }

    public Long getKuota() {
        return kuota;
    }

    public void setKuota(Long kuota) {
        this.kuota = kuota;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public Long getDurasi() {
        return durasi;
    }

    public void setDurasi(Long durasi) {
        this.durasi = durasi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    public Boolean isLayananOnline() {
        return layananOnline;
    }

    public void setLayananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
    }

    public Long getIdKiosk() {
        return idKiosk;
    }

    public void setIdKiosk(Long idKiosk) {
        this.idKiosk = idKiosk;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubLayananDTO subLayananDTO = (SubLayananDTO) o;
        if (subLayananDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subLayananDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubLayananDTO{" +
            "id=" + getId() +
            ", idLayanan=" + getIdLayanan() +
            ", kodeLayanan='" + getKodeLayanan() + "'" +
            ", namaLayanan='" + getNamaLayanan() + "'" +
            ", idLantai=" + getIdLantai() +
            ", kuota=" + getKuota() +
            ", jamAwal='" + getJamAwal() + "'" +
            ", jamAkhir='" + getJamAkhir() + "'" +
            ", durasi=" + getDurasi() +
            ", status='" + getStatus() + "'" +
            ", prefixSuara='" + getPrefixSuara() + "'" +
            ", layananOnline='" + isLayananOnline() + "'" +
            ", idKiosk=" + getIdKiosk() +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            ", sk='" + getSk() + "'" +
            "}";
    }
}
