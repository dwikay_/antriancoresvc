package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.PendaftaranService;
import com.antrian.inti.core.domain.Pendaftaran;
import com.antrian.inti.core.repository.PendaftaranRepository;
import com.antrian.inti.core.service.dto.PendaftaranDTO;
import com.antrian.inti.core.service.mapper.PendaftaranMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Pendaftaran}.
 */
@Service
@Transactional
public class PendaftaranServiceImpl implements PendaftaranService {

    private final Logger log = LoggerFactory.getLogger(PendaftaranServiceImpl.class);

    private final PendaftaranRepository pendaftaranRepository;

    private final PendaftaranMapper pendaftaranMapper;

    public PendaftaranServiceImpl(PendaftaranRepository pendaftaranRepository, PendaftaranMapper pendaftaranMapper) {
        this.pendaftaranRepository = pendaftaranRepository;
        this.pendaftaranMapper = pendaftaranMapper;
    }

    /**
     * Save a pendaftaran.
     *
     * @param pendaftaranDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PendaftaranDTO save(PendaftaranDTO pendaftaranDTO) {
        log.debug("Request to save Pendaftaran : {}", pendaftaranDTO);
        Pendaftaran pendaftaran = pendaftaranMapper.toEntity(pendaftaranDTO);
        pendaftaran = pendaftaranRepository.save(pendaftaran);
        return pendaftaranMapper.toDto(pendaftaran);
    }

    /**
     * Get all the pendaftarans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PendaftaranDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pendaftarans");
        return pendaftaranRepository.findAll(pageable)
            .map(pendaftaranMapper::toDto);
    }


    /**
     * Get one pendaftaran by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PendaftaranDTO> findOne(Long id) {
        log.debug("Request to get Pendaftaran : {}", id);
        return pendaftaranRepository.findById(id)
            .map(pendaftaranMapper::toDto);
    }

    /**
     * Delete the pendaftaran by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pendaftaran : {}", id);
        pendaftaranRepository.deleteById(id);
    }
}
