package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.SimpleSubLayananDTO;
import com.antrian.inti.core.service.dto.SubLayananDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.SubLayanan}.
 */
public interface SubLayananService {

    /**
     * Save a subLayanan.
     *
     * @param subLayananDTO the entity to save.
     * @return the persisted entity.
     */
    SubLayananDTO save(SubLayananDTO subLayananDTO);

    /**
     * Get all the subLayanans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SubLayananDTO> findAll(Pageable pageable);
    Page<SimpleSubLayananDTO> findSimpleAll(Pageable pageable);
    List<SubLayananDTO> getAllByIdLayanan(Long idLayanan);
    List<SubLayananDTO> getLayananOnline(boolean layananOnline);
    List<SubLayananDTO> getLayananOnlineByStatus(boolean layananOnline);
    List<SubLayananDTO> getLayananAktif(String status);
    List<SubLayananDTO> getLayananWithLantai(Long idLantai);
    Page<SimpleSubLayananDTO> getSimpleLayananAktif(Pageable pageable,String status);
    List<SubLayananDTO> findListSimple();


    /**
     * Get the "id" subLayanan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SubLayananDTO> findOne(Long id);
    Optional<SimpleSubLayananDTO> findSimpleOne(Long id);

    /**
     * Delete the "id" subLayanan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
