package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.BannedDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Banned}.
 */
public interface BannedService {

    /**
     * Save a banned.
     *
     * @param bannedDTO the entity to save.
     * @return the persisted entity.
     */
    BannedDTO save(BannedDTO bannedDTO);

    /**
     * Get all the banneds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BannedDTO> findAll(Pageable pageable);
    Page<BannedDTO> getAllBlokirs(Pageable pageable, Integer jumlah);


    /**
     * Get the "id" banned.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BannedDTO> findOne(Long id);
    Optional<BannedDTO> checkingRegistrasi(String nik, Integer jumlah);

    /**
     * Delete the "id" banned.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
