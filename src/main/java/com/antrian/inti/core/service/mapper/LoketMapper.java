package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.LoketDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Loket} and its DTO {@link LoketDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LoketMapper extends EntityMapper<LoketDTO, Loket> {



    default Loket fromId(Long id) {
        if (id == null) {
            return null;
        }
        Loket loket = new Loket();
        loket.setId(id);
        return loket;
    }
}
