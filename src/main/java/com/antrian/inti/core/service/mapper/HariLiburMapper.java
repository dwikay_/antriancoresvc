package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.HariLiburDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link HariLibur} and its DTO {@link HariLiburDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HariLiburMapper extends EntityMapper<HariLiburDTO, HariLibur> {



    default HariLibur fromId(Long id) {
        if (id == null) {
            return null;
        }
        HariLibur hariLibur = new HariLibur();
        hariLibur.setId(id);
        return hariLibur;
    }
}
