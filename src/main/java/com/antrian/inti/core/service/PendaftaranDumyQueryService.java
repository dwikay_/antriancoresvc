package com.antrian.inti.core.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.antrian.inti.core.domain.PendaftaranDumy;
import com.antrian.inti.core.domain.*; // for static metamodels
import com.antrian.inti.core.repository.PendaftaranDumyRepository;
import com.antrian.inti.core.service.dto.PendaftaranDumyCriteria;
import com.antrian.inti.core.service.dto.PendaftaranDumyDTO;
import com.antrian.inti.core.service.mapper.PendaftaranDumyMapper;

/**
 * Service for executing complex queries for {@link PendaftaranDumy} entities in the database.
 * The main input is a {@link PendaftaranDumyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PendaftaranDumyDTO} or a {@link Page} of {@link PendaftaranDumyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PendaftaranDumyQueryService extends QueryService<PendaftaranDumy> {

    private final Logger log = LoggerFactory.getLogger(PendaftaranDumyQueryService.class);

    private final PendaftaranDumyRepository pendaftaranDumyRepository;

    private final PendaftaranDumyMapper pendaftaranDumyMapper;

    public PendaftaranDumyQueryService(PendaftaranDumyRepository pendaftaranDumyRepository, PendaftaranDumyMapper pendaftaranDumyMapper) {
        this.pendaftaranDumyRepository = pendaftaranDumyRepository;
        this.pendaftaranDumyMapper = pendaftaranDumyMapper;
    }

    /**
     * Return a {@link List} of {@link PendaftaranDumyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PendaftaranDumyDTO> findByCriteria(PendaftaranDumyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PendaftaranDumy> specification = createSpecification(criteria);
        return pendaftaranDumyMapper.toDto(pendaftaranDumyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PendaftaranDumyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PendaftaranDumyDTO> findByCriteria(PendaftaranDumyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PendaftaranDumy> specification = createSpecification(criteria);
        return pendaftaranDumyRepository.findAll(specification, page)
            .map(pendaftaranDumyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PendaftaranDumyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PendaftaranDumy> specification = createSpecification(criteria);
        return pendaftaranDumyRepository.count(specification);
    }

    /**
     * Function to convert {@link PendaftaranDumyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PendaftaranDumy> createSpecification(PendaftaranDumyCriteria criteria) {
        Specification<PendaftaranDumy> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PendaftaranDumy_.id));
            }
            if (criteria.getNamaBadanUsaha() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNamaBadanUsaha(), PendaftaranDumy_.namaBadanUsaha));
            }
            if (criteria.getNamaDireksi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNamaDireksi(), PendaftaranDumy_.namaDireksi));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), PendaftaranDumy_.email));
            }
            if (criteria.getTanggalPengambilan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTanggalPengambilan(), PendaftaranDumy_.tanggalPengambilan));
            }
            if (criteria.getKodeKonfirmasi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKodeKonfirmasi(), PendaftaranDumy_.kodeKonfirmasi));
            }
            if (criteria.getIdKloter() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdKloter(), PendaftaranDumy_.idKloter));
            }
            if (criteria.getPengambilanStruk() != null) {
                specification = specification.and(buildSpecification(criteria.getPengambilanStruk(), PendaftaranDumy_.pengambilanStruk));
            }
            if (criteria.getJamKloter() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJamKloter(), PendaftaranDumy_.jamKloter));
            }
            if (criteria.getTipeKtp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipeKtp(), PendaftaranDumy_.tipeKtp));
            }
            if (criteria.getStatusTiket() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatusTiket(), PendaftaranDumy_.statusTiket));
            }
            if (criteria.getTypeVirtual() != null) {
                specification = specification.and(buildSpecification(criteria.getTypeVirtual(), PendaftaranDumy_.typeVirtual));
            }
            if (criteria.getJamLayanan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJamLayanan(), PendaftaranDumy_.jamLayanan));
            }
            if (criteria.getNoTiket() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNoTiket(), PendaftaranDumy_.noTiket));
            }
            if (criteria.getAgent() != null) {
                specification = specification.and(buildSpecification(criteria.getAgent(), PendaftaranDumy_.agent));
            }
            if (criteria.getIdDaftar() != null) {
                specification = specification.and(buildSpecification(criteria.getIdDaftar(), PendaftaranDumy_.idDaftar));
            }
            if (criteria.getNamaPerseorangan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNamaPerseorangan(), PendaftaranDumy_.namaPerseorangan));
            }
            if (criteria.getInstansi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInstansi(), PendaftaranDumy_.instansi));
            }
            if (criteria.getUnit() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUnit(), PendaftaranDumy_.unit));
            }
            if (criteria.getJabatan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJabatan(), PendaftaranDumy_.jabatan));
            }
            if (criteria.getNamaPejabat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNamaPejabat(), PendaftaranDumy_.namaPejabat));
            }
            if (criteria.getUntukLayanan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUntukLayanan(), PendaftaranDumy_.untukLayanan));
            }
            if (criteria.getNik() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNik(), PendaftaranDumy_.nik));
            }
            if (criteria.getFlagBanned() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFlagBanned(), PendaftaranDumy_.flagBanned));
            }
            if (criteria.getIdSubLayanan() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdSubLayanan(), PendaftaranDumy_.idSubLayanan));
            }
            if (criteria.getJenisKendala() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJenisKendala(), PendaftaranDumy_.jenisKendala));
            }
            if (criteria.getCreateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateDate(), PendaftaranDumy_.createDate));
            }
        }
        return specification;
    }
}
