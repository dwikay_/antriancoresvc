package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.KioskDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Kiosk} and its DTO {@link KioskDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KioskMapper extends EntityMapper<KioskDTO, Kiosk> {



    default Kiosk fromId(Long id) {
        if (id == null) {
            return null;
        }
        Kiosk kiosk = new Kiosk();
        kiosk.setId(id);
        return kiosk;
    }
}
