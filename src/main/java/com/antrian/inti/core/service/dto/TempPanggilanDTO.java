package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.TempPanggilan} entity.
 */
public class TempPanggilanDTO implements Serializable {

    private Long id;

    private UUID idTempPanggilan;

    private String panggilan;

    private Boolean status;

    private String noUrut;

    private Long idSubLayanan;

    private Long idLantai;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getIdTempPanggilan() {
        return idTempPanggilan;
    }

    public void setIdTempPanggilan(UUID idTempPanggilan) {
        this.idTempPanggilan = idTempPanggilan;
    }

    public String getPanggilan() {
        return panggilan;
    }

    public void setPanggilan(String panggilan) {
        this.panggilan = panggilan;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TempPanggilanDTO tempPanggilanDTO = (TempPanggilanDTO) o;
        if (tempPanggilanDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tempPanggilanDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TempPanggilanDTO{" +
            "id=" + getId() +
            ", idTempPanggilan='" + getIdTempPanggilan() + "'" +
            ", panggilan='" + getPanggilan() + "'" +
            ", status='" + isStatus() + "'" +
            ", noUrut='" + getNoUrut() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", idLantai=" + getIdLantai() +
            "}";
    }
}
