package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.domain.SubLayanan;
import com.antrian.inti.core.repository.SubLayananRepository;
import com.antrian.inti.core.service.LoketService;
import com.antrian.inti.core.domain.Loket;
import com.antrian.inti.core.repository.LoketRepository;
import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.LoketDTO;
import com.antrian.inti.core.service.dto.SimpleLoketDTO;
import com.antrian.inti.core.service.dto.SubLayananDTO;
import com.antrian.inti.core.service.mapper.LoketMapper;
import com.antrian.inti.core.service.mapper.SubLayananMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Loket}.
 */
@Service
@Transactional
public class LoketServiceImpl implements LoketService {

    private final Logger log = LoggerFactory.getLogger(LoketServiceImpl.class);

    private final LoketRepository loketRepository;

    private final SubLayananRepository subLayananRepository;

    private final SubLayananMapper subLayananMapper;

    private final SubLayananService subLayananService;

    private final LoketMapper loketMapper;

    public LoketServiceImpl(LoketRepository loketRepository, LoketMapper loketMapper,
                            SubLayananRepository subLayananRepository, SubLayananMapper subLayananMapper, SubLayananService subLayananService
    ) {
        this.loketRepository = loketRepository;
        this.loketMapper = loketMapper;
        this.subLayananRepository = subLayananRepository;
        this.subLayananMapper = subLayananMapper;
        this.subLayananService = subLayananService;
    }

    /**
     * Save a loket.
     *
     * @param loketDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LoketDTO save(LoketDTO loketDTO) {
        log.debug("Request to save Loket : {}", loketDTO);
        Loket loket = loketMapper.toEntity(loketDTO);
        loket = loketRepository.save(loket);
        return loketMapper.toDto(loket);
    }

    /**
     * Get all the lokets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoketDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Lokets");
        return loketRepository.findAll(pageable)
            .map(loketMapper::toDto);
    }


    /**
     * Get one loket by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoketDTO> findOne(Long id) {
        log.debug("Request to get Loket : {}", id);
        return loketRepository.findById(id)
            .map(loketMapper::toDto);
    }

    /**
     * Delete the loket by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Loket : {}", id);
        loketRepository.deleteById(id);
    }

    /*
        Tambahan
     */

    @Override
    public Page<SimpleLoketDTO> findSimpleAll(Pageable pageable) {
        log.debug("Request to get all Lokets");
        return loketRepository.findAll(pageable)
            .map(loketMapper::toDto).map(e -> {
                SimpleLoketDTO simpleLoketDTO = new SimpleLoketDTO();
                Optional<SubLayananDTO> subLayanan = subLayananService.findOne(e.getIdSubLayanan());
                simpleLoketDTO.setId(e.getId());
                simpleLoketDTO.setUsername(e.getUsername());
                simpleLoketDTO.setPassword(e.getPassword());
                simpleLoketDTO.setStatus(e.getStatus());
                simpleLoketDTO.setDeskripsi(e.getDeskripsi());
                simpleLoketDTO.setCreateUserId(e.getCreateUserId());
                simpleLoketDTO.setCreateDate(e.getCreateDate());
                simpleLoketDTO.setCreateDateTime(e.getCreateDateTime());
                simpleLoketDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleLoketDTO.setIpController(e.getIpController());
                simpleLoketDTO.setLastModificationUserId(e.getLastModificationUserId());
                simpleLoketDTO.setLastModificationDate(e.getLastModificationDate());
                simpleLoketDTO.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleLoketDTO.setLayanan(subLayanan);

                return simpleLoketDTO;
            });
    }

    @Override
    public Optional<SimpleLoketDTO> findSimpleOne(Long id) {
        log.debug("Request to get all Lokets by id",id);
        return loketRepository.findById(id)
            .map(loketMapper::toDto).map(e -> {
                SimpleLoketDTO simpleLoketDTO = new SimpleLoketDTO();
                Optional<SubLayananDTO> subLayanan = subLayananService.findOne(e.getIdSubLayanan());
                simpleLoketDTO.setId(e.getId());
                simpleLoketDTO.setUsername(e.getUsername());
                simpleLoketDTO.setPassword(e.getPassword());
                simpleLoketDTO.setStatus(e.getStatus());
                simpleLoketDTO.setDeskripsi(e.getDeskripsi());
                simpleLoketDTO.setCreateUserId(e.getCreateUserId());
                simpleLoketDTO.setCreateDate(e.getCreateDate());
                simpleLoketDTO.setCreateDateTime(e.getCreateDateTime());
                simpleLoketDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleLoketDTO.setLastModificationUserId(e.getLastModificationUserId());
                simpleLoketDTO.setLastModificationDate(e.getLastModificationDate());
                simpleLoketDTO.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleLoketDTO.setIpController(e.getIpController());
                simpleLoketDTO.setLayanan(subLayanan);

                return simpleLoketDTO;
            });
    }

    @Override
    public Optional<SimpleLoketDTO> findSimpleOneIdSubLayanan(Long idSubLayanan) {
        log.debug("Request to get all Lokets by idSubLayanan",idSubLayanan);
        return loketRepository.findByIdSubLayanan(idSubLayanan)
            .map(loketMapper::toDto).map(e -> {
                SimpleLoketDTO simpleLoketDTO = new SimpleLoketDTO();
                Optional<SubLayananDTO> subLayanan = subLayananService.findOne(e.getIdSubLayanan());
                simpleLoketDTO.setId(e.getId());
                simpleLoketDTO.setUsername(e.getUsername());
                simpleLoketDTO.setPassword(e.getPassword());
                simpleLoketDTO.setStatus(e.getStatus());
                simpleLoketDTO.setDeskripsi(e.getDeskripsi());
                simpleLoketDTO.setCreateUserId(e.getCreateUserId());
                simpleLoketDTO.setCreateDate(e.getCreateDate());
                simpleLoketDTO.setCreateDateTime(e.getCreateDateTime());
                simpleLoketDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleLoketDTO.setLastModificationUserId(e.getLastModificationUserId());
                simpleLoketDTO.setLastModificationDate(e.getLastModificationDate());
                simpleLoketDTO.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleLoketDTO.setIpController(e.getIpController());
                simpleLoketDTO.setLayanan(subLayanan);

                return simpleLoketDTO;
            });
    }
    @Override
    public Optional<SimpleLoketDTO> findSimpleOneIpController(String ipController) {
        log.debug("Request to get all Lokets by ipController",ipController);
        return loketRepository.findByIpController(ipController)
            .map(loketMapper::toDto).map(e -> {
                SimpleLoketDTO simpleLoketDTO = new SimpleLoketDTO();
                Optional<SubLayananDTO> subLayanan = subLayananService.findOne(e.getIdSubLayanan());
                simpleLoketDTO.setId(e.getId());
                simpleLoketDTO.setUsername(e.getUsername());
                simpleLoketDTO.setPassword(e.getPassword());
                simpleLoketDTO.setStatus(e.getStatus());
                simpleLoketDTO.setDeskripsi(e.getDeskripsi());
                simpleLoketDTO.setCreateUserId(e.getCreateUserId());
                simpleLoketDTO.setCreateDate(e.getCreateDate());
                simpleLoketDTO.setCreateDateTime(e.getCreateDateTime());
                simpleLoketDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleLoketDTO.setLastModificationUserId(e.getLastModificationUserId());
                simpleLoketDTO.setLastModificationDate(e.getLastModificationDate());
                simpleLoketDTO.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleLoketDTO.setIpController(e.getIpController());
                simpleLoketDTO.setLayanan(subLayanan);

                return simpleLoketDTO;
            });
    }
}
