package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.RegistrasiDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Registrasi} and its DTO {@link RegistrasiDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RegistrasiMapper extends EntityMapper<RegistrasiDTO, Registrasi> {



    default Registrasi fromId(Long id) {
        if (id == null) {
            return null;
        }
        Registrasi registrasi = new Registrasi();
        registrasi.setId(id);
        return registrasi;
    }
}
