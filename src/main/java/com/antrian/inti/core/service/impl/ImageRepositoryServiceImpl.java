package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.ImageRepositoryService;
import com.antrian.inti.core.domain.ImageRepository;
import com.antrian.inti.core.repository.ImageRepositoryRepository;
import com.antrian.inti.core.service.dto.ImageRepositoryDTO;
import com.antrian.inti.core.service.mapper.ImageRepositoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ImageRepository}.
 */
@Service
@Transactional
public class ImageRepositoryServiceImpl implements ImageRepositoryService {

    private final Logger log = LoggerFactory.getLogger(ImageRepositoryServiceImpl.class);

    private final ImageRepositoryRepository imageRepositoryRepository;

    private final ImageRepositoryMapper imageRepositoryMapper;

    public ImageRepositoryServiceImpl(ImageRepositoryRepository imageRepositoryRepository, ImageRepositoryMapper imageRepositoryMapper) {
        this.imageRepositoryRepository = imageRepositoryRepository;
        this.imageRepositoryMapper = imageRepositoryMapper;
    }

    /**
     * Save a imageRepository.
     *
     * @param imageRepositoryDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ImageRepositoryDTO save(ImageRepositoryDTO imageRepositoryDTO) {
        log.debug("Request to save ImageRepository : {}", imageRepositoryDTO);
        ImageRepository imageRepository = imageRepositoryMapper.toEntity(imageRepositoryDTO);
        imageRepository = imageRepositoryRepository.save(imageRepository);
        return imageRepositoryMapper.toDto(imageRepository);
    }

    /**
     * Get all the imageRepositories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ImageRepositoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImageRepositories");
        return imageRepositoryRepository.findAll(pageable)
            .map(imageRepositoryMapper::toDto);
    }


    /**
     * Get one imageRepository by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ImageRepositoryDTO> findOne(Long id) {
        log.debug("Request to get ImageRepository : {}", id);
        return imageRepositoryRepository.findById(id)
            .map(imageRepositoryMapper::toDto);
    }

    /**
     * Delete the imageRepository by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ImageRepository : {}", id);
        imageRepositoryRepository.deleteById(id);
    }
}
