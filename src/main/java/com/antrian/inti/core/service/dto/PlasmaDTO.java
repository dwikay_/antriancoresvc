package com.antrian.inti.core.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class PlasmaDTO implements Serializable {

    private String id;
    private String namaLayanan;
    private Long idSubLayanan;
    private Long sisaAntrian;
    private String loket;
    private Long idLoket;
    private Long noUrut;
    private String prefixSuara;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public Long getSisaAntrian() {
        return sisaAntrian;
    }

    public void setSisaAntrian(Long sisaAntrian) {
        this.sisaAntrian = sisaAntrian;
    }

    public String getLoket() {
        return loket;
    }

    public void setLoket(String loket) {
        this.loket = loket;
    }

    public Long getIdLoket() {
        return idLoket;
    }

    public void setIdLoket(Long idLoket) {
        this.idLoket = idLoket;
    }

    public Long getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(Long noUrut) {
        this.noUrut = noUrut;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlasmaDTO plasmaDTO = (PlasmaDTO) o;
        return Objects.equals(id, plasmaDTO.id) &&
            Objects.equals(namaLayanan, plasmaDTO.namaLayanan) &&
            Objects.equals(idSubLayanan, plasmaDTO.idSubLayanan) &&
            Objects.equals(sisaAntrian, plasmaDTO.sisaAntrian) &&
            Objects.equals(loket, plasmaDTO.loket) &&
            Objects.equals(idLoket, plasmaDTO.idLoket) &&
            Objects.equals(noUrut, plasmaDTO.noUrut) &&
            Objects.equals(prefixSuara, plasmaDTO.prefixSuara);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, namaLayanan, idSubLayanan, sisaAntrian, loket, idLoket, noUrut, prefixSuara);
    }

    @Override
    public String toString() {
        return "PlasmaDTO{" +
            "id='" + id + '\'' +
            ", namaLayanan='" + namaLayanan + '\'' +
            ", idSubLayanan=" + idSubLayanan +
            ", sisaAntrian=" + sisaAntrian +
            ", loket='" + loket + '\'' +
            ", idLoket=" + idLoket +
            ", noUrut=" + noUrut +
            ", prefixSuara='" + prefixSuara + '\'' +
            '}';
    }
}
