package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.PendaftaranDumyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PendaftaranDumy} and its DTO {@link PendaftaranDumyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PendaftaranDumyMapper extends EntityMapper<PendaftaranDumyDTO, PendaftaranDumy> {



    default PendaftaranDumy fromId(Long id) {
        if (id == null) {
            return null;
        }
        PendaftaranDumy pendaftaranDumy = new PendaftaranDumy();
        pendaftaranDumy.setId(id);
        return pendaftaranDumy;
    }
}
