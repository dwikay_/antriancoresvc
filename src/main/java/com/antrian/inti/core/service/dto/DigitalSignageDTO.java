package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.DigitalSignage} entity.
 */
public class DigitalSignageDTO implements Serializable {

    private Long id;

    private String namaVideo;

    private String status;

    private String device;

    private String pathFile;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaVideo() {
        return namaVideo;
    }

    public void setNamaVideo(String namaVideo) {
        this.namaVideo = namaVideo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DigitalSignageDTO digitalSignageDTO = (DigitalSignageDTO) o;
        if (digitalSignageDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), digitalSignageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DigitalSignageDTO{" +
            "id=" + getId() +
            ", namaVideo='" + getNamaVideo() + "'" +
            ", status='" + getStatus() + "'" +
            ", device='" + getDevice() + "'" +
            ", pathFile='" + getPathFile() + "'" +
            "}";
    }
}
