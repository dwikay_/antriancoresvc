package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.ProsedurDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Prosedur}.
 */
public interface ProsedurService {

    /**
     * Save a prosedur.
     *
     * @param prosedurDTO the entity to save.
     * @return the persisted entity.
     */
    ProsedurDTO save(ProsedurDTO prosedurDTO);

    /**
     * Get all the prosedurs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProsedurDTO> findAll(Pageable pageable);


    /**
     * Get the "id" prosedur.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProsedurDTO> findOne(Long id);

    /**
     * Delete the "id" prosedur.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
