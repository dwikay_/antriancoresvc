package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.PlasmaDTO;
import com.antrian.inti.core.service.dto.TempPanggilanDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.TempPanggilan}.
 */
public interface TempPanggilanService {

    /**
     * Save a tempPanggilan.
     *
     * @param tempPanggilanDTO the entity to save.
     * @return the persisted entity.
     */
    TempPanggilanDTO save(TempPanggilanDTO tempPanggilanDTO);

    /**
     * Get all the tempPanggilans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TempPanggilanDTO> findAll(Pageable pageable);



    /**
     * Get the "id" tempPanggilan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TempPanggilanDTO> findOne(Long id);
    Optional<TempPanggilanDTO> panggilanBerikutnya(boolean statusPanggilan, Long idLantai);
    Integer jumlahPanggilan(Long idSubLayanan,String noUrut);
    /**
     * Delete the "id" tempPanggilan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    void deleteTempPanggilan();
}
