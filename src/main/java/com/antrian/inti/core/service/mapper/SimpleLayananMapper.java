package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.Layanan;
import com.antrian.inti.core.service.dto.LayananDTO;
import com.antrian.inti.core.service.dto.SimpleLayananDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Layanan} and its DTO {@link SimpleLayananDTO}.
 */

@Mapper(componentModel = "spring", uses = {})
public interface SimpleLayananMapper extends EntityMapper<SimpleLayananDTO, Layanan> {

    default Layanan fromId(Long id) {
        if (id == null) {
            return null;
        }
        Layanan layanan = new Layanan();
        layanan.setId(id);
        return layanan;
    }
}
