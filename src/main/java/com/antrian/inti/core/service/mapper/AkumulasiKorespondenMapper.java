package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.AkumulasiKorespondenDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AkumulasiKoresponden} and its DTO {@link AkumulasiKorespondenDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AkumulasiKorespondenMapper extends EntityMapper<AkumulasiKorespondenDTO, AkumulasiKoresponden> {



    default AkumulasiKoresponden fromId(Long id) {
        if (id == null) {
            return null;
        }
        AkumulasiKoresponden akumulasiKoresponden = new AkumulasiKoresponden();
        akumulasiKoresponden.setId(id);
        return akumulasiKoresponden;
    }
}
