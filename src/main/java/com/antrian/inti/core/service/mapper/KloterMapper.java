package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.KloterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Kloter} and its DTO {@link KloterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KloterMapper extends EntityMapper<KloterDTO, Kloter> {



    default Kloter fromId(Long id) {
        if (id == null) {
            return null;
        }
        Kloter kloter = new Kloter();
        kloter.setId(id);
        return kloter;
    }
}
