package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Registrasi} entity.
 */
public class RegistrasiDTO implements Serializable {

    private Long id;

    private String idDaftar;

    private String email;

    private String hubunganPerusahaan;

    private String namaPerusahaan;

    private LocalDate tanggalPengambilan;

    private String tanggalWaktuPengambilan;

    @Lob
    private String pertanyaan;

    private String telepon;

    @Lob
    private String kodeVerifikasi;

    private String namaLengkap;

    private Boolean verifikasiEmail;

    @Size(max = 16)
    private String nik;

    private String namaPerusahaanPendaftar;

    private String kodeKonfirmasi;

    private Long idKloter;

    private Boolean pengambilanStruk;

    private String jamKloter;

    private String waktuJamKloter;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private LocalDate modificationDate;

    private Long idSubLayanan;

    @Lob
    private byte[] ktp;

    private String ktpContentType;
    @Lob
    private String jawaban;

    private String tipe_ktp;

    private String path_files;

    private String statusTiket;

    private String jamLayanan;

    private String noTiket;

    private Boolean typeVirtual;

    private Boolean agent;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDaftar() {
        return idDaftar;
    }

    public void setIdDaftar(String idDaftar) {
        this.idDaftar = idDaftar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHubunganPerusahaan() {
        return hubunganPerusahaan;
    }

    public void setHubunganPerusahaan(String hubunganPerusahaan) {
        this.hubunganPerusahaan = hubunganPerusahaan;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public LocalDate getTanggalPengambilan() {
        return tanggalPengambilan;
    }

    public void setTanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
    }

    public String getTanggalWaktuPengambilan() {
        return tanggalWaktuPengambilan;
    }

    public void setTanggalWaktuPengambilan(String tanggalWaktuPengambilan) {
        this.tanggalWaktuPengambilan = tanggalWaktuPengambilan;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getKodeVerifikasi() {
        return kodeVerifikasi;
    }

    public void setKodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public Boolean isVerifikasiEmail() {
        return verifikasiEmail;
    }

    public void setVerifikasiEmail(Boolean verifikasiEmail) {
        this.verifikasiEmail = verifikasiEmail;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaPerusahaanPendaftar() {
        return namaPerusahaanPendaftar;
    }

    public void setNamaPerusahaanPendaftar(String namaPerusahaanPendaftar) {
        this.namaPerusahaanPendaftar = namaPerusahaanPendaftar;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public Long getIdKloter() {
        return idKloter;
    }

    public void setIdKloter(Long idKloter) {
        this.idKloter = idKloter;
    }

    public Boolean isPengambilanStruk() {
        return pengambilanStruk;
    }

    public void setPengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
    }

    public String getJamKloter() {
        return jamKloter;
    }

    public void setJamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
    }

    public String getWaktuJamKloter() {
        return waktuJamKloter;
    }

    public void setWaktuJamKloter(String waktuJamKloter) {
        this.waktuJamKloter = waktuJamKloter;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public byte[] getKtp() {
        return ktp;
    }

    public void setKtp(byte[] ktp) {
        this.ktp = ktp;
    }

    public String getKtpContentType() {
        return ktpContentType;
    }

    public void setKtpContentType(String ktpContentType) {
        this.ktpContentType = ktpContentType;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getTipe_ktp() {
        return tipe_ktp;
    }

    public void setTipe_ktp(String tipe_ktp) {
        this.tipe_ktp = tipe_ktp;
    }

    public String getPath_files() {
        return path_files;
    }

    public void setPath_files(String path_files) {
        this.path_files = path_files;
    }

    public String getStatusTiket() {
        return statusTiket;
    }

    public void setStatusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
    }

    public String getJamLayanan() {
        return jamLayanan;
    }

    public void setJamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
    }

    public String getNoTiket() {
        return noTiket;
    }

    public void setNoTiket(String noTiket) {
        this.noTiket = noTiket;
    }

    public Boolean isTypeVirtual() {
        return typeVirtual;
    }

    public void setTypeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
    }

    public Boolean isAgent() {
        return agent;
    }

    public void setAgent(Boolean agent) {
        this.agent = agent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RegistrasiDTO registrasiDTO = (RegistrasiDTO) o;
        if (registrasiDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), registrasiDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RegistrasiDTO{" +
            "id=" + getId() +
            ", idDaftar='" + getIdDaftar() + "'" +
            ", email='" + getEmail() + "'" +
            ", hubunganPerusahaan='" + getHubunganPerusahaan() + "'" +
            ", namaPerusahaan='" + getNamaPerusahaan() + "'" +
            ", tanggalPengambilan='" + getTanggalPengambilan() + "'" +
            ", tanggalWaktuPengambilan='" + getTanggalWaktuPengambilan() + "'" +
            ", pertanyaan='" + getPertanyaan() + "'" +
            ", telepon='" + getTelepon() + "'" +
            ", kodeVerifikasi='" + getKodeVerifikasi() + "'" +
            ", namaLengkap='" + getNamaLengkap() + "'" +
            ", verifikasiEmail='" + isVerifikasiEmail() + "'" +
            ", nik='" + getNik() + "'" +
            ", namaPerusahaanPendaftar='" + getNamaPerusahaanPendaftar() + "'" +
            ", kodeKonfirmasi='" + getKodeKonfirmasi() + "'" +
            ", idKloter=" + getIdKloter() +
            ", pengambilanStruk='" + isPengambilanStruk() + "'" +
            ", jamKloter='" + getJamKloter() + "'" +
            ", waktuJamKloter='" + getWaktuJamKloter() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", modificationDate='" + getModificationDate() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", ktp='" + getKtp() + "'" +
            ", jawaban='" + getJawaban() + "'" +
            ", tipe_ktp='" + getTipe_ktp() + "'" +
            ", path_files='" + getPath_files() + "'" +
            ", statusTiket='" + getStatusTiket() + "'" +
            ", jamLayanan='" + getJamLayanan() + "'" +
            ", noTiket='" + getNoTiket() + "'" +
            ", typeVirtual='" + isTypeVirtual() + "'" +
            ", agent='" + isAgent() + "'" +
            "}";
    }
}
