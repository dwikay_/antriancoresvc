package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.BannedDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Banned} and its DTO {@link BannedDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BannedMapper extends EntityMapper<BannedDTO, Banned> {



    default Banned fromId(Long id) {
        if (id == null) {
            return null;
        }
        Banned banned = new Banned();
        banned.setId(id);
        return banned;
    }
}
