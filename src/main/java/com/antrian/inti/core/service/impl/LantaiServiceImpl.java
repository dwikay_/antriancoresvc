package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.LantaiService;
import com.antrian.inti.core.domain.Lantai;
import com.antrian.inti.core.repository.LantaiRepository;
import com.antrian.inti.core.service.dto.LantaiDTO;
import com.antrian.inti.core.service.mapper.LantaiMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Lantai}.
 */
@Service
@Transactional
public class LantaiServiceImpl implements LantaiService {

    private final Logger log = LoggerFactory.getLogger(LantaiServiceImpl.class);

    private final LantaiRepository lantaiRepository;

    private final LantaiMapper lantaiMapper;

    public LantaiServiceImpl(LantaiRepository lantaiRepository, LantaiMapper lantaiMapper) {
        this.lantaiRepository = lantaiRepository;
        this.lantaiMapper = lantaiMapper;
    }

    /**
     * Save a lantai.
     *
     * @param lantaiDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LantaiDTO save(LantaiDTO lantaiDTO) {
        log.debug("Request to save Lantai : {}", lantaiDTO);
        Lantai lantai = lantaiMapper.toEntity(lantaiDTO);
        lantai = lantaiRepository.save(lantai);
        return lantaiMapper.toDto(lantai);
    }

    /**
     * Get all the lantais.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LantaiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Lantais");
        return lantaiRepository.findAll(pageable)
            .map(lantaiMapper::toDto);
    }


    /**
     * Get one lantai by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LantaiDTO> findOne(Long id) {
        log.debug("Request to get Lantai : {}", id);
        return lantaiRepository.findById(id)
            .map(lantaiMapper::toDto);
    }

    /**
     * Delete the lantai by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Lantai : {}", id);
        lantaiRepository.deleteById(id);
    }
}
