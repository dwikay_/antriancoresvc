package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.KorespondenDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Koresponden}.
 */
public interface KorespondenService {

    /**
     * Save a koresponden.
     *
     * @param korespondenDTO the entity to save.
     * @return the persisted entity.
     */
    KorespondenDTO save(KorespondenDTO korespondenDTO);

    /**
     * Get all the korespondens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KorespondenDTO> findAll(Pageable pageable);
    Page<KorespondenDTO> findAllRangeDate(Pageable pageable, LocalDate startDate,LocalDate endDate);


    /**
     * Get the "id" koresponden.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KorespondenDTO> findOne(Long id);

    /**
     * Delete the "id" koresponden.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
