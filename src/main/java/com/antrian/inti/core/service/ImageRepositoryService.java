package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.ImageRepositoryDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.ImageRepository}.
 */
public interface ImageRepositoryService {

    /**
     * Save a imageRepository.
     *
     * @param imageRepositoryDTO the entity to save.
     * @return the persisted entity.
     */
    ImageRepositoryDTO save(ImageRepositoryDTO imageRepositoryDTO);

    /**
     * Get all the imageRepositories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ImageRepositoryDTO> findAll(Pageable pageable);


    /**
     * Get the "id" imageRepository.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ImageRepositoryDTO> findOne(Long id);

    /**
     * Delete the "id" imageRepository.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
