package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.JadwalMasterDTO;

import com.antrian.inti.core.service.dto.SimpleJadwalDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.JadwalMaster}.
 */
public interface JadwalMasterService {

    /**
     * Save a jadwalMaster.
     *
     * @param jadwalMasterDTO the entity to save.
     * @return the persisted entity.
     */
    JadwalMasterDTO save(JadwalMasterDTO jadwalMasterDTO);

    /**
     * Get all the jadwalMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<JadwalMasterDTO> findAll(Pageable pageable);
    /*
        Tambahan
     */
    Page<SimpleJadwalDTO> findSimpleAll(Pageable pageable);
    Page<SimpleJadwalDTO> findSimpleAllByTipeVirtual(Pageable pageable, boolean tipeVirtual);

    /**
     * Get the "id" jadwalMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JadwalMasterDTO> findOne(Long id);
    /*
        Tambahan
     */
    Optional<SimpleJadwalDTO> findSimpleOne(Long id);
    Optional<SimpleJadwalDTO> findSimpleIdSubLayanan(Long idSubLayanan);
    Optional<SimpleJadwalDTO> findSimpleIdSubLayananAndTipeVirtual(Long idSubLayanan, boolean tipeVirtual);



    Optional<JadwalMasterDTO> findLastJadwalMaster();
    /**
     * Delete the "id" jadwalMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
