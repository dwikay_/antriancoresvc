package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.PendaftaranOfflineService;
import com.antrian.inti.core.domain.PendaftaranOffline;
import com.antrian.inti.core.repository.PendaftaranOfflineRepository;
import com.antrian.inti.core.service.dto.PendaftaranOfflineDTO;
import com.antrian.inti.core.service.mapper.PendaftaranOfflineMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PendaftaranOffline}.
 */
@Service
@Transactional
public class PendaftaranOfflineServiceImpl implements PendaftaranOfflineService {

    private final Logger log = LoggerFactory.getLogger(PendaftaranOfflineServiceImpl.class);

    private final PendaftaranOfflineRepository pendaftaranOfflineRepository;

    private final PendaftaranOfflineMapper pendaftaranOfflineMapper;

    public PendaftaranOfflineServiceImpl(PendaftaranOfflineRepository pendaftaranOfflineRepository, PendaftaranOfflineMapper pendaftaranOfflineMapper) {
        this.pendaftaranOfflineRepository = pendaftaranOfflineRepository;
        this.pendaftaranOfflineMapper = pendaftaranOfflineMapper;
    }

    /**
     * Save a pendaftaranOffline.
     *
     * @param pendaftaranOfflineDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PendaftaranOfflineDTO save(PendaftaranOfflineDTO pendaftaranOfflineDTO) {
        log.debug("Request to save PendaftaranOffline : {}", pendaftaranOfflineDTO);
        PendaftaranOffline pendaftaranOffline = pendaftaranOfflineMapper.toEntity(pendaftaranOfflineDTO);
        pendaftaranOffline = pendaftaranOfflineRepository.save(pendaftaranOffline);
        return pendaftaranOfflineMapper.toDto(pendaftaranOffline);
    }

    /**
     * Get all the pendaftaranOfflines.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PendaftaranOfflineDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PendaftaranOfflines");
        return pendaftaranOfflineRepository.findAll(pageable)
            .map(pendaftaranOfflineMapper::toDto);
    }


    /**
     * Get one pendaftaranOffline by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PendaftaranOfflineDTO> findOne(Long id) {
        log.debug("Request to get PendaftaranOffline : {}", id);
        return pendaftaranOfflineRepository.findById(id)
            .map(pendaftaranOfflineMapper::toDto);
    }

    /**
     * Delete the pendaftaranOffline by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PendaftaranOffline : {}", id);
        pendaftaranOfflineRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PendaftaranOfflineDTO> validToken(String token) {
        log.debug("Request to get PendaftaranOffline by token : {}", token);
        return pendaftaranOfflineRepository.findByTokenValidasi(token)
            .map(pendaftaranOfflineMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PendaftaranOfflineDTO> getByNikAndEmailAndIdSubLayanan(String nik, String email, Long idSubLayanan) {
        log.debug("Request to get PendaftaranOffline by nik,email,idSubLayanan : {}", nik,email,idSubLayanan);
        LocalDate tanggal = LocalDate.now();
        return pendaftaranOfflineRepository.findByNikAndEmailAndIdSubLayananAndTanggal(nik,email,idSubLayanan,tanggal)
            .map(pendaftaranOfflineMapper::toDto);
    }
    @Override
    @Transactional(readOnly = true)
    public Optional<PendaftaranOfflineDTO> getByNoUrutAndIdSubLayanan(String noUrut, Long idSubLayanan) {
        log.debug("Request to get PendaftaranOffline by nik,email : {}", noUrut,idSubLayanan);
        LocalDate tanggal = LocalDate.now();
        return pendaftaranOfflineRepository.findByNoUrutAndIdSubLayananAndTanggal(noUrut,idSubLayanan,tanggal)
            .map(pendaftaranOfflineMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PendaftaranOfflineDTO> getByDate(Pageable pageable, LocalDate startDate, LocalDate endDate) {
        log.debug("Request to get all PendaftaranOfflines by tanggal : {},{}", startDate,endDate);
        return pendaftaranOfflineRepository.findAllByTanggalBetween(pageable,startDate,endDate)
            .map(pendaftaranOfflineMapper::toDto);
    }
}
