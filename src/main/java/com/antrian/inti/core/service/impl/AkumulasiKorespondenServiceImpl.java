package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.AkumulasiKorespondenService;
import com.antrian.inti.core.domain.AkumulasiKoresponden;
import com.antrian.inti.core.repository.AkumulasiKorespondenRepository;
import com.antrian.inti.core.service.dto.AkumulasiKorespondenDTO;
import com.antrian.inti.core.service.mapper.AkumulasiKorespondenMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AkumulasiKoresponden}.
 */
@Service
@Transactional
public class AkumulasiKorespondenServiceImpl implements AkumulasiKorespondenService {

    private final Logger log = LoggerFactory.getLogger(AkumulasiKorespondenServiceImpl.class);

    private final AkumulasiKorespondenRepository akumulasiKorespondenRepository;

    private final AkumulasiKorespondenMapper akumulasiKorespondenMapper;

    public AkumulasiKorespondenServiceImpl(AkumulasiKorespondenRepository akumulasiKorespondenRepository, AkumulasiKorespondenMapper akumulasiKorespondenMapper) {
        this.akumulasiKorespondenRepository = akumulasiKorespondenRepository;
        this.akumulasiKorespondenMapper = akumulasiKorespondenMapper;
    }

    /**
     * Save a akumulasiKoresponden.
     *
     * @param akumulasiKorespondenDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AkumulasiKorespondenDTO save(AkumulasiKorespondenDTO akumulasiKorespondenDTO) {
        log.debug("Request to save AkumulasiKoresponden : {}", akumulasiKorespondenDTO);
        AkumulasiKoresponden akumulasiKoresponden = akumulasiKorespondenMapper.toEntity(akumulasiKorespondenDTO);
        akumulasiKoresponden = akumulasiKorespondenRepository.save(akumulasiKoresponden);
        return akumulasiKorespondenMapper.toDto(akumulasiKoresponden);
    }

    /**
     * Get all the akumulasiKorespondens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AkumulasiKorespondenDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AkumulasiKorespondens");
        return akumulasiKorespondenRepository.findAll(pageable)
            .map(akumulasiKorespondenMapper::toDto);
    }


    /**
     * Get one akumulasiKoresponden by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AkumulasiKorespondenDTO> findOne(Long id) {
        log.debug("Request to get AkumulasiKoresponden : {}", id);
        return akumulasiKorespondenRepository.findById(id)
            .map(akumulasiKorespondenMapper::toDto);
    }

    /**
     * Delete the akumulasiKoresponden by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AkumulasiKoresponden : {}", id);
        akumulasiKorespondenRepository.deleteById(id);
    }
}
