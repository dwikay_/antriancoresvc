package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.RunningTextDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.RunningText}.
 */
public interface RunningTextService {

    /**
     * Save a runningText.
     *
     * @param runningTextDTO the entity to save.
     * @return the persisted entity.
     */
    RunningTextDTO save(RunningTextDTO runningTextDTO);

    /**
     * Get all the runningTexts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RunningTextDTO> findAll(Pageable pageable);


    /**
     * Get the "id" runningText.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RunningTextDTO> findOne(Long id);

    /**
     * Delete the "id" runningText.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
