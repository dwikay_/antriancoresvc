package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.KioskService;
import com.antrian.inti.core.domain.Kiosk;
import com.antrian.inti.core.repository.KioskRepository;
import com.antrian.inti.core.service.dto.KioskDTO;
import com.antrian.inti.core.service.mapper.KioskMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Kiosk}.
 */
@Service
@Transactional
public class KioskServiceImpl implements KioskService {

    private final Logger log = LoggerFactory.getLogger(KioskServiceImpl.class);

    private final KioskRepository kioskRepository;

    private final KioskMapper kioskMapper;

    public KioskServiceImpl(KioskRepository kioskRepository, KioskMapper kioskMapper) {
        this.kioskRepository = kioskRepository;
        this.kioskMapper = kioskMapper;
    }

    /**
     * Save a kiosk.
     *
     * @param kioskDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public KioskDTO save(KioskDTO kioskDTO) {
        log.debug("Request to save Kiosk : {}", kioskDTO);
        Kiosk kiosk = kioskMapper.toEntity(kioskDTO);
        kiosk = kioskRepository.save(kiosk);
        return kioskMapper.toDto(kiosk);
    }

    /**
     * Get all the kiosks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KioskDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Kiosks");
        return kioskRepository.findAll(pageable)
            .map(kioskMapper::toDto);
    }


    /**
     * Get one kiosk by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KioskDTO> findOne(Long id) {
        log.debug("Request to get Kiosk : {}", id);
        return kioskRepository.findById(id)
            .map(kioskMapper::toDto);
    }

    /**
     * Delete the kiosk by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kiosk : {}", id);
        kioskRepository.deleteById(id);
    }
}
