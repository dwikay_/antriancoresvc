package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.PelayananLoketService;
import com.antrian.inti.core.domain.PelayananLoket;
import com.antrian.inti.core.repository.PelayananLoketRepository;
import com.antrian.inti.core.service.dto.PelayananLoketDTO;
import com.antrian.inti.core.service.mapper.PelayananLoketMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PelayananLoket}.
 */
@Service
@Transactional
public class PelayananLoketServiceImpl implements PelayananLoketService {

    private final Logger log = LoggerFactory.getLogger(PelayananLoketServiceImpl.class);

    private final PelayananLoketRepository pelayananLoketRepository;

    private final PelayananLoketMapper pelayananLoketMapper;

    public PelayananLoketServiceImpl(PelayananLoketRepository pelayananLoketRepository, PelayananLoketMapper pelayananLoketMapper) {
        this.pelayananLoketRepository = pelayananLoketRepository;
        this.pelayananLoketMapper = pelayananLoketMapper;
    }

    /**
     * Save a pelayananLoket.
     *
     * @param pelayananLoketDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PelayananLoketDTO save(PelayananLoketDTO pelayananLoketDTO) {
        log.debug("Request to save PelayananLoket : {}", pelayananLoketDTO);
        PelayananLoket pelayananLoket = pelayananLoketMapper.toEntity(pelayananLoketDTO);
        pelayananLoket = pelayananLoketRepository.save(pelayananLoket);
        return pelayananLoketMapper.toDto(pelayananLoket);
    }

    /**
     * Get all the pelayananLokets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PelayananLoketDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PelayananLokets");
        return pelayananLoketRepository.findAll(pageable)
            .map(pelayananLoketMapper::toDto);
    }


    /**
     * Get one pelayananLoket by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PelayananLoketDTO> findOne(Long id) {
        log.debug("Request to get PelayananLoket : {}", id);
        return pelayananLoketRepository.findById(id)
            .map(pelayananLoketMapper::toDto);
    }

    /**
     * Delete the pelayananLoket by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PelayananLoket : {}", id);
        pelayananLoketRepository.deleteById(id);
    }
}
