package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.AkumulasiKorespondenDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.AkumulasiKoresponden}.
 */
public interface AkumulasiKorespondenService {

    /**
     * Save a akumulasiKoresponden.
     *
     * @param akumulasiKorespondenDTO the entity to save.
     * @return the persisted entity.
     */
    AkumulasiKorespondenDTO save(AkumulasiKorespondenDTO akumulasiKorespondenDTO);

    /**
     * Get all the akumulasiKorespondens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AkumulasiKorespondenDTO> findAll(Pageable pageable);


    /**
     * Get the "id" akumulasiKoresponden.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AkumulasiKorespondenDTO> findOne(Long id);

    /**
     * Delete the "id" akumulasiKoresponden.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
