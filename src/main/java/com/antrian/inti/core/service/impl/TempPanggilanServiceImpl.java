package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.TempPanggilanService;
import com.antrian.inti.core.domain.TempPanggilan;
import com.antrian.inti.core.repository.TempPanggilanRepository;
import com.antrian.inti.core.service.dto.PlasmaDTO;
import com.antrian.inti.core.service.dto.TempPanggilanDTO;
import com.antrian.inti.core.service.mapper.TempPanggilanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Implementation for managing {@link TempPanggilan}.
 */
@Service
@Transactional
public class TempPanggilanServiceImpl implements TempPanggilanService {

    private final Logger log = LoggerFactory.getLogger(TempPanggilanServiceImpl.class);

    private final TempPanggilanRepository tempPanggilanRepository;

    private final TempPanggilanMapper tempPanggilanMapper;

    public TempPanggilanServiceImpl(TempPanggilanRepository tempPanggilanRepository, TempPanggilanMapper tempPanggilanMapper) {
        this.tempPanggilanRepository = tempPanggilanRepository;
        this.tempPanggilanMapper = tempPanggilanMapper;
    }

    /**
     * Save a tempPanggilan.
     *
     * @param tempPanggilanDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TempPanggilanDTO save(TempPanggilanDTO tempPanggilanDTO) {
        log.debug("Request to save TempPanggilan : {}", tempPanggilanDTO);
        TempPanggilan tempPanggilan = tempPanggilanMapper.toEntity(tempPanggilanDTO);
        tempPanggilan = tempPanggilanRepository.save(tempPanggilan);
        return tempPanggilanMapper.toDto(tempPanggilan);
    }

    /**
     * Get all the tempPanggilans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TempPanggilanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TempPanggilans");
        return tempPanggilanRepository.findAll(pageable)
            .map(tempPanggilanMapper::toDto);
    }


    /**
     * Get one tempPanggilan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TempPanggilanDTO> findOne(Long id) {
        log.debug("Request to get TempPanggilan : {}", id);
        return tempPanggilanRepository.findById(id)
            .map(tempPanggilanMapper::toDto);
    }

    /**
     * Delete the tempPanggilan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TempPanggilan : {}", id);
        tempPanggilanRepository.deleteById(id);
    }
    /*
        Tambahan
     */

    @Override
    @Transactional(readOnly = true)
    public Optional<TempPanggilanDTO> panggilanBerikutnya(boolean statusPanggilan, Long idLantai) {
        log.debug("Request to get panggilanBerikutnya : {}", statusPanggilan,idLantai);
        return tempPanggilanRepository.findFirstByStatusAndIdLantaiOrderByIdAsc(statusPanggilan,idLantai)
            .map(tempPanggilanMapper::toDto);
    }

    @Override
    public Integer jumlahPanggilan(Long idSubLayanan,String noUrut) {
        log.debug("Request count all jumlahPanggilan by idSubLayanan and noUrut ",idSubLayanan,noUrut);
        return tempPanggilanRepository.countByIdSubLayananAndNoUrut(idSubLayanan,noUrut);
    }

    public void deleteTempPanggilan() {
        log.debug("Request to delete all TempPanggilan at ", LocalDate.now());
        tempPanggilanRepository.deleteAll();
    }
}
