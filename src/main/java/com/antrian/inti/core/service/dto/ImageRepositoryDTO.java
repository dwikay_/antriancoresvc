package com.antrian.inti.core.service.dto;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.ImageRepository} entity.
 */
public class ImageRepositoryDTO implements Serializable {

    private Long id;

    private String namaFile;

    private Long relasiId;

    private Long idSubLayanan;

    @Lob
    private String document;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public Long getRelasiId() {
        return relasiId;
    }

    public void setRelasiId(Long relasiId) {
        this.relasiId = relasiId;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImageRepositoryDTO imageRepositoryDTO = (ImageRepositoryDTO) o;
        if (imageRepositoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), imageRepositoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ImageRepositoryDTO{" +
            "id=" + getId() +
            ", namaFile='" + getNamaFile() + "'" +
            ", relasiId=" + getRelasiId() +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", document='" + getDocument() + "'" +
            "}";
    }
}
