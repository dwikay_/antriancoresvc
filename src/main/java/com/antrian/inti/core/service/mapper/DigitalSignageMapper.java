package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.DigitalSignageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DigitalSignage} and its DTO {@link DigitalSignageDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DigitalSignageMapper extends EntityMapper<DigitalSignageDTO, DigitalSignage> {



    default DigitalSignage fromId(Long id) {
        if (id == null) {
            return null;
        }
        DigitalSignage digitalSignage = new DigitalSignage();
        digitalSignage.setId(id);
        return digitalSignage;
    }
}
