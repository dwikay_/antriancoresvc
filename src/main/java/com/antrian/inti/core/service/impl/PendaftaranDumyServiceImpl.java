package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.PendaftaranDumyService;
import com.antrian.inti.core.domain.PendaftaranDumy;
import com.antrian.inti.core.repository.PendaftaranDumyRepository;
import com.antrian.inti.core.service.dto.PendaftaranDumyDTO;
import com.antrian.inti.core.service.mapper.PendaftaranDumyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PendaftaranDumy}.
 */
@Service
@Transactional
public class PendaftaranDumyServiceImpl implements PendaftaranDumyService {

    private final Logger log = LoggerFactory.getLogger(PendaftaranDumyServiceImpl.class);

    private final PendaftaranDumyRepository pendaftaranDumyRepository;

    private final PendaftaranDumyMapper pendaftaranDumyMapper;

    public PendaftaranDumyServiceImpl(PendaftaranDumyRepository pendaftaranDumyRepository, PendaftaranDumyMapper pendaftaranDumyMapper) {
        this.pendaftaranDumyRepository = pendaftaranDumyRepository;
        this.pendaftaranDumyMapper = pendaftaranDumyMapper;
    }

    /**
     * Save a pendaftaranDumy.
     *
     * @param pendaftaranDumyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PendaftaranDumyDTO save(PendaftaranDumyDTO pendaftaranDumyDTO) {
        log.debug("Request to save PendaftaranDumy : {}", pendaftaranDumyDTO);
        PendaftaranDumy pendaftaranDumy = pendaftaranDumyMapper.toEntity(pendaftaranDumyDTO);
        pendaftaranDumy = pendaftaranDumyRepository.save(pendaftaranDumy);
        return pendaftaranDumyMapper.toDto(pendaftaranDumy);
    }

    /**
     * Get all the pendaftaranDumies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PendaftaranDumyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PendaftaranDumies");
        return pendaftaranDumyRepository.findAll(pageable)
            .map(pendaftaranDumyMapper::toDto);
    }


    /**
     * Get one pendaftaranDumy by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PendaftaranDumyDTO> findOne(Long id) {
        log.debug("Request to get PendaftaranDumy : {}", id);
        return pendaftaranDumyRepository.findById(id)
            .map(pendaftaranDumyMapper::toDto);
    }

    /**
     * Delete the pendaftaranDumy by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PendaftaranDumy : {}", id);
        pendaftaranDumyRepository.deleteById(id);
    }
}
