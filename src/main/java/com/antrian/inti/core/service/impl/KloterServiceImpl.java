package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.domain.SubLayanan;
import com.antrian.inti.core.repository.JadwalDetilRepository;
import com.antrian.inti.core.repository.SubLayananRepository;
import com.antrian.inti.core.service.KloterService;
import com.antrian.inti.core.domain.Kloter;
import com.antrian.inti.core.repository.KloterRepository;
import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.JadwalDetilDTO;
import com.antrian.inti.core.service.dto.KloterDTO;
import com.antrian.inti.core.service.dto.SimpleJadwalDTO;
import com.antrian.inti.core.service.dto.SimpleSubLayananDTO;
import com.antrian.inti.core.service.mapper.KloterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Kloter}.
 */
@Service
@Transactional
public class KloterServiceImpl implements KloterService {

    private final Logger log = LoggerFactory.getLogger(KloterServiceImpl.class);

    private final KloterRepository kloterRepository;

    private final KloterMapper kloterMapper;

    @Autowired
    SubLayananRepository subLayananRepository;

    @Autowired
    JadwalDetilRepository jadwalDetilRepository;

    @Autowired
    SubLayananService subLayananService;

    public KloterServiceImpl(KloterRepository kloterRepository, KloterMapper kloterMapper) {
        this.kloterRepository = kloterRepository;
        this.kloterMapper = kloterMapper;
    }

    /**
     * Save a kloter.
     *
     * @param kloterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public KloterDTO save(KloterDTO kloterDTO) {
        log.debug("Request to save Kloter : {}", kloterDTO);
        Kloter kloter = kloterMapper.toEntity(kloterDTO);
        kloter = kloterRepository.save(kloter);
        return kloterMapper.toDto(kloter);
    }

    /**
     * Get all the kloters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KloterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Kloters");
        return kloterRepository.findAll(pageable)
            .map(kloterMapper::toDto);
    }


    /**
     * Get one kloter by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KloterDTO> findOne(Long id) {
        log.debug("Request to get Kloter : {}", id);
        return kloterRepository.findById(id)
            .map(kloterMapper::toDto);
    }

    /**
     * Delete the kloter by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kloter : {}", id);
        kloterRepository.deleteById(id);
    }
}
