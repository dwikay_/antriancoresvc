package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.LayananDTO;

import com.antrian.inti.core.service.dto.SimpleLantaiDTO;
import com.antrian.inti.core.service.dto.SimpleLayananDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Layanan}.
 */
public interface LayananService {

    /**
     * Save a layanan.
     *
     * @param layananDTO the entity to save.
     * @return the persisted entity.
     */
    LayananDTO save(LayananDTO layananDTO);

    /**
     * Get all the layanans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LayananDTO> findAll(Pageable pageable);
    /*
        Tambahan
     */
    Page<SimpleLayananDTO> findSimpleAll(Pageable pageable);
    List<SimpleLantaiDTO> getLayananByLantai();
    List<SimpleLayananDTO> findSimpleAllByIdLantai(Long idLantai);

    /**
     * Get the "id" layanan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LayananDTO> findOne(Long id);
    Optional<SimpleLayananDTO> findOneSimple(Long id);


    /**
     * Delete the "id" layanan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
