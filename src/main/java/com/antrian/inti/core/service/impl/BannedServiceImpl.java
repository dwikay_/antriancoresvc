package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.BannedService;
import com.antrian.inti.core.domain.Banned;
import com.antrian.inti.core.repository.BannedRepository;
import com.antrian.inti.core.service.dto.BannedDTO;
import com.antrian.inti.core.service.dto.RegistrasiDTO;
import com.antrian.inti.core.service.mapper.BannedMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Banned}.
 */
@Service
@Transactional
public class BannedServiceImpl implements BannedService {

    private final Logger log = LoggerFactory.getLogger(BannedServiceImpl.class);

    private final BannedRepository bannedRepository;

    private final BannedMapper bannedMapper;

    public BannedServiceImpl(BannedRepository bannedRepository, BannedMapper bannedMapper) {
        this.bannedRepository = bannedRepository;
        this.bannedMapper = bannedMapper;
    }

    /**
     * Save a banned.
     *
     * @param bannedDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BannedDTO save(BannedDTO bannedDTO) {
        log.debug("Request to save Banned : {}", bannedDTO);
        Banned banned = bannedMapper.toEntity(bannedDTO);
        banned = bannedRepository.save(banned);
        return bannedMapper.toDto(banned);
    }

    /**
     * Get all the banneds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BannedDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Banneds");
        return bannedRepository.findAll(pageable)
            .map(bannedMapper::toDto);
    }


    /**
     * Get one banned by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BannedDTO> findOne(Long id) {
        log.debug("Request to get Banned : {}", id);
        return bannedRepository.findById(id)
            .map(bannedMapper::toDto);
    }

    /**
     * Delete the banned by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Banned : {}", id);
        bannedRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BannedDTO> getAllBlokirs(Pageable pageable, Integer jumlah) {
        log.debug("Request to get all Banneds");
        return bannedRepository.findByJumlahGreaterThan(pageable,jumlah)
            .map(bannedMapper::toDto);
    }
    /*
        Tambahan
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BannedDTO> checkingRegistrasi(String nik, Integer jumlah) {
        log.debug("Request to get Registrasi by nik, email, verifikasiEmail: {}", nik, jumlah);
        return bannedRepository.findOneByNikAndJumlahGreaterThan(nik,jumlah)
            .map(bannedMapper::toDto);
    }
}
