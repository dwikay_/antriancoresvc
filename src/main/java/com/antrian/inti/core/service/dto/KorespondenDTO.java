package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Koresponden} entity.
 */
public class KorespondenDTO implements Serializable {

    private Long id;

    private String nik;

    private String nama;

    private String namaPerusahaan;

    private String noTelp;

    private String email;

    @Lob
    private String kodeVerifikasi;

    private Double nilaiPersyaratan;

    private Double nilaiProsedur;

    private Double nilaiWaktuPelayanan;

    private Double nilaiBiayaTarif;

    private Double nilaiProdukPelayanan1;

    private Double nilaiProdukPelayanan2;

    private Double nilaiKomPel1;

    private Double nilaiKomPel2;

    private Double nilaiKomPel3;

    private Double nilaiKomPel4;

    private Double nilaiPerilakuPelaksana1;

    private Double nilaiPrilakuPelaksana2;

    private Double nilaiSarPras;

    private Double nilaiSaranMasuk;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Boolean jenisKelamin;

    private String pekerjaan;

    private String pendidikan;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKodeVerifikasi() {
        return kodeVerifikasi;
    }

    public void setKodeVerifikasi(String kodeVerifikasi) {
        this.kodeVerifikasi = kodeVerifikasi;
    }

    public Double getNilaiPersyaratan() {
        return nilaiPersyaratan;
    }

    public void setNilaiPersyaratan(Double nilaiPersyaratan) {
        this.nilaiPersyaratan = nilaiPersyaratan;
    }

    public Double getNilaiProsedur() {
        return nilaiProsedur;
    }

    public void setNilaiProsedur(Double nilaiProsedur) {
        this.nilaiProsedur = nilaiProsedur;
    }

    public Double getNilaiWaktuPelayanan() {
        return nilaiWaktuPelayanan;
    }

    public void setNilaiWaktuPelayanan(Double nilaiWaktuPelayanan) {
        this.nilaiWaktuPelayanan = nilaiWaktuPelayanan;
    }

    public Double getNilaiBiayaTarif() {
        return nilaiBiayaTarif;
    }

    public void setNilaiBiayaTarif(Double nilaiBiayaTarif) {
        this.nilaiBiayaTarif = nilaiBiayaTarif;
    }

    public Double getNilaiProdukPelayanan1() {
        return nilaiProdukPelayanan1;
    }

    public void setNilaiProdukPelayanan1(Double nilaiProdukPelayanan1) {
        this.nilaiProdukPelayanan1 = nilaiProdukPelayanan1;
    }

    public Double getNilaiProdukPelayanan2() {
        return nilaiProdukPelayanan2;
    }

    public void setNilaiProdukPelayanan2(Double nilaiProdukPelayanan2) {
        this.nilaiProdukPelayanan2 = nilaiProdukPelayanan2;
    }

    public Double getNilaiKomPel1() {
        return nilaiKomPel1;
    }

    public void setNilaiKomPel1(Double nilaiKomPel1) {
        this.nilaiKomPel1 = nilaiKomPel1;
    }

    public Double getNilaiKomPel2() {
        return nilaiKomPel2;
    }

    public void setNilaiKomPel2(Double nilaiKomPel2) {
        this.nilaiKomPel2 = nilaiKomPel2;
    }

    public Double getNilaiKomPel3() {
        return nilaiKomPel3;
    }

    public void setNilaiKomPel3(Double nilaiKomPel3) {
        this.nilaiKomPel3 = nilaiKomPel3;
    }

    public Double getNilaiKomPel4() {
        return nilaiKomPel4;
    }

    public void setNilaiKomPel4(Double nilaiKomPel4) {
        this.nilaiKomPel4 = nilaiKomPel4;
    }

    public Double getNilaiPerilakuPelaksana1() {
        return nilaiPerilakuPelaksana1;
    }

    public void setNilaiPerilakuPelaksana1(Double nilaiPerilakuPelaksana1) {
        this.nilaiPerilakuPelaksana1 = nilaiPerilakuPelaksana1;
    }

    public Double getNilaiPrilakuPelaksana2() {
        return nilaiPrilakuPelaksana2;
    }

    public void setNilaiPrilakuPelaksana2(Double nilaiPrilakuPelaksana2) {
        this.nilaiPrilakuPelaksana2 = nilaiPrilakuPelaksana2;
    }

    public Double getNilaiSarPras() {
        return nilaiSarPras;
    }

    public void setNilaiSarPras(Double nilaiSarPras) {
        this.nilaiSarPras = nilaiSarPras;
    }

    public Double getNilaiSaranMasuk() {
        return nilaiSaranMasuk;
    }

    public void setNilaiSaranMasuk(Double nilaiSaranMasuk) {
        this.nilaiSaranMasuk = nilaiSaranMasuk;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Boolean isJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(Boolean jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KorespondenDTO korespondenDTO = (KorespondenDTO) o;
        if (korespondenDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), korespondenDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KorespondenDTO{" +
            "id=" + getId() +
            ", nik='" + getNik() + "'" +
            ", nama='" + getNama() + "'" +
            ", namaPerusahaan='" + getNamaPerusahaan() + "'" +
            ", noTelp='" + getNoTelp() + "'" +
            ", email='" + getEmail() + "'" +
            ", kodeVerifikasi='" + getKodeVerifikasi() + "'" +
            ", nilaiPersyaratan=" + getNilaiPersyaratan() +
            ", nilaiProsedur=" + getNilaiProsedur() +
            ", nilaiWaktuPelayanan=" + getNilaiWaktuPelayanan() +
            ", nilaiBiayaTarif=" + getNilaiBiayaTarif() +
            ", nilaiProdukPelayanan1=" + getNilaiProdukPelayanan1() +
            ", nilaiProdukPelayanan2=" + getNilaiProdukPelayanan2() +
            ", nilaiKomPel1=" + getNilaiKomPel1() +
            ", nilaiKomPel2=" + getNilaiKomPel2() +
            ", nilaiKomPel3=" + getNilaiKomPel3() +
            ", nilaiKomPel4=" + getNilaiKomPel4() +
            ", nilaiPerilakuPelaksana1=" + getNilaiPerilakuPelaksana1() +
            ", nilaiPrilakuPelaksana2=" + getNilaiPrilakuPelaksana2() +
            ", nilaiSarPras=" + getNilaiSarPras() +
            ", nilaiSaranMasuk=" + getNilaiSaranMasuk() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", jenisKelamin='" + isJenisKelamin() + "'" +
            ", pekerjaan='" + getPekerjaan() + "'" +
            ", pendidikan='" + getPendidikan() + "'" +
            "}";
    }
}
