package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Layanan} entity.
 */
public class LayananDTO implements Serializable {

    private Long id;

    private String kodeLayanan;

    private String namaLayanan;

    @Lob
    private byte[] gambar;

    private String gambarContentType;
    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;

    private Long lastModificationUserId;

    private Long idLantai;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKodeLayanan() {
        return kodeLayanan;
    }

    public void setKodeLayanan(String kodeLayanan) {
        this.kodeLayanan = kodeLayanan;
    }

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public byte[] getGambar() {
        return gambar;
    }

    public void setGambar(byte[] gambar) {
        this.gambar = gambar;
    }

    public String getGambarContentType() {
        return gambarContentType;
    }

    public void setGambarContentType(String gambarContentType) {
        this.gambarContentType = gambarContentType;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public Long getIdLantai() {
        return idLantai;
    }

    public void setIdLantai(Long idLantai) {
        this.idLantai = idLantai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LayananDTO layananDTO = (LayananDTO) o;
        if (layananDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), layananDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LayananDTO{" +
            "id=" + getId() +
            ", kodeLayanan='" + getKodeLayanan() + "'" +
            ", namaLayanan='" + getNamaLayanan() + "'" +
            ", gambar='" + getGambar() + "'" +
            ", createUserId=" + getCreateUserId() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", lastModificationDate='" + getLastModificationDate() + "'" +
            ", lastModificationDateTime='" + getLastModificationDateTime() + "'" +
            ", lastModificationUserId=" + getLastModificationUserId() +
            ", idLantai=" + getIdLantai() +
            "}";
    }
}
