package com.antrian.inti.core.service.dto;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;

public class SimplePendaftaranOfflineDTO implements Serializable {

    private Long id;
    private String namaPelaku;
    private String email;
    private String nik;
    private String namaPerusahaan;
    private String telepon;
    @Lob
    private String pertanyaan;
    private String noUrut;
    private Long idSubLayanan;
    private String tokenValidasi;
    private LocalDate tanggal;

}
