package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.JadwalDetilService;
import com.antrian.inti.core.service.JadwalMasterService;
import com.antrian.inti.core.domain.JadwalMaster;
import com.antrian.inti.core.repository.JadwalMasterRepository;
import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.JadwalDetilDTO;
import com.antrian.inti.core.service.dto.JadwalMasterDTO;
import com.antrian.inti.core.service.dto.SimpleJadwalDTO;
import com.antrian.inti.core.service.dto.SimpleSubLayananDTO;
import com.antrian.inti.core.service.mapper.JadwalMasterMapper;
import com.netflix.discovery.converters.Auto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link JadwalMaster}.
 */
@Service
@Transactional
public class JadwalMasterServiceImpl implements JadwalMasterService {

    private final Logger log = LoggerFactory.getLogger(JadwalMasterServiceImpl.class);

    private final JadwalMasterRepository jadwalMasterRepository;

    private final JadwalMasterMapper jadwalMasterMapper;

    @Autowired
    SubLayananService subLayananService;

    @Autowired
    JadwalDetilService jadwalDetilService;

    public JadwalMasterServiceImpl(JadwalMasterRepository jadwalMasterRepository, JadwalMasterMapper jadwalMasterMapper) {
        this.jadwalMasterRepository = jadwalMasterRepository;
        this.jadwalMasterMapper = jadwalMasterMapper;
    }

    /**
     * Save a jadwalMaster.
     *
     * @param jadwalMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public JadwalMasterDTO save(JadwalMasterDTO jadwalMasterDTO) {
        log.debug("Request to save JadwalMaster : {}", jadwalMasterDTO);
        JadwalMaster jadwalMaster = jadwalMasterMapper.toEntity(jadwalMasterDTO);
        jadwalMaster = jadwalMasterRepository.save(jadwalMaster);
        return jadwalMasterMapper.toDto(jadwalMaster);
    }

    /**
     * Get all the jadwalMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JadwalMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JadwalMasters");
        return jadwalMasterRepository.findAll(pageable)
            .map(jadwalMasterMapper::toDto);
    }


    /**
     * Get one jadwalMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<JadwalMasterDTO> findOne(Long id) {
        log.debug("Request to get JadwalMaster : {}", id);
        return jadwalMasterRepository.findById(id)
            .map(jadwalMasterMapper::toDto);
    }

    /**
     * Delete the jadwalMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JadwalMaster : {}", id);
        jadwalMasterRepository.deleteById(id);
    }

    /*
        Tambahan
     */

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleJadwalDTO> findSimpleAll(Pageable pageable) {
        log.debug("Request to get all JadwalMasters");
        return jadwalMasterRepository.findAll(pageable)
            .map(jadwalMasterMapper::toDto).map(e -> {

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(e.getIdSubLayanan());
                List<JadwalDetilDTO> jadwalDetilDTOS = jadwalDetilService.findAllByIdMaster(e.getId());
                SimpleJadwalDTO simpleJadwalDTO = new SimpleJadwalDTO();
                simpleJadwalDTO.setId(e.getId());
                simpleJadwalDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleJadwalDTO.setSubLayananDTO(simpleSubLayananDTO);
                simpleJadwalDTO.setJadwalDetilDTOList(jadwalDetilDTOS);

                return simpleJadwalDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SimpleJadwalDTO> findSimpleAllByTipeVirtual(Pageable pageable, boolean tipeVirtual) {
        log.debug("Request to get all JadwalMasters");
        return jadwalMasterRepository.findAll(pageable)
            .map(jadwalMasterMapper::toDto).map(e -> {

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(e.getIdSubLayanan());
                List<JadwalDetilDTO> jadwalDetilDTOS = jadwalDetilService.findAllByIdMasterAndTipeVirtual(e.getId(),tipeVirtual);
                SimpleJadwalDTO simpleJadwalDTO = new SimpleJadwalDTO();
                simpleJadwalDTO.setId(e.getId());
                simpleJadwalDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleJadwalDTO.setSubLayananDTO(simpleSubLayananDTO);
                simpleJadwalDTO.setJadwalDetilDTOList(jadwalDetilDTOS);

                return simpleJadwalDTO;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleJadwalDTO> findSimpleOne(Long id) {
        log.debug("Request to get JadwalMaster : {}", id);
        return jadwalMasterRepository.findById(id)
            .map(jadwalMasterMapper::toDto).map(e -> {

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(e.getIdSubLayanan());
                List<JadwalDetilDTO> jadwalDetilDTOS = jadwalDetilService.findAllByIdMaster(e.getId());
                SimpleJadwalDTO simpleJadwalDTO = new SimpleJadwalDTO();
                simpleJadwalDTO.setId(e.getId());
                simpleJadwalDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleJadwalDTO.setSubLayananDTO(simpleSubLayananDTO);
                simpleJadwalDTO.setJadwalDetilDTOList(jadwalDetilDTOS);

                return simpleJadwalDTO;

            });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleJadwalDTO> findSimpleIdSubLayanan(Long idSubLayanan) {
        log.debug("Request to get JadwalMaster by idSubLayanan : {}", idSubLayanan);
        return jadwalMasterRepository.findByIdSubLayanan(idSubLayanan)
            .map(jadwalMasterMapper::toDto).map(e -> {

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(e.getIdSubLayanan());
                List<JadwalDetilDTO> jadwalDetilDTOS = jadwalDetilService.findAllByIdMaster(e.getId());
                SimpleJadwalDTO simpleJadwalDTO = new SimpleJadwalDTO();
                simpleJadwalDTO.setId(e.getId());
                simpleJadwalDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleJadwalDTO.setSubLayananDTO(simpleSubLayananDTO);
                simpleJadwalDTO.setJadwalDetilDTOList(jadwalDetilDTOS);

                return simpleJadwalDTO;

            });
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SimpleJadwalDTO> findSimpleIdSubLayananAndTipeVirtual(Long idSubLayanan, boolean tipeVirtual) {
        log.debug("Request to get JadwalMaster by idSubLayanan : {}", idSubLayanan);
        return jadwalMasterRepository.findByIdSubLayanan(idSubLayanan)
            .map(jadwalMasterMapper::toDto).map(e -> {

                Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(e.getIdSubLayanan());
                List<JadwalDetilDTO> jadwalDetilDTOS = jadwalDetilService.findAllByIdMasterAndTipeVirtual(e.getId(),tipeVirtual);
                SimpleJadwalDTO simpleJadwalDTO = new SimpleJadwalDTO();
                simpleJadwalDTO.setId(e.getId());
                simpleJadwalDTO.setIdSubLayanan(e.getIdSubLayanan());
                simpleJadwalDTO.setSubLayananDTO(simpleSubLayananDTO);
                simpleJadwalDTO.setJadwalDetilDTOList(jadwalDetilDTOS);

                return simpleJadwalDTO;

            });
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<JadwalMasterDTO> findLastJadwalMaster() {
        log.debug("Request to get JadwalMaster : {}");
        return jadwalMasterRepository.findFirstByOrderByIdDesc()
            .map(jadwalMasterMapper::toDto);
    }
}
