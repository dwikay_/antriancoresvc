package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.domain.DataAntrian;
import com.antrian.inti.core.domain.JadwalMaster;
import com.antrian.inti.core.repository.JadwalMasterRepository;
import com.antrian.inti.core.service.JadwalDetilService;
import com.antrian.inti.core.domain.JadwalDetil;
import com.antrian.inti.core.repository.JadwalDetilRepository;
import com.antrian.inti.core.service.dto.DataAntrianDTO;
import com.antrian.inti.core.service.dto.JadwalDetilDTO;
import com.antrian.inti.core.service.dto.PlasmaDTO;
import com.antrian.inti.core.service.mapper.JadwalDetilMapper;
import com.netflix.discovery.converters.Auto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link JadwalDetil}.
 */
@Service
@Transactional
public class JadwalDetilServiceImpl implements JadwalDetilService {

    private final Logger log = LoggerFactory.getLogger(JadwalDetilServiceImpl.class);

    private final JadwalDetilRepository jadwalDetilRepository;

    private final JadwalDetilMapper jadwalDetilMapper;

    @Autowired
    JadwalMasterRepository jadwalMasterRepository;

    public JadwalDetilServiceImpl(JadwalDetilRepository jadwalDetilRepository, JadwalDetilMapper jadwalDetilMapper) {
        this.jadwalDetilRepository = jadwalDetilRepository;
        this.jadwalDetilMapper = jadwalDetilMapper;
    }

    /**
     * Save a jadwalDetil.
     *
     * @param jadwalDetilDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public JadwalDetilDTO save(JadwalDetilDTO jadwalDetilDTO) {
        log.debug("Request to save JadwalDetil : {}", jadwalDetilDTO);
        JadwalDetil jadwalDetil = jadwalDetilMapper.toEntity(jadwalDetilDTO);

//        Optional<JadwalMaster> jadwalMaster = jadwalMasterRepository.findFirstByOrderByIdDesc();
//        jadwalDetil.setIdMaster(jadwalMaster.get().getId());

        jadwalDetil = jadwalDetilRepository.save(jadwalDetil);
        return jadwalDetilMapper.toDto(jadwalDetil);
    }

    /**
     * Get all the jadwalDetils.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JadwalDetilDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JadwalDetils");
        return jadwalDetilRepository.findAll(pageable)
            .map(jadwalDetilMapper::toDto);
    }


    /**
     * Get one jadwalDetil by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<JadwalDetilDTO> findOne(Long id) {
        log.debug("Request to get JadwalDetil : {}", id);
        return jadwalDetilRepository.findById(id)
            .map(jadwalDetilMapper::toDto);
    }

    /**
     * Delete the jadwalDetil by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JadwalDetil : {}", id);
        jadwalDetilRepository.deleteById(id);
    }
    /*
        Tambahan
     */
    @Override
    @Transactional(readOnly = true)
    public List<JadwalDetilDTO> findAllByIdMaster(Long idMaster) {

        List<JadwalDetil> getDataDetil = jadwalDetilRepository.findAllByIdMasterOrderByKloterAsc(idMaster);

        if(getDataDetil.size() > 0){

            List<JadwalDetilDTO> listJadwalDetil = new ArrayList<>();

            for (JadwalDetil detil:getDataDetil){

                JadwalDetilDTO jadwalDetilDTO = new JadwalDetilDTO();
                jadwalDetilDTO.setId(detil.getId());
                jadwalDetilDTO.setIdMaster(detil.getIdMaster());
                jadwalDetilDTO.setJamAwal(detil.getJamAwal());
                jadwalDetilDTO.setJamAkhir(detil.getJamAkhir());
                jadwalDetilDTO.setKloter(detil.getKloter());
                jadwalDetilDTO.setJumlahKuota(detil.getJumlahKuota());
                jadwalDetilDTO.setVirtual(detil.isVirtual());

                listJadwalDetil.add(jadwalDetilDTO);
            }

            return listJadwalDetil;
        }

        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<JadwalDetilDTO> findAllByIdMasterAndTipeVirtual(Long idMaster, boolean virtual) {

        List<JadwalDetil> getDataDetil = jadwalDetilRepository.findAllByIdMasterAndVirtualOrderByKloterAsc(idMaster, virtual);

        if(getDataDetil.size() > 0){

            List<JadwalDetilDTO> listJadwalDetil = new ArrayList<>();

            for (JadwalDetil detil:getDataDetil){

                JadwalDetilDTO jadwalDetilDTO = new JadwalDetilDTO();
                jadwalDetilDTO.setId(detil.getId());
                jadwalDetilDTO.setIdMaster(detil.getIdMaster());
                jadwalDetilDTO.setJamAwal(detil.getJamAwal());
                jadwalDetilDTO.setJamAkhir(detil.getJamAkhir());
                jadwalDetilDTO.setKloter(detil.getKloter());
                jadwalDetilDTO.setJumlahKuota(detil.getJumlahKuota());
                jadwalDetilDTO.setVirtual(detil.isVirtual());

                listJadwalDetil.add(jadwalDetilDTO);
            }

            return listJadwalDetil;
        }

        return null;
    }

    @Override
    public Integer countByTipeVirtual(boolean typeVirtual) {
        log.debug("Request count all DataAntrian by dateNow and typeVirtual : {},{},{}",typeVirtual);
        return jadwalDetilRepository.countByVirtual(typeVirtual);
    }
}
