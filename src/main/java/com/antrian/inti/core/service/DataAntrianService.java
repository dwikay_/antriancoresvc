package com.antrian.inti.core.service;

import com.antrian.inti.core.domain.DataAntrian;
import com.antrian.inti.core.service.dto.*;

import org.checkerframework.checker.nullness.Opt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;

import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.DataAntrian}.
 */
public interface DataAntrianService {

    /**
     * Save a dataAntrian.
     *
     * @param dataAntrianDTO the entity to save.
     * @return the persisted entity.
     */
    DataAntrianDTO save(DataAntrianDTO dataAntrianDTO);

    /**
     * Get all the dataAntrians.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DataAntrianDTO> findAll(Pageable pageable);
    Page<SimpleDataAntrianDTO> antrianSisaList(Pageable pageable, LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan);
    List<PlasmaDTO> findAllPlasma(LocalDate tanggalSekarang, Long idLantai);
    List<PlasmaDTO> findAllPlasma2(LocalDate tanggalSekarang, Long idLantai);
    Page<SimpleDataAntrianDTO> historyAntrian(Pageable pageable, LocalDate dateNow, Long idSubLayanan, Long idLoket);
    Page<SimpleDataAntrianDTO> antreanLayanan(Pageable pageable, LocalDate dateNow, Long idSubLayanan);
    /**
     * Get the "id" dataAntrian.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DataAntrianDTO> findOne(Long id);
    /*
        Tambahan
     */
    Optional<DataAntrianDTO> getDataPrev(Long idSubLayanan, Long noUrut);
    Optional<SimpleDataAntrianDTO> getDataAntrianLast(Long idSubLayanan, LocalDate tanggalLayanan);
    Optional<SimpleDataAntrianDTO> panggilanBerikutnya(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, boolean statusPanggilan);
    Optional<SimpleDataAntrianDTO> panggilanSaatIni(Long loket,Long idSubLayanan, boolean statusCurrent, LocalDate tanggal);
    Optional<SimpleDataAntrianDTO> lastPanggilan(Long loket,Long idSubLayanan, boolean statusCurrent, LocalDate tanggal);
    Optional<SimpleDataAntrianDTO> fastCall(Long idSubLayanan,Long noUrut,boolean statusKonseling, LocalDate tanggal);
    Optional<LostFoundDTO> foundAndLost(Long idLantai, Long idSubLayanan, Long noUrut);
    Optional<DataAntrianDTO> getByNoUrutAndIdSubLayanan(Long noUrut, Long idSubLayanan);
    Optional<DataAntrianDTO> panggilanBerikutnya2(Long idSubLayanan, boolean statusCurrent, LocalDate tanggal, boolean statusPanggilan);
    Optional<DataAntrianDTO> getPanggilanSebelumnya(Long loket,Long idSubLayanan, LocalDate tanggal);
    //Optional<DataAntrianDTO> findOneByIdAndStatusPanggilanAndStatusCurrent(Long idAntrian, boolean statusPanggilan, boolean statusCurrent);


    //new UPDATE
    //NEXT
    Optional<DataAntrianDTO> getNewPanggilanSebelumnya(Long idLoket, Long idSubLayanan);
    Optional<DataAntrianDTO> getNewPanggilanSaatIni(Long idLoket, Long idSubLayanan);
    Optional<DataAntrianDTO> getFastCallBelumDipanggil(Long idLoket, Long idSubLayanan);
    Optional<DataAntrianDTO> getFastCallSesudahDipanggil(Long idLoket, Long idSubLayanan);

    //PREV
    List<DataAntrianDTO> getAllDataAntrianPerloket(Long idLoket, Long idSubLayanan);
    Optional<DataAntrianDTO> getPrevPanggilanSebelumnya(Long idLoket, Long idSubLayanan);

    //FASTCALL
    Optional<DataAntrianDTO> getNewFastCall(Long noUrut,Long idSubLayanan);
    //REFR
    Optional<DataAntrianDTO> newRfr(Long idLoket, Long idSubLayanan);


    /**
     * Delete the "id" dataAntrian.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
    void deleteDataAntrian();

    Integer getTotalAntrianNow(LocalDate dateNow, Long idSubLayanan);
    Integer totalAntrianSisa(LocalDate dateNow, Long idSubLayanan, boolean statusPanggilan);

}
