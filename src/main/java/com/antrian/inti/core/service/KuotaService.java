package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.KuotaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Kuota}.
 */
public interface KuotaService {

    /**
     * Save a kuota.
     *
     * @param kuotaDTO the entity to save.
     * @return the persisted entity.
     */
    KuotaDTO save(KuotaDTO kuotaDTO);

    /**
     * Get all the kuotas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KuotaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" kuota.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KuotaDTO> findOne(Long id);

    /**
     * Delete the "id" kuota.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
