package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.KuotaService;
import com.antrian.inti.core.domain.Kuota;
import com.antrian.inti.core.repository.KuotaRepository;
import com.antrian.inti.core.service.dto.KuotaDTO;
import com.antrian.inti.core.service.mapper.KuotaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Kuota}.
 */
@Service
@Transactional
public class KuotaServiceImpl implements KuotaService {

    private final Logger log = LoggerFactory.getLogger(KuotaServiceImpl.class);

    private final KuotaRepository kuotaRepository;

    private final KuotaMapper kuotaMapper;

    public KuotaServiceImpl(KuotaRepository kuotaRepository, KuotaMapper kuotaMapper) {
        this.kuotaRepository = kuotaRepository;
        this.kuotaMapper = kuotaMapper;
    }

    /**
     * Save a kuota.
     *
     * @param kuotaDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public KuotaDTO save(KuotaDTO kuotaDTO) {
        log.debug("Request to save Kuota : {}", kuotaDTO);
        Kuota kuota = kuotaMapper.toEntity(kuotaDTO);
        kuota = kuotaRepository.save(kuota);
        return kuotaMapper.toDto(kuota);
    }

    /**
     * Get all the kuotas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<KuotaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Kuotas");
        return kuotaRepository.findAll(pageable)
            .map(kuotaMapper::toDto);
    }


    /**
     * Get one kuota by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<KuotaDTO> findOne(Long id) {
        log.debug("Request to get Kuota : {}", id);
        return kuotaRepository.findById(id)
            .map(kuotaMapper::toDto);
    }

    /**
     * Delete the kuota by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kuota : {}", id);
        kuotaRepository.deleteById(id);
    }
}
