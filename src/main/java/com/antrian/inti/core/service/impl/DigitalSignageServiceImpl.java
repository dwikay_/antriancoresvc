package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.DigitalSignageService;
import com.antrian.inti.core.domain.DigitalSignage;
import com.antrian.inti.core.repository.DigitalSignageRepository;
import com.antrian.inti.core.service.dto.DigitalSignageDTO;
import com.antrian.inti.core.service.mapper.DigitalSignageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DigitalSignage}.
 */
@Service
@Transactional
public class DigitalSignageServiceImpl implements DigitalSignageService {

    private final Logger log = LoggerFactory.getLogger(DigitalSignageServiceImpl.class);

    private final DigitalSignageRepository digitalSignageRepository;

    private final DigitalSignageMapper digitalSignageMapper;

    public DigitalSignageServiceImpl(DigitalSignageRepository digitalSignageRepository, DigitalSignageMapper digitalSignageMapper) {
        this.digitalSignageRepository = digitalSignageRepository;
        this.digitalSignageMapper = digitalSignageMapper;
    }

    /**
     * Save a digitalSignage.
     *
     * @param digitalSignageDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DigitalSignageDTO save(DigitalSignageDTO digitalSignageDTO) {
        log.debug("Request to save DigitalSignage : {}", digitalSignageDTO);
        DigitalSignage digitalSignage = digitalSignageMapper.toEntity(digitalSignageDTO);
        digitalSignage = digitalSignageRepository.save(digitalSignage);
        return digitalSignageMapper.toDto(digitalSignage);
    }

    /**
     * Get all the digitalSignages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DigitalSignageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DigitalSignages");
        return digitalSignageRepository.findAll(pageable)
            .map(digitalSignageMapper::toDto);
    }


    /**
     * Get one digitalSignage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DigitalSignageDTO> findOne(Long id) {
        log.debug("Request to get DigitalSignage : {}", id);
        return digitalSignageRepository.findById(id)
            .map(digitalSignageMapper::toDto);
    }

    /**
     * Delete the digitalSignage by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DigitalSignage : {}", id);
        digitalSignageRepository.deleteById(id);
    }
}
