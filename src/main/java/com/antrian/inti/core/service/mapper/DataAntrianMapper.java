package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.DataAntrianDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DataAntrian} and its DTO {@link DataAntrianDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DataAntrianMapper extends EntityMapper<DataAntrianDTO, DataAntrian> {



    default DataAntrian fromId(Long id) {
        if (id == null) {
            return null;
        }
        DataAntrian dataAntrian = new DataAntrian();
        dataAntrian.setId(id);
        return dataAntrian;
    }
}
