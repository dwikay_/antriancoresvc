package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.AkumulasiKoresponden} entity.
 */
public class AkumulasiKorespondenDTO implements Serializable {

    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;

    private Double akumulasiPersyaratan;

    private Double akumulasiProsedur;

    private Double akumulasiWaktuPelayanan;

    private Double akumulasiBiayaTarif;

    private Double akumulasiProdukPelayanan;

    private Double akumulasiKomPel;

    private Double akumulasiPrilakuPelaksana;

    private Double akumulasiSarPras;

    private Double akumulasiSaranMasuk;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Double akumulasiRataRata;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getAkumulasiPersyaratan() {
        return akumulasiPersyaratan;
    }

    public void setAkumulasiPersyaratan(Double akumulasiPersyaratan) {
        this.akumulasiPersyaratan = akumulasiPersyaratan;
    }

    public Double getAkumulasiProsedur() {
        return akumulasiProsedur;
    }

    public void setAkumulasiProsedur(Double akumulasiProsedur) {
        this.akumulasiProsedur = akumulasiProsedur;
    }

    public Double getAkumulasiWaktuPelayanan() {
        return akumulasiWaktuPelayanan;
    }

    public void setAkumulasiWaktuPelayanan(Double akumulasiWaktuPelayanan) {
        this.akumulasiWaktuPelayanan = akumulasiWaktuPelayanan;
    }

    public Double getAkumulasiBiayaTarif() {
        return akumulasiBiayaTarif;
    }

    public void setAkumulasiBiayaTarif(Double akumulasiBiayaTarif) {
        this.akumulasiBiayaTarif = akumulasiBiayaTarif;
    }

    public Double getAkumulasiProdukPelayanan() {
        return akumulasiProdukPelayanan;
    }

    public void setAkumulasiProdukPelayanan(Double akumulasiProdukPelayanan) {
        this.akumulasiProdukPelayanan = akumulasiProdukPelayanan;
    }

    public Double getAkumulasiKomPel() {
        return akumulasiKomPel;
    }

    public void setAkumulasiKomPel(Double akumulasiKomPel) {
        this.akumulasiKomPel = akumulasiKomPel;
    }

    public Double getAkumulasiPrilakuPelaksana() {
        return akumulasiPrilakuPelaksana;
    }

    public void setAkumulasiPrilakuPelaksana(Double akumulasiPrilakuPelaksana) {
        this.akumulasiPrilakuPelaksana = akumulasiPrilakuPelaksana;
    }

    public Double getAkumulasiSarPras() {
        return akumulasiSarPras;
    }

    public void setAkumulasiSarPras(Double akumulasiSarPras) {
        this.akumulasiSarPras = akumulasiSarPras;
    }

    public Double getAkumulasiSaranMasuk() {
        return akumulasiSaranMasuk;
    }

    public void setAkumulasiSaranMasuk(Double akumulasiSaranMasuk) {
        this.akumulasiSaranMasuk = akumulasiSaranMasuk;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Double getAkumulasiRataRata() {
        return akumulasiRataRata;
    }

    public void setAkumulasiRataRata(Double akumulasiRataRata) {
        this.akumulasiRataRata = akumulasiRataRata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AkumulasiKorespondenDTO akumulasiKorespondenDTO = (AkumulasiKorespondenDTO) o;
        if (akumulasiKorespondenDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), akumulasiKorespondenDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AkumulasiKorespondenDTO{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", akumulasiPersyaratan=" + getAkumulasiPersyaratan() +
            ", akumulasiProsedur=" + getAkumulasiProsedur() +
            ", akumulasiWaktuPelayanan=" + getAkumulasiWaktuPelayanan() +
            ", akumulasiBiayaTarif=" + getAkumulasiBiayaTarif() +
            ", akumulasiProdukPelayanan=" + getAkumulasiProdukPelayanan() +
            ", akumulasiKomPel=" + getAkumulasiKomPel() +
            ", akumulasiPrilakuPelaksana=" + getAkumulasiPrilakuPelaksana() +
            ", akumulasiSarPras=" + getAkumulasiSarPras() +
            ", akumulasiSaranMasuk=" + getAkumulasiSaranMasuk() +
            ", createDate='" + getCreateDate() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", akumulasiRataRata=" + getAkumulasiRataRata() +
            "}";
    }
}
