package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.HariLiburDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.HariLibur}.
 */
public interface HariLiburService {

    /**
     * Save a hariLibur.
     *
     * @param hariLiburDTO the entity to save.
     * @return the persisted entity.
     */
    HariLiburDTO save(HariLiburDTO hariLiburDTO);

    /**
     * Get all the hariLiburs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HariLiburDTO> findAll(Pageable pageable);


    /**
     * Get the "id" hariLibur.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HariLiburDTO> findOne(Long id);

    /**
     * Delete the "id" hariLibur.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
