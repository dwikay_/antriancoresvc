package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.service.ProsedurService;
import com.antrian.inti.core.domain.Prosedur;
import com.antrian.inti.core.repository.ProsedurRepository;
import com.antrian.inti.core.service.dto.ProsedurDTO;
import com.antrian.inti.core.service.mapper.ProsedurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Prosedur}.
 */
@Service
@Transactional
public class ProsedurServiceImpl implements ProsedurService {

    private final Logger log = LoggerFactory.getLogger(ProsedurServiceImpl.class);

    private final ProsedurRepository prosedurRepository;

    private final ProsedurMapper prosedurMapper;

    public ProsedurServiceImpl(ProsedurRepository prosedurRepository, ProsedurMapper prosedurMapper) {
        this.prosedurRepository = prosedurRepository;
        this.prosedurMapper = prosedurMapper;
    }

    /**
     * Save a prosedur.
     *
     * @param prosedurDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProsedurDTO save(ProsedurDTO prosedurDTO) {
        log.debug("Request to save Prosedur : {}", prosedurDTO);
        Prosedur prosedur = prosedurMapper.toEntity(prosedurDTO);
        prosedur = prosedurRepository.save(prosedur);
        return prosedurMapper.toDto(prosedur);
    }

    /**
     * Get all the prosedurs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProsedurDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Prosedurs");
        return prosedurRepository.findAll(pageable)
            .map(prosedurMapper::toDto);
    }


    /**
     * Get one prosedur by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProsedurDTO> findOne(Long id) {
        log.debug("Request to get Prosedur : {}", id);
        return prosedurRepository.findById(id)
            .map(prosedurMapper::toDto);
    }

    /**
     * Delete the prosedur by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Prosedur : {}", id);
        prosedurRepository.deleteById(id);
    }
}
