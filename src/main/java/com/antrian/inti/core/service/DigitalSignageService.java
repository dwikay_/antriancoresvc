package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.DigitalSignageDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.DigitalSignage}.
 */
public interface DigitalSignageService {

    /**
     * Save a digitalSignage.
     *
     * @param digitalSignageDTO the entity to save.
     * @return the persisted entity.
     */
    DigitalSignageDTO save(DigitalSignageDTO digitalSignageDTO);

    /**
     * Get all the digitalSignages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DigitalSignageDTO> findAll(Pageable pageable);


    /**
     * Get the "id" digitalSignage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DigitalSignageDTO> findOne(Long id);

    /**
     * Delete the "id" digitalSignage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
