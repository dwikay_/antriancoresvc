package com.antrian.inti.core.service.impl;

import com.antrian.inti.core.business.domain.SimpleLayanan;
import com.antrian.inti.core.domain.Kiosk;
import com.antrian.inti.core.domain.Lantai;
import com.antrian.inti.core.domain.Layanan;
import com.antrian.inti.core.repository.KioskRepository;
import com.antrian.inti.core.repository.LantaiRepository;
import com.antrian.inti.core.repository.LayananRepository;
import com.antrian.inti.core.service.*;
import com.antrian.inti.core.domain.SubLayanan;
import com.antrian.inti.core.repository.SubLayananRepository;
import com.antrian.inti.core.service.dto.*;
import com.antrian.inti.core.service.mapper.SubLayananMapper;
import org.checkerframework.checker.nullness.Opt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link SubLayanan}.
 */
@Service
@Transactional
public class SubLayananServiceImpl implements SubLayananService {

    private final Logger log = LoggerFactory.getLogger(SubLayananServiceImpl.class);

    private final SubLayananRepository subLayananRepository;

    private final SubLayananMapper subLayananMapper;

    private final KioskRepository kioskRepository;

    private final LantaiRepository lantaiRepository;

    private final LayananRepository layananRepository;

    private final LayananService layananService;

    public SubLayananServiceImpl(LayananService layananService,KioskRepository kioskRepository,LantaiRepository lantaiRepository,LayananRepository layananRepository,LantaiService lantaiService,SubLayananRepository subLayananRepository, SubLayananMapper subLayananMapper) {
        this.subLayananRepository = subLayananRepository;
        this.subLayananMapper = subLayananMapper;
        this.kioskRepository = kioskRepository;
        this.lantaiRepository = lantaiRepository;
        this.layananRepository = layananRepository;
        this.layananService = layananService;
    }

    /**
     * Save a subLayanan.
     *
     * @param subLayananDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SubLayananDTO save(SubLayananDTO subLayananDTO) {
        log.debug("Request to save SubLayanan : {}", subLayananDTO);
        SubLayanan subLayanan = subLayananMapper.toEntity(subLayananDTO);
        subLayanan = subLayananRepository.save(subLayanan);
        return subLayananMapper.toDto(subLayanan);
    }

    /**
     * Get all the subLayanans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubLayananDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubLayanans");
        return subLayananRepository.findAll(pageable)
            .map(subLayananMapper::toDto);
    }


    /**
     * Get one subLayanan by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SubLayananDTO> findOne(Long id) {
        log.debug("Request to get SubLayanan : {}", id);
        return subLayananRepository.findById(id)
            .map(subLayananMapper::toDto);
    }

    /**
     * Delete the subLayanan by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubLayanan : {}", id);
        subLayananRepository.deleteById(id);
    }
    /*
        Tambahan
     */


    @Override
    public List<SubLayananDTO> getAllByIdLayanan(Long idLayanan){
        log.debug("Request to get SubLayanan by idLayanan: {}", idLayanan);
        return subLayananRepository.findAllByIdLayanan(idLayanan).stream()
            .map(subLayananMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<SubLayananDTO> findListSimple(){
        log.debug("Request to get SubLayanan: {}");
        return subLayananRepository.findAll().stream()
            .map(subLayananMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<SubLayananDTO> getLayananAktif(String status){
        log.debug("Request to get SubLayanan by status: {}", status);
        return subLayananRepository.findAllByStatusList(status).stream()
            .map(subLayananMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<SubLayananDTO> getLayananOnline(boolean layananOnline){
        log.debug("Request to get SubLayanan by layananOnline: {}", layananOnline);
        return subLayananRepository.findAllByLayananOnlineOrderByPrefixSuaraAsc(layananOnline).stream()
            .map(subLayananMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<SubLayananDTO> getLayananOnlineByStatus(boolean layananOnline){
        log.debug("Request to get SubLayanan by layananOnline: {}", layananOnline);
        return subLayananRepository.findAllByLayananOnlineAndStatusOrderByPrefixSuaraAsc(layananOnline,"1").stream()
            .map(subLayananMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<SubLayananDTO> getLayananWithLantai(Long idLantai){
        log.debug("Request to get SubLayanan by layananOnline with idLantai: {}", idLantai);
        return subLayananRepository.findAllByIdLantaiOrderByPrefixSuaraAsc(idLantai).stream()
            .map(subLayananMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Page<SimpleSubLayananDTO> findSimpleAll(Pageable pageable) {
        log.debug("Request to get all SubLayanans");
        return subLayananRepository.findAll(pageable)
            .map(subLayananMapper::toDto).map(e -> {
                Optional<Lantai> lantai = lantaiRepository.findById(e.getIdLantai());
                Optional<Kiosk> kiosk = kioskRepository.findById(e.getIdKiosk());
                Optional<SimpleLayananDTO> layanan = layananService.findOneSimple(e.getIdLayanan());
                SimpleSubLayananDTO simpleSubLayanan = new SimpleSubLayananDTO();
                simpleSubLayanan.setKodeLayanan(e.getKodeLayanan());
                simpleSubLayanan.setNamaLayanan(e.getNamaLayanan());
                simpleSubLayanan.setKuota(e.getKuota());
                simpleSubLayanan.setId(e.getId());
                simpleSubLayanan.setSk(e.getSk());
                simpleSubLayanan.setIdLayanan(e.getIdLayanan());
                simpleSubLayanan.setIdKiosk(e.getIdKiosk());
                simpleSubLayanan.setIdLantai(e.getIdLantai());
                simpleSubLayanan.setJamAkhir(e.getJamAkhir());
                simpleSubLayanan.setJamAwal(e.getJamAwal());
                simpleSubLayanan.setDurasi(e.getDurasi());
                simpleSubLayanan.setStatus(e.getStatus());
                simpleSubLayanan.setPrefixSuara(e.getPrefixSuara());
                simpleSubLayanan.setLayananOnline(e.isLayananOnline());
                simpleSubLayanan.setCreateUserId(e.getCreateUserId());
                simpleSubLayanan.setCreateDate(e.getCreateDate());
                simpleSubLayanan.setCreateDateTime(e.getCreateDateTime());
                simpleSubLayanan.setLastModificationUserId(e.getLastModificationUserId());
                simpleSubLayanan.setLastModificationDate(e.getLastModificationDate());
                simpleSubLayanan.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleSubLayanan.setLantai(lantai);
                simpleSubLayanan.setKiosk(kiosk);
                simpleSubLayanan.setLayanan(layanan);
                return simpleSubLayanan;
            });
    }

    @Override
    public Page<SimpleSubLayananDTO> getSimpleLayananAktif(Pageable pageable, String status) {
        log.debug("Request to get all SubLayanans by status",status);
        return subLayananRepository.findAllByStatus(pageable,status)
            .map(subLayananMapper::toDto).map(e -> {
                Optional<Lantai> lantai = lantaiRepository.findById(e.getIdLantai());
                Optional<Kiosk> kiosk = kioskRepository.findById(e.getIdKiosk());
                Optional<SimpleLayananDTO> layanan = layananService.findOneSimple(e.getIdLayanan());
                SimpleSubLayananDTO simpleSubLayanan = new SimpleSubLayananDTO();
                simpleSubLayanan.setKodeLayanan(e.getKodeLayanan());
                simpleSubLayanan.setNamaLayanan(e.getNamaLayanan());
                simpleSubLayanan.setKuota(e.getKuota());
                simpleSubLayanan.setId(e.getId());
                simpleSubLayanan.setSk(e.getSk());
                simpleSubLayanan.setIdLayanan(e.getIdLayanan());
                simpleSubLayanan.setIdKiosk(e.getIdKiosk());
                simpleSubLayanan.setIdLantai(e.getIdLantai());
                simpleSubLayanan.setJamAkhir(e.getJamAkhir());
                simpleSubLayanan.setJamAwal(e.getJamAwal());
                simpleSubLayanan.setDurasi(e.getDurasi());
                simpleSubLayanan.setStatus(e.getStatus());
                simpleSubLayanan.setPrefixSuara(e.getPrefixSuara());
                simpleSubLayanan.setLayananOnline(e.isLayananOnline());
                simpleSubLayanan.setCreateUserId(e.getCreateUserId());
                simpleSubLayanan.setCreateDate(e.getCreateDate());
                simpleSubLayanan.setCreateDateTime(e.getCreateDateTime());
                simpleSubLayanan.setLastModificationUserId(e.getLastModificationUserId());
                simpleSubLayanan.setLastModificationDate(e.getLastModificationDate());
                simpleSubLayanan.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleSubLayanan.setLantai(lantai);
                simpleSubLayanan.setKiosk(kiosk);
                simpleSubLayanan.setLayanan(layanan);
                return simpleSubLayanan;
            });
    }

    @Override
    public Optional<SimpleSubLayananDTO> findSimpleOne(Long id) {
        log.debug("Request to get all SubLayanans");
        return subLayananRepository.findById(id)
            .map(subLayananMapper::toDto).map(e -> {
                Optional<Lantai> lantai = lantaiRepository.findById(e.getIdLantai());
                Optional<Kiosk> kiosk = kioskRepository.findById(e.getIdKiosk());
                Optional<SimpleLayananDTO> layanan = layananService.findOneSimple(e.getIdLayanan());
                SimpleSubLayananDTO simpleSubLayanan = new SimpleSubLayananDTO();
                simpleSubLayanan.setKodeLayanan(e.getKodeLayanan());
                simpleSubLayanan.setNamaLayanan(e.getNamaLayanan());
                simpleSubLayanan.setKuota(e.getKuota());
                simpleSubLayanan.setId(e.getId());
                simpleSubLayanan.setSk(e.getSk());
                simpleSubLayanan.setIdLayanan(e.getIdLayanan());
                simpleSubLayanan.setIdKiosk(e.getIdKiosk());
                simpleSubLayanan.setIdLantai(e.getIdLantai());
                simpleSubLayanan.setJamAkhir(e.getJamAkhir());
                simpleSubLayanan.setJamAwal(e.getJamAwal());
                simpleSubLayanan.setDurasi(e.getDurasi());
                simpleSubLayanan.setStatus(e.getStatus());
                simpleSubLayanan.setPrefixSuara(e.getPrefixSuara());
                simpleSubLayanan.setLayananOnline(e.isLayananOnline());
                simpleSubLayanan.setCreateUserId(e.getCreateUserId());
                simpleSubLayanan.setCreateDate(e.getCreateDate());
                simpleSubLayanan.setCreateDateTime(e.getCreateDateTime());
                simpleSubLayanan.setLastModificationUserId(e.getLastModificationUserId());
                simpleSubLayanan.setLastModificationDate(e.getLastModificationDate());
                simpleSubLayanan.setLastModificationDateTime(e.getLastModificationDateTime());
                simpleSubLayanan.setLantai(lantai);
                simpleSubLayanan.setKiosk(kiosk);
                simpleSubLayanan.setLayanan(layanan);
                return simpleSubLayanan;
            });
    }

}
