package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.PelayananLoketDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.PelayananLoket}.
 */
public interface PelayananLoketService {

    /**
     * Save a pelayananLoket.
     *
     * @param pelayananLoketDTO the entity to save.
     * @return the persisted entity.
     */
    PelayananLoketDTO save(PelayananLoketDTO pelayananLoketDTO);

    /**
     * Get all the pelayananLokets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PelayananLoketDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pelayananLoket.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PelayananLoketDTO> findOne(Long id);

    /**
     * Delete the "id" pelayananLoket.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
