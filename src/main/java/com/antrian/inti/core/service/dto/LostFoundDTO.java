package com.antrian.inti.core.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class LostFoundDTO implements Serializable {

    String namaLayanan;
    Long idSubLayanan;
    String namaLoket;
    Long idLoket;
    String prefix;
    Long noUrut;
    Integer jumlahPanggilan;

    public String getNamaLayanan() {
        return namaLayanan;
    }

    public void setNamaLayanan(String namaLayanan) {
        this.namaLayanan = namaLayanan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getNamaLoket() {
        return namaLoket;
    }

    public void setNamaLoket(String namaLoket) {
        this.namaLoket = namaLoket;
    }

    public Long getIdLoket() {
        return idLoket;
    }

    public void setIdLoket(Long idLoket) {
        this.idLoket = idLoket;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Long getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(Long noUrut) {
        this.noUrut = noUrut;
    }

    public Integer getJumlahPanggilan() {
        return jumlahPanggilan;
    }

    public void setJumlahPanggilan(Integer jumlahPanggilan) {
        this.jumlahPanggilan = jumlahPanggilan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LostFoundDTO that = (LostFoundDTO) o;
        return Objects.equals(namaLayanan, that.namaLayanan) &&
            Objects.equals(idSubLayanan, that.idSubLayanan) &&
            Objects.equals(namaLoket, that.namaLoket) &&
            Objects.equals(idLoket, that.idLoket) &&
            Objects.equals(prefix, that.prefix) &&
            Objects.equals(noUrut, that.noUrut) &&
            Objects.equals(jumlahPanggilan, that.jumlahPanggilan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namaLayanan, idSubLayanan, namaLoket, idLoket, prefix, noUrut, jumlahPanggilan);
    }

    @Override
    public String toString() {
        return "LostFoundDTO{" +
            "namaLayanan='" + namaLayanan + '\'' +
            ", idSubLayanan=" + idSubLayanan +
            ", namaLoket='" + namaLoket + '\'' +
            ", idLoket=" + idLoket +
            ", prefix='" + prefix + '\'' +
            ", noUrut=" + noUrut +
            ", jumlahPanggilan=" + jumlahPanggilan +
            '}';
    }
}
