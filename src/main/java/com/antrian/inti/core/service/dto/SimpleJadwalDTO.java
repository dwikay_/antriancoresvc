package com.antrian.inti.core.service.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SimpleJadwalDTO implements Serializable {

    private Long id;
    private Long idSubLayanan;
    List<JadwalDetilDTO> jadwalDetilDTOList;
    Optional<SimpleSubLayananDTO> subLayananDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public List<JadwalDetilDTO> getJadwalDetilDTOList() {
        return jadwalDetilDTOList;
    }

    public void setJadwalDetilDTOList(List<JadwalDetilDTO> jadwalDetilDTOList) {
        this.jadwalDetilDTOList = jadwalDetilDTOList;
    }

    public Optional<SimpleSubLayananDTO> getSubLayananDTO() {
        return subLayananDTO;
    }

    public void setSubLayananDTO(Optional<SimpleSubLayananDTO> subLayananDTO) {
        this.subLayananDTO = subLayananDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleJadwalDTO that = (SimpleJadwalDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(idSubLayanan, that.idSubLayanan) &&
            Objects.equals(jadwalDetilDTOList, that.jadwalDetilDTOList) &&
            Objects.equals(subLayananDTO, that.subLayananDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idSubLayanan, jadwalDetilDTOList, subLayananDTO);
    }

    @Override
    public String toString() {
        return "SimpleJadwalDTO{" +
            "id=" + id +
            ", idSubLayanan=" + idSubLayanan +
            ", jadwalDetilDTOList=" + jadwalDetilDTOList +
            ", subLayananDTO=" + subLayananDTO +
            '}';
    }
}
