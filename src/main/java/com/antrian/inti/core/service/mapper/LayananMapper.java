package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.LayananDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Layanan} and its DTO {@link LayananDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LayananMapper extends EntityMapper<LayananDTO, Layanan> {



    default Layanan fromId(Long id) {
        if (id == null) {
            return null;
        }
        Layanan layanan = new Layanan();
        layanan.setId(id);
        return layanan;
    }
}
