package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.ProsedurDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Prosedur} and its DTO {@link ProsedurDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProsedurMapper extends EntityMapper<ProsedurDTO, Prosedur> {



    default Prosedur fromId(Long id) {
        if (id == null) {
            return null;
        }
        Prosedur prosedur = new Prosedur();
        prosedur.setId(id);
        return prosedur;
    }
}
