package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.LantaiDTO;

import com.antrian.inti.core.service.dto.SimpleLantaiDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.Lantai}.
 */
public interface LantaiService {

    /**
     * Save a lantai.
     *
     * @param lantaiDTO the entity to save.
     * @return the persisted entity.
     */
    LantaiDTO save(LantaiDTO lantaiDTO);

    /**
     * Get all the lantais.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LantaiDTO> findAll(Pageable pageable);

    /**
     * Get the "id" lantai.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LantaiDTO> findOne(Long id);

    /**
     * Delete the "id" lantai.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
