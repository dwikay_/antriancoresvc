package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.DataAntrian} entity.
 */
public class DataAntrianDTO implements Serializable {

    private Long id;

    private LocalDate tanggal;

    private Long noUrut;

    private Boolean statusPanggilan;

    private Long idSubLayanan;

    private String layanan;

    private Long loket;

    private String prefixSuara;

    private Long lantai;

    private Boolean statusCurrent;

    private String kodeKonfirmasi;

    @Lob
    private byte[] imageWebcam;

    private String imageWebcamContentType;

    private Boolean layananOnline;

    private String durasi;

    private String sisaDurasi;

    private Boolean statusKonseling;

    private String tanggalWaktu;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Long getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(Long noUrut) {
        this.noUrut = noUrut;
    }

    public Boolean isStatusPanggilan() {
        return statusPanggilan;
    }

    public void setStatusPanggilan(Boolean statusPanggilan) {
        this.statusPanggilan = statusPanggilan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public Long getLoket() {
        return loket;
    }

    public void setLoket(Long loket) {
        this.loket = loket;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    public Long getLantai() {
        return lantai;
    }

    public void setLantai(Long lantai) {
        this.lantai = lantai;
    }

    public Boolean isStatusCurrent() {
        return statusCurrent;
    }

    public void setStatusCurrent(Boolean statusCurrent) {
        this.statusCurrent = statusCurrent;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public byte[] getImageWebcam() {
        return imageWebcam;
    }

    public void setImageWebcam(byte[] imageWebcam) {
        this.imageWebcam = imageWebcam;
    }

    public String getImageWebcamContentType() {
        return imageWebcamContentType;
    }

    public void setImageWebcamContentType(String imageWebcamContentType) {
        this.imageWebcamContentType = imageWebcamContentType;
    }

    public Boolean isLayananOnline() {
        return layananOnline;
    }

    public void setLayananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getSisaDurasi() {
        return sisaDurasi;
    }

    public void setSisaDurasi(String sisaDurasi) {
        this.sisaDurasi = sisaDurasi;
    }

    public Boolean isStatusKonseling() {
        return statusKonseling;
    }

    public void setStatusKonseling(Boolean statusKonseling) {
        this.statusKonseling = statusKonseling;
    }

    public String getTanggalWaktu() {
        return tanggalWaktu;
    }

    public void setTanggalWaktu(String tanggalWaktu) {
        this.tanggalWaktu = tanggalWaktu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DataAntrianDTO dataAntrianDTO = (DataAntrianDTO) o;
        if (dataAntrianDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataAntrianDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataAntrianDTO{" +
            "id=" + getId() +
            ", tanggal='" + getTanggal() + "'" +
            ", noUrut=" + getNoUrut() +
            ", statusPanggilan='" + isStatusPanggilan() + "'" +
            ", idSubLayanan=" + getIdSubLayanan() +
            ", layanan='" + getLayanan() + "'" +
            ", loket=" + getLoket() +
            ", prefixSuara='" + getPrefixSuara() + "'" +
            ", lantai=" + getLantai() +
            ", statusCurrent='" + isStatusCurrent() + "'" +
            ", kodeKonfirmasi='" + getKodeKonfirmasi() + "'" +
            ", imageWebcam='" + getImageWebcam() + "'" +
            ", layananOnline='" + isLayananOnline() + "'" +
            ", durasi='" + getDurasi() + "'" +
            ", sisaDurasi='" + getSisaDurasi() + "'" +
            ", statusKonseling='" + isStatusKonseling() + "'" +
            ", tanggalWaktu='" + getTanggalWaktu() + "'" +
            "}";
    }
}
