package com.antrian.inti.core.service;

import com.antrian.inti.core.service.dto.PendaftaranDumyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.antrian.inti.core.domain.PendaftaranDumy}.
 */
public interface PendaftaranDumyService {

    /**
     * Save a pendaftaranDumy.
     *
     * @param pendaftaranDumyDTO the entity to save.
     * @return the persisted entity.
     */
    PendaftaranDumyDTO save(PendaftaranDumyDTO pendaftaranDumyDTO);

    /**
     * Get all the pendaftaranDumies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PendaftaranDumyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" pendaftaranDumy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PendaftaranDumyDTO> findOne(Long id);

    /**
     * Delete the "id" pendaftaranDumy.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
