package com.antrian.inti.core.service.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class SimpleLantaiDTO implements Serializable {

    private Long id;
    private Long lantai;
    private String deskripsi;
    @Size(max = 3)
    private String status;
    private Long createUserId;
    private LocalDate createDate;
    private ZonedDateTime createDateTime;
    private Long lastModificationUserId;
    private LocalDate lastModificationDate;
    private ZonedDateTime lastModificationDateTime;
    private List<SimpleLayananDTO> layanan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLantai() {
        return lantai;
    }

    public void setLantai(Long lantai) {
        this.lantai = lantai;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public List<SimpleLayananDTO> getLayanan() {
        return layanan;
    }

    public void setLayanan(List<SimpleLayananDTO> layanan) {
        this.layanan = layanan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleLantaiDTO that = (SimpleLantaiDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(lantai, that.lantai) &&
            Objects.equals(deskripsi, that.deskripsi) &&
            Objects.equals(status, that.status) &&
            Objects.equals(createUserId, that.createUserId) &&
            Objects.equals(createDate, that.createDate) &&
            Objects.equals(createDateTime, that.createDateTime) &&
            Objects.equals(lastModificationUserId, that.lastModificationUserId) &&
            Objects.equals(lastModificationDate, that.lastModificationDate) &&
            Objects.equals(lastModificationDateTime, that.lastModificationDateTime) &&
            Objects.equals(layanan, that.layanan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lantai, deskripsi, status, createUserId, createDate, createDateTime, lastModificationUserId, lastModificationDate, lastModificationDateTime, layanan);
    }

    @Override
    public String toString() {
        return "SimpleLantaiDTO{" +
            "id=" + id +
            ", lantai=" + lantai +
            ", deskripsi='" + deskripsi + '\'' +
            ", status='" + status + '\'' +
            ", createUserId=" + createUserId +
            ", createDate=" + createDate +
            ", createDateTime=" + createDateTime +
            ", lastModificationUserId=" + lastModificationUserId +
            ", lastModificationDate=" + lastModificationDate +
            ", lastModificationDateTime=" + lastModificationDateTime +
            ", layanan=" + layanan +
            '}';
    }
}
