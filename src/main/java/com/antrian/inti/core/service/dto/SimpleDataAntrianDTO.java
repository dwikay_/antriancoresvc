package com.antrian.inti.core.service.dto;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

public class SimpleDataAntrianDTO implements Serializable {

    private Long id;
    private LocalDate tanggal;
    private Long noUrut;
    private Boolean statusPanggilan;
    private Long idSubLayanan;
    private String layanan;
    private Long loket;
    private String prefixSuara;
    private Long lantai;
    private Boolean statusCurrent;
    private String kodeKonfirmasi;
    private Boolean layananOnline;
    private String durasi;
    private String sisaDurasi;
    private Boolean statusKonseling;
    private String urlWebcam;
    @Lob
    private byte[] imageWebcam;
    private String imageWebcamContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Long getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(Long noUrut) {
        this.noUrut = noUrut;
    }

    public Boolean getStatusPanggilan() {
        return statusPanggilan;
    }

    public void setStatusPanggilan(Boolean statusPanggilan) {
        this.statusPanggilan = statusPanggilan;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public Long getLoket() {
        return loket;
    }

    public void setLoket(Long loket) {
        this.loket = loket;
    }

    public String getPrefixSuara() {
        return prefixSuara;
    }

    public void setPrefixSuara(String prefixSuara) {
        this.prefixSuara = prefixSuara;
    }

    public Long getLantai() {
        return lantai;
    }

    public void setLantai(Long lantai) {
        this.lantai = lantai;
    }

    public Boolean getStatusCurrent() {
        return statusCurrent;
    }

    public void setStatusCurrent(Boolean statusCurrent) {
        this.statusCurrent = statusCurrent;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public Boolean getLayananOnline() {
        return layananOnline;
    }

    public void setLayananOnline(Boolean layananOnline) {
        this.layananOnline = layananOnline;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getSisaDurasi() {
        return sisaDurasi;
    }

    public void setSisaDurasi(String sisaDurasi) {
        this.sisaDurasi = sisaDurasi;
    }

    public Boolean getStatusKonseling() {
        return statusKonseling;
    }

    public void setStatusKonseling(Boolean statusKonseling) {
        this.statusKonseling = statusKonseling;
    }

    public String getUrlWebcam() {
        return urlWebcam;
    }

    public void setUrlWebcam(String urlWebcam) {
        this.urlWebcam = urlWebcam;
    }

    public byte[] getImageWebcam() {
        return imageWebcam;
    }

    public void setImageWebcam(byte[] imageWebcam) {
        this.imageWebcam = imageWebcam;
    }

    public String getImageWebcamContentType() {
        return imageWebcamContentType;
    }

    public void setImageWebcamContentType(String imageWebcamContentType) {
        this.imageWebcamContentType = imageWebcamContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleDataAntrianDTO that = (SimpleDataAntrianDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(tanggal, that.tanggal) &&
            Objects.equals(noUrut, that.noUrut) &&
            Objects.equals(statusPanggilan, that.statusPanggilan) &&
            Objects.equals(idSubLayanan, that.idSubLayanan) &&
            Objects.equals(layanan, that.layanan) &&
            Objects.equals(loket, that.loket) &&
            Objects.equals(prefixSuara, that.prefixSuara) &&
            Objects.equals(lantai, that.lantai) &&
            Objects.equals(statusCurrent, that.statusCurrent) &&
            Objects.equals(kodeKonfirmasi, that.kodeKonfirmasi) &&
            Objects.equals(layananOnline, that.layananOnline) &&
            Objects.equals(durasi, that.durasi) &&
            Objects.equals(sisaDurasi, that.sisaDurasi) &&
            Objects.equals(statusKonseling, that.statusKonseling) &&
            Objects.equals(urlWebcam, that.urlWebcam) &&
            Arrays.equals(imageWebcam, that.imageWebcam) &&
            Objects.equals(imageWebcamContentType, that.imageWebcamContentType);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, tanggal, noUrut, statusPanggilan, idSubLayanan, layanan, loket, prefixSuara, lantai, statusCurrent, kodeKonfirmasi, layananOnline, durasi, sisaDurasi, statusKonseling, urlWebcam, imageWebcamContentType);
        result = 31 * result + Arrays.hashCode(imageWebcam);
        return result;
    }

    @Override
    public String toString() {
        return "SimpleDataAntrianDTO{" +
            "id=" + id +
            ", tanggal=" + tanggal +
            ", noUrut=" + noUrut +
            ", statusPanggilan=" + statusPanggilan +
            ", idSubLayanan=" + idSubLayanan +
            ", layanan='" + layanan + '\'' +
            ", loket=" + loket +
            ", prefixSuara='" + prefixSuara + '\'' +
            ", lantai=" + lantai +
            ", statusCurrent=" + statusCurrent +
            ", kodeKonfirmasi='" + kodeKonfirmasi + '\'' +
            ", layananOnline=" + layananOnline +
            ", durasi='" + durasi + '\'' +
            ", sisaDurasi='" + sisaDurasi + '\'' +
            ", statusKonseling=" + statusKonseling +
            ", urlWebcam='" + urlWebcam + '\'' +
            ", imageWebcam=" + Arrays.toString(imageWebcam) +
            ", imageWebcamContentType='" + imageWebcamContentType + '\'' +
            '}';
    }
}
