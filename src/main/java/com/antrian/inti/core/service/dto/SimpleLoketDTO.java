package com.antrian.inti.core.service.dto;

import com.antrian.inti.core.domain.SubLayanan;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SimpleLoketDTO implements Serializable {

    private Long id;

    private String username;

    private String password;

    @Size(max = 3)
    private String status;

    private String deskripsi;

    private Long createUserId;

    private LocalDate createDate;

    private ZonedDateTime createDateTime;

    private Long lastModificationUserId;

    private LocalDate lastModificationDate;

    private ZonedDateTime lastModificationDateTime;

    private Long idSubLayanan;

    private String ipController;

    private Optional<SubLayananDTO> layanan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Long getLastModificationUserId() {
        return lastModificationUserId;
    }

    public void setLastModificationUserId(Long lastModificationUserId) {
        this.lastModificationUserId = lastModificationUserId;
    }

    public LocalDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(LocalDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public ZonedDateTime getLastModificationDateTime() {
        return lastModificationDateTime;
    }

    public void setLastModificationDateTime(ZonedDateTime lastModificationDateTime) {
        this.lastModificationDateTime = lastModificationDateTime;
    }

    public Long getIdSubLayanan() {
        return idSubLayanan;
    }

    public void setIdSubLayanan(Long idSubLayanan) {
        this.idSubLayanan = idSubLayanan;
    }

    public String getIpController() {
        return ipController;
    }

    public void setIpController(String ipController) {
        this.ipController = ipController;
    }

    public Optional<SubLayananDTO> getLayanan() {
        return layanan;
    }

    public void setLayanan(Optional<SubLayananDTO> layanan) {
        this.layanan = layanan;
    }

    @Override
    public String toString() {
        return "SimpleLoketDTO{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", status='" + status + '\'' +
            ", deskripsi='" + deskripsi + '\'' +
            ", createUserId=" + createUserId +
            ", createDate=" + createDate +
            ", createDateTime=" + createDateTime +
            ", lastModificationUserId=" + lastModificationUserId +
            ", lastModificationDate=" + lastModificationDate +
            ", lastModificationDateTime=" + lastModificationDateTime +
            ", idSubLayanan=" + idSubLayanan +
            ", ipController='" + ipController + '\'' +
            ", layanan=" + layanan +
            '}';
    }
}
