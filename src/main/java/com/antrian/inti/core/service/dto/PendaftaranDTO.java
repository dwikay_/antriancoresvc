package com.antrian.inti.core.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.antrian.inti.core.domain.Pendaftaran} entity.
 */
public class PendaftaranDTO implements Serializable {

    private Long id;

    private String namaBadanUsaha;

    private String namaDireksi;

    private String nikDireksi;

    private String email;

    private LocalDate tanggalPengambilan;

    private String kodeKonfirmasi;

    private Long idKloter;

    private Boolean pengambilanStruk;

    private String jamKloter;

    @Lob
    private String pertanyaan;

    @Lob
    private String jawaban;

    private String tipeKtp;

    private String statusTiket;

    private Boolean typeVirtual;

    private String jamLayanan;

    private String noTiket;

    private Boolean agent;

    private UUID idDaftar;

    private String namaPerseorangan;

    private String nikPerseorangan;

    private String instansi;

    private String unit;

    private String jabatan;

    private String namaPejabat;

    private String untukLayanan;

    @Lob
    private String jwtToken;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaBadanUsaha() {
        return namaBadanUsaha;
    }

    public void setNamaBadanUsaha(String namaBadanUsaha) {
        this.namaBadanUsaha = namaBadanUsaha;
    }

    public String getNamaDireksi() {
        return namaDireksi;
    }

    public void setNamaDireksi(String namaDireksi) {
        this.namaDireksi = namaDireksi;
    }

    public String getNikDireksi() {
        return nikDireksi;
    }

    public void setNikDireksi(String nikDireksi) {
        this.nikDireksi = nikDireksi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getTanggalPengambilan() {
        return tanggalPengambilan;
    }

    public void setTanggalPengambilan(LocalDate tanggalPengambilan) {
        this.tanggalPengambilan = tanggalPengambilan;
    }

    public String getKodeKonfirmasi() {
        return kodeKonfirmasi;
    }

    public void setKodeKonfirmasi(String kodeKonfirmasi) {
        this.kodeKonfirmasi = kodeKonfirmasi;
    }

    public Long getIdKloter() {
        return idKloter;
    }

    public void setIdKloter(Long idKloter) {
        this.idKloter = idKloter;
    }

    public Boolean isPengambilanStruk() {
        return pengambilanStruk;
    }

    public void setPengambilanStruk(Boolean pengambilanStruk) {
        this.pengambilanStruk = pengambilanStruk;
    }

    public String getJamKloter() {
        return jamKloter;
    }

    public void setJamKloter(String jamKloter) {
        this.jamKloter = jamKloter;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }

    public String getTipeKtp() {
        return tipeKtp;
    }

    public void setTipeKtp(String tipeKtp) {
        this.tipeKtp = tipeKtp;
    }

    public String getStatusTiket() {
        return statusTiket;
    }

    public void setStatusTiket(String statusTiket) {
        this.statusTiket = statusTiket;
    }

    public Boolean isTypeVirtual() {
        return typeVirtual;
    }

    public void setTypeVirtual(Boolean typeVirtual) {
        this.typeVirtual = typeVirtual;
    }

    public String getJamLayanan() {
        return jamLayanan;
    }

    public void setJamLayanan(String jamLayanan) {
        this.jamLayanan = jamLayanan;
    }

    public String getNoTiket() {
        return noTiket;
    }

    public void setNoTiket(String noTiket) {
        this.noTiket = noTiket;
    }

    public Boolean isAgent() {
        return agent;
    }

    public void setAgent(Boolean agent) {
        this.agent = agent;
    }

    public UUID getIdDaftar() {
        return idDaftar;
    }

    public void setIdDaftar(UUID idDaftar) {
        this.idDaftar = idDaftar;
    }

    public String getNamaPerseorangan() {
        return namaPerseorangan;
    }

    public void setNamaPerseorangan(String namaPerseorangan) {
        this.namaPerseorangan = namaPerseorangan;
    }

    public String getNikPerseorangan() {
        return nikPerseorangan;
    }

    public void setNikPerseorangan(String nikPerseorangan) {
        this.nikPerseorangan = nikPerseorangan;
    }

    public String getInstansi() {
        return instansi;
    }

    public void setInstansi(String instansi) {
        this.instansi = instansi;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getNamaPejabat() {
        return namaPejabat;
    }

    public void setNamaPejabat(String namaPejabat) {
        this.namaPejabat = namaPejabat;
    }

    public String getUntukLayanan() {
        return untukLayanan;
    }

    public void setUntukLayanan(String untukLayanan) {
        this.untukLayanan = untukLayanan;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PendaftaranDTO pendaftaranDTO = (PendaftaranDTO) o;
        if (pendaftaranDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pendaftaranDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PendaftaranDTO{" +
            "id=" + getId() +
            ", namaBadanUsaha='" + getNamaBadanUsaha() + "'" +
            ", namaDireksi='" + getNamaDireksi() + "'" +
            ", nikDireksi='" + getNikDireksi() + "'" +
            ", email='" + getEmail() + "'" +
            ", tanggalPengambilan='" + getTanggalPengambilan() + "'" +
            ", kodeKonfirmasi='" + getKodeKonfirmasi() + "'" +
            ", idKloter=" + getIdKloter() +
            ", pengambilanStruk='" + isPengambilanStruk() + "'" +
            ", jamKloter='" + getJamKloter() + "'" +
            ", pertanyaan='" + getPertanyaan() + "'" +
            ", jawaban='" + getJawaban() + "'" +
            ", tipeKtp='" + getTipeKtp() + "'" +
            ", statusTiket='" + getStatusTiket() + "'" +
            ", typeVirtual='" + isTypeVirtual() + "'" +
            ", jamLayanan='" + getJamLayanan() + "'" +
            ", noTiket='" + getNoTiket() + "'" +
            ", agent='" + isAgent() + "'" +
            ", idDaftar='" + getIdDaftar() + "'" +
            ", namaPerseorangan='" + getNamaPerseorangan() + "'" +
            ", nikPerseorangan='" + getNikPerseorangan() + "'" +
            ", instansi='" + getInstansi() + "'" +
            ", unit='" + getUnit() + "'" +
            ", jabatan='" + getJabatan() + "'" +
            ", namaPejabat='" + getNamaPejabat() + "'" +
            ", untukLayanan='" + getUntukLayanan() + "'" +
            ", jwtToken='" + getJwtToken() + "'" +
            "}";
    }
}
