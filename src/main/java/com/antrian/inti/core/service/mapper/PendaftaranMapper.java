package com.antrian.inti.core.service.mapper;

import com.antrian.inti.core.domain.*;
import com.antrian.inti.core.service.dto.PendaftaranDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pendaftaran} and its DTO {@link PendaftaranDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PendaftaranMapper extends EntityMapper<PendaftaranDTO, Pendaftaran> {



    default Pendaftaran fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pendaftaran pendaftaran = new Pendaftaran();
        pendaftaran.setId(id);
        return pendaftaran;
    }
}
