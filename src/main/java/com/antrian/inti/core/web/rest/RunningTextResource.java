package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.RunningTextService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.RunningTextDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.RunningText}.
 */
@RestController
@RequestMapping("/api")
public class RunningTextResource {

    private final Logger log = LoggerFactory.getLogger(RunningTextResource.class);

    private static final String ENTITY_NAME = "antriancoresvcRunningText";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RunningTextService runningTextService;

    public RunningTextResource(RunningTextService runningTextService) {
        this.runningTextService = runningTextService;
    }

    /**
     * {@code POST  /running-texts} : Create a new runningText.
     *
     * @param runningTextDTO the runningTextDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new runningTextDTO, or with status {@code 400 (Bad Request)} if the runningText has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/running-texts")
    public ResponseEntity<RunningTextDTO> createRunningText(@Valid @RequestBody RunningTextDTO runningTextDTO) throws URISyntaxException {
        log.debug("REST request to save RunningText : {}", runningTextDTO);
        if (runningTextDTO.getId() != null) {
            throw new BadRequestAlertException("A new runningText cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RunningTextDTO result = runningTextService.save(runningTextDTO);
        return ResponseEntity.created(new URI("/api/running-texts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /running-texts} : Updates an existing runningText.
     *
     * @param runningTextDTO the runningTextDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated runningTextDTO,
     * or with status {@code 400 (Bad Request)} if the runningTextDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the runningTextDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/running-texts")
    public ResponseEntity<RunningTextDTO> updateRunningText(@Valid @RequestBody RunningTextDTO runningTextDTO) throws URISyntaxException {
        log.debug("REST request to update RunningText : {}", runningTextDTO);
        if (runningTextDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RunningTextDTO result = runningTextService.save(runningTextDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, runningTextDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /running-texts} : get all the runningTexts.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of runningTexts in body.
     */
    @GetMapping("/running-texts")
    public ResponseEntity<List<RunningTextDTO>> getAllRunningTexts(Pageable pageable) {
        log.debug("REST request to get a page of RunningTexts");
        Page<RunningTextDTO> page = runningTextService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /running-texts/:id} : get the "id" runningText.
     *
     * @param id the id of the runningTextDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the runningTextDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/running-texts/{id}")
    public ResponseEntity<RunningTextDTO> getRunningText(@PathVariable Long id) {
        log.debug("REST request to get RunningText : {}", id);
        Optional<RunningTextDTO> runningTextDTO = runningTextService.findOne(id);
        return ResponseUtil.wrapOrNotFound(runningTextDTO);
    }

    /**
     * {@code DELETE  /running-texts/:id} : delete the "id" runningText.
     *
     * @param id the id of the runningTextDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/running-texts/{id}")
    public ResponseEntity<Void> deleteRunningText(@PathVariable Long id) {
        log.debug("REST request to delete RunningText : {}", id);
        runningTextService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
