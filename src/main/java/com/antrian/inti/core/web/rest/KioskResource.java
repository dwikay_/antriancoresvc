package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.KioskService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.KioskDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Kiosk}.
 */
@RestController
@RequestMapping("/api")
public class KioskResource {

    private final Logger log = LoggerFactory.getLogger(KioskResource.class);

    private static final String ENTITY_NAME = "antriancoresvcKiosk";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KioskService kioskService;

    public KioskResource(KioskService kioskService) {
        this.kioskService = kioskService;
    }

    /**
     * {@code POST  /kiosks} : Create a new kiosk.
     *
     * @param kioskDTO the kioskDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kioskDTO, or with status {@code 400 (Bad Request)} if the kiosk has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kiosks")
    public ResponseEntity<KioskDTO> createKiosk(@Valid @RequestBody KioskDTO kioskDTO) throws URISyntaxException {
        log.debug("REST request to save Kiosk : {}", kioskDTO);
        if (kioskDTO.getId() != null) {
            throw new BadRequestAlertException("A new kiosk cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KioskDTO result = kioskService.save(kioskDTO);
        return ResponseEntity.created(new URI("/api/kiosks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kiosks} : Updates an existing kiosk.
     *
     * @param kioskDTO the kioskDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kioskDTO,
     * or with status {@code 400 (Bad Request)} if the kioskDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kioskDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kiosks")
    public ResponseEntity<KioskDTO> updateKiosk(@Valid @RequestBody KioskDTO kioskDTO) throws URISyntaxException {
        log.debug("REST request to update Kiosk : {}", kioskDTO);
        if (kioskDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KioskDTO result = kioskService.save(kioskDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kioskDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /kiosks} : get all the kiosks.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kiosks in body.
     */
    @GetMapping("/kiosks")
    public ResponseEntity<List<KioskDTO>> getAllKiosks(Pageable pageable) {
        log.debug("REST request to get a page of Kiosks");
        Page<KioskDTO> page = kioskService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kiosks/:id} : get the "id" kiosk.
     *
     * @param id the id of the kioskDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kioskDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kiosks/{id}")
    public ResponseEntity<KioskDTO> getKiosk(@PathVariable Long id) {
        log.debug("REST request to get Kiosk : {}", id);
        Optional<KioskDTO> kioskDTO = kioskService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kioskDTO);
    }

    /**
     * {@code DELETE  /kiosks/:id} : delete the "id" kiosk.
     *
     * @param id the id of the kioskDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kiosks/{id}")
    public ResponseEntity<Void> deleteKiosk(@PathVariable Long id) {
        log.debug("REST request to delete Kiosk : {}", id);
        kioskService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
