package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.RegistrasiService;
import com.antrian.inti.core.service.dto.*;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Registrasi}.
 */
@RestController
@RequestMapping("/api")
public class RegistrasiResource {

    private final Logger log = LoggerFactory.getLogger(RegistrasiResource.class);

    private static final String ENTITY_NAME = "antriancoresvcRegistrasi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegistrasiService registrasiService;

    public RegistrasiResource(RegistrasiService registrasiService) {
        this.registrasiService = registrasiService;
    }

    /**
     * {@code POST  /registrasis} : Create a new registrasi.
     *
     * @param registrasiDTO the registrasiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new registrasiDTO, or with status {@code 400 (Bad Request)} if the registrasi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/registrasis")
    public ResponseEntity<RegistrasiDTO> createRegistrasi(@Valid @RequestBody RegistrasiDTO registrasiDTO) throws URISyntaxException {
        log.debug("REST request to save Registrasi : {}", registrasiDTO);
        if (registrasiDTO.getId() != null) {
            throw new BadRequestAlertException("A new registrasi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegistrasiDTO result = registrasiService.save(registrasiDTO);
        return ResponseEntity.created(new URI("/api/registrasis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /registrasis} : Updates an existing registrasi.
     *
     * @param registrasiDTO the registrasiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated registrasiDTO,
     * or with status {@code 400 (Bad Request)} if the registrasiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the registrasiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/registrasis")
    public ResponseEntity<RegistrasiDTO> updateRegistrasi(@Valid @RequestBody RegistrasiDTO registrasiDTO) throws URISyntaxException {
        log.debug("REST request to update Registrasi : {}", registrasiDTO);
        if (registrasiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RegistrasiDTO result = registrasiService.save(registrasiDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, registrasiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /registrasis} : get all the registrasis.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of registrasis in body.
     */
    @GetMapping("/registrasis")
    public ResponseEntity<List<RegistrasiDTO>> getAllRegistrasis(Pageable pageable) {
        log.debug("REST request to get a page of Registrasis");
        Page<RegistrasiDTO> page = registrasiService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /registrasis/:id} : get the "id" registrasi.
     *
     * @param id the id of the registrasiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the registrasiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/registrasis/{id}")
    public ResponseEntity<RegistrasiDTO> getRegistrasi(@PathVariable Long id) {
        log.debug("REST request to get Registrasi : {}", id);
        Optional<RegistrasiDTO> registrasiDTO = registrasiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(registrasiDTO);
    }

    /**
     * {@code DELETE  /registrasis/:id} : delete the "id" registrasi.
     *
     * @param id the id of the registrasiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/registrasis/{id}")
    public ResponseEntity<Void> deleteRegistrasi(@PathVariable Long id) {
        log.debug("REST request to delete Registrasi : {}", id);
        registrasiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /*
        Tambahan
     */

    @GetMapping("/checkingRegistrasi/{nik}/{email}/{verifikasiEmail}")
    public ResponseEntity<RegistrasiDTO> checkingRegistrasi(@PathVariable String nik, @PathVariable String email, @PathVariable boolean verifikasiEmail) {
        log.debug("REST request to get Registrasi by Nik, Email and VerifikasiEmail : {}", nik, email, verifikasiEmail);
        Optional<RegistrasiDTO> registrasiDTO = registrasiService.checkingRegistrasi(nik,email,verifikasiEmail);
        return ResponseUtil.wrapOrNotFound(registrasiDTO);
    }
    @GetMapping("/checkingAvailable/{nik}/{email}/{verifikasiEmail}/{pengambilanStruk}/{tanggalPengambilan}")
    public ResponseEntity<RegistrasiDTO> checkingAvailable(@PathVariable String nik, @PathVariable String email, @PathVariable boolean verifikasiEmail, @PathVariable boolean pengambilanStruk, @PathVariable LocalDate tanggalPengambilan) {
        log.debug("REST request to get Registrasi by Nik, Email, VerifikasiEmail, pengambilanStruk and tanggalPengambilan: {}", nik, email, verifikasiEmail, pengambilanStruk, tanggalPengambilan);
        Optional<RegistrasiDTO> registrasiDTO = registrasiService.checkingAvailable(nik, email, verifikasiEmail, pengambilanStruk, tanggalPengambilan);
        return ResponseUtil.wrapOrNotFound(registrasiDTO);
    }
    @GetMapping("/checkingVerifikasiEmail/{kodeVerifikasi}/{verifikasiEmail}")
    public ResponseEntity<SimpleRegistrasiDTO> checkingAvailable(@PathVariable String kodeVerifikasi, @PathVariable boolean verifikasiEmail) {
        log.debug("REST request to get Registrasi by kodeVerifikasi, VerifikasiEmail: {}", kodeVerifikasi, verifikasiEmail);
        Optional<SimpleRegistrasiDTO> simpleRegistrasiDTO = registrasiService.checkingVerifikasiEmail(kodeVerifikasi, verifikasiEmail);
        return ResponseUtil.wrapOrNotFound(simpleRegistrasiDTO);
    }
    @GetMapping("/verifikasi/{kodeVerifikasi}")
    public ResponseEntity<RegistrasiDTO> checkingAvailable(@PathVariable String kodeVerifikasi) {
        log.debug("REST request to get Registrasi by kodeVerifikasi: {}", kodeVerifikasi);
        Optional<RegistrasiDTO> registrasiDTO = registrasiService.verifikasi(kodeVerifikasi);
        return ResponseUtil.wrapOrNotFound(registrasiDTO);
    }
    @GetMapping("/checkingKodeKonfirmasi/{kodeKonfirmasi}/{idSubLayanan}")
    public ResponseEntity<RegistrasiDTO> checkingKodeKonfirmasi(@PathVariable String kodeKonfirmasi, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get Registrasi by kodeVerifikasi, pengambilanStruk, tanggalPengambilan: {}", kodeKonfirmasi, idSubLayanan);
        Optional<RegistrasiDTO> registrasiDTO = registrasiService.checkingKodeKonfirmasi(kodeKonfirmasi,idSubLayanan);
        return ResponseUtil.wrapOrNotFound(registrasiDTO);
    }



    @GetMapping("/simple-regstrasi")
    public ResponseEntity<List<SimpleRegistrasiDTO>> getAllSimpleRegisters(Pageable pageable) {
        log.debug("REST request to get a page of Layanans");
        Page<SimpleRegistrasiDTO> page = registrasiService.findSimpleAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-regstrasi/rangeDate/{startDate}/{endDate}")
    public ResponseEntity<List<SimpleRegistrasiDTO>> rangeDate(Pageable pageable,@PathVariable LocalDate startDate, @PathVariable LocalDate endDate) {
        log.debug("REST request to get a page of Layanans");
        Page<SimpleRegistrasiDTO> page = registrasiService.findSimpleAllrangeDate(pageable,startDate,endDate);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-regstrasi/selectByDate/{dateNow}")
    public ResponseEntity<List<SimpleRegistrasiDTO>> selectByDate(Pageable pageable,@PathVariable LocalDate dateNow) {
        log.debug("REST request to get a page of Layanans");
        Page<SimpleRegistrasiDTO> page = registrasiService.findSimpleAllselectByDate(pageable,dateNow);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-regstrasi/getDate/{limitQuota}")
    public List<HavinRegistrasiDTO> getDate(@PathVariable Long limitQuota) {
        log.debug("REST request to get a getDate with limit quota ",limitQuota);
        return registrasiService.getDate(limitQuota);
    }


    @RequestMapping(value = "/imageKtp/{kodeVerifikasi}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImageAsResponseEntity(@PathVariable String kodeVerifikasi) {
        HttpHeaders headers = new HttpHeaders();
        Optional<RegistrasiDTO> optionalLayananDTO = registrasiService.verifikasi(kodeVerifikasi);
        if(!optionalLayananDTO.isPresent())
        {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        RegistrasiDTO registrasiDTO = optionalLayananDTO.get();

        byte[] media = registrasiDTO.getKtp();
        String mediaType = registrasiDTO.getKtpContentType();

        headers.set("Content-Type", mediaType);

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);

        return responseEntity;
    }

    @GetMapping("/getTotalRegistrasi/{dateRegistrasi}/{idSubLayanan}")
    public Integer getTotalRegistrasi(@PathVariable LocalDate dateRegistrasi, @PathVariable Long idSubLayanan) {
        log.debug("REST request count all antrianSisa by dateNow and idSubLayanan : {}", dateRegistrasi,idSubLayanan);
        return registrasiService.totalRegistrasi(dateRegistrasi,idSubLayanan);
    }

    @GetMapping("/getTotalRegistrasiWithVirtual/{dateRegistrasi}/{idSubLayanan}/{typeVirtual}")
    public Integer getTotalRegistrasiWithVirtual(@PathVariable LocalDate dateRegistrasi, @PathVariable Long idSubLayanan, @PathVariable boolean typeVirtual) {
        log.debug("REST request count all antrianSisa by dateNow and idSubLayanan, typeVirtual : {}", dateRegistrasi,idSubLayanan,typeVirtual);
        return registrasiService.totalRegistrasiWithTypeVirtual(dateRegistrasi,idSubLayanan,typeVirtual);
    }

}
