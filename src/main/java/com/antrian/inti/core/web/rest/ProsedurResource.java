package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.ProsedurService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.ProsedurDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Prosedur}.
 */
@RestController
@RequestMapping("/api")
public class ProsedurResource {

    private final Logger log = LoggerFactory.getLogger(ProsedurResource.class);

    private static final String ENTITY_NAME = "antriancoresvcProsedur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProsedurService prosedurService;

    public ProsedurResource(ProsedurService prosedurService) {
        this.prosedurService = prosedurService;
    }

    /**
     * {@code POST  /prosedurs} : Create a new prosedur.
     *
     * @param prosedurDTO the prosedurDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new prosedurDTO, or with status {@code 400 (Bad Request)} if the prosedur has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/prosedurs")
    public ResponseEntity<ProsedurDTO> createProsedur(@RequestBody ProsedurDTO prosedurDTO) throws URISyntaxException {
        log.debug("REST request to save Prosedur : {}", prosedurDTO);
        if (prosedurDTO.getId() != null) {
            throw new BadRequestAlertException("A new prosedur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProsedurDTO result = prosedurService.save(prosedurDTO);
        return ResponseEntity.created(new URI("/api/prosedurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /prosedurs} : Updates an existing prosedur.
     *
     * @param prosedurDTO the prosedurDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prosedurDTO,
     * or with status {@code 400 (Bad Request)} if the prosedurDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the prosedurDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/prosedurs")
    public ResponseEntity<ProsedurDTO> updateProsedur(@RequestBody ProsedurDTO prosedurDTO) throws URISyntaxException {
        log.debug("REST request to update Prosedur : {}", prosedurDTO);
        if (prosedurDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProsedurDTO result = prosedurService.save(prosedurDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, prosedurDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /prosedurs} : get all the prosedurs.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of prosedurs in body.
     */
    @GetMapping("/prosedurs")
    public ResponseEntity<List<ProsedurDTO>> getAllProsedurs(Pageable pageable) {
        log.debug("REST request to get a page of Prosedurs");
        Page<ProsedurDTO> page = prosedurService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /prosedurs/:id} : get the "id" prosedur.
     *
     * @param id the id of the prosedurDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the prosedurDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/prosedurs/{id}")
    public ResponseEntity<ProsedurDTO> getProsedur(@PathVariable Long id) {
        log.debug("REST request to get Prosedur : {}", id);
        Optional<ProsedurDTO> prosedurDTO = prosedurService.findOne(id);
        return ResponseUtil.wrapOrNotFound(prosedurDTO);
    }

    /**
     * {@code DELETE  /prosedurs/:id} : delete the "id" prosedur.
     *
     * @param id the id of the prosedurDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/prosedurs/{id}")
    public ResponseEntity<Void> deleteProsedur(@PathVariable Long id) {
        log.debug("REST request to delete Prosedur : {}", id);
        prosedurService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
