package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.BannedService;
import com.antrian.inti.core.service.dto.RegistrasiDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.BannedDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Banned}.
 */
@RestController
@RequestMapping("/api")
public class BannedResource {

    private final Logger log = LoggerFactory.getLogger(BannedResource.class);

    private static final String ENTITY_NAME = "antriancoresvcBanned";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BannedService bannedService;

    public BannedResource(BannedService bannedService) {
        this.bannedService = bannedService;
    }

    /**
     * {@code POST  /banneds} : Create a new banned.
     *
     * @param bannedDTO the bannedDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bannedDTO, or with status {@code 400 (Bad Request)} if the banned has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/banneds")
    public ResponseEntity<BannedDTO> createBanned(@Valid @RequestBody BannedDTO bannedDTO) throws URISyntaxException {
        log.debug("REST request to save Banned : {}", bannedDTO);
        if (bannedDTO.getId() != null) {
            throw new BadRequestAlertException("A new banned cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BannedDTO result = bannedService.save(bannedDTO);
        return ResponseEntity.created(new URI("/api/banneds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /banneds} : Updates an existing banned.
     *
     * @param bannedDTO the bannedDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bannedDTO,
     * or with status {@code 400 (Bad Request)} if the bannedDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bannedDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/banneds")
    public ResponseEntity<BannedDTO> updateBanned(@Valid @RequestBody BannedDTO bannedDTO) throws URISyntaxException {
        log.debug("REST request to update Banned : {}", bannedDTO);
        if (bannedDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BannedDTO result = bannedService.save(bannedDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bannedDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /banneds} : get all the banneds.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of banneds in body.
     */
    @GetMapping("/banneds")
    public ResponseEntity<List<BannedDTO>> getAllBanneds(Pageable pageable) {
        log.debug("REST request to get a page of Banneds");
        Page<BannedDTO> page = bannedService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /banneds/:id} : get the "id" banned.
     *
     * @param id the id of the bannedDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bannedDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/banneds/{id}")
    public ResponseEntity<BannedDTO> getBanned(@PathVariable Long id) {
        log.debug("REST request to get Banned : {}", id);
        Optional<BannedDTO> bannedDTO = bannedService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bannedDTO);
    }

    /**
     * {@code DELETE  /banneds/:id} : delete the "id" banned.
     *
     * @param id the id of the bannedDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/banneds/{id}")
    public ResponseEntity<Void> deleteBanned(@PathVariable Long id) {
        log.debug("REST request to delete Banned : {}", id);
        bannedService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    /*
        Tambahan
     */
    @GetMapping("/get-blokir")
    public ResponseEntity<List<BannedDTO>> getAllBlokirs(Pageable pageable, Integer jumlah) {
        log.debug("REST request to get a page of Blokir >= ",jumlah);
        Page<BannedDTO> page = bannedService.getAllBlokirs(pageable,jumlah);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    @GetMapping("/checkingBlokir/{nik}")
    public ResponseEntity<BannedDTO> checkingBlokir(@PathVariable String nik, Integer jumlah) {
        log.debug("REST request to get Registrasi by Nik, Jumlah > : {}", nik, jumlah);
        Optional<BannedDTO> bannedDTO = bannedService.checkingRegistrasi(nik,jumlah);
        return ResponseUtil.wrapOrNotFound(bannedDTO);
    }
}
