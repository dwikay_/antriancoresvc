package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.SubLayananService;
import com.antrian.inti.core.service.dto.SimpleSubLayananDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.SubLayananDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.SubLayanan}.
 */
@RestController
@RequestMapping("/api")
public class SubLayananResource {

    private final Logger log = LoggerFactory.getLogger(SubLayananResource.class);

    private static final String ENTITY_NAME = "antriancoresvcSubLayanan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubLayananService subLayananService;

    public SubLayananResource(SubLayananService subLayananService) {
        this.subLayananService = subLayananService;
    }

    /**
     * {@code POST  /sub-layanans} : Create a new subLayanan.
     *
     * @param subLayananDTO the subLayananDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subLayananDTO, or with status {@code 400 (Bad Request)} if the subLayanan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sub-layanans")
    public ResponseEntity<SubLayananDTO> createSubLayanan(@Valid @RequestBody SubLayananDTO subLayananDTO) throws URISyntaxException {
        log.debug("REST request to save SubLayanan : {}", subLayananDTO);
        if (subLayananDTO.getId() != null) {
            throw new BadRequestAlertException("A new subLayanan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubLayananDTO result = subLayananService.save(subLayananDTO);
        return ResponseEntity.created(new URI("/api/sub-layanans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sub-layanans} : Updates an existing subLayanan.
     *
     * @param subLayananDTO the subLayananDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subLayananDTO,
     * or with status {@code 400 (Bad Request)} if the subLayananDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subLayananDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sub-layanans")
    public ResponseEntity<SubLayananDTO> updateSubLayanan(@Valid @RequestBody SubLayananDTO subLayananDTO) throws URISyntaxException {
        log.debug("REST request to update SubLayanan : {}", subLayananDTO);
        if (subLayananDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubLayananDTO result = subLayananService.save(subLayananDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subLayananDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sub-layanans} : get all the subLayanans.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subLayanans in body.
     */
    @GetMapping("/sub-layanans")
    public ResponseEntity<List<SubLayananDTO>> getAllSubLayanans(Pageable pageable) {
        log.debug("REST request to get a page of SubLayanans");
        Page<SubLayananDTO> page = subLayananService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sub-layanans/:id} : get the "id" subLayanan.
     *
     * @param id the id of the subLayananDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subLayananDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sub-layanans/{id}")
    public ResponseEntity<SubLayananDTO> getSubLayanan(@PathVariable Long id) {
        log.debug("REST request to get SubLayanan : {}", id);
        Optional<SubLayananDTO> subLayananDTO = subLayananService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subLayananDTO);
    }

    /**
     * {@code DELETE  /sub-layanans/:id} : delete the "id" subLayanan.
     *
     * @param id the id of the subLayananDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sub-layanans/{id}")
    public ResponseEntity<Void> deleteSubLayanan(@PathVariable Long id) {
        log.debug("REST request to delete SubLayanan with id : {}", id);
        subLayananService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    /*
        Tambahan
     */
    @GetMapping("/sub-layanans/getByLayanan/{layananId}")
    public List<SubLayananDTO> getByLayanan(@PathVariable Long layananId) {
        log.debug("REST request to get a page of SubLayanans by layananId", layananId);
        return subLayananService.getAllByIdLayanan(layananId);
    }

    @GetMapping("/sub-layanans/getLayananOnline/{layananOnline}")
    public List<SubLayananDTO> getLayananOnline(@PathVariable boolean layananOnline) {
        log.debug("REST request to get a page of SubLayanans by layananOnline", layananOnline);
        return subLayananService.getLayananOnline(layananOnline);
    }

    @GetMapping("/sub-layanans/getLayananOnlineByStatusAktif/{layananOnline}")
    public List<SubLayananDTO> getLayananOnlineByStatus(@PathVariable boolean layananOnline) {
        log.debug("REST request to get a page of SubLayanans by layananOnline", layananOnline);
        return subLayananService.getLayananOnlineByStatus(layananOnline);
    }

    @GetMapping("/simple-sub-layanans")
    public ResponseEntity<List<SimpleSubLayananDTO>> getAllSimpleSubLayanans(Pageable pageable) {
        log.debug("REST request to get a page of SubLayanans");
        Page<SimpleSubLayananDTO> page = subLayananService.findSimpleAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-sub-layanans/layananAktif")
    public ResponseEntity<List<SimpleSubLayananDTO>> getLayananAktif(Pageable pageable, String status) {
        log.debug("REST request to get a page of SubLayanans");
        Page<SimpleSubLayananDTO> page = subLayananService.getSimpleLayananAktif(pageable,status);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-sub-layanans/{id}")
    public ResponseEntity<SimpleSubLayananDTO> getSimpleSubLayananById(@PathVariable Long id) {
        log.debug("REST request to get SubLayanan : {}", id);
        Optional<SimpleSubLayananDTO> simpleSubLayananDTO = subLayananService.findSimpleOne(id);
        return ResponseUtil.wrapOrNotFound(simpleSubLayananDTO);
    }

    @GetMapping("/sub-layanans/aktif")
    public List<SubLayananDTO> getAllSimpleSubLayanans(@PathVariable String status) {
        log.debug("REST request to get a page of SubLayanans by status", status);
        return subLayananService.getLayananAktif(status);
    }

    @GetMapping("/sub-layanans/cekLantai/{idLantai}")
    public List<SubLayananDTO> getAllSimpleSubLayanansCekLantai(@PathVariable  Long idLantai) {
        log.debug("REST request to get a page of SubLayanans by idLantai", idLantai);
        return subLayananService.getLayananWithLantai(idLantai);
    }


}
