package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.JadwalDetilService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.JadwalDetilDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.JadwalDetil}.
 */
@RestController
@RequestMapping("/api")
public class JadwalDetilResource {

    private final Logger log = LoggerFactory.getLogger(JadwalDetilResource.class);

    private static final String ENTITY_NAME = "antriancoresvcJadwalDetil";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JadwalDetilService jadwalDetilService;

    public JadwalDetilResource(JadwalDetilService jadwalDetilService) {
        this.jadwalDetilService = jadwalDetilService;
    }

    /**
     * {@code POST  /jadwal-detils} : Create a new jadwalDetil.
     *
     * @param jadwalDetilDTO the jadwalDetilDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new jadwalDetilDTO, or with status {@code 400 (Bad Request)} if the jadwalDetil has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/jadwal-detils")
    public ResponseEntity<JadwalDetilDTO> createJadwalDetil(@RequestBody JadwalDetilDTO jadwalDetilDTO) throws URISyntaxException {
        log.debug("REST request to save JadwalDetil : {}", jadwalDetilDTO);
        if (jadwalDetilDTO.getId() != null) {
            throw new BadRequestAlertException("A new jadwalDetil cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JadwalDetilDTO result = jadwalDetilService.save(jadwalDetilDTO);
        return ResponseEntity.created(new URI("/api/jadwal-detils/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /jadwal-detils} : Updates an existing jadwalDetil.
     *
     * @param jadwalDetilDTO the jadwalDetilDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated jadwalDetilDTO,
     * or with status {@code 400 (Bad Request)} if the jadwalDetilDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the jadwalDetilDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/jadwal-detils")
    public ResponseEntity<JadwalDetilDTO> updateJadwalDetil(@RequestBody JadwalDetilDTO jadwalDetilDTO) throws URISyntaxException {
        log.debug("REST request to update JadwalDetil : {}", jadwalDetilDTO);
        if (jadwalDetilDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JadwalDetilDTO result = jadwalDetilService.save(jadwalDetilDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jadwalDetilDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /jadwal-detils} : get all the jadwalDetils.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of jadwalDetils in body.
     */
    @GetMapping("/jadwal-detils")
    public ResponseEntity<List<JadwalDetilDTO>> getAllJadwalDetils(Pageable pageable) {
        log.debug("REST request to get a page of JadwalDetils");
        Page<JadwalDetilDTO> page = jadwalDetilService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /jadwal-detils/:id} : get the "id" jadwalDetil.
     *
     * @param id the id of the jadwalDetilDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the jadwalDetilDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/jadwal-detils/{id}")
    public ResponseEntity<JadwalDetilDTO> getJadwalDetil(@PathVariable Long id) {
        log.debug("REST request to get JadwalDetil : {}", id);
        Optional<JadwalDetilDTO> jadwalDetilDTO = jadwalDetilService.findOne(id);
        return ResponseUtil.wrapOrNotFound(jadwalDetilDTO);
    }

    /**
     * {@code DELETE  /jadwal-detils/:id} : delete the "id" jadwalDetil.
     *
     * @param id the id of the jadwalDetilDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/jadwal-detils/{id}")
    public ResponseEntity<Void> deleteJadwalDetil(@PathVariable Long id) {
        log.debug("REST request to delete JadwalDetil : {}", id);
        jadwalDetilService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
