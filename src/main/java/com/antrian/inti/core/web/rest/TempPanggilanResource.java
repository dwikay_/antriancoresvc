package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.TempPanggilanService;
import com.antrian.inti.core.service.dto.DataAntrianDTO;
import com.antrian.inti.core.service.dto.PlasmaDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.TempPanggilanDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.TempPanggilan}.
 */
@RestController
@RequestMapping("/api")
public class TempPanggilanResource {

    private final Logger log = LoggerFactory.getLogger(TempPanggilanResource.class);

    private static final String ENTITY_NAME = "antriancoresvcTempPanggilan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TempPanggilanService tempPanggilanService;

    public TempPanggilanResource(TempPanggilanService tempPanggilanService) {
        this.tempPanggilanService = tempPanggilanService;
    }

    /**
     * {@code POST  /temp-panggilans} : Create a new tempPanggilan.
     *
     * test
     * @param tempPanggilanDTO the tempPanggilanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tempPanggilanDTO, or with status {@code 400 (Bad Request)} if the tempPanggilan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/temp-panggilans")
    public ResponseEntity<TempPanggilanDTO> createTempPanggilan(@RequestBody TempPanggilanDTO tempPanggilanDTO) throws URISyntaxException {
        log.debug("REST request to save TempPanggilan : {}", tempPanggilanDTO);
        if (tempPanggilanDTO.getId() != null) {
            throw new BadRequestAlertException("A new tempPanggilan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TempPanggilanDTO result = tempPanggilanService.save(tempPanggilanDTO);
        return ResponseEntity.created(new URI("/api/temp-panggilans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /temp-panggilans} : Updates an existing tempPanggilan.
     *
     * @param tempPanggilanDTO the tempPanggilanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tempPanggilanDTO,
     * or with status {@code 400 (Bad Request)} if the tempPanggilanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tempPanggilanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/temp-panggilans")
    public ResponseEntity<TempPanggilanDTO> updateTempPanggilan(@RequestBody TempPanggilanDTO tempPanggilanDTO) throws URISyntaxException {
        log.debug("REST request to update TempPanggilan : {}", tempPanggilanDTO);
        if (tempPanggilanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TempPanggilanDTO result = tempPanggilanService.save(tempPanggilanDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, tempPanggilanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /temp-panggilans} : get all the tempPanggilans.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tempPanggilans in body.
     */
    @GetMapping("/temp-panggilans")
    public ResponseEntity<List<TempPanggilanDTO>> getAllTempPanggilans(Pageable pageable) {
        log.debug("REST request to get a page of TempPanggilans");
        Page<TempPanggilanDTO> page = tempPanggilanService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /temp-panggilans/:id} : get the "id" tempPanggilan.
     *
     * @param id the id of the tempPanggilanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tempPanggilanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/temp-panggilans/{id}")
    public ResponseEntity<TempPanggilanDTO> getTempPanggilan(@PathVariable Long id) {
        log.debug("REST request to get TempPanggilan : {}", id);
        Optional<TempPanggilanDTO> tempPanggilanDTO = tempPanggilanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tempPanggilanDTO);
    }

    /**
     * {@code DELETE  /temp-panggilans/:id} : delete the "id" tempPanggilan.
     *
     * @param id the id of the tempPanggilanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/temp-panggilans/{id}")
    public ResponseEntity<Void> deleteTempPanggilan(@PathVariable Long id) {
        log.debug("REST request to delete TempPanggilan : {}", id);
        tempPanggilanService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    /*
        Tambahan
     */

    @GetMapping("/panggilanBerikutnya/{statusCurrent}/{idLantai}")
    public ResponseEntity<TempPanggilanDTO> panggilanBerikutnya(@PathVariable boolean statusCurrent, @PathVariable Long idLantai) {
        log.debug("REST request to get DataAntrian by idSubLayanan,statusCurrent,tanggal: {}",statusCurrent,idLantai);
        Optional<TempPanggilanDTO> tempPanggilanDTO = tempPanggilanService.panggilanBerikutnya(statusCurrent,idLantai);
        return ResponseUtil.wrapOrNotFound(tempPanggilanDTO);
    }


    @GetMapping("/jumlahPanggilan/{idSubLayanan}/{noUrut}")
    public Integer jumlahPanggilan(@PathVariable Long idSubLayanan,@PathVariable String noUrut) {
        log.debug("REST request count all antrianSisa by dateNow and idSubLayanan : {}", idSubLayanan,noUrut);
        return tempPanggilanService.jumlahPanggilan(idSubLayanan,noUrut);
    }
    @DeleteMapping("/clean-data")
    public ResponseEntity<Void> cleanData() {
        log.debug("REST request to clean TempPanggilan  at ", LocalDate.now());
        LocalDate date = LocalDate.now();
        tempPanggilanService.deleteTempPanggilan();
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, date.toString())).build();
    }

}
