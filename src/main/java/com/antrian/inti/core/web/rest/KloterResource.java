package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.KloterService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.KloterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Kloter}.
 */
@RestController
@RequestMapping("/api")
public class KloterResource {

    private final Logger log = LoggerFactory.getLogger(KloterResource.class);

    private static final String ENTITY_NAME = "antriancoresvcKloter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KloterService kloterService;

    public KloterResource(KloterService kloterService) {
        this.kloterService = kloterService;
    }

    /**
     * {@code POST  /kloters} : Create a new kloter.
     *
     * @param kloterDTO the kloterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kloterDTO, or with status {@code 400 (Bad Request)} if the kloter has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kloters")
    public ResponseEntity<KloterDTO> createKloter(@RequestBody KloterDTO kloterDTO) throws URISyntaxException {
        log.debug("REST request to save Kloter : {}", kloterDTO);
        if (kloterDTO.getId() != null) {
            throw new BadRequestAlertException("A new kloter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KloterDTO result = kloterService.save(kloterDTO);
        return ResponseEntity.created(new URI("/api/kloters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kloters} : Updates an existing kloter.
     *
     * @param kloterDTO the kloterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kloterDTO,
     * or with status {@code 400 (Bad Request)} if the kloterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kloterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kloters")
    public ResponseEntity<KloterDTO> updateKloter(@RequestBody KloterDTO kloterDTO) throws URISyntaxException {
        log.debug("REST request to update Kloter : {}", kloterDTO);
        if (kloterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KloterDTO result = kloterService.save(kloterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kloterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /kloters} : get all the kloters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kloters in body.
     */
    @GetMapping("/kloters")
    public ResponseEntity<List<KloterDTO>> getAllKloters(Pageable pageable) {
        log.debug("REST request to get a page of Kloters");
        Page<KloterDTO> page = kloterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kloters/:id} : get the "id" kloter.
     *
     * @param id the id of the kloterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kloterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kloters/{id}")
    public ResponseEntity<KloterDTO> getKloter(@PathVariable Long id) {
        log.debug("REST request to get Kloter : {}", id);
        Optional<KloterDTO> kloterDTO = kloterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kloterDTO);
    }

    /**
     * {@code DELETE  /kloters/:id} : delete the "id" kloter.
     *
     * @param id the id of the kloterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kloters/{id}")
    public ResponseEntity<Void> deleteKloter(@PathVariable Long id) {
        log.debug("REST request to delete Kloter : {}", id);
        kloterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
