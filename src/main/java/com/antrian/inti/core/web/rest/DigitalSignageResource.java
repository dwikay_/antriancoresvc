package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.DigitalSignageService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.DigitalSignageDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.DigitalSignage}.
 */
@RestController
@RequestMapping("/api")
public class DigitalSignageResource {

    private final Logger log = LoggerFactory.getLogger(DigitalSignageResource.class);

    private static final String ENTITY_NAME = "antriancoresvcDigitalSignage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DigitalSignageService digitalSignageService;

    public DigitalSignageResource(DigitalSignageService digitalSignageService) {
        this.digitalSignageService = digitalSignageService;
    }

    /**
     * {@code POST  /digital-signages} : Create a new digitalSignage.
     *
     * @param digitalSignageDTO the digitalSignageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new digitalSignageDTO, or with status {@code 400 (Bad Request)} if the digitalSignage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/digital-signages")
    public ResponseEntity<DigitalSignageDTO> createDigitalSignage(@RequestBody DigitalSignageDTO digitalSignageDTO) throws URISyntaxException {
        log.debug("REST request to save DigitalSignage : {}", digitalSignageDTO);
        if (digitalSignageDTO.getId() != null) {
            throw new BadRequestAlertException("A new digitalSignage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DigitalSignageDTO result = digitalSignageService.save(digitalSignageDTO);
        return ResponseEntity.created(new URI("/api/digital-signages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /digital-signages} : Updates an existing digitalSignage.
     *
     * @param digitalSignageDTO the digitalSignageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated digitalSignageDTO,
     * or with status {@code 400 (Bad Request)} if the digitalSignageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the digitalSignageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/digital-signages")
    public ResponseEntity<DigitalSignageDTO> updateDigitalSignage(@RequestBody DigitalSignageDTO digitalSignageDTO) throws URISyntaxException {
        log.debug("REST request to update DigitalSignage : {}", digitalSignageDTO);
        if (digitalSignageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DigitalSignageDTO result = digitalSignageService.save(digitalSignageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, digitalSignageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /digital-signages} : get all the digitalSignages.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of digitalSignages in body.
     */
    @GetMapping("/digital-signages")
    public ResponseEntity<List<DigitalSignageDTO>> getAllDigitalSignages(Pageable pageable) {
        log.debug("REST request to get a page of DigitalSignages");
        Page<DigitalSignageDTO> page = digitalSignageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /digital-signages/:id} : get the "id" digitalSignage.
     *
     * @param id the id of the digitalSignageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the digitalSignageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/digital-signages/{id}")
    public ResponseEntity<DigitalSignageDTO> getDigitalSignage(@PathVariable Long id) {
        log.debug("REST request to get DigitalSignage : {}", id);
        Optional<DigitalSignageDTO> digitalSignageDTO = digitalSignageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(digitalSignageDTO);
    }

    /**
     * {@code DELETE  /digital-signages/:id} : delete the "id" digitalSignage.
     *
     * @param id the id of the digitalSignageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/digital-signages/{id}")
    public ResponseEntity<Void> deleteDigitalSignage(@PathVariable Long id) {
        log.debug("REST request to delete DigitalSignage : {}", id);
        digitalSignageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
