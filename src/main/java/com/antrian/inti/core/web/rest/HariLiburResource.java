package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.HariLiburService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.HariLiburDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.HariLibur}.
 */
@RestController
@RequestMapping("/api")
public class HariLiburResource {

    private final Logger log = LoggerFactory.getLogger(HariLiburResource.class);

    private static final String ENTITY_NAME = "antriancoresvcHariLibur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HariLiburService hariLiburService;

    public HariLiburResource(HariLiburService hariLiburService) {
        this.hariLiburService = hariLiburService;
    }

    /**
     * {@code POST  /hari-liburs} : Create a new hariLibur.
     *
     * @param hariLiburDTO the hariLiburDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hariLiburDTO, or with status {@code 400 (Bad Request)} if the hariLibur has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hari-liburs")
    public ResponseEntity<HariLiburDTO> createHariLibur(@RequestBody HariLiburDTO hariLiburDTO) throws URISyntaxException {
        log.debug("REST request to save HariLibur : {}", hariLiburDTO);
        if (hariLiburDTO.getId() != null) {
            throw new BadRequestAlertException("A new hariLibur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HariLiburDTO result = hariLiburService.save(hariLiburDTO);
        return ResponseEntity.created(new URI("/api/hari-liburs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hari-liburs} : Updates an existing hariLibur.
     *
     * @param hariLiburDTO the hariLiburDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hariLiburDTO,
     * or with status {@code 400 (Bad Request)} if the hariLiburDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hariLiburDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hari-liburs")
    public ResponseEntity<HariLiburDTO> updateHariLibur(@RequestBody HariLiburDTO hariLiburDTO) throws URISyntaxException {
        log.debug("REST request to update HariLibur : {}", hariLiburDTO);
        if (hariLiburDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HariLiburDTO result = hariLiburService.save(hariLiburDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, hariLiburDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /hari-liburs} : get all the hariLiburs.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hariLiburs in body.
     */
    @GetMapping("/hari-liburs")
    public ResponseEntity<List<HariLiburDTO>> getAllHariLiburs(Pageable pageable) {
        log.debug("REST request to get a page of HariLiburs");
        Page<HariLiburDTO> page = hariLiburService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /hari-liburs/:id} : get the "id" hariLibur.
     *
     * @param id the id of the hariLiburDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hariLiburDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hari-liburs/{id}")
    public ResponseEntity<HariLiburDTO> getHariLibur(@PathVariable Long id) {
        log.debug("REST request to get HariLibur : {}", id);
        Optional<HariLiburDTO> hariLiburDTO = hariLiburService.findOne(id);
        return ResponseUtil.wrapOrNotFound(hariLiburDTO);
    }

    /**
     * {@code DELETE  /hari-liburs/:id} : delete the "id" hariLibur.
     *
     * @param id the id of the hariLiburDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hari-liburs/{id}")
    public ResponseEntity<Void> deleteHariLibur(@PathVariable Long id) {
        log.debug("REST request to delete HariLibur : {}", id);
        hariLiburService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
