package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.JadwalDetilService;
import com.antrian.inti.core.service.JadwalMasterService;
import com.antrian.inti.core.service.dto.SimpleJadwalDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.JadwalMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.JadwalMaster}.
 */
@RestController
@RequestMapping("/api")
public class JadwalMasterResource {

    private final Logger log = LoggerFactory.getLogger(JadwalMasterResource.class);

    private static final String ENTITY_NAME = "antriancoresvcJadwalMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JadwalMasterService jadwalMasterService;

    @Autowired
    JadwalDetilService jadwalDetilService;

    public JadwalMasterResource(JadwalMasterService jadwalMasterService) {
        this.jadwalMasterService = jadwalMasterService;
    }

    /**
     * {@code POST  /jadwal-masters} : Create a new jadwalMaster.
     *
     * @param jadwalMasterDTO the jadwalMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new jadwalMasterDTO, or with status {@code 400 (Bad Request)} if the jadwalMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/jadwal-masters")
    public ResponseEntity<JadwalMasterDTO> createJadwalMaster(@RequestBody JadwalMasterDTO jadwalMasterDTO) throws URISyntaxException {
        log.debug("REST request to save JadwalMaster : {}", jadwalMasterDTO);
        if (jadwalMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new jadwalMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JadwalMasterDTO result = jadwalMasterService.save(jadwalMasterDTO);
        return ResponseEntity.created(new URI("/api/jadwal-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /jadwal-masters} : Updates an existing jadwalMaster.
     *
     * @param jadwalMasterDTO the jadwalMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated jadwalMasterDTO,
     * or with status {@code 400 (Bad Request)} if the jadwalMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the jadwalMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/jadwal-masters")
    public ResponseEntity<JadwalMasterDTO> updateJadwalMaster(@RequestBody JadwalMasterDTO jadwalMasterDTO) throws URISyntaxException {
        log.debug("REST request to update JadwalMaster : {}", jadwalMasterDTO);
        if (jadwalMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JadwalMasterDTO result = jadwalMasterService.save(jadwalMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, jadwalMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /jadwal-masters} : get all the jadwalMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of jadwalMasters in body.
     */
    @GetMapping("/jadwal-masters")
    public ResponseEntity<List<JadwalMasterDTO>> getAllJadwalMasters(Pageable pageable) {
        log.debug("REST request to get a page of JadwalMasters");
        Page<JadwalMasterDTO> page = jadwalMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /jadwal-masters/:id} : get the "id" jadwalMaster.
     *
     * @param id the id of the jadwalMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the jadwalMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/jadwal-masters/{id}")
    public ResponseEntity<JadwalMasterDTO> getJadwalMaster(@PathVariable Long id) {
        log.debug("REST request to get JadwalMaster : {}", id);
        Optional<JadwalMasterDTO> jadwalMasterDTO = jadwalMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(jadwalMasterDTO);
    }

    /**
     * {@code DELETE  /jadwal-masters/:id} : delete the "id" jadwalMaster.
     *
     * @param id the id of the jadwalMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/jadwal-masters/{id}")
    public ResponseEntity<Void> deleteJadwalMaster(@PathVariable Long id) {
        log.debug("REST request to delete JadwalMaster : {}", id);
        jadwalMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /*
        Tambahan
     */
    @GetMapping("/simple-jadwal-masters")
    public ResponseEntity<List<SimpleJadwalDTO>> getAllSimpleJadwalMasters(Pageable pageable) {
        log.debug("REST request to get a page of JadwalMasters ");
        Page<SimpleJadwalDTO> page = jadwalMasterService.findSimpleAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-jadwal-masters/getByTypeVirtual/{tipeVirtual}")
    public ResponseEntity<List<SimpleJadwalDTO>> getByTypeVirtual(Pageable pageable, @PathVariable boolean tipeVirtual) {
        log.debug("REST request to get a page of JadwalMasters by : tipeVirtual : {} ", tipeVirtual);

        Page<SimpleJadwalDTO> page = jadwalMasterService.findSimpleAllByTipeVirtual(pageable,tipeVirtual);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());

    }

    @GetMapping("/simple-jadwal-masters/{id}")
    public ResponseEntity<SimpleJadwalDTO> getAllSimpleJadwalMastersOne(@PathVariable Long id) {
        log.debug("REST request to get JadwalMaster : {}", id);
        Optional<SimpleJadwalDTO> simpleJadwalDTO = jadwalMasterService.findSimpleOne(id);
        return ResponseUtil.wrapOrNotFound(simpleJadwalDTO);
    }
    @GetMapping("/simple-jadwal-masters/getByIdSubLayanan/{idSubLayanan}")
    public ResponseEntity<SimpleJadwalDTO> getByIdSubLayanan(@PathVariable Long idSubLayanan) {
        log.debug("REST request to get JadwalMaster by idSubLayanan : {}", idSubLayanan);
        Optional<SimpleJadwalDTO> simpleJadwalDTO = jadwalMasterService.findSimpleIdSubLayanan(idSubLayanan);
        return ResponseUtil.wrapOrNotFound(simpleJadwalDTO);
    }
    @GetMapping("/simple-jadwal-masters/getByIdSubLayananAndTipeVirtual/{idSubLayanan}/{tipeVirtual}")
    public ResponseEntity<SimpleJadwalDTO> getByIdSubLayananAndTipeVirtual(@PathVariable Long idSubLayanan, @PathVariable boolean tipeVirtual) {
        log.debug("REST request to get JadwalMaster by idSubLayanan and tipeVirtual : {}", idSubLayanan, tipeVirtual);
        Optional<SimpleJadwalDTO> simpleJadwalDTO = jadwalMasterService.findSimpleIdSubLayananAndTipeVirtual(idSubLayanan,tipeVirtual);
        return ResponseUtil.wrapOrNotFound(simpleJadwalDTO);
    }
    @GetMapping("/getLastJadwalMaster")
    public ResponseEntity<JadwalMasterDTO> getLastJadwalMaster() {
        log.debug("REST request to get last data on JadwalMaster : {}");
        Optional<JadwalMasterDTO> jadwalMasterDTO = jadwalMasterService.findLastJadwalMaster();
        return ResponseUtil.wrapOrNotFound(jadwalMasterDTO);
    }


}
