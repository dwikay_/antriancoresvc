/**
 * View Models used by Spring MVC REST controllers.
 */
package com.antrian.inti.core.web.rest.vm;
