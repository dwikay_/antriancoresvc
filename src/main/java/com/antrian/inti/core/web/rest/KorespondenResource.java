package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.KorespondenService;
import com.antrian.inti.core.service.dto.SimpleRegistrasiDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.KorespondenDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Koresponden}.
 */
@RestController
@RequestMapping("/api")
public class KorespondenResource {

    private final Logger log = LoggerFactory.getLogger(KorespondenResource.class);

    private static final String ENTITY_NAME = "antriancoresvcKoresponden";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KorespondenService korespondenService;

    public KorespondenResource(KorespondenService korespondenService) {
        this.korespondenService = korespondenService;
    }

    /**
     * {@code POST  /korespondens} : Create a new koresponden.
     *
     * @param korespondenDTO the korespondenDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new korespondenDTO, or with status {@code 400 (Bad Request)} if the koresponden has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/korespondens")
    public ResponseEntity<KorespondenDTO> createKoresponden(@RequestBody KorespondenDTO korespondenDTO) throws URISyntaxException {
        log.debug("REST request to save Koresponden : {}", korespondenDTO);
        if (korespondenDTO.getId() != null) {
            throw new BadRequestAlertException("A new koresponden cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KorespondenDTO result = korespondenService.save(korespondenDTO);
        return ResponseEntity.created(new URI("/api/korespondens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /korespondens} : Updates an existing koresponden.
     *
     * @param korespondenDTO the korespondenDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated korespondenDTO,
     * or with status {@code 400 (Bad Request)} if the korespondenDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the korespondenDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/korespondens")
    public ResponseEntity<KorespondenDTO> updateKoresponden(@RequestBody KorespondenDTO korespondenDTO) throws URISyntaxException {
        log.debug("REST request to update Koresponden : {}", korespondenDTO);
        if (korespondenDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KorespondenDTO result = korespondenService.save(korespondenDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, korespondenDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /korespondens} : get all the korespondens.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of korespondens in body.
     */
    @GetMapping("/korespondens")
    public ResponseEntity<List<KorespondenDTO>> getAllKorespondens(Pageable pageable) {
        log.debug("REST request to get a page of Korespondens");
        Page<KorespondenDTO> page = korespondenService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /korespondens/:id} : get the "id" koresponden.
     *
     * @param id the id of the korespondenDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the korespondenDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/korespondens/{id}")
    public ResponseEntity<KorespondenDTO> getKoresponden(@PathVariable Long id) {
        log.debug("REST request to get Koresponden : {}", id);
        Optional<KorespondenDTO> korespondenDTO = korespondenService.findOne(id);
        return ResponseUtil.wrapOrNotFound(korespondenDTO);
    }

    /**
     * {@code DELETE  /korespondens/:id} : delete the "id" koresponden.
     *
     * @param id the id of the korespondenDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/korespondens/{id}")
    public ResponseEntity<Void> deleteKoresponden(@PathVariable Long id) {
        log.debug("REST request to delete Koresponden : {}", id);
        korespondenService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    /*
        Tambahan
     */
    @GetMapping("/korespondens/rangeDate/{startDate}/{endDate}")
    public ResponseEntity<List<KorespondenDTO>> rangeDate(Pageable pageable, @PathVariable LocalDate startDate, @PathVariable LocalDate endDate) {
        log.debug("REST request to get a page of Layanans by Rage Date ",startDate,endDate);
        Page<KorespondenDTO> page = korespondenService.findAllRangeDate(pageable,startDate,endDate);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
