package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.PendaftaranOfflineService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.PendaftaranOfflineDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.PendaftaranOffline}.
 */
@RestController
@RequestMapping("/api")
public class PendaftaranOfflineResource {

    private final Logger log = LoggerFactory.getLogger(PendaftaranOfflineResource.class);

    private static final String ENTITY_NAME = "antriancoresvcPendaftaranOffline";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PendaftaranOfflineService pendaftaranOfflineService;

    public PendaftaranOfflineResource(PendaftaranOfflineService pendaftaranOfflineService) {
        this.pendaftaranOfflineService = pendaftaranOfflineService;
    }

    /**
     * {@code POST  /pendaftaran-offlines} : Create a new pendaftaranOffline.
     *
     * @param pendaftaranOfflineDTO the pendaftaranOfflineDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pendaftaranOfflineDTO, or with status {@code 400 (Bad Request)} if the pendaftaranOffline has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pendaftaran-offlines")
    public ResponseEntity<PendaftaranOfflineDTO> createPendaftaranOffline(@RequestBody PendaftaranOfflineDTO pendaftaranOfflineDTO) throws URISyntaxException {
        log.debug("REST request to save PendaftaranOffline : {}", pendaftaranOfflineDTO);
        if (pendaftaranOfflineDTO.getId() != null) {
            throw new BadRequestAlertException("A new pendaftaranOffline cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PendaftaranOfflineDTO result = pendaftaranOfflineService.save(pendaftaranOfflineDTO);
        return ResponseEntity.created(new URI("/api/pendaftaran-offlines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pendaftaran-offlines} : Updates an existing pendaftaranOffline.
     *
     * @param pendaftaranOfflineDTO the pendaftaranOfflineDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pendaftaranOfflineDTO,
     * or with status {@code 400 (Bad Request)} if the pendaftaranOfflineDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pendaftaranOfflineDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pendaftaran-offlines")
    public ResponseEntity<PendaftaranOfflineDTO> updatePendaftaranOffline(@RequestBody PendaftaranOfflineDTO pendaftaranOfflineDTO) throws URISyntaxException {
        log.debug("REST request to update PendaftaranOffline : {}", pendaftaranOfflineDTO);
        if (pendaftaranOfflineDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PendaftaranOfflineDTO result = pendaftaranOfflineService.save(pendaftaranOfflineDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pendaftaranOfflineDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pendaftaran-offlines} : get all the pendaftaranOfflines.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pendaftaranOfflines in body.
     */
    @GetMapping("/pendaftaran-offlines")
    public ResponseEntity<List<PendaftaranOfflineDTO>> getAllPendaftaranOfflines(Pageable pageable) {
        log.debug("REST request to get a page of PendaftaranOfflines");
        Page<PendaftaranOfflineDTO> page = pendaftaranOfflineService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pendaftaran-offlines/:id} : get the "id" pendaftaranOffline.
     *
     * @param id the id of the pendaftaranOfflineDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pendaftaranOfflineDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pendaftaran-offlines/{id}")
    public ResponseEntity<PendaftaranOfflineDTO> getPendaftaranOffline(@PathVariable Long id) {
        log.debug("REST request to get PendaftaranOffline : {}", id);
        Optional<PendaftaranOfflineDTO> pendaftaranOfflineDTO = pendaftaranOfflineService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pendaftaranOfflineDTO);
    }

    /**
     * {@code DELETE  /pendaftaran-offlines/:id} : delete the "id" pendaftaranOffline.
     *
     * @param id the id of the pendaftaranOfflineDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pendaftaran-offlines/{id}")
    public ResponseEntity<Void> deletePendaftaranOffline(@PathVariable Long id) {
        log.debug("REST request to delete PendaftaranOffline : {}", id);
        pendaftaranOfflineService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/pendaftaran-offlines/validToken/{token}")
    public ResponseEntity<PendaftaranOfflineDTO> validToken(@PathVariable String token) {
        log.debug("REST request to get PendaftaranOffline by token : {}", token);
        Optional<PendaftaranOfflineDTO> pendaftaranOfflineDTO = pendaftaranOfflineService.validToken(token);
        return ResponseUtil.wrapOrNotFound(pendaftaranOfflineDTO);
    }
    @GetMapping("/pendaftaran-offlines/getByNikAndEmail/{nik}/{email}/{idSubLayanan}")
    public ResponseEntity<PendaftaranOfflineDTO> getByNikAndEmail(@PathVariable String nik, @PathVariable String email, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get PendaftaranOffline by nik,email,idSubLayanan : {}", nik,email,idSubLayanan);
        Optional<PendaftaranOfflineDTO> pendaftaranOfflineDTO = pendaftaranOfflineService.getByNikAndEmailAndIdSubLayanan(nik,email,idSubLayanan);
        return ResponseUtil.wrapOrNotFound(pendaftaranOfflineDTO);
    }
    @GetMapping("/pendaftaran-offlines/getByNoUrutAndIdSubLayanan/{noUrut}/{idSubLayanan}")
    public ResponseEntity<PendaftaranOfflineDTO> getByNoUrutAndIdSubLayanan(@PathVariable String noUrut, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get PendaftaranOffline by noUrut,idSubLayanan : {}", noUrut,idSubLayanan);
        Optional<PendaftaranOfflineDTO> pendaftaranOfflineDTO = pendaftaranOfflineService.getByNoUrutAndIdSubLayanan(noUrut,idSubLayanan);
        return ResponseUtil.wrapOrNotFound(pendaftaranOfflineDTO);
    }

    @GetMapping("/pendaftaran-offlines/getByDate/{startDate}/{endDate}")
    public ResponseEntity<List<PendaftaranOfflineDTO>> getByDate(Pageable pageable, @PathVariable LocalDate startDate, @PathVariable LocalDate endDate) {
        log.debug("REST request to get a page of PendaftaranOfflines");
        Page<PendaftaranOfflineDTO> page = pendaftaranOfflineService.getByDate(pageable,startDate,endDate);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
