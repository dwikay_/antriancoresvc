package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.LantaiService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.LantaiDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Lantai}.
 */
@RestController
@RequestMapping("/api")
public class LantaiResource {

    private final Logger log = LoggerFactory.getLogger(LantaiResource.class);

    private static final String ENTITY_NAME = "antriancoresvcLantai";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LantaiService lantaiService;

    public LantaiResource(LantaiService lantaiService) {
        this.lantaiService = lantaiService;
    }

    /**
     * {@code POST  /lantais} : Create a new lantai.
     *
     * @param lantaiDTO the lantaiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new lantaiDTO, or with status {@code 400 (Bad Request)} if the lantai has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/lantais")
    public ResponseEntity<LantaiDTO> createLantai(@Valid @RequestBody LantaiDTO lantaiDTO) throws URISyntaxException {
        log.debug("REST request to save Lantai : {}", lantaiDTO);
        if (lantaiDTO.getId() != null) {
            throw new BadRequestAlertException("A new lantai cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LantaiDTO result = lantaiService.save(lantaiDTO);
        return ResponseEntity.created(new URI("/api/lantais/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /lantais} : Updates an existing lantai.
     *
     * @param lantaiDTO the lantaiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated lantaiDTO,
     * or with status {@code 400 (Bad Request)} if the lantaiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the lantaiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/lantais")
    public ResponseEntity<LantaiDTO> updateLantai(@Valid @RequestBody LantaiDTO lantaiDTO) throws URISyntaxException {
        log.debug("REST request to update Lantai : {}", lantaiDTO);
        if (lantaiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LantaiDTO result = lantaiService.save(lantaiDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, lantaiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /lantais} : get all the lantais.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of lantais in body.
     */
    @GetMapping("/lantais")
    public ResponseEntity<List<LantaiDTO>> getAllLantais(Pageable pageable) {
        log.debug("REST request to get a page of Lantais");
        Page<LantaiDTO> page = lantaiService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /lantais/:id} : get the "id" lantai.
     *
     * @param id the id of the lantaiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the lantaiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/lantais/{id}")
    public ResponseEntity<LantaiDTO> getLantai(@PathVariable Long id) {
        log.debug("REST request to get Lantai : {}", id);
        Optional<LantaiDTO> lantaiDTO = lantaiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(lantaiDTO);
    }

    /**
     * {@code DELETE  /lantais/:id} : delete the "id" lantai.
     *
     * @param id the id of the lantaiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/lantais/{id}")
    public ResponseEntity<Void> deleteLantai(@PathVariable Long id) {
        log.debug("REST request to delete Lantai : {}", id);
        lantaiService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
