package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.ImageRepositoryService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.ImageRepositoryDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.ImageRepository}.
 */
@RestController
@RequestMapping("/api")
public class ImageRepositoryResource {

    private final Logger log = LoggerFactory.getLogger(ImageRepositoryResource.class);

    private static final String ENTITY_NAME = "antriancoresvcImageRepository";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImageRepositoryService imageRepositoryService;

    public ImageRepositoryResource(ImageRepositoryService imageRepositoryService) {
        this.imageRepositoryService = imageRepositoryService;
    }

    /**
     * {@code POST  /image-repositories} : Create a new imageRepository.
     *
     * @param imageRepositoryDTO the imageRepositoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new imageRepositoryDTO, or with status {@code 400 (Bad Request)} if the imageRepository has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/image-repositories")
    public ResponseEntity<ImageRepositoryDTO> createImageRepository(@RequestBody ImageRepositoryDTO imageRepositoryDTO) throws URISyntaxException {
        log.debug("REST request to save ImageRepository : {}", imageRepositoryDTO);
        if (imageRepositoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new imageRepository cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ImageRepositoryDTO result = imageRepositoryService.save(imageRepositoryDTO);
        return ResponseEntity.created(new URI("/api/image-repositories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /image-repositories} : Updates an existing imageRepository.
     *
     * @param imageRepositoryDTO the imageRepositoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated imageRepositoryDTO,
     * or with status {@code 400 (Bad Request)} if the imageRepositoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the imageRepositoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/image-repositories")
    public ResponseEntity<ImageRepositoryDTO> updateImageRepository(@RequestBody ImageRepositoryDTO imageRepositoryDTO) throws URISyntaxException {
        log.debug("REST request to update ImageRepository : {}", imageRepositoryDTO);
        if (imageRepositoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ImageRepositoryDTO result = imageRepositoryService.save(imageRepositoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, imageRepositoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /image-repositories} : get all the imageRepositories.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of imageRepositories in body.
     */
    @GetMapping("/image-repositories")
    public ResponseEntity<List<ImageRepositoryDTO>> getAllImageRepositories(Pageable pageable) {
        log.debug("REST request to get a page of ImageRepositories");
        Page<ImageRepositoryDTO> page = imageRepositoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /image-repositories/:id} : get the "id" imageRepository.
     *
     * @param id the id of the imageRepositoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the imageRepositoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/image-repositories/{id}")
    public ResponseEntity<ImageRepositoryDTO> getImageRepository(@PathVariable Long id) {
        log.debug("REST request to get ImageRepository : {}", id);
        Optional<ImageRepositoryDTO> imageRepositoryDTO = imageRepositoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(imageRepositoryDTO);
    }

    /**
     * {@code DELETE  /image-repositories/:id} : delete the "id" imageRepository.
     *
     * @param id the id of the imageRepositoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/image-repositories/{id}")
    public ResponseEntity<Void> deleteImageRepository(@PathVariable Long id) {
        log.debug("REST request to delete ImageRepository : {}", id);
        imageRepositoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
