package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.PendaftaranDumyService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.PendaftaranDumyDTO;
import com.antrian.inti.core.service.dto.PendaftaranDumyCriteria;
import com.antrian.inti.core.service.PendaftaranDumyQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.PendaftaranDumy}.
 */
@RestController
@RequestMapping("/api")
public class PendaftaranDumyResource {

    private final Logger log = LoggerFactory.getLogger(PendaftaranDumyResource.class);

    private static final String ENTITY_NAME = "antriancoresvcPendaftaranDumy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PendaftaranDumyService pendaftaranDumyService;

    private final PendaftaranDumyQueryService pendaftaranDumyQueryService;

    public PendaftaranDumyResource(PendaftaranDumyService pendaftaranDumyService, PendaftaranDumyQueryService pendaftaranDumyQueryService) {
        this.pendaftaranDumyService = pendaftaranDumyService;
        this.pendaftaranDumyQueryService = pendaftaranDumyQueryService;
    }

    /**
     * {@code POST  /pendaftaran-dumies} : Create a new pendaftaranDumy.
     *
     * @param pendaftaranDumyDTO the pendaftaranDumyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pendaftaranDumyDTO, or with status {@code 400 (Bad Request)} if the pendaftaranDumy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pendaftaran-dumies")
    public ResponseEntity<PendaftaranDumyDTO> createPendaftaranDumy(@RequestBody PendaftaranDumyDTO pendaftaranDumyDTO) throws URISyntaxException {
        log.debug("REST request to save PendaftaranDumy : {}", pendaftaranDumyDTO);
        if (pendaftaranDumyDTO.getId() != null) {
            throw new BadRequestAlertException("A new pendaftaranDumy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PendaftaranDumyDTO result = pendaftaranDumyService.save(pendaftaranDumyDTO);
        return ResponseEntity.created(new URI("/api/pendaftaran-dumies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pendaftaran-dumies} : Updates an existing pendaftaranDumy.
     *
     * @param pendaftaranDumyDTO the pendaftaranDumyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pendaftaranDumyDTO,
     * or with status {@code 400 (Bad Request)} if the pendaftaranDumyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pendaftaranDumyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pendaftaran-dumies")
    public ResponseEntity<PendaftaranDumyDTO> updatePendaftaranDumy(@RequestBody PendaftaranDumyDTO pendaftaranDumyDTO) throws URISyntaxException {
        log.debug("REST request to update PendaftaranDumy : {}", pendaftaranDumyDTO);
        if (pendaftaranDumyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PendaftaranDumyDTO result = pendaftaranDumyService.save(pendaftaranDumyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pendaftaranDumyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pendaftaran-dumies} : get all the pendaftaranDumies.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pendaftaranDumies in body.
     */
    @GetMapping("/pendaftaran-dumies")
    public ResponseEntity<List<PendaftaranDumyDTO>> getAllPendaftaranDumies(PendaftaranDumyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PendaftaranDumies by criteria: {}", criteria);
        Page<PendaftaranDumyDTO> page = pendaftaranDumyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /pendaftaran-dumies/count} : count all the pendaftaranDumies.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/pendaftaran-dumies/count")
    public ResponseEntity<Long> countPendaftaranDumies(PendaftaranDumyCriteria criteria) {
        log.debug("REST request to count PendaftaranDumies by criteria: {}", criteria);
        return ResponseEntity.ok().body(pendaftaranDumyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /pendaftaran-dumies/:id} : get the "id" pendaftaranDumy.
     *
     * @param id the id of the pendaftaranDumyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pendaftaranDumyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pendaftaran-dumies/{id}")
    public ResponseEntity<PendaftaranDumyDTO> getPendaftaranDumy(@PathVariable Long id) {
        log.debug("REST request to get PendaftaranDumy : {}", id);
        Optional<PendaftaranDumyDTO> pendaftaranDumyDTO = pendaftaranDumyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pendaftaranDumyDTO);
    }

    /**
     * {@code DELETE  /pendaftaran-dumies/:id} : delete the "id" pendaftaranDumy.
     *
     * @param id the id of the pendaftaranDumyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pendaftaran-dumies/{id}")
    public ResponseEntity<Void> deletePendaftaranDumy(@PathVariable Long id) {
        log.debug("REST request to delete PendaftaranDumy : {}", id);
        pendaftaranDumyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
