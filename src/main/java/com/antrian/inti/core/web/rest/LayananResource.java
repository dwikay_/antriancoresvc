package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.LantaiService;
import com.antrian.inti.core.service.LayananService;
import com.antrian.inti.core.service.dto.PlasmaDTO;
import com.antrian.inti.core.service.dto.SimpleLantaiDTO;
import com.antrian.inti.core.service.dto.SimpleLayananDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.LayananDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Layanan}.
 */
@RestController
@RequestMapping("/api")
public class LayananResource {

    private final Logger log = LoggerFactory.getLogger(LayananResource.class);

    private static final String ENTITY_NAME = "antriancoresvcLayanan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LayananService layananService;

    @Autowired
    LantaiService lantaiService;
    public LayananResource(LayananService layananService) {
        this.layananService = layananService;
    }

    /**
     * {@code POST  /layanans} : Create a new layanan.
     *
     * @param layananDTO the layananDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new layananDTO, or with status {@code 400 (Bad Request)} if the layanan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/layanans")
    public ResponseEntity<LayananDTO> createLayanan(@RequestBody LayananDTO layananDTO) throws URISyntaxException {
        log.debug("REST request to save Layanan : {}", layananDTO);
        if (layananDTO.getId() != null) {
            throw new BadRequestAlertException("A new layanan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LayananDTO result = layananService.save(layananDTO);
        return ResponseEntity.created(new URI("/api/layanans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /layanans} : Updates an existing layanan.
     *
     * @param layananDTO the layananDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated layananDTO,
     * or with status {@code 400 (Bad Request)} if the layananDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the layananDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/layanans")
    public ResponseEntity<LayananDTO> updateLayanan(@RequestBody LayananDTO layananDTO) throws URISyntaxException {
        log.debug("REST request to update Layanan : {}", layananDTO);
        if (layananDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LayananDTO result = layananService.save(layananDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, layananDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /layanans} : get all the layanans.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of layanans in body.
     */
    @GetMapping("/layanans")
    public ResponseEntity<List<LayananDTO>> getAllLayanans(Pageable pageable) {
        log.debug("REST request to get a page of Layanans");
        Page<LayananDTO> page = layananService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /layanans/:id} : get the "id" layanan.
     *
     * @param id the id of the layananDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the layananDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/layanans/{id}")
    public ResponseEntity<LayananDTO> getLayanan(@PathVariable Long id) {
        log.debug("REST request to get Layanan : {}", id);
        Optional<LayananDTO> layananDTO = layananService.findOne(id);
        return ResponseUtil.wrapOrNotFound(layananDTO);
    }

    /**
     * {@code DELETE  /layanans/:id} : delete the "id" layanan.
     *
     * @param id the id of the layananDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/layanans/{id}")
    public ResponseEntity<Void> deleteLayanan(@PathVariable Long id) {
        log.debug("REST request to delete Layanan : {}", id);
        layananService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /*
        Tambahan
     */

    @GetMapping("/getLayananByLantai")
    public List<SimpleLantaiDTO> getLayananByLantai() {
        log.debug("REST request to get a page of Layanan ");
        return layananService.getLayananByLantai();
    }

    @GetMapping("/simple-layanans")
    public ResponseEntity<List<SimpleLayananDTO>> getAllSimpleLayanans(Pageable pageable) {
        log.debug("REST request to get a page of Layanans");
        Page<SimpleLayananDTO> page = layananService.findSimpleAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/simple-layanans/{id}")
    public ResponseEntity<SimpleLayananDTO> getOneSimpleLayanans(@PathVariable  Long id) {
        log.debug("REST request to get a page of Layanans by id ", id);
        Optional<SimpleLayananDTO> simpleLayananDTO = layananService.findOneSimple(id);
        return ResponseUtil.wrapOrNotFound(simpleLayananDTO);
    }


    @RequestMapping(value = "/imageLayanan/{layananId}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImageAsResponseEntity(@PathVariable Long layananId) {
        HttpHeaders headers = new HttpHeaders();
        Optional<LayananDTO> optionalLayananDTO = layananService.findOne(layananId);
        if(!optionalLayananDTO.isPresent())
        {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        LayananDTO layananDTO = optionalLayananDTO.get();

        if(optionalLayananDTO.get().getGambar().equals("")){

            log.info("checkNull");
            byte[] media = layananDTO.getGambar();
            String mediaType = layananDTO.getGambarContentType();

            headers.set("Content-Type", mediaType);

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.NOT_FOUND);

            return responseEntity;
        }else{

            log.info("notcheckNull");
            byte[] media = layananDTO.getGambar();
            String mediaType = layananDTO.getGambarContentType();

            headers.set("Content-Type", mediaType);

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);

            return responseEntity;
        }



    }
}
