package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.domain.DataAntrian;
import com.antrian.inti.core.domain.Loket;
import com.antrian.inti.core.repository.DataAntrianRepository;
import com.antrian.inti.core.service.DataAntrianService;
import com.antrian.inti.core.service.LoketService;
import com.antrian.inti.core.service.TempPanggilanService;
import com.antrian.inti.core.service.dto.*;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.Data;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.DataAntrian}.
 */
@RestController
@RequestMapping("/api")
public class DataAntrianResource {

    private final Logger log = LoggerFactory.getLogger(DataAntrianResource.class);

    private static final String ENTITY_NAME = "antriancoresvcDataAntrian";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DataAntrianService dataAntrianService;

    @Autowired
    DataAntrianRepository dataAntrianRepository;

    @Autowired
    TempPanggilanService tempPanggilanService;

    @Autowired
    LoketService loketService;


    public DataAntrianResource(DataAntrianService dataAntrianService) {
        this.dataAntrianService = dataAntrianService;
    }

    /**
     * {@code POST  /data-antrians} : Create a new dataAntrian.
     *
     * @param dataAntrianDTO the dataAntrianDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dataAntrianDTO, or with status {@code 400 (Bad Request)} if the dataAntrian has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/data-antrians")
    public ResponseEntity<DataAntrianDTO> createDataAntrian(@RequestBody DataAntrianDTO dataAntrianDTO) throws URISyntaxException {
        log.debug("REST request to save DataAntrian : {}", dataAntrianDTO);
        if (dataAntrianDTO.getId() != null) {
            throw new BadRequestAlertException("A new dataAntrian cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DataAntrianDTO result = dataAntrianService.save(dataAntrianDTO);
        return ResponseEntity.created(new URI("/api/data-antrians/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /data-antrians} : Updates an existing dataAntrian.
     *
     * @param dataAntrianDTO the dataAntrianDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dataAntrianDTO,
     * or with status {@code 400 (Bad Request)} if the dataAntrianDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dataAntrianDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/data-antrians")
    public ResponseEntity<DataAntrianDTO> updateDataAntrian(@RequestBody DataAntrianDTO dataAntrianDTO) throws URISyntaxException {
        log.debug("REST request to update DataAntrian : {}", dataAntrianDTO);
        if (dataAntrianDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DataAntrianDTO result = dataAntrianService.save(dataAntrianDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dataAntrianDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /data-antrians} : get all the dataAntrians.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dataAntrians in body.
     */
    @GetMapping("/data-antrians")
    public ResponseEntity<List<DataAntrianDTO>> getAllDataAntrians(Pageable pageable) {
        log.debug("REST request to get a page of DataAntrians");
        Page<DataAntrianDTO> page = dataAntrianService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /data-antrians/:id} : get the "id" dataAntrian.
     *
     * @param id the id of the dataAntrianDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dataAntrianDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/data-antrians/{id}")
    public ResponseEntity<DataAntrianDTO> getDataAntrian(@PathVariable Long id) {
        log.debug("REST request to get DataAntrian : {}", id);
        Optional<DataAntrianDTO> dataAntrianDTO = dataAntrianService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }

    /**
     * {@code DELETE  /data-antrians/:id} : delete the "id" dataAntrian.
     *
     * @param id the id of the dataAntrianDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/data-antrians/{id}")
    public ResponseEntity<Void> deleteDataAntrian(@PathVariable Long id) {
        log.debug("REST request to delete DataAntrian : {}", id);
        dataAntrianService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    /*
        Tambahan
     */
    @GetMapping("/getDataAntrianFirst/{idSubLayanan}/{tanggalLayanan}")
    public ResponseEntity<SimpleDataAntrianDTO> getDataAntrianFirst(@PathVariable Long idSubLayanan, @PathVariable LocalDate tanggalLayanan) {
        log.debug("REST request to get Registrasi by idSubLayanan and tanggalLayanan: {}", idSubLayanan, tanggalLayanan);
        Optional<SimpleDataAntrianDTO> dataAntrianDTO = dataAntrianService.getDataAntrianLast(idSubLayanan, tanggalLayanan);
        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }

    @GetMapping("/getTotalAntrianNow/{dateNow}/{idSubLayanan}")
    public Integer getTotalAntrianNow(@PathVariable LocalDate dateNow, @PathVariable Long idSubLayanan) {
        log.debug("REST request count all DataAntrian by dateNow and idSubLayanan : {}", dateNow,idSubLayanan);
        return dataAntrianService.getTotalAntrianNow(dateNow,idSubLayanan);
    }

    @GetMapping("/panggilanBerikutnya/{idSubLayanan}/{statusCurrent}/{tanggal}/{statusPanggilan}")
    public ResponseEntity<SimpleDataAntrianDTO> panggilanBerikutnya(@PathVariable Long idSubLayanan, @PathVariable boolean statusCurrent, @PathVariable LocalDate tanggal,@PathVariable  boolean statusPanggilan) {
        log.debug("REST request to get DataAntrian by idSubLayanan,statusCurrent,tanggal: {}",idSubLayanan,statusCurrent,tanggal,statusPanggilan);
        Optional<SimpleDataAntrianDTO> dataAntrianDTO = dataAntrianService.panggilanBerikutnya(idSubLayanan,statusCurrent,tanggal,statusPanggilan);
        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }


    @GetMapping("/panggilanSaatIni/{loket}/{idSubLayanan}/{statusCurrent}/{tanggal}")
    public ResponseEntity<SimpleDataAntrianDTO> panggilanSaatIni(@PathVariable Long loket, @PathVariable Long idSubLayanan, @PathVariable boolean statusCurrent, @PathVariable LocalDate tanggal) {
        log.debug("REST request to get DataAntrian by idLoket: {}",loket,idSubLayanan,statusCurrent,tanggal);
        Optional<SimpleDataAntrianDTO> dataAntrianDTO = dataAntrianService.panggilanSaatIni(loket,idSubLayanan,statusCurrent,tanggal);
        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }

    @GetMapping("/lastPanggilan/{loket}/{idSubLayanan}/{statusCurrent}/{tanggal}")
    public ResponseEntity<SimpleDataAntrianDTO> lastPanggilan(@PathVariable Long loket, @PathVariable Long idSubLayanan, @PathVariable boolean statusCurrent, @PathVariable LocalDate tanggal) {
        log.debug("REST request to get DataAntrian by idLoket: {}",loket,idSubLayanan,statusCurrent,tanggal);
        Optional<SimpleDataAntrianDTO> dataAntrianDTO = dataAntrianService.lastPanggilan(loket,idSubLayanan,statusCurrent,tanggal);
        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }

    @GetMapping("/fastCall/{idSubLayanan}/{noUrut}/{statusKonseling}/{tanggal}")
    public ResponseEntity<SimpleDataAntrianDTO> fastCall(@PathVariable Long idSubLayanan, @PathVariable Long noUrut, @PathVariable boolean statusKonseling, @PathVariable LocalDate tanggal) {
        log.debug("REST request to get DataAntrian by idLoket: {}",idSubLayanan,noUrut,statusKonseling,tanggal);
        Optional<SimpleDataAntrianDTO> dataAntrianDTO = dataAntrianService.fastCall(idSubLayanan,noUrut,statusKonseling,tanggal);
        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }


    @GetMapping("/antrianSisa/{dateNow}/{idSubLayanan}/{statusPanggilan}")
    public Integer antrianSisa(@PathVariable LocalDate dateNow, @PathVariable Long idSubLayanan, @PathVariable  boolean statusPanggilan) {
        log.debug("REST request count all antrianSisa by dateNow and idSubLayanan : {}", dateNow,idSubLayanan,statusPanggilan);
        return dataAntrianService.totalAntrianSisa(dateNow,idSubLayanan,statusPanggilan);
    }

    @GetMapping("/antrianSisaList/{dateNow}/{idSubLayanan}/{statusPanggilan}")
    public ResponseEntity<List<SimpleDataAntrianDTO>> antrianSisaList(Pageable pageable,@PathVariable LocalDate dateNow,@PathVariable Long idSubLayanan, @PathVariable boolean statusPanggilan) {
        log.debug("REST request to get a page of DataAntrians by dateNow, idSubLayanan ",dateNow,idSubLayanan,statusPanggilan);
        Page<SimpleDataAntrianDTO> page = dataAntrianService.antrianSisaList(pageable,dateNow,idSubLayanan,statusPanggilan);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/historyAntrianList/{dateNow}/{idSubLayanan}/{idLoket}")
    public ResponseEntity<List<SimpleDataAntrianDTO>> historyAntrian(Pageable pageable,@PathVariable LocalDate dateNow,@PathVariable Long idSubLayanan, @PathVariable long idLoket) {
        log.debug("REST request to get a page of DataAntrians by dateNow, idSubLayanan, idLoket ",dateNow,idSubLayanan,idLoket);
        Page<SimpleDataAntrianDTO> page = dataAntrianService.historyAntrian(pageable,dateNow,idSubLayanan,idLoket);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/antreanLayanan/{dateNow}/{idSubLayanan}")
    public ResponseEntity<List<SimpleDataAntrianDTO>> antreanLayanan(Pageable pageable,@PathVariable LocalDate dateNow,@PathVariable Long idSubLayanan) {
        log.debug("REST request to get a page of DataAntrians by dateNow, idSubLayanan ",dateNow,idSubLayanan);
        Page<SimpleDataAntrianDTO> page = dataAntrianService.antreanLayanan(pageable,dateNow,idSubLayanan);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/plasma/{tanggalSekarang}/{idLantai}")
    public List<PlasmaDTO> getPlasma(@PathVariable LocalDate tanggalSekarang, @PathVariable Long idLantai) {
        log.debug("REST request to get a page of TempPanggilans with date and idLantai ",tanggalSekarang,idLantai);
        return dataAntrianService.findAllPlasma(tanggalSekarang, idLantai);
    }
    @GetMapping("/plasma2/{tanggalSekarang}/{idLantai}")
    public List<PlasmaDTO> z(@PathVariable LocalDate tanggalSekarang, @PathVariable Long idLantai) {
        log.debug("REST request to get a page of TempPanggilans with date and idLantai ",tanggalSekarang,idLantai);
        return dataAntrianService.findAllPlasma2(tanggalSekarang, idLantai);
    }
    @GetMapping("/foundAndLost/{idLantai}/{idSubLayanan}/{noUrut}")
    public ResponseEntity<LostFoundDTO> foundAndLost(@PathVariable Long idLantai, @PathVariable Long idSubLayanan, @PathVariable Long noUrut) {
        log.debug("REST request to get a page of TempPanggilans with date and idLantai ",idLantai,idSubLayanan,noUrut);
        Optional<LostFoundDTO> lostFoundDTO = dataAntrianService.foundAndLost(idLantai, idSubLayanan,noUrut);
        return ResponseUtil.wrapOrNotFound(lostFoundDTO);
    }

//    @GetMapping("/plasmaGetDataNow/{idLantai}/{idSubLayanan}")
//    public ResponseEntity<DataAntrian> plasmaGetDataNow(@PathVariable Long idLantai, @PathVariable Long idSubLayanan) {
//        log.debug("REST request to get a page of TempPanggilans with date and idLantai ",idLantai,idSubLayanan);
//        Optional<DataAntrian> simpleDataAntrianDTO = dataAntrianService.plasmaGetDataNow(idLantai, idSubLayanan);
//        return ResponseUtil.wrapOrNotFound(simpleDataAntrianDTO);
//    }

    @DeleteMapping("/data-antrian/clean-data")
    public ResponseEntity<Void> cleanData() {
        log.debug("REST request to clean TempPanggilan  at ", LocalDate.now());
        LocalDate date = LocalDate.now();
        dataAntrianService.deleteDataAntrian();
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, date.toString())).build();
    }

    @RequestMapping(value = "/imageWebcam/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImageAsResponseEntity(@PathVariable Long id) {
        HttpHeaders headers = new HttpHeaders();
        Optional<DataAntrianDTO> dataAntrianDTO = dataAntrianService.findOne(id);
        if(!dataAntrianDTO.isPresent())
        {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        DataAntrianDTO dataAntrianDTO1 = dataAntrianDTO.get();

        byte[] media = dataAntrianDTO1.getImageWebcam();
        String mediaType = dataAntrianDTO1.getImageWebcamContentType();

        headers.set("Content-Type", mediaType);

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);

        return responseEntity;
    }

    @PostMapping("/data-antrians/postAntrianBerikut/{idAntrian}/{idLoket}")
    public ResponseEntity<DataAntrianDTO> postAntrianBerikut(@PathVariable Long idAntrian, @PathVariable Long idLoket) throws URISyntaxException {
        log.debug("REST request to save DataAntrian : {}", idAntrian,idLoket);

        Optional<DataAntrian> dataAntrian = dataAntrianRepository.findByIdAndStatusPanggilanAndStatusCurrent(idAntrian, false, false);

        if(dataAntrian.isPresent()){
            Optional<DataAntrianDTO> dataAntrianDTO = dataAntrianService.findOne(idAntrian);
            Optional<LoketDTO> loketDTO = loketService.findOne(idLoket);

        if(dataAntrianDTO.isPresent()){
            if(dataAntrianDTO.get().getLoket() == null){
                throw new BadRequestAlertException("Invalid idLoket", ENTITY_NAME, "idnull");
            }else{
                dataAntrianDTO.get().setId(idAntrian);
                dataAntrianDTO.get().setStatusCurrent(true);
                dataAntrianDTO.get().setStatusPanggilan(true);
                dataAntrianDTO.get().setLoket(idLoket);

                TempPanggilanDTO tempPanggilanDTO = new TempPanggilanDTO();
                tempPanggilanDTO.setIdLantai(dataAntrianDTO.get().getLantai());
                tempPanggilanDTO.setIdSubLayanan(dataAntrianDTO.get().getIdSubLayanan());
                tempPanggilanDTO.setStatus(false);
                tempPanggilanDTO.setNoUrut(dataAntrianDTO.get().getNoUrut().toString());
                tempPanggilanDTO.setPanggilan(loketDTO.get().getDeskripsi()+";"+dataAntrianDTO.get().getPrefixSuara()+dataAntrianDTO.get().getNoUrut().toString());
                tempPanggilanService.save(tempPanggilanDTO);

            }
        }else{
            throw new BadRequestAlertException("Invalid idAntrian", ENTITY_NAME, "idnull");
        }

            DataAntrianDTO result = dataAntrianService.save(dataAntrianDTO.get());
            return ResponseEntity.created(new URI("/api/data-antrians/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);

        }else{
            throw new BadRequestAlertException("Invalid idAntrian", ENTITY_NAME, "idnull");
        }
    }

    @GetMapping("/nextPanggilanBerikutnya/{idSubLayanan}/{idLoket}/{tanggal}")
    public ResponseEntity<DataAntrianDTO> nextPanggilanBerikutnya(@PathVariable Long idSubLayanan, @PathVariable Long idLoket, @PathVariable LocalDate tanggal) {
        log.debug("REST request to get DataAntrian by idSubLayanan,idLoket,tanggal: {}",idSubLayanan,idLoket,tanggal);

        Optional<DataAntrianDTO> getPanggilanSebelumnya = dataAntrianService.getPanggilanSebelumnya(idSubLayanan,idLoket,tanggal);

        getPanggilanSebelumnya.get().setId(getPanggilanSebelumnya.get().getId());
        getPanggilanSebelumnya.get().setStatusCurrent(false);
        getPanggilanSebelumnya.get().setStatusKonseling(true);
        dataAntrianService.save(getPanggilanSebelumnya.get());

        Optional<DataAntrianDTO> getPanggilanBerikutnya = dataAntrianService.panggilanBerikutnya2(idSubLayanan,false,tanggal,false);

        getPanggilanBerikutnya.get().setId(getPanggilanBerikutnya.get().getId());
        getPanggilanBerikutnya.get().setStatusCurrent(true);
        getPanggilanBerikutnya.get().setStatusPanggilan(true);
        dataAntrianService.save(getPanggilanBerikutnya.get());

        Optional<LoketDTO> loketDTO = loketService.findOne(idLoket);

        TempPanggilanDTO tempPanggilanDTO = new TempPanggilanDTO();
        tempPanggilanDTO.setIdLantai(getPanggilanBerikutnya.get().getLantai());
        tempPanggilanDTO.setIdSubLayanan(getPanggilanBerikutnya.get().getIdSubLayanan());
        tempPanggilanDTO.setStatus(false);
        tempPanggilanDTO.setNoUrut(getPanggilanBerikutnya.get().getNoUrut().toString());
        tempPanggilanDTO.setPanggilan(loketDTO.get().getDeskripsi()+";"+getPanggilanBerikutnya.get().getPrefixSuara()+getPanggilanBerikutnya.get().getNoUrut().toString());
        tempPanggilanService.save(tempPanggilanDTO);

        return ResponseUtil.wrapOrNotFound(getPanggilanBerikutnya);
    }

    @GetMapping("/callPrev/{noUrut}/{idSubLayanan}")
    public ResponseEntity<DataAntrianDTO> callPrev(@PathVariable Long noUrut, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get DataAntrian by noUrt - 1, idSubLayanan: {}",noUrut, idSubLayanan);

        Optional<DataAntrianDTO> dataAntrianDTO = dataAntrianService.getDataPrev(idSubLayanan, noUrut);
        dataAntrianDTO.get().setId(dataAntrianDTO.get().getId());
        dataAntrianDTO.get().setStatusCurrent(true);
        dataAntrianDTO.get().setStatusKonseling(true);
        dataAntrianService.save(dataAntrianDTO.get());

        Optional<DataAntrianDTO> updatePanggilan = dataAntrianService.getByNoUrutAndIdSubLayanan(noUrut, idSubLayanan);
        updatePanggilan.get().setStatusPanggilan(false);
        updatePanggilan.get().setStatusCurrent(false);
        updatePanggilan.get().setStatusKonseling(false);
        updatePanggilan.get().setId(updatePanggilan.get().getId());

        dataAntrianService.save(updatePanggilan.get());

        Optional<LoketDTO> loketDTO = loketService.findOne(updatePanggilan.get().getLoket());

        TempPanggilanDTO tempPanggilanDTO = new TempPanggilanDTO();
        tempPanggilanDTO.setIdLantai(dataAntrianDTO.get().getLantai());
        tempPanggilanDTO.setIdSubLayanan(dataAntrianDTO.get().getIdSubLayanan());
        tempPanggilanDTO.setStatus(false);
        tempPanggilanDTO.setNoUrut(dataAntrianDTO.get().getNoUrut().toString());
        tempPanggilanDTO.setPanggilan(loketDTO.get().getDeskripsi()+";"+dataAntrianDTO.get().getPrefixSuara()+dataAntrianDTO.get().getNoUrut().toString());
        tempPanggilanService.save(tempPanggilanDTO);

        return ResponseUtil.wrapOrNotFound(dataAntrianDTO);
    }


    @GetMapping("/newNext/{idLoket}/{idSubLayanan}")
    public ResponseEntity<DataAntrianDTO> newNext(@PathVariable Long idLoket, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get next panggilan new by idLoket, idSubLayanan: {}",idLoket, idSubLayanan);

        Optional<DataAntrianDTO> getPanggilanSebelumnya = dataAntrianService.getNewPanggilanSebelumnya(idLoket,idSubLayanan);
        if(getPanggilanSebelumnya.isPresent()){
            getPanggilanSebelumnya.get().setId(getPanggilanSebelumnya.get().getId());
            getPanggilanSebelumnya.get().setStatusPanggilan(true);
            getPanggilanSebelumnya.get().setStatusCurrent(false);
            getPanggilanSebelumnya.get().setStatusKonseling(true);
            dataAntrianService.save(getPanggilanSebelumnya.get());
        }

        Optional<DataAntrianDTO> getPanggilanSaatIni = dataAntrianService.getNewPanggilanSaatIni(idLoket,idSubLayanan);
        if(getPanggilanSaatIni.isPresent()){
            getPanggilanSaatIni.get().setId(getPanggilanSaatIni.get().getId());
            getPanggilanSaatIni.get().setStatusPanggilan(true);
            getPanggilanSaatIni.get().setStatusCurrent(false);
            getPanggilanSaatIni.get().setStatusKonseling(true);
            dataAntrianService.save(getPanggilanSaatIni.get());
        }

        Optional<DataAntrianDTO> getFastCallBelumDipanggil = dataAntrianService.getFastCallBelumDipanggil(idLoket,idSubLayanan);
        if(getFastCallBelumDipanggil.isPresent()){
            getFastCallBelumDipanggil.get().setId(getFastCallBelumDipanggil.get().getId());
            getFastCallBelumDipanggil.get().setStatusPanggilan(false);
            getFastCallBelumDipanggil.get().setStatusCurrent(true);
            getFastCallBelumDipanggil.get().setStatusKonseling(false);
            dataAntrianService.save(getFastCallBelumDipanggil.get());
        }
        Optional<DataAntrianDTO> getFastCallSesudahDipanggil = dataAntrianService.getFastCallSesudahDipanggil(idLoket,idSubLayanan);

        if(getFastCallSesudahDipanggil.isPresent()){
            getFastCallSesudahDipanggil.get().setId(getFastCallSesudahDipanggil.get().getId());
            getFastCallSesudahDipanggil.get().setStatusPanggilan(true);
            getFastCallSesudahDipanggil.get().setStatusCurrent(false);
            getFastCallSesudahDipanggil.get().setStatusKonseling(true);
            dataAntrianService.save(getFastCallSesudahDipanggil.get());
        }

        Optional<DataAntrianDTO> result = dataAntrianService.panggilanBerikutnya2(idSubLayanan,false,LocalDate.now(),false);
        result.get().setStatusCurrent(true);
        result.get().setStatusPanggilan(true);
        result.get().setStatusKonseling(false);
        result.get().setLoket(idLoket);
        dataAntrianService.save(result.get());

        Optional<LoketDTO> loketDTO = loketService.findOne(idLoket);


        TempPanggilanDTO tempPanggilanDTO = new TempPanggilanDTO();
        tempPanggilanDTO.setPanggilan(loketDTO.get().getDeskripsi() + ";" + result.get().getPrefixSuara() + result.get().getNoUrut().toString());
        tempPanggilanDTO.setIdTempPanggilan(UUID.randomUUID());
        tempPanggilanDTO.setIdLantai(result.get().getLantai());
        tempPanggilanDTO.setIdSubLayanan(result.get().getIdSubLayanan());
        tempPanggilanDTO.setNoUrut(result.get().getNoUrut().toString());
        tempPanggilanDTO.setStatus(false);
        tempPanggilanService.save(tempPanggilanDTO);

        return ResponseUtil.wrapOrNotFound(result);
    }

    @GetMapping("/newPrev/{idLoket}/{idSubLayanan}")
    public ResponseEntity<DataAntrianDTO> newPrev(@PathVariable Long idLoket, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get prev panggilan new by idLoket, idSubLayanan: {}",idLoket, idSubLayanan);

        Optional<DataAntrianDTO> getPanggilanSebelumnya = dataAntrianService.getPrevPanggilanSebelumnya(idLoket, idSubLayanan);

        if(getPanggilanSebelumnya.isPresent()){

            List<DataAntrianDTO> getAllDataAntrianPerloket1 = dataAntrianService.getAllDataAntrianPerloket(idLoket,idSubLayanan);
            if(getAllDataAntrianPerloket1.size() > 0){
                for (DataAntrianDTO updateData:getAllDataAntrianPerloket1){
                    updateData.setId(updateData.getId());
                    updateData.setStatusCurrent(false);
                    dataAntrianService.save(updateData);
                }
            }

            getPanggilanSebelumnya.get().setId(getPanggilanSebelumnya.get().getId());
            getPanggilanSebelumnya.get().setStatusPanggilan(true);
            getPanggilanSebelumnya.get().setStatusCurrent(true);
            getPanggilanSebelumnya.get().setStatusKonseling(true);
            dataAntrianService.save(getPanggilanSebelumnya.get());

            Optional<LoketDTO> loketDTO = loketService.findOne(idLoket);

            TempPanggilanDTO tempPanggilanDTO = new TempPanggilanDTO();
            tempPanggilanDTO.setPanggilan(loketDTO.get().getDeskripsi() + ";" + getPanggilanSebelumnya.get().getPrefixSuara() + getPanggilanSebelumnya.get().getNoUrut().toString());
            tempPanggilanDTO.setIdTempPanggilan(UUID.randomUUID());
            tempPanggilanDTO.setIdLantai(getPanggilanSebelumnya.get().getLantai());
            tempPanggilanDTO.setIdSubLayanan(getPanggilanSebelumnya.get().getIdSubLayanan());
            tempPanggilanDTO.setNoUrut(getPanggilanSebelumnya.get().getNoUrut().toString());
            tempPanggilanDTO.setStatus(false);
            tempPanggilanService.save(tempPanggilanDTO);


        }

        return ResponseUtil.wrapOrNotFound(getPanggilanSebelumnya);
    }

    @GetMapping("/newFastCall/{idLoket}/{idSubLayanan}/{noUrut}")
    public ResponseEntity<DataAntrianDTO> newFastCall(@PathVariable Long idLoket, @PathVariable Long idSubLayanan, @PathVariable Long noUrut) {
        log.debug("REST request to get prev panggilan new by idLoket, idSubLayanan, noUrut: {},{},{}",idLoket, idSubLayanan,noUrut);


        Optional<DataAntrianDTO> getFastCall = dataAntrianService.getNewFastCall(noUrut,idSubLayanan);


        if(getFastCall.isPresent()){

            List<DataAntrianDTO> getAllDataAntrianPerloket = dataAntrianService.getAllDataAntrianPerloket(idLoket,idSubLayanan);
            if(getAllDataAntrianPerloket.size() > 0){
                for (DataAntrianDTO updateData:getAllDataAntrianPerloket){
                    updateData.setId(updateData.getId());
                    updateData.setStatusCurrent(false);
                    dataAntrianService.save(updateData);
                }
            }

            getFastCall.get().setId(getFastCall.get().getId());
            getFastCall.get().setStatusCurrent(true);
            getFastCall.get().setLoket(idLoket);
            dataAntrianService.save(getFastCall.get());

            Optional<LoketDTO> loketDTO = loketService.findOne(idLoket);

            TempPanggilanDTO tempPanggilanDTO = new TempPanggilanDTO();
            tempPanggilanDTO.setPanggilan(loketDTO.get().getDeskripsi() + ";" + getFastCall.get().getPrefixSuara() + getFastCall.get().getNoUrut().toString());
            tempPanggilanDTO.setIdTempPanggilan(UUID.randomUUID());
            tempPanggilanDTO.setIdLantai(getFastCall.get().getLantai());
            tempPanggilanDTO.setIdSubLayanan(getFastCall.get().getIdSubLayanan());
            tempPanggilanDTO.setNoUrut(getFastCall.get().getNoUrut().toString());
            tempPanggilanDTO.setStatus(false);
            tempPanggilanService.save(tempPanggilanDTO);
        }


        return ResponseUtil.wrapOrNotFound(getFastCall);
    }

    @GetMapping("/newRfr/{idLoket}/{idSubLayanan}")
    public ResponseEntity<DataAntrianDTO> newRfr(@PathVariable Long idLoket, @PathVariable Long idSubLayanan) {
        log.debug("REST request to get prev panggilan new by idLoket, idSubLayanan: {},{},{}",idLoket, idSubLayanan);

        Optional<DataAntrianDTO> newRfr = dataAntrianService.newRfr(idLoket,idSubLayanan);

        return ResponseUtil.wrapOrNotFound(newRfr);
    }
}
