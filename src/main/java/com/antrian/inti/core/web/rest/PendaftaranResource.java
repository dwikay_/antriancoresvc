package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.PendaftaranService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.PendaftaranDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Pendaftaran}.
 */
@RestController
@RequestMapping("/api")
public class PendaftaranResource {

    private final Logger log = LoggerFactory.getLogger(PendaftaranResource.class);

    private static final String ENTITY_NAME = "antriancoresvcPendaftaran";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PendaftaranService pendaftaranService;

    public PendaftaranResource(PendaftaranService pendaftaranService) {
        this.pendaftaranService = pendaftaranService;
    }

    /**
     * {@code POST  /pendaftarans} : Create a new pendaftaran.
     *
     * @param pendaftaranDTO the pendaftaranDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pendaftaranDTO, or with status {@code 400 (Bad Request)} if the pendaftaran has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pendaftarans")
    public ResponseEntity<PendaftaranDTO> createPendaftaran(@RequestBody PendaftaranDTO pendaftaranDTO) throws URISyntaxException {
        log.debug("REST request to save Pendaftaran : {}", pendaftaranDTO);
        if (pendaftaranDTO.getId() != null) {
            throw new BadRequestAlertException("A new pendaftaran cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PendaftaranDTO result = pendaftaranService.save(pendaftaranDTO);
        return ResponseEntity.created(new URI("/api/pendaftarans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pendaftarans} : Updates an existing pendaftaran.
     *
     * @param pendaftaranDTO the pendaftaranDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pendaftaranDTO,
     * or with status {@code 400 (Bad Request)} if the pendaftaranDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pendaftaranDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pendaftarans")
    public ResponseEntity<PendaftaranDTO> updatePendaftaran(@RequestBody PendaftaranDTO pendaftaranDTO) throws URISyntaxException {
        log.debug("REST request to update Pendaftaran : {}", pendaftaranDTO);
        if (pendaftaranDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PendaftaranDTO result = pendaftaranService.save(pendaftaranDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, pendaftaranDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pendaftarans} : get all the pendaftarans.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pendaftarans in body.
     */
    @GetMapping("/pendaftarans")
    public ResponseEntity<List<PendaftaranDTO>> getAllPendaftarans(Pageable pageable) {
        log.debug("REST request to get a page of Pendaftarans");
        Page<PendaftaranDTO> page = pendaftaranService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pendaftarans/:id} : get the "id" pendaftaran.
     *
     * @param id the id of the pendaftaranDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pendaftaranDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pendaftarans/{id}")
    public ResponseEntity<PendaftaranDTO> getPendaftaran(@PathVariable Long id) {
        log.debug("REST request to get Pendaftaran : {}", id);
        Optional<PendaftaranDTO> pendaftaranDTO = pendaftaranService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pendaftaranDTO);
    }

    /**
     * {@code DELETE  /pendaftarans/:id} : delete the "id" pendaftaran.
     *
     * @param id the id of the pendaftaranDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pendaftarans/{id}")
    public ResponseEntity<Void> deletePendaftaran(@PathVariable Long id) {
        log.debug("REST request to delete Pendaftaran : {}", id);
        pendaftaranService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
