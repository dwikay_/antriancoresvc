package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.KuotaService;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.KuotaDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Kuota}.
 */
@RestController
@RequestMapping("/api")
public class KuotaResource {

    private final Logger log = LoggerFactory.getLogger(KuotaResource.class);

    private static final String ENTITY_NAME = "antriancoresvcKuota";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KuotaService kuotaService;

    public KuotaResource(KuotaService kuotaService) {
        this.kuotaService = kuotaService;
    }

    /**
     * {@code POST  /kuotas} : Create a new kuota.
     *
     * @param kuotaDTO the kuotaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kuotaDTO, or with status {@code 400 (Bad Request)} if the kuota has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kuotas")
    public ResponseEntity<KuotaDTO> createKuota(@RequestBody KuotaDTO kuotaDTO) throws URISyntaxException {
        log.debug("REST request to save Kuota : {}", kuotaDTO);
        if (kuotaDTO.getId() != null) {
            throw new BadRequestAlertException("A new kuota cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KuotaDTO result = kuotaService.save(kuotaDTO);
        return ResponseEntity.created(new URI("/api/kuotas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kuotas} : Updates an existing kuota.
     *
     * @param kuotaDTO the kuotaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kuotaDTO,
     * or with status {@code 400 (Bad Request)} if the kuotaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kuotaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kuotas")
    public ResponseEntity<KuotaDTO> updateKuota(@RequestBody KuotaDTO kuotaDTO) throws URISyntaxException {
        log.debug("REST request to update Kuota : {}", kuotaDTO);
        if (kuotaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KuotaDTO result = kuotaService.save(kuotaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kuotaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /kuotas} : get all the kuotas.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kuotas in body.
     */
    @GetMapping("/kuotas")
    public ResponseEntity<List<KuotaDTO>> getAllKuotas(Pageable pageable) {
        log.debug("REST request to get a page of Kuotas");
        Page<KuotaDTO> page = kuotaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kuotas/:id} : get the "id" kuota.
     *
     * @param id the id of the kuotaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kuotaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kuotas/{id}")
    public ResponseEntity<KuotaDTO> getKuota(@PathVariable Long id) {
        log.debug("REST request to get Kuota : {}", id);
        Optional<KuotaDTO> kuotaDTO = kuotaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kuotaDTO);
    }

    /**
     * {@code DELETE  /kuotas/:id} : delete the "id" kuota.
     *
     * @param id the id of the kuotaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kuotas/{id}")
    public ResponseEntity<Void> deleteKuota(@PathVariable Long id) {
        log.debug("REST request to delete Kuota : {}", id);
        kuotaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
