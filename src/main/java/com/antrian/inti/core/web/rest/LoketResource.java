package com.antrian.inti.core.web.rest;

import com.antrian.inti.core.service.LoketService;
import com.antrian.inti.core.service.dto.SimpleLoketDTO;
import com.antrian.inti.core.web.rest.errors.BadRequestAlertException;
import com.antrian.inti.core.service.dto.LoketDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.antrian.inti.core.domain.Loket}.
 */
@RestController
@RequestMapping("/api")
public class LoketResource {

    private final Logger log = LoggerFactory.getLogger(LoketResource.class);

    private static final String ENTITY_NAME = "antriancoresvcLoket";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoketService loketService;

    public LoketResource(LoketService loketService) {
        this.loketService = loketService;
    }

    /**
     * {@code POST  /lokets} : Create a new loket.
     *
     * @param loketDTO the loketDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loketDTO, or with status {@code 400 (Bad Request)} if the loket has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/lokets")
    public ResponseEntity<LoketDTO> createLoket(@Valid @RequestBody LoketDTO loketDTO) throws URISyntaxException {
        log.debug("REST request to save Loket : {}", loketDTO);
        if (loketDTO.getId() != null) {
            throw new BadRequestAlertException("A new loket cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoketDTO result = loketService.save(loketDTO);
        return ResponseEntity.created(new URI("/api/lokets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /lokets} : Updates an existing loket.
     *
     * @param loketDTO the loketDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loketDTO,
     * or with status {@code 400 (Bad Request)} if the loketDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loketDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/lokets")
    public ResponseEntity<LoketDTO> updateLoket(@Valid @RequestBody LoketDTO loketDTO) throws URISyntaxException {
        log.debug("REST request to update Loket : {}", loketDTO);
        if (loketDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoketDTO result = loketService.save(loketDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, loketDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /lokets} : get all the lokets.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of lokets in body.
     */
    @GetMapping("/lokets")
    public ResponseEntity<List<LoketDTO>> getAllLokets(Pageable pageable) {
        log.debug("REST request to get a page of Lokets");
        Page<LoketDTO> page = loketService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /lokets/:id} : get the "id" loket.
     *
     * @param id the id of the loketDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loketDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/lokets/{id}")
    public ResponseEntity<LoketDTO> getLoket(@PathVariable Long id) {
        log.debug("REST request to get Loket : {}", id);
        Optional<LoketDTO> loketDTO = loketService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loketDTO);
    }

    /**
     * {@code DELETE  /lokets/:id} : delete the "id" loket.
     *
     * @param id the id of the loketDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/lokets/{id}")
    public ResponseEntity<Void> deleteLoket(@PathVariable Long id) {
        log.debug("REST request to delete Loket : {}", id);
        loketService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /*
        Tambahan
     */

    @GetMapping("/simple-loket")
    public ResponseEntity<List<SimpleLoketDTO>> getSimpleLoket(Pageable pageable) {
        log.debug("REST request to get a page of Lokets");
        Page<SimpleLoketDTO> page = loketService.findSimpleAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    @GetMapping("/simple-lokets/getById/{id}")
    public ResponseEntity<SimpleLoketDTO> getById(@PathVariable Long id) {
        log.debug("REST request to get Loket by id : {}", id);
        Optional<SimpleLoketDTO> simpleLoketDTO = loketService.findSimpleOne(id);
        return ResponseUtil.wrapOrNotFound(simpleLoketDTO);
    }
    @GetMapping("/simple-lokets/getSimpleBySubLayanan/{idSubLayanan}")
    public ResponseEntity<SimpleLoketDTO> getSimpleBySubLayanan(@PathVariable Long idSubLayanan) {
        log.debug("REST request to get Loket : {}", idSubLayanan);
        Optional<SimpleLoketDTO> simpleLoketDTO = loketService.findSimpleOneIdSubLayanan(idSubLayanan);
        return ResponseUtil.wrapOrNotFound(simpleLoketDTO);
    }
    @GetMapping("/simple-lokets/getByIpController/{ipController}")
    public ResponseEntity<SimpleLoketDTO> getByIpController(@PathVariable String ipController) {
        log.debug("REST request to get Loket by ipController : {}", ipController);
        Optional<SimpleLoketDTO> simpleLoketDTO = loketService.findSimpleOneIpController(ipController);
        return ResponseUtil.wrapOrNotFound(simpleLoketDTO);
    }
}
